﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using BusinessAccessLayer.InterfaceDeclaration;
using BusinessAccessLayer.ServiceDefination;
using CommonMethod.Interface;
using Microsoft.AspNetCore.Authorization;
using BusinessEntites;

namespace OpinionestWebApi.Controllers
{
    [Microsoft.AspNetCore.Cors.EnableCors("AllowOrigin")]
    [Route("api/[controller]")]
    [ApiController]
    public class TestimonialController : ControllerBase
    {
        SqlConnection _Conn;
        IConfiguration _iconfiguration;
        ITestimonial _Testimonial;

        public TestimonialController(IConfiguration iconfiguration)
        {
            _iconfiguration = iconfiguration;
            string Connection = _iconfiguration.GetSection("ConnectionStrings").GetSection("DefaultConnection").Value;
            _Conn = new SqlConnection(Connection);
            _Testimonial = new TestimonialServicesDefination(_Conn);
        }

        [HttpGet, Route("List")]
        public IActionResult GetTestimonialList()
        {
            var myList = new List<BusinessEntites.TestimonialEntites>();
            try
            {
                myList = _Testimonial.GetTestimonialList();
                if (myList.Count >= 1)
                {
                    return Ok(new { Code = 200, Status = true, Message = "Success", Data = myList });
                }
                else
                {
                    return Ok(new { Code = 401, Status = false, Message = "Something wrong please check again.", Data = "" });
                }

            }
            catch (Exception e)
            {
                return Ok(new { Code = 401, Status = false, Message = e.Message, Data = "" });
            }

        }
        [HttpPost, Route("Add")]
        public IActionResult AddTestimonial(GetTestimonialEntites getTestimonial)
        {
            bool IsAuth = User.Identity.IsAuthenticated;
            if (IsAuth != false)
            {
                var Result = _Testimonial.AddTestimonial(getTestimonial);
                if (Result != 0)
                {
                    return Ok(new { Code = 200, Status = true, Message = "Data Submited Successfully !", Data = Result, MessageCode = 1 });
                }
                else
                {
                    return Ok(new { Code = 201, Status = false, Message = "Something wrong please Try again.", MessageCode = 0 });
                }
            }
            else
            {
                return Ok(new { Code = 200, Message = "Token is Invalid", MessageCode = 2 });
            }
        }
    }
}