﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using BusinessAccessLayer.InterfaceDeclaration;
using BusinessAccessLayer.ServiceDefination;
using BusinessEntites;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace OpinionestWebApi.Controllers
{
    [Microsoft.AspNetCore.Cors.EnableCors("AllowOrigin")]
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class SurveyController : ControllerBase
    {
        SqlConnection _Conn;
        IConfiguration _iconfiguration;
        ISurvey _Survey;
        ICommon _common;
        public const int RecordsPerPage = 15;
        public SurveyController(IConfiguration iconfiguration)
        {
            _iconfiguration = iconfiguration;
            string Connection = _iconfiguration.GetSection("ConnectionStrings").GetSection("DefaultConnection").Value;
            _Conn = new SqlConnection(Connection);
            _Survey = new SurveyServicesDefination(_Conn);

            _common = new CommonServicesDefination(_Conn);
        }
        [HttpGet, Route("MySurvey")]
        public IActionResult MySurvey(int? pageIndex,int pageSize)
        {
            var myList = new List<BusinessEntites.MySurveyEntites>();
            bool IsAuth = User.Identity.IsAuthenticated;
            if (IsAuth != false)
            {
                long UserId = Convert.ToInt64(User.Identity.Name); bool userActive = _common.ValidUser(UserId);
                if (userActive == false)
                {
                    return Ok(new { Code = 201, Status = false, Message = "Invalid User, Please create new account.", MessageCode = 2 });
                }
                if (ModelState.IsValid)
                {
                    myList = _Survey.MySurveylist(UserId);
                    var list = myList.Skip((pageIndex - 1 ?? 0) * pageSize).Take(pageSize).ToList();
                    if (myList.Count >= 1)
                    {
                        return Ok(new { Code = 200, Status = true, Message = "Success", Data = list, TotalRecords = myList.Count()});
                    }
                    else
                    {
                        return Ok(new { Code = 200, Status = false, Message = "No records are found.", Data = "" });
                    }

                }
                else
                {
                    return BadRequest(new { Code = 401, Status = false, Message = "Something wrong please check again.", Data = "" });
                }

            }
            else
            {
                return Ok(new { Code = 200, Message = "Token is Invalid", MessageCode = 2 });
            }
        }
        [HttpGet, Route("OtherSurvey")]
        public IActionResult OtherSurvey(int? pageIndex,  int pageSize, int CountryId)
        {
            var myList = new List<BusinessEntites.MySurveyEntites>();
            bool IsAuth = User.Identity.IsAuthenticated;
            if (IsAuth != false)
            {
                long UserId = Convert.ToInt64(User.Identity.Name);
                bool userActive = _common.ValidUser(UserId);
                if (userActive == false)
                {
                    return Ok(new { Code = 201, Status = false, Message = "Invalid User, Please create new account.", MessageCode = 2 });
                }
                if (ModelState.IsValid)
                {
                    myList = _Survey.OtherSurveylist(UserId,CountryId);
                    var list = myList.Skip((pageIndex - 1 ?? 0) * pageSize).Take(pageSize).ToList();
                    if (myList.Count >= 1)
                    {
                        return Ok(new { Code = 200, Status = true, Message = "Success", Data = list, TotalRecords = myList.Count() });
                    }
                    else
                    {
                        return Ok(new { Code = 200, Status = false, Message = "No records are found.", Data = "" });
                    }

                }
                else
                {
                    return BadRequest(new { Code = 401, Status = false, Message = "Something wrong please check again.", Data = "" });
                }

            }
            else
            {
                return Ok(new { Code = 200, Message = "Token is Invalid", MessageCode = 2 });
            }
        }

        [HttpGet, Route("SurveyTotalCount")]
        public IActionResult SurveyTotalCount()
        {
            var data = new FormsCount();
            bool IsAuth = User.Identity.IsAuthenticated;
            if (IsAuth != false)
            {
                long UserId = Convert.ToInt64(User.Identity.Name);
                bool userActive = _common.ValidUser(UserId);
                if (userActive == false)
                {
                    return Ok(new { Code = 201, Status = false, Message = "Invalid User, Please create new account.", MessageCode = 2 });
                }
                if (ModelState.IsValid)
                {
                    data = _Survey.FromsCount(UserId);
                    if (data!=null)
                    {
                        return Ok(new { Code = 200, Status = true, Message = "Success", Data = data, MessageCode = 1 });
                    }
                    else
                    {
                        return Ok(new { Code = 200, Status = false, Message = "No records are found.", Data = "" , MessageCode = 2 });
                    }

                }
                else
                {
                    return BadRequest(new { Code = 401, Status = false, Message = "Something wrong please check again.", Data = "", MessageCode = 2 });
                }

            }
            else
            {
                return Ok(new { Code = 200, Message = "Token is Invalid", MessageCode = 2 });
            }
        }
        [HttpGet, Route("SurveyResultView")]
        public IActionResult SurveyResultView(long surveyId)
        {
            var data = new SurveyResult();
            bool IsAuth = User.Identity.IsAuthenticated;
            if (IsAuth != false)
            {
                long UserId = Convert.ToInt64(User.Identity.Name);
                bool userActive = _common.ValidUser(UserId);
                if (userActive == false)
                {
                    return Ok(new { Code = 201, Status = false, Message = "Invalid User, Please create new account.", MessageCode = 2 });
                }
                if (ModelState.IsValid)
                {
                    data = _Survey.ViewSurveyResult(UserId, surveyId);
                    if (data != null)
                    {
                        return Ok(new { Code = 200, Status = true, Message = "Success", Data = data, MessageCode = 1 });
                    }
                    else
                    {
                        return Ok(new { Code = 200, Status = false, Message = "No records are found.", Data = "", MessageCode = 2 });
                    }

                }
                else
                {
                    return BadRequest(new { Code = 401, Status = false, Message = "Something wrong please check again.", Data = "", MessageCode = 2 });
                }

            }
            else
            {
                return Ok(new { Code = 200, Message = "Token is Invalid", MessageCode = 2 });
            }
        }
        [HttpPost,Route("createFourm")]
        public IActionResult CreateFourm(ForumCreate forumCreate)
        {
            
            bool IsAuth = User.Identity.IsAuthenticated;
            if (IsAuth != false)
            {
                long UserId = Convert.ToInt64(User.Identity.Name);
                bool userActive = _common.ValidUser(UserId);
                if (userActive == false)
                {
                    return Ok(new { Code = 201, Status = false, Message = "Invalid User, Please create new account.", MessageCode = 2 });
                }
                if (ModelState.IsValid)
                {
                    forumCreate.UserId = UserId;
                    var list = _Survey.addSurvey(forumCreate);
                    if (list== 1)
                    {
                        return Ok(new { Code = 200, Status = true, Message = "Survey saved Successfully",surveyId= list });
                    }
                    else
                    {
                        return Ok(new { Code = 200, Status = false, Message = "Survey allready in draft", Data = "" });
                    }

                }
                else
                {
                    return BadRequest(new { Code = 401, Status = false, Message = "Something wrong please check again.", Data = "" });
                }

            }
            else
            {
                return Ok(new { Code = 200, Message = "Token is Invalid", MessageCode = 2 });
            }

        }
        [HttpGet, Route("fourmQuestionCategory")]
        public IActionResult fourmQuestionCategory()
        {

            bool IsAuth = User.Identity.IsAuthenticated;
            if (IsAuth != false)
            {
                
                if (ModelState.IsValid)
                {
                    List<SurveyStatusEntites> list = new List<SurveyStatusEntites>();
                    list.Add(new SurveyStatusEntites { id = "4", Name = "Radio Button" });
                    list.Add(new SurveyStatusEntites { id = "1", Name = "Dropdown" });
                    list.Add(new SurveyStatusEntites { id = "8", Name = "Text Box" });

                    if (list.Count> 0)
                    {
                        return Ok(new { Code = 200, Status = true, data= list });
                    }
                    else
                    {
                        return Ok(new { Code = 200, Status = false, Message = "No records are found.", Data = "" });
                    }

                }
                else
                {
                    return BadRequest(new { Code = 401, Status = false, Message = "Something wrong please check again.", Data = "" });
                }

            }
            else
            {
                return Ok(new { Code = 200, Message = "Token is Invalid", MessageCode = 2 });
            }

        }
        [HttpPost, Route("createFourmQuestion")]
        public IActionResult createFourmQuestion(QuestionEntites forumCreate)
        {

            bool IsAuth = User.Identity.IsAuthenticated;
            if (IsAuth != false)
            {
                long UserId = Convert.ToInt64(User.Identity.Name);
                bool userActive = _common.ValidUser(UserId);
                if (userActive == false)
                {
                    return Ok(new { Code = 201, Status = false, Message = "Invalid User, Please create new account.", MessageCode = 2 });
                }
                if (ModelState.IsValid)
                {
                    forumCreate.UserId = UserId;
                    var list = _Survey.addForumQuestion(forumCreate);
                    if (list > 0)
                    {
                        return Ok(new { Code = 200, Status = true, Message = "Survey saved Successfully" });
                    }
                    else
                    {
                        return Ok(new { Code = 200, Status = false, Message = "Survey allready in draft", Data = "" });
                    }

                }
                else
                {
                    return BadRequest(new { Code = 401, Status = false, Message = "Something wrong please check again.", Data = "" });
                }

            }
            else
            {
                return Ok(new { Code = 200, Message = "Token is Invalid", MessageCode = 2 });
            }

        }

    }
}