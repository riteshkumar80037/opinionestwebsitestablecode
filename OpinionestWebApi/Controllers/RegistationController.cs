﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using BusinessAccessLayer.InterfaceDeclaration;
using BusinessAccessLayer.ServiceDefination;
using BusinessEntites;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Twilio;
using Twilio.Rest.Api.V2010.Account;

namespace OpinionestWebApi.Controllers
{
    [Microsoft.AspNetCore.Cors.EnableCors("AllowOrigin")]
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class RegistationController : Controller
    {
        ILogin _Login;
        SqlConnection _Conn;
        IConfiguration _iconfiguration;
        int MacValue = 0;
        Iuser _user;
        IPartner _Partner;
        ICommon _common;
        public RegistationController(IConfiguration iconfiguration)
        {
            _iconfiguration = iconfiguration;
            string Connection = _iconfiguration.GetSection("ConnectionStrings").GetSection("DefaultConnection").Value;
            MacValue = Convert.ToInt32(_iconfiguration.GetSection("MacValue").Value);
            _Conn = new SqlConnection(Connection);
            _Login = new LoginServicesDefination(_Conn);
            _Partner = new ParnterServicesDefination(_Conn);
            _user = new UserServicesDefination(_Conn);
            _common = new CommonServicesDefination(_Conn);
        }
        [HttpPost, Route("Registation")]
        [AllowAnonymous]
        public IActionResult Index(RegistationEntites _User)
        {
            if (ModelState.IsValid)
            {
                int Mac = MacValue;
                string SendGridKey = _iconfiguration.GetSection("SendGridKey").Value;
                var Result = _Login.UserRegistation(_User, Mac, SendGridKey);
                if (Result.Item2 == 1)
                {
                    return Ok(new { Code = 201, Message = "User Already Exist !", MessageCode = 2 });
                }
                else if (Result.Item3 == 1)
                {
                    return Ok(new { Code = 202, Message = "Another user is already using this Device !", MessageCode = 2 });
                }
                else
                {
                    return Ok(new { Code = 200, Data = Result.Item1, MessageCode = 1, Message = "OTP has been sent to your Registered Email ID" });
                }
            }
            else
            {
                return BadRequest(new { Code = 200, Message = ModelState, MessageCode = 2 });
            }
        }
        [HttpPost, Route("EmailSubscrible")]
        public IActionResult EmailSubscrible(emailSubscription _obj )
        {
            bool IsAuth = User.Identity.IsAuthenticated;
            if (IsAuth != false)
            {
                long UserId = Convert.ToInt64(User.Identity.Name);
                var Result = _Login.UserEmailSubscrible(UserId, _obj.UserEmailSubscrible, _obj.OfferEmailSubscription, _obj.Cookies);
                if (Result == 1)
                {
                    return Ok(new { Code = 200, Message = "User Data Saved Successfully !", MessageCode = 1 });
                }
                else
                {
                    return Ok(new { Code = 200, Message = "Unable to Save Data", MessageCode = 2 });
                }
        }
            else
            {
                return Ok(new { Code = 200, Message = "Token is Invalid", MessageCode = 2 });
            }
        }

        [HttpGet, Route("GetEmailSubscription")]
        public IActionResult GetEmailSubscription()
        {
            bool IsAuth = User.Identity.IsAuthenticated;
            if (IsAuth != false)
            {
                long UserId = Convert.ToInt64(User.Identity.Name);
            var Result = _Login.GetUserEmailSubscrible(UserId);
            if (Result != null)
            {
                return Ok(new { Code = 200, Message = Result, MessageCode = 1 });
            }
            else
            {
                return Ok(new { Code = 200, Message = "No Data Found !", MessageCode = 2 });
            }
            }
            else
            {
                return Ok(new { Code = 200, Message = "Token is Invalid", MessageCode = 2 });
            }
        }
        [HttpPost, Route("DeleteAccount")]
        public IActionResult DeleteAccount()
        {
            bool IsAuth = User.Identity.IsAuthenticated;
            if (IsAuth != false)
            {
                long UserId = Convert.ToInt64(User.Identity.Name);
                var Result = _Login.DeleteAccount(UserId);
                if (Result != -1)
                {
                    return Ok(new { Code = 200, Message = "User Account Delete Successfully", MessageCode = 1 });
                }

                else
                {
                    return Ok(new { Code = 201, Message = "Unable to Delete Account", MessageCode = 2 });
                }


            }
            else
            {
                return Ok(new { Code = 201, Message = "Token is Invalid", MessageCode = 2 });
            }




        }
        [HttpPost, Route("OtpVerify")]
        [AllowAnonymous]
        public IActionResult OtpVerify(SendEmailLink _obj)
        {
            string SendGridKey = _iconfiguration.GetSection("SendGridKey").Value;
            if(_obj.OTP==null || _obj.OTP<=0)
            {
                return Ok(new { Code = 201, Message = "OTP is Requried.", MessageCode = 2 });
            }
            if (String.IsNullOrEmpty(_obj.EmailId))
            {
                return Ok(new { Code = 201, Message = "Email Id is Requried.", MessageCode = 2 });
            }
            UserEntites userEntites = new UserEntites();
            int OTPVerifyTime = Convert.ToInt32(_iconfiguration.GetSection("OTPVerifyTime").Value);
            var Result = _Login.OTPVerifiy(_obj.OTP, _obj.EmailId, OTPVerifyTime,SendGridKey);
            if (Result == 1)
            {

                //***************Checking for partner *************
                userEntites = _user.GetUserDetailsByEmailAddress(_obj.EmailId);
                if (userEntites.Partner_Id != 0 && !string.IsNullOrEmpty(userEntites.Partner_Params))
                {
                    _Partner.PartnerVerification(userEntites.Partner_Id, userEntites.Partner_Params);
                }
                //***************#******************

                return Ok(new { Code = 200, Message = "User Account Verified Successfully", MessageCode = 1 });
            }
            else if (Result == -2)
            {
                return Ok(new { Code = 201, Message = "Sorry! Email Id does not Exist", MessageCode = 2 });
            }
            else if (Result == -1)
            {
                return Ok(new { Code = 201, Message = "OTP You have entered is invalid.", MessageCode = 2 });
            }
            else
            {
                return Ok(new { Code = 201, Message = "OTP Has beed expired , Please Regenerate It", MessageCode = 2 });
            }

        }
        [HttpPost, Route("ResetPassword")]
        [AllowAnonymous]
        public IActionResult ResetPassword(resetpassword _obj)
        {
            int OTPVerifyTime = Convert.ToInt32(_iconfiguration.GetSection("OTPVerifyTime").Value);
            _obj.minutes = OTPVerifyTime;
            var Result = _Login.ResetPassword(_obj);
            if (Result == 1)
            {
                return Ok(new { Code = 200, Message = "Your new password updated successfully", MessageCode = 1 });
            }
            else if (Result == -3)
            {
                return Ok(new { Code = 201, Message = "Sorry! Email Id does not Exist", MessageCode = 2 });
            }
            else if (Result == -1)
            {
                return Ok(new { Code = 201, Message = "OOPS! Your OTP has been wrong", MessageCode = 2 });
            }
            else
            {
                return Ok(new { Code = 201, Message = "OTP Has beed expired , Please Regenerate It", MessageCode = 2 });
            }

        }
        [HttpPost, Route("ReGenerateOtp")]
        [AllowAnonymous]
        public IActionResult ReGenerateOtp(SendEmailLink email)
        {
            string SendGridKey = _iconfiguration.GetSection("SendGridKey").Value;
            var Result = _Login.ReGenerateOtp(email.EmailId, SendGridKey,true);
            if (Result < 0)
            {
                return Ok(new { Code = 201, Message = "Sorry! Email Id does not Exist", MessageCode = 2 });
            }
           else if (Result > 0)
            {
                return Ok(new { Code = 200, Message = "New OTP Sent to your registered Email", MessageCode = 1 });
            }
            else if (Result == -1)
            {
                return Ok(new { Code = 201, Message = "OOPS! Something went wrong", MessageCode = 2 });
            }
            else
            {
                return Ok(new { Code = 201, Message = "OOPS! Something went wrong", MessageCode = 2 });
            }

        }
        [HttpPost, Route("MobileRegistation")]
        [AllowAnonymous]
        public IActionResult MobileRegistation(RegistationEntites _User)
        {
            if(String.IsNullOrEmpty(_User.CountrCode))
            {
                return Ok(new { Code = 201, Message = "Country Code is requried", MessageCode = 2 });
            }
            if (String.IsNullOrEmpty(_User.mobileNo))
            {
                return Ok(new { Code = 201, Message = "Mobile number is requried", MessageCode = 2 });
            }
            if (String.IsNullOrEmpty(_User.EmailAddress))
            {
                return Ok(new { Code = 201, Message = "Email Id  is requried", MessageCode = 2 });
            }
            if (String.IsNullOrEmpty(_User.FullName))
            {
                return Ok(new { Code = 201, Message = "Full Name  is requried", MessageCode = 2 });
            }
            if (String.IsNullOrEmpty(_User.IPAddress))
            {
                return Ok(new { Code = 201, Message = "IP Address  is requried", MessageCode = 2 });
            }
            if (_User.IPAddress.Equals("undefined"))
            {
                return Ok(new { Code = 201, Message = "IP Address can not be undefined", MessageCode = 2 });
            }
            string MessageText = "";
            if (ModelState.IsValid)
            {
                int Mac = MacValue;
                string SendGridKey = _iconfiguration.GetSection("SendGridKey").Value;
                bool userActive = _common.MobileNumberUnique(_User.mobileNo);
                if (userActive == true)
                {
                    return Ok(new { Code = 201, Status = false, Message = "Mobile Number already Exists.", MessageCode = 2 });
                }
                var Result = _Login.UserMobileRegistation(_User, Mac, SendGridKey);
                if(Result.Item4!=0)
                {
                    if (Result.Item4 == 0)
                    {
                        MessageText = "You can not use self generated Referral code";
                    }
                    else if (Result.Item4 == 1)
                    {
                        if (Result.Item2 == 1)
                        {
                            return Ok(new { Code = 201, Message = "User Already Exist !", MessageCode = 2 });
                        }
                        else if (Result.Item3 == 1)
                        {
                            return Ok(new { Code = 202, Message = "Another user is already using this Device !", MessageCode = 2 });
                        }
                        else
                        {
                            try
                            {
                                string accountSid = _iconfiguration.GetSection("accountSid").Value.ToString();
                                string authToken = _iconfiguration.GetSection("authToken").Value.ToString();
                                string fromNo = _iconfiguration.GetSection("fromNo").Value.ToString();
                                // int otp = _Login.mobileOtp(mobile);
                                TwilioClient.Init(accountSid, authToken);
                                var type = new List<string> {
                                       "NationalFormat",
                                         "PhoneNumber"
                                                                 };

                                _User.mobileNo = _User.CountrCode + _User.mobileNo;
                                var message = MessageResource.Create(
                                    body: "Your OTP for Opinionest registration is " + Result.Item5+ " Please use this OTP for verifying your account.The OTP is valid for only 60 mins.",
                                    from: new Twilio.Types.PhoneNumber(fromNo),
                                    to: new Twilio.Types.PhoneNumber(_User.mobileNo)
                                );
                                //return Ok(new { Code = 200, Status = true, Message = "OTP Sent to your number.", MessageCode = 1 });
                            }
                            catch (Exception ex)
                            {
                               // return Ok(new { Code = 201, Status = true, Message = ex.Message, MessageCode = 2 });
                            }
                            return Ok(new { Code = 200, Data = Result.Item1, MessageCode = 1, Message = "OTP has been sent to your Registered Mobile Number or Email Id" });
                        }
                    }
                    else if (Result.Item4 == -1)
                    {
                        MessageText = "Referral code is not valid";
                    }
                    else if (Result.Item4 == 2)
                    {
                        MessageText = "You have already applied a referral code";
                    }
                    else if (Result.Item4 == 3)
                    {
                        MessageText = "Sorry! You can not user referral code who refered you.";
                    }
                    else if (Result.Item4 == 4)
                    {
                        MessageText = "Sorry! You can not use the referral code after 48 hours from the registration. ";
                    }
                    else if (Result.Item4 == 5)
                    {
                        MessageText = "You have already used a referral code in Old account.";
                    }
                    return Ok(new { Code = 201, Status = true, Message = MessageText, MessageCode = 2 });
                }
                if (Result.Item2 == 1)
                {
                    return Ok(new { Code = 201, Message = "User Already Exist !", MessageCode = 2 });
                }
                else if (Result.Item3 == 1)
                {
                    return Ok(new { Code = 202, Message = "Another user is already using this Device !", MessageCode = 2 });
                }
                else if (Result.Item4 == 0 && _User.referalCode!="")
                {
                    return Ok(new { Code = 202, Message = "Wrong Referal Code !", MessageCode = 2 });
                }
                else
                {
                    //return Ok(new { Code = 200, Data = Result.Item1, MessageCode = 1, Message = "OTP has been sent to your Registered Email ID" });
                    try
                    {
                        string accountSid = _iconfiguration.GetSection("accountSid").Value.ToString();
                        string authToken = _iconfiguration.GetSection("authToken").Value.ToString();
                        string fromNo = _iconfiguration.GetSection("fromNo").Value.ToString();
                        // int otp = _Login.mobileOtp(mobile);
                        TwilioClient.Init(accountSid, authToken);
                        var type = new List<string> {
                                       "NationalFormat",
                                         "PhoneNumber"
                                                                 };

                        _User.mobileNo = _User.CountrCode + _User.mobileNo;
                        var message = MessageResource.Create(
                            body: "Your OTP for Opinionest registration is " + Result.Item5 + " Please use this OTP for verifying your account.The OTP is valid for only 60 mins.",
                            from: new Twilio.Types.PhoneNumber(fromNo),
                            to: new Twilio.Types.PhoneNumber(_User.mobileNo)
                        );
                        //return Ok(new { Code = 200, Status = true, Message = "OTP Sent to your number.", MessageCode = 1 });
                    }
                    catch (Exception ex)
                    {
                        // return Ok(new { Code = 201, Status = true, Message = ex.Message, MessageCode = 2 });
                    }
                    return Ok(new { Code = 200, Data = Result.Item1, MessageCode = 1, Message = "OTP has been sent to your Registered Mobile Number or Email Id" });

                }
            }
            else
            {
                return Ok(new { Code = 201, Message = ModelState, MessageCode = 2 });
            }
        }
        [HttpGet, Route("CountryCode")]
        [AllowAnonymous]
        public IActionResult CountryCode(string Ip)
        {

            if (String.IsNullOrEmpty(Ip))
            {
                return Ok(new { Code = 201, Message = "IP Address  is requried", MessageCode = 2 });
            }
            if (Ip.Equals("undefined"))
            {
                return Ok(new { Code = 201, Message = "IP Address can not be undefined", MessageCode = 2 });
            }
            var Result = _Login.countryInfo(Ip);
            if (Result !=null)
            {
                return Ok(new { Code = 200, Message = "Data fetch succefully", MessageCode = 1,data= Result });
            }
            else
            {
                return Ok(new { Code = 201, Message = "Something went wrong", MessageCode = 2 });
            }
            

        }
    }

}