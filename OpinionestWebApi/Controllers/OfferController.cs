﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using BusinessAccessLayer.InterfaceDeclaration;
using BusinessAccessLayer.ServiceDefination;
using BusinessEntites;
using CommonMethod;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RestSharp;

namespace OpinionestWebApi.Controllers
{
    [Microsoft.AspNetCore.Cors.EnableCors("AllowOrigin")]
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class OfferController : Controller
    {
        SqlConnection _Conn;
        IConfiguration _iconfiguration;
        IOffer _Offer;
        Iuser _IUser;
        ILogin _Login;
        ICommon _common;
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="iconfiguration"></param>
        public OfferController(IConfiguration iconfiguration)
        {
            _iconfiguration = iconfiguration;
            string Connection = _iconfiguration.GetSection("ConnectionStrings").GetSection("DefaultConnection").Value;
            _Conn = new SqlConnection(Connection);
            _Offer = new OfferServiceDefination(_Conn);
            _IUser = new UserServicesDefination(_Conn);
            _Login = new LoginServicesDefination(_Conn);
            _common = new CommonServicesDefination(_Conn);
        }
        [AllowAnonymous]
        [HttpGet, Route("OfferList")]
        public IActionResult OfferList(string offerType,int? pageIndex, int pageSize)
        {
            var myList = new List<BusinessEntites.Offer>();
            var favt = new List<BusinessEntites.OfferFavirout>();
            bool IsAuth = User.Identity.IsAuthenticated;
            if (IsAuth != false)
            {
                long UserId = Convert.ToInt64(User.Identity.Name);
                bool userActive = _common.ValidUser(UserId);
                if (userActive == false)
                {
                    return Ok(new { Code = 201, Status = false, Message = "Invalid User, Please create new account.", MessageCode = 2 });
                }
                if (ModelState.IsValid)
                {
                   // var data = _Offer.OfferList(UserId, offerType, pageIndex, pageSize);

                    var data = _Offer.offerUserDetails(UserId);
                    favt = data.Item4;
                    ///<summary>
                    /// call the Tango api 
                    /// </summary>
                    /// 
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    var client1 =  new RestClient("https://api.tangocard.com/raas/v2/catalogs?verbose=true");
                    client1.Timeout = -1;
                    var request = new RestRequest(Method.GET);
                    request.AddHeader("Authorization", "Basic T3BpbmlvbmVzdC02ODE6cG1GanhAQW9EU1NUU0VJclVqd1hqWiRjeiYkeXVYU3l5QE1ZSGN1dWFZaGpT");
                    IRestResponse response = client1.Execute(request);
                    Application bsObj = JsonConvert.DeserializeObject<Application>(response.Content);
                    //dynamic bsObj = JsonConvert.DeserializeObject<dynamic>(response.Content);

                    foreach (var v in bsObj.brands)
                    {
                        Offer _offer = new Offer();

                       
                        if (!String.IsNullOrEmpty(data.Item1))
                        {
                            _offer.AvailablePoints=Convert.ToDecimal(data.Item1);
                        }
                        
                        _offer.Currency = v.items[0].currencyCode;
                        _offer.Country = v.items[0].countries[0].ToString();
                        _offer.Denomination = v.brandName;
                        _offer.Disclaimer = v.disclaimer;
                        _offer.RewardName = v.brandName;
                        _offer.UtId = v.items[0].utid;
                        _offer.ImagePath = v.imageUrls.The1200W326Ppi.AbsoluteUri;
                        var check = favt.Where(x => x.TangoId == _offer.UtId).FirstOrDefault();
                        
                        if(check!=null)
                        {
                            _offer.IsFavourite = 1;
                        }
                        _offer.MaxRange = ( (v.items[0].faceValue.ToString()));
                        if (!String.IsNullOrEmpty(data.Item2))
                        {
                            _offer.PointsRequried = (Convert.ToDecimal(_offer.MaxRange) / Convert.ToDecimal(data.Item2)).ToString();
                        }
                        if(Convert.ToDecimal(_offer.MaxRange)>0)
                        {
                            myList.Add(_offer);
                        }
                        
                    }
                    myList = myList.Where(x => x.Country == data.Item3).ToList();
                   // Console.WriteLine(response.Content);


                    // var list = myList.Skip((pageIndex - 1 ?? 0) * pageSize).Take(pageSize).ToList();
                    if (myList.Count >= 1)
                    {
                        return Ok(new { Code = 200, Status = true, Message = "Success", Data = myList, TotalRecords = myList.Count() });
                    }
                    else
                    {
                        return Ok(new { Code = 200, Status = false, Message = "No records are found.", Data = "" });
                    }

                }
                else
                {
                    return BadRequest(new { Code = 401, Status = false, Message = "Something wrong please check again.", Data = "" });
                }

            }
            else
            {
                return Ok(new { Code = 200, Message = "Token is Invalid", MessageCode = 2 });
            }
        }
        [HttpPost, Route("UpdatePoints")]
        public IActionResult UserPointsDeduction(PointsUpdateInputModel obj)
        {
            if ((obj.DeductionPoints)<=0)
            {
                return Ok(new { Code = 201, Status = false, Message = "Transection amount is requried.", MessageCode = 2 });
            }
            if (String.IsNullOrEmpty(obj.utid))
            {
                return Ok(new { Code = 201, Status = false, Message = "UTID is requried.", MessageCode = 2 });
            }
            //if (String.IsNullOrEmpty(obj.payment_type))
            //{
            //    return Ok(new { Code = 201, Status = false, Message = "Payment type is requried.", MessageCode = 2 });
            //}
            bool IsAuth = User.Identity.IsAuthenticated;
            string SendGridKey = _iconfiguration.GetSection("SendGridKey").Value;
            string paytmEmail = _iconfiguration.GetSection("paytmEmail").Value;
            IsAuth = true;

            if (IsAuth != false)
            {
                long UserId = Convert.ToInt64(User.Identity.Name);
                bool userActive = _common.ValidUser(UserId);
                if (userActive == false)
                {
                    return Ok(new { Code = 201, Status = false, Message = "Invalid User, Please create new account.", MessageCode = 2 });
                }
                var result = "";
                obj.UserId = UserId;
                obj.Fk_PaymentGetwayId = (int)PaymentGatewayMaster.Tango;
                result = _Offer.UpdatePoints(obj);
                var getUser = _IUser.GetUserProfile(UserId);
                if (!string.IsNullOrEmpty(result))
                {

                    if (result == "1")
                    {

                        DailyFeedEntites dailyFeedEntites = new DailyFeedEntites
                        {
                            Header = "Offer",
                            USERiD = obj.UserId.ToString(),
                            IconPath = obj.logoPath,
                            Message = "We have received your redeem request  for " + obj.DeductionPoints + " points. We will be processing your request within 7 days",
                            Points = obj.DeductionPoints.ToString(),

                        };

                        _Offer.DailyFeedSave(dailyFeedEntites);

                        string message = "You have received the new offer reedem Request for UserId " + getUser.EmailAddress + " Amout is:" + obj.DeductionPoints;
                        _Login.sendEmail(paytmEmail, "offer reedem Request", message, SendGridKey);

                        return Ok(new { Code = 200, Status = true, Message = "Created Successfully", MessageCode = 1 });
                    }
                    else if (result == "-1")
                    {
                        return Ok(new { Code = 201, Status = true, Message = "Deduction Points can not be greater then total points.", MessageCode = 2 });
                    }
                    else if (result == "-2")
                    {
                        return Ok(new { Code = 201, Status = true, Message = "You need a minimum of 1000 points for the first time to be eligible for converting your points to voucher/money.", MessageCode = 2 });
                    }
                    else if (result == "-3")
                    {
                        return Ok(new { Code = 201, Status = true, Message = "Invalid User", MessageCode = 2 });
                    }
                    else
                    {
                        return Ok(new { Code = 201, Status = true, Message = "Something went wrong.", MessageCode = 2 });
                    }


                }
                else
                {
                    return Ok(new { Code = 201, Status = false, Message = "Something wrong please Try again.", MessageCode = 2 });
                }
            }
            else
            {
                return Ok(new { Code = 200, Message = "Token is Invalid", MessageCode = 2 });
            }

        }

        //[HttpPost, Route("UpdatePoints")]
        //public IActionResult UserPointsDeduction(PointsUpdateInputModel obj)
        //{
        //    bool IsAuth = User.Identity.IsAuthenticated;
        //    string SendGridKey = _iconfiguration.GetSection("SendGridKey").Value;
        //    string paytmEmail = _iconfiguration.GetSection("paytmEmail").Value;
        //    IsAuth = true;
        //    if (IsAuth != false)
        //    {
        //        long UserId = Convert.ToInt64(User.Identity.Name);
        //        bool userActive = _common.ValidUser(UserId);
        //        if (userActive == false)
        //        {
        //            return Ok(new { Code = 201, Status = false, Message = "Invalid User, Please create new account.", MessageCode = 2 });
        //        }
        //        //long UserId = Convert.ToInt64(122390);
        //        var result = "";
        //        obj.UserId = UserId;
        //         result = _Offer.UpdatePoints(obj); 
        //        if (!string.IsNullOrEmpty(result))
        //        {
        //            var id = result.Split('~');
        //            if (id[1].ToString() == "1")
        //            {
        //                var Data = PostTango(obj, UserId);
        //                if (Data.httpCode == 0 && Data.status != null)
        //                {
        //                    DailyFeedEntites dailyFeedEntites = new DailyFeedEntites
        //                    {
        //                        Header= "Offer",
        //                        USERiD = obj.UserId.ToString(),
        //                        IconPath=obj.logoPath,
        //                        Message= "You have applied for new voucher, voucher value is : "+obj.DeductionPoints,
        //                        Points=obj.DeductionPoints.ToString(),

        //                    };

        //                    _Offer.DailyFeedSave(dailyFeedEntites);

        //                        string message = "You have received the new offer reedem Request for UserId " + UserId + " Amout is:" + obj.DeductionPoints;
        //                        _Login.sendEmail(paytmEmail, "offer reedem Request", message, SendGridKey);

        //                    return Ok(new { Code = 200, Status = true, Message = "Created Successfully", MessageCode = 1 });
        //                }
        //                else
        //                {
        //                    //var id = result.Split('~');
        //                    //var id = result;
        //                    var result1 = _Offer.DeletePointTransaction(Convert.ToInt64(id[2].ToString()));
        //                    return Ok(new { Code = 201, Status = true, Message = Data.errors[0].message, MessageCode = 2 });
        //                }
        //            }
        //            else if(id[1].ToString() == "-1")
        //            {
        //                return Ok(new { Code = 201, Status = true, Message = "Deduction Points can not be greater then total points.", MessageCode = 2 });
        //            }
        //            else if (id[1].ToString() == "-2")
        //            {
        //                return Ok(new { Code = 201, Status = true, Message = "You need a minimum of 1000 points for the first time to be eligible for converting your points to voucher/money.", MessageCode = 2 });
        //            }
        //            else
        //            {
        //                return Ok(new { Code = 201, Status = true, Message = "Something went wrong.", MessageCode = 2 });
        //            }


        //        }
        //        else
        //        {
        //            return Ok(new { Code = 201, Status = false, Message = "Something wrong please Try again.", MessageCode = 2 });
        //        }
        //    }
        //    else
        //    {
        //        return Ok(new { Code = 200, Message = "Token is Invalid", MessageCode = 2 });
        //    }

        //}
        //[HttpPost, Route("PostTango")]
        //public ApplicationResponse PostTango(PointsUpdateInputModel obj,long UserId)
        //{
        //    string accountIdentifier = _iconfiguration.GetSection("accountIdentifier").Value;
        //    string etid = _iconfiguration.GetSection("etid").Value;
        //    string token = _iconfiguration.GetSection("token").Value;
        //    string tangoEmailSubject = _iconfiguration.GetSection("tangoEmailSubject").Value;
        //    string tangoEmailMessaget = _iconfiguration.GetSection("tangoEmailMessaget").Value;
        //    string tangoEmailSender = _iconfiguration.GetSection("tangoEmailSender").Value;
        //    string tangoSenderFirstName = _iconfiguration.GetSection("tangoSenderFirstName").Value;
        //    string tangoSenderLastName = _iconfiguration.GetSection("tangoSenderLastName").Value;
        //    var Result = _IUser.GetUserProfile(UserId);
        //    TangoPost _obj = new TangoPost
        //    {
        //        AccountIdentifier= accountIdentifier,
        //        Amount= obj.DeductionPoints.ToString(),
        //        CustomerIdentifier= "G28339710",
        //        EmailSubject= tangoEmailSubject,
        //        Etid= etid,
        //        Message= tangoEmailMessaget,
        //        Campaign="",
        //        ExternalRefId="",
        //        Notes="",
        //        Recipient= new Recipient
        //        {
        //            Email= Result.EmailAddress,
        //            FirstName= Result.FirstName,
        //            LastName= Result.LastName,
        //        },
        //        SendEmail=true,
        //        Sender=  new Recipient
        //        {
        //            Email = tangoEmailSender,
        //            FirstName = tangoSenderFirstName,
        //            LastName = tangoSenderLastName
        //        },
        //        Utid=obj.utid



        //    };
        //    string json = JsonConvert.SerializeObject(_obj);
        //    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
        //    var client = new RestClient("https://api.tangocard.com/raas/v2/orders");
        //    client.Timeout = -1;
        //    var request = new RestRequest(Method.POST);
        //    //request.AddHeader("Authorization", "Basic b3BpbmlvbmVzdHRlc3Q6QVF4aW9EalV1ZEx4TmE/SXZxQmZ1VktmQ21MYSZwWHd0d0x4T01lUWtVZm9obQ==");
        //    request.AddHeader("Authorization", "Basic T3BpbmlvbmVzdC02ODE6cG1GanhAQW9EU1NUU0VJclVqd1hqWiRjeiYkeXVYU3l5QE1ZSGN1dWFZaGpT");
        //    request.AddHeader("Content-Type", "application/json");
        //    request.AddParameter("application/json", json, ParameterType.RequestBody);
        //    IRestResponse response = client.Execute(request);
        //    //Console.WriteLine(response.Content);

        //    tangoLogs tangoLogs = new tangoLogs
        //    {
        //        RequestJson= json,
        //        UserId= UserId,
        //        ResponseJson= response.Content,
        //        Status= response.StatusCode.ToString()
        //    };
        //    string i = _Offer.tangoLogs(tangoLogs);

        //    ApplicationResponse bsObj = JsonConvert.DeserializeObject<ApplicationResponse>(response.Content);

        //    return bsObj;

        //}

        [HttpPost, Route("MarkOfferFavourite")]
        public IActionResult MarkOfferFavourite(MarkOfferFavouriteInputModel obj)
        {
            bool IsAuth = User.Identity.IsAuthenticated;
            if (IsAuth != false)
            {
                long UserId = Convert.ToInt64(User.Identity.Name);
                bool userActive = _common.ValidUser(UserId);
                if (userActive == false)
                {
                    return Ok(new { Code = 201, Status = false, Message = "Invalid User, Please create new account.", MessageCode = 2 });
                }
                obj.UserId = UserId;
                var Result = _Offer.MarkOfferFavourite(obj);
                if (Result == 1)
                {
                    return Ok(new { Code = 200, Status = true, Message = "Offer added to favourite successfully.", MessageCode = 1 });
                }
                else if (Result == 0)
                {
                    return Ok(new { Code = 200, Status = true, Message = "Offer removed from favourite.", MessageCode = 1 });
                }
                else
                {
                    return Ok(new { Code = 201, Status = false, Message = "Something wrong please Try again.", MessageCode = 2 });
                }
            }
            else
            {
                return Ok(new { Code = 200, Message = "Token is Invalid", MessageCode = 2 });
            }

        }

        /// <summary>
        /// Offer History
        /// </summary>
        /// <param name="offerType"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [HttpGet, Route("OfferHistory")]
        public IActionResult OfferHistory(string offerType,int? pageIndex, int pageSize)
        {
            var myList = new List<BusinessEntites.OfferHistoryDetails>();
            bool IsAuth = User.Identity.IsAuthenticated;
            if (IsAuth != false)
            {
                long UserId = Convert.ToInt64(User.Identity.Name);
                bool userActive = _common.ValidUser(UserId);
                if (userActive == false)
                {
                    return Ok(new { Code = 201, Status = false, Message = "Invalid User, Please create new account.", MessageCode = 2 });
                }
                if (ModelState.IsValid)
                {
                    myList = _Offer.OfferHistoryDetails(UserId, offerType, pageIndex, pageSize);
                    var list = myList.Skip((pageIndex - 1 ?? 0) * pageSize).Take(pageSize).ToList();
                    if (myList.Count >= 1)
                    {
                        return Ok(new { Code = 200, Status = true, Message = "Success", Data = list, TotalRecords = myList.Count() });
                    }
                    else
                    {
                        return Ok(new { Code = 200, Status = false, Message = "No records are found.", Data = "" });
                    }

                }
                else
                {
                    return BadRequest(new { Code = 401, Status = false, Message = "Something wrong please check again.", Data = "" });
                }

            }
            else
            {
                return Ok(new { Code = 200, Message = "Token is Invalid", MessageCode = 2 });
            }
        }

        [HttpGet, Route("CashVoucherDetails")]
        public IActionResult CashVoucherDetails(long countryId)
        {
            var myList = new List<BusinessEntites.UserRedeemRequestModel>();
            bool IsAuth = User.Identity.IsAuthenticated;
            if (IsAuth != false)
            {
                long UserId = Convert.ToInt64(User.Identity.Name);
                bool userActive = _common.ValidUser(UserId);
                if (userActive == false)
                {
                    return Ok(new { Code = 201, Status = false, Message = "Invalid User, Please create new account.", MessageCode = 2 });
                }
                if (ModelState.IsValid)
                {
                    myList = _Offer.CashVoucherDetails(countryId);
                    var list = myList.ToList();
                    list.ForEach(x => x.PaymentGatewayToken = x.PaymentGatewayId.ToString().EncryptID());
                    if (myList.Count >= 1)
                    {
                        return Ok(new { Code = 200, Status = true, Message = "Success", Data = list, TotalRecords = myList.Count() });
                    }
                    else
                    {
                        return Ok(new { Code = 200, Status = false, Message = "No records are found.", Data = "" });
                    }

                }
                else
                {
                    return BadRequest(new { Code = 401, Status = false, Message = "Something wrong please check again.", Data = "" });
                }

            }
            else
            {
                return Ok(new { Code = 200, Message = "Token is Invalid", MessageCode = 2 });
            }
        }

        [HttpGet, Route("CheckUserAccountLinked")]
        public IActionResult CheckUserAccountLinked(long paymentGatewayId)
        {
            var myList = new List<BusinessEntites.UserAccountLinkedStatus>();
            bool IsAuth = User.Identity.IsAuthenticated;
            if (IsAuth != false)
            {
                long userId = Convert.ToInt64(User.Identity.Name);
                bool userActive = _common.ValidUser(userId);
                if (userActive == false)
                {
                    return Ok(new { Code = 201, Status = false, Message = "Invalid User, Please create new account.", MessageCode = 2 });
                }
                if (ModelState.IsValid)
                {
                    myList = _Offer.CheckUserAccountLinked(userId, paymentGatewayId);
                    var list = myList.ToList();
                   
                    if (myList.Count >= 1)
                    {
                        return Ok(new { Code = 200, Status = true, Message = "Success", Data = list, TotalRecords = myList.Count() });
                    }
                    else
                    {
                        return Ok(new { Code = 200, Status = false, Message = "No records are found.", Data = "" });
                    }

                }
                else
                {
                    return BadRequest(new { Code = 401, Status = false, Message = "Something wrong please check again.", Data = "" });
                }

            }
            else
            {
                return Ok(new { Code = 200, Message = "Token is Invalid", MessageCode = 2 });
            }
        }

        [HttpPost, Route("LinkWalletByPhoneNumber")]
        public IActionResult LinkWalletByPhoneNumber(UserLinkWalletInputModel obj)
        {
            bool IsAuth = User.Identity.IsAuthenticated;
            if (IsAuth != false)
            {
                long UserId = Convert.ToInt64(User.Identity.Name);
                bool userActive = _common.ValidUser(UserId);
                if (userActive == false)
                {
                    return Ok(new { Code = 201, Status = false, Message = "Invalid User, Please create new account.", MessageCode = 2 });
                }
                obj.UserId = UserId;
                var Result = _Offer.LinkUserWallet(obj);
                if (Result == 1)
                {
                    return Ok(new { Code = 200, Status = true, Message = "Account linked successfully.", MessageCode = 1 });
                }
                else if (Result == 0)
                {
                    return Ok(new { Code = 200, Status = true, Message = "Account doesnot exist.", MessageCode = 1 });
                }
                else
                {
                    return Ok(new { Code = 201, Status = false, Message = "Something wrong please Try again.", MessageCode = 2 });
                }
            }
            else
            {
                return Ok(new { Code = 200, Message = "Token is Invalid", MessageCode = 2 });
            }

        }

        [HttpPost, Route("LinkWalletByEmailAddress")]
        public IActionResult LinkWalletByEmailAddress(UserLinkWalletInputModel obj)
        {
            bool IsAuth = User.Identity.IsAuthenticated;
            if (IsAuth != false)
            {
                long UserId = Convert.ToInt64(User.Identity.Name);
                bool userActive = _common.ValidUser(UserId);
                if (userActive == false)
                {
                    return Ok(new { Code = 201, Status = false, Message = "Invalid User, Please create new account.", MessageCode = 2 });
                }
                obj.UserId = UserId;
                var Result = _Offer.LinkUserWallet(obj);
                if (Result == 1)
                {
                    return Ok(new { Code = 200, Status = true, Message = "Account linked successfully.", MessageCode = 1 });
                }
                else if (Result == 0)
                {
                    return Ok(new { Code = 200, Status = true, Message = "Account doesnot exist.", MessageCode = 1 });
                }
                else
                {
                    return Ok(new { Code = 201, Status = false, Message = "Something wrong please Try again.", MessageCode = 2 });
                }
            }
            else
            {
                return Ok(new { Code = 200, Message = "Token is Invalid", MessageCode = 2 });
            }

        }

        /// <summary>
        /// statusMessage = 1 then success
        /// </summary>
        /// <param name="paymentGatewayId"></param>
        /// <returns></returns>
        [HttpGet, Route("GetPaytmUserStatus")]
        public IActionResult GetPaytmUserStatus(string PhoneNo, long paymentgatewayId)
        {
            var myList = new PaytmApiModel();
            bool IsAuth = User.Identity.IsAuthenticated;
            if (IsAuth != false)
            {
                long userId = Convert.ToInt64(User.Identity.Name);
                bool userActive = _common.ValidUser(userId);
                if (userActive == false)
                {
                    return Ok(new { Code = 201, Status = false, Message = "Invalid User, Please create new account.", MessageCode = 2 });
                }
                if (ModelState.IsValid)
                {
                    var ExistCheck = _Offer.CheckExistVefication(userId, PhoneNo);
                    if (ExistCheck == true)
                    {
                        return Ok(new { Code = 200, Status = true, Message = "Success", Data = "", TotalRecords = "" });
                    }
                    else
                    {


                        myList = _Offer.GetPaytmUserStatus(PhoneNo, paymentgatewayId, userId);

                        if (myList.statusMessage == "SUCCESS")
                        {
                            return Ok(new { Code = 200, Status = true, Message = "Success", Data = myList, TotalRecords = myList });
                        }
                        else
                        {
                            return Ok(new { Code = 201, Status = false, Message = myList.statusMessage, Data = myList });
                        }
                    }

                }
                else
                {
                    return BadRequest(new { Code = 401, Status = false, Message = "Something wrong please check again.", Data = "" });
                }

            }
            else
            {
                return Ok(new { Code = 200, Message = "Token is Invalid", MessageCode = 2 });
            }
        }

        /// <summary>
        /// statusMessage = 1 then success
        /// </summary>
        /// <param name="paymentGatewayId"></param>
        /// <returns></returns>
        [HttpGet, Route("SendUserPointRedeemRequest")]
        public IActionResult SendUserPointRedeemRequest(long PointsRedeem, decimal RequestedAmount, string PGId, string mobileNumber, long Points)
        {
            long result = 0;
            bool IsAuth = User.Identity.IsAuthenticated;
            string SendGridKey = _iconfiguration.GetSection("SendGridKey").Value;
            string paytmEmail = _iconfiguration.GetSection("paytmEmail").Value;
            if (IsAuth != false)
            {
                long userId = Convert.ToInt64(User.Identity.Name);
                bool userActive = _common.ValidUser(userId);
                var getUser = _IUser.GetUserProfile(userId);
                if (userActive == false)
                {
                    return Ok(new { Code = 201, Status = false, Message = "Invalid User, Please create new account.", MessageCode = 2 });
                }
                if (getUser.PhoneNumber != mobileNumber)
                {
                    return Ok(new { Code = 201, Status = false, Message = "Please update your Mobile number linked with your Paytm Account on your Profile.", MessageCode = 2 });
                }
                if (ModelState.IsValid)
                {
                    // result = _Offer.SendUserPointRedeemRequest(PointsRedeem, RequestedAmount, PGId, mobileNumber, userId, Points);
                    PaymentCookies paymentCookies = new PaymentCookies
                    {
                            Fk_UserId= userId,
                            PGId=PGId,
                            MobileNo=mobileNumber,
                            Point=Convert.ToInt32(Points),
                            PointsRedeem= Convert.ToInt32(PointsRedeem),
                            RequestedAmount= RequestedAmount,
                            payment_type=(int)PaymentType.Cash,
                            Fk_PaymentGetwayId=(int)PaymentGatewayMaster.PayTM

                    };
                    if (paymentCookies.RequestedAmount < 100)
                    {
                        return Ok(new { Code = 201, Status = false, Message = "Minimun Amount should be 100.", Data = "" });
                    }
                    else
                    {


                        result = _Offer.PaymentCookiesSave(paymentCookies);
                        if (result > 0)
                        {
                            string message = "You have received the new paytm Request for Email: " + getUser.EmailAddress + " Amout is:" + RequestedAmount;
                            _Login.sendEmail(paytmEmail, "Paytm Request", message, SendGridKey);
                            return Ok(new { Code = 200, Status = true, Message = "Success", Data = "Congratulations,Your paytm requeset has beed accepted !", TotalRecords = 0 });
                        }
                        else if (result == -1)
                        {
                            return Ok(new { Code = 201, Status = false, Message = "Deduction Points can not be greater then total points.", Data = "" });
                        }
                        else if (result == -2)
                        {
                            return Ok(new { Code = 201, Status = false, Message = "You need a minimum of 1000 points for the first time to be eligible for converting your points to money.", Data = "" });
                        }
                        else
                        {
                            return Ok(new { Code = 201, Status = false, Message = "Something wrong please check again.", Data = "" });
                        }
                    }

                }
                else
                {
                    return BadRequest(new { Code = 401, Status = false, Message = "Something wrong please check again.", Data = "" });
                }

            }
            else
            {
                return Ok(new { Code = 200, Message = "Token is Invalid", MessageCode = 2 });
            }
        }
        [AllowAnonymous]
        [HttpGet, Route("OfferTransectionJob")]
        public IActionResult PaymentTransection(string key)
        {
            if (key == "Qo08_g36mU6VKiwqmcHMpA")
            {
                string accountIdentifier = _iconfiguration.GetSection("accountIdentifier").Value;
                string etid = _iconfiguration.GetSection("etid").Value;
                string token = _iconfiguration.GetSection("token").Value;
                string tangoEmailSubject = _iconfiguration.GetSection("tangoEmailSubject").Value;
                string tangoEmailMessaget = _iconfiguration.GetSection("tangoEmailMessaget").Value;
                string tangoEmailSender = _iconfiguration.GetSection("tangoEmailSender").Value;
                string tangoSenderFirstName = _iconfiguration.GetSection("tangoSenderFirstName").Value;
                string tangoSenderLastName = _iconfiguration.GetSection("tangoSenderLastName").Value;
                string SendGridKey = _iconfiguration.GetSection("SendGridKey").Value;
                string EmailReceiver = _iconfiguration.GetSection("paytmEmail").Value;
                var Result = _IUser.TangoProcess();
                foreach (var v in Result)
                {
                    try
                    {
                        if (v.Type == 1)
                        {
                            PaymentCookiesPending paymentCookiesaEntity = new PaymentCookiesPending
                            {
                                Fk_UserId = v.Fk_UserId,
                                Id = v.Id,
                                MobileNo = v.MobileNo,
                                PGId = v.PGId,
                                Point = v.Point,
                                RequestedAmount = v.RequestedAmount,
                                email = v.EmailAddress,
                                Type = v.Type
                            };
                            var result = _Offer.SendUserPointRedeemRequest(paymentCookiesaEntity, EmailReceiver, SendGridKey);
                        }
                        else
                        {
                            TangoPost _obj = new TangoPost
                            {
                                AccountIdentifier = accountIdentifier,
                                Amount = v.RequestedAmount.ToString(),
                                //CustomerIdentifier = "G28339710",   //Live
                                CustomerIdentifier = accountIdentifier,
                                EmailSubject = tangoEmailSubject,
                                Etid = etid,
                                Message = tangoEmailMessaget,
                                Campaign = "",
                                ExternalRefId = "",
                                Notes = "",
                                Recipient = new Recipient
                                {
                                    Email = v.EmailAddress,
                                    FirstName = v.FirstName,
                                    LastName = v.LastName,
                                },
                                SendEmail = true,
                                Sender = new Recipient
                                {
                                    Email = tangoEmailSender,
                                    FirstName = tangoSenderFirstName,
                                    LastName = tangoSenderLastName
                                },
                                Utid = v.PGId
                            };
                            string json = JsonConvert.SerializeObject(_obj);
                            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                            //var client = new RestClient("https://api.tangocard.com/raas/v2/orders");
                            var client = new RestClient("https://integration-api.tangocard.com/raas/v2/orders");
                            client.Timeout = -1;
                            var request = new RestRequest(Method.POST);
                            request.AddHeader("Authorization", "Basic b3BpbmlvbmVzdHRlc3Q6QVF4aW9EalV1ZEx4TmE/SXZxQmZ1VktmQ21MYSZwWHd0d0x4T01lUWtVZm9obQ==");
                            //request.AddHeader("Authorization", "Basic T3BpbmlvbmVzdC02ODE6cG1GanhAQW9EU1NUU0VJclVqd1hqWiRjeiYkeXVYU3l5QE1ZSGN1dWFZaGpT");
                            request.AddHeader("Content-Type", "application/json");
                            request.AddParameter("application/json", json, ParameterType.RequestBody);
                            IRestResponse response = client.Execute(request);
                            ApplicationResponse bsObj = JsonConvert.DeserializeObject<ApplicationResponse>(response.Content);

                            //if (bsObj.status == "Created")
                            if (bsObj.status == "Created" || bsObj.status == "COMPLETE")
                            {
                                PaymentGatewaysLogs paymentGatewaysLogs = new PaymentGatewaysLogs
                                {
                                    Fk_PaymentgatewayId = (int)PaymentGatewayMaster.Tango,
                                    Fk_UserId = v.Fk_UserId,
                                    Points = v.Point,
                                    RequestJson = json,
                                    Request_Type = (int)PaymentType.Voucher,
                                    ResponseJson = response.Content,
                                    ResponseStatus = bsObj.status,
                                    RequestedAmount = v.RequestedAmount,
                                    paymentCookiesId = v.Id
                                };
                                _IUser.paymentGetwayLogs(paymentGatewaysLogs);
                                string message = "Successfully Points have been transferred for email address " + v.EmailAddress + " redeemed the tango voucher , the Amout is:" + v.RequestedAmount + " payment Cookies request id: " + v.Id;
                                _Login.sendEmail(EmailReceiver, "Tango Status: " + bsObj.status, message, SendGridKey);

                            }
                            else
                            {
                                string message = "We have found some issue with tango  for User emailaddress " + v.EmailAddress + " Amount is:" + v.RequestedAmount + " payment Cookies request id: " + v.Id + " Error: " + bsObj.errors[0].message;
                                _Login.sendEmail(tangoEmailSender, "Tango Status: " + bsObj.status, message, SendGridKey);
                                PaymentGatewaysErrorLogs paymentGatewaysLog = new PaymentGatewaysErrorLogs
                                {
                                    Fk_PaymentgatewayId = (int)PaymentGatewayMaster.Tango,
                                    Fk_UserId = v.Fk_UserId,
                                    Points = v.Point,
                                    RequestJson = json,
                                    Request_Type = v.Type,
                                    ResponseJson = response.Content,
                                    ResponseStatus = bsObj.status,
                                    RequestedAmount = v.RequestedAmount,
                                    paymentCookiesId = v.Id,
                                    Error = bsObj.errors[0].message
                                };
                                _IUser.paymentGetwayErrorLogs(paymentGatewaysLog);

                            }

                        }

                    }
                    catch (Exception ex)
                    {
                        PaymentGatewaysErrorLogs paymentGatewaysLogs = new PaymentGatewaysErrorLogs
                        {
                            Fk_PaymentgatewayId = (int)PaymentGatewayMaster.Tango,
                            Fk_UserId = v.Fk_UserId,
                            Points = v.Point,
                            RequestJson = "",
                            Request_Type = v.Type,
                            ResponseJson = "",
                            ResponseStatus = "",
                            RequestedAmount = v.RequestedAmount,
                            paymentCookiesId = v.Id,
                            Error = ex.Message
                        };
                        _IUser.paymentGetwayErrorLogs(paymentGatewaysLogs);
                        string message = "Tango Request ProcessFaild for UserId:  " + v.EmailAddress + " Amout is:" + v.RequestedAmount;
                        _Login.sendEmail(tangoEmailSender, "Tango Request Process Failed", message, SendGridKey);


                    }
                }
                return Ok();
            }
            else
            {
                return BadRequest();
            }

        }
        /// <summary>
        /// GetPointsValue
        /// </summary>
        /// <param name="paymentGatewayId"></param>
        /// <returns></returns>
        [HttpGet, Route("GetPointsValue")]
        public IActionResult GetPointsValue()
        {
            decimal pointValue = 0;
            bool IsAuth = User.Identity.IsAuthenticated;
            if (IsAuth != false)
            {
                long userId = Convert.ToInt64(User.Identity.Name);
                bool userActive = _common.ValidUser(userId);
                if (userActive == false)
                {
                    return Ok(new { Code = 201, Status = false, Message = "Invalid User, Please create new account.", MessageCode = 2 });
                }
                if (ModelState.IsValid)
                {
                    pointValue = _Offer.GetPointsValue(userId);

                    if (pointValue >= 0)
                    {
                        return Ok(new { Code = 200, Status = true, Message = "Success", Data = pointValue });
                    }
                    else
                    {
                        return Ok(new { Code = 200, Status = false, Message = "No records are found.", Data = "" });
                    }

                }
                else
                {
                    return BadRequest(new { Code = 401, Status = false, Message = "Something wrong please check again.", Data = "" });
                }

            }
            else
            {
                return Ok(new { Code = 200, Message = "Token is Invalid", MessageCode = 2 });
            }
        }
        [HttpPost, Route("ConvertPointToCurrency")]
        public IActionResult ConvertPointToCurrency(ConvertPointToCurrency obj)
        {
            bool IsAuth = User.Identity.IsAuthenticated;
            if (IsAuth != false)
            {
                long UserId = Convert.ToInt64(User.Identity.Name);
                bool userActive = _common.ValidUser(UserId);
                if (userActive == false)
                {
                    return Ok(new { Code = 201, Status = false, Message = "Invalid User, Please create new account.", MessageCode = 2 });
                }
                var Result = _Offer.ConvertPointToCurrency(obj, UserId);
                if (Result.Item2 >0)
                {
                    return Ok(new { Code = 200, Status = true, Message = "Point fetch successfully.",data= Result, MessageCode = 1 });
                }
                else if(Result.Item2 == -2)
                {
                    return Ok(new { Code = 200, Status = true, Message = "Someting went wrong.", MessageCode = 1 });
                }
                else
                {
                    return Ok(new { Code = 201, Status = false, Message = "Sorry you need to earn more points.", MessageCode = 2 });
                }
            }
            else
            {
                return Ok(new { Code = 200, Message = "Token is Invalid", MessageCode = 2 });
            }

        }
    }
}