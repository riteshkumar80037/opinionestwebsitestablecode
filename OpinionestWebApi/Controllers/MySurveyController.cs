﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using BusinessAccessLayer.InterfaceDeclaration;
using BusinessAccessLayer.ServiceDefination;
using BusinessEntites;
using CommonMethod;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using SurveyType = BusinessEntites.SurveyType;

namespace OpinionestWebApi.Controllers
{
    [Microsoft.AspNetCore.Cors.EnableCors("AllowOrigin")]
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class MySurveyController : ControllerBase
    {
        SqlConnection _Conn;
        IConfiguration _iconfiguration;
        IMySurvey _Survey;
        IQuestion _IQuestion;
        ICommon _common;

        public MySurveyController(IConfiguration iconfiguration)
        {
            _iconfiguration = iconfiguration;
            string Connection = _iconfiguration.GetSection("ConnectionStrings").GetSection("DefaultConnection").Value;
            _Conn = new SqlConnection(Connection);
            _Survey = new MySurveyServicesDefination(_Conn);
            _IQuestion = new QuestionServicesDefination(_Conn);
            _common = new CommonServicesDefination(_Conn);
        }

        [HttpGet]
        public IActionResult Index(int pageindex, int pageSize,string statusId,int CategoryId=0)

        {
            var skip = (pageindex - 1) * pageSize;
            var myCollectionList = new BusinessEntites.MySurveyColection();
            bool IsAuth = User.Identity.IsAuthenticated;
            List<MySurveyDataListEntites> modellist = new List<MySurveyDataListEntites>();
            if (IsAuth != false)
            {
                long UserId = Convert.ToInt64(User.Identity.Name);
                bool userActive = _common.ValidUser(UserId);
                if (userActive == false)
                {
                    return Ok(new { Code = 201, Status = false, Message = "Invalid User, Please create new account.", MessageCode = 2 });
                }
                if (ModelState.IsValid)
                {
                    //UserId = 128002;
                    myCollectionList = _Survey.MySurveylist(UserId.ToString(), 1, 1000);
                    modellist = myCollectionList.MySurveyDataListEntites;
                    if(CategoryId>0)
                    {
                        modellist = modellist.Where(x => x.CategoryId == CategoryId).ToList();
                    }
                    //if(statusId>0)
                    //{
                    //    if(statusId==1)
                    //    {
                    //        modellist = modellist.Where(x => x.SurveyStatus == 1).ToList();
                    //    }
                    //    // 2[Description("InCompleted")] InProgress = 2,[Description("InCompleted")] InCompleted = 6
                    //    if (statusId == 2) 
                    //    {
                    //        modellist = modellist.Where(x => x.SurveyStatus == 2 ||  x.SurveyStatus == 6).ToList();
                    //    }
                    //    if (statusId == 3) //Taken
                    //    {
                    //        modellist = modellist.Where(x => x.SurveyStatus == 4 || x.SurveyStatus == 3 || x.SurveyStatus == 5 || x.SurveyStatus == 7).ToList();
                    //    }

                    //}
                    List<BusinessEntites.MySurveyDataListEntites> filterSurvey = new List<MySurveyDataListEntites>();
                    modellist.ForEach(x =>
                    {
                        x.Surveycount = modellist.Where(y => y.SurveyId == x.SurveyId && y.Survey_Type == BusinessEntites.SurveyType.Static).Count();
                    });
                    filterSurvey.AddRange(modellist.Where(x => x.Survey_Type == BusinessEntites.SurveyType.Static

        && ((x.CreatedOn.Value.Date != AppHelper.CurrentDateTime.Date && x.UserSurveyStatus == BusinessEntites.UserSurveyStatus.Release)
        || (x.CreatedOn.Value.Date == AppHelper.CurrentDateTime.Date && x.UserSurveyStatus != BusinessEntites.UserSurveyStatus.Release)
         || (x.CreatedOn.Value.Date != AppHelper.CurrentDateTime.Date && x.UserSurveyStatus != BusinessEntites.UserSurveyStatus.Release)
        || (x.Surveycount == 1)))
            .OrderByDescending(x => x.CreatedOn)
            .GroupBy(x => x.SurveyId)
            .Select(x => x.FirstOrDefault()));
                    filterSurvey.AddRange(modellist.Where(x => x.Survey_Type == SurveyType.Dynamic));
                    filterSurvey = filterSurvey.OrderBy(z => z.UserSurveyStatus).Select(z => z).ToList();
                    int Srno = 1;
                    if (filterSurvey != null)
                    {
                        filterSurvey.ForEach(X =>
                        {
                            X.SNo = Srno;
                            Srno++;
                            if ((X.SurveyStatus == Convert.ToInt32(SurveyStatus.Live) && X.FilterStatus == Convert.ToInt32(FilterStatus.InComplete)) || (X.SurveyStatus == Convert.ToInt32(SurveyStatus.Paused) || X.FilterStatus == Convert.ToInt32(FilterStatus.Pause)))
                            {
                                //X.Color = "Grey";
                                X.SurveyStatusText = X.UserSurveyStatus.EnumDescription();
                                X.SurveyAttedmptedText = "Not Attempted";
                                X.URL = X.UserSurveyStatus == BusinessEntites.UserSurveyStatus.Release ? X.URL : "";
                                X.PointsStatusText = X.UserSurveyStatus == BusinessEntites.UserSurveyStatus.Completed ? Survey_User_Point_Status.Qualified.EnumDescription()
                                : X.UserSurveyStatus == BusinessEntites.UserSurveyStatus.Credited ? Survey_User_Point_Status.Credited.EnumDescription()
                                : (X.UserSurveyStatus == BusinessEntites.UserSurveyStatus.Terminated && X.TerminateRewards == 0) ? Survey_User_Point_Status.NotQualified.EnumDescription()
                                 : (X.UserSurveyStatus == BusinessEntites.UserSurveyStatus.Terminated && X.TerminateRewards != 0) ? Survey_User_Point_Status.Qualified.EnumDescription()
                                 : (X.UserSurveyStatus == BusinessEntites.UserSurveyStatus.QuotaFull && X.QuotaFullRewards == 0) ? Survey_User_Point_Status.NotQualified.EnumDescription()
                                  : (X.UserSurveyStatus == BusinessEntites.UserSurveyStatus.QuotaFull && X.QuotaFullRewards != 0) ? Survey_User_Point_Status.Qualified.EnumDescription()
                                 : Survey_User_Point_Status.Pending.EnumDescription();
                            }
                            else
                            {
                                // X.Color = "Brown";
                                X.SurveyStatusText = "Closed";
                                X.SurveyAttedmptedText = "Attempted";
                                X.URL = "";
                                X.PointsStatusText = X.UserSurveyStatus == BusinessEntites.UserSurveyStatus.Completed ? Survey_User_Point_Status.Qualified.EnumDescription()
                                : X.UserSurveyStatus == BusinessEntites.UserSurveyStatus.Credited ? Survey_User_Point_Status.Credited.EnumDescription()
                                : (X.UserSurveyStatus == BusinessEntites.UserSurveyStatus.Terminated && X.TerminateRewards == 0) ? Survey_User_Point_Status.NotQualified.EnumDescription()
                                 : (X.UserSurveyStatus == BusinessEntites.UserSurveyStatus.Terminated && X.TerminateRewards != 0) ? Survey_User_Point_Status.Qualified.EnumDescription()
                                 : (X.UserSurveyStatus == BusinessEntites.UserSurveyStatus.QuotaFull && X.QuotaFullRewards == 0) ? Survey_User_Point_Status.NotQualified.EnumDescription()
                                  : (X.UserSurveyStatus == BusinessEntites.UserSurveyStatus.QuotaFull && X.QuotaFullRewards != 0) ? Survey_User_Point_Status.Qualified.EnumDescription()
                                   : (X.UserSurveyStatus == BusinessEntites.UserSurveyStatus.Release && X.SurveyStatus == Convert.ToInt32(SurveyStatus.Close)) ? "Not Attempted"
                                   : (X.UserSurveyStatus == BusinessEntites.UserSurveyStatus.InProgress && X.SurveyStatus == Convert.ToInt32(SurveyStatus.Close)) ? "Not Qualified"
                                  : Survey_User_Point_Status.Pending.EnumDescription();
                            }
                        });
                    }
                    foreach (var item in filterSurvey)
                    {
                        if (!string.IsNullOrEmpty(item.URL))
                        {
                            Uri myUri = new Uri(item.URL);
                            string param1 = HttpUtility.ParseQueryString(myUri.Query).Get("tforid");
                            string param2 = HttpUtility.ParseQueryString(myUri.Query).Get("utype");
                            string Log_EncryptedId1 = param1.EncryptID();
                            string Log_EncryptedId2 = param2.EncryptID();
                            item.URL = AppHelper.replaceQueryString(item.URL, "tforid", Log_EncryptedId1);
                            item.URL = AppHelper.replaceQueryString(item.URL, "utype", Log_EncryptedId2);
                            item.URL = item.URL + "&t=" + item.Survey_Type.ToString().EncryptID();
                        }
                    }
                    if(!String.IsNullOrEmpty(statusId))
                    {
                        filterSurvey = filterSurvey.Where(x => x.SurveyStatusText == statusId).ToList();
                    }
                    myCollectionList.MySurveyDataListEntites = filterSurvey.Skip(skip).Take(pageSize).OrderByDescending(x=>x.CreatedOn).ToList();
                    myCollectionList.TotalRecords = filterSurvey.Count();
                    foreach(var v in myCollectionList.MySurveyDataListEntites)
                    {
                        v.ImagePath= "https://api2.opinionest.com/Uplodaed_Documents/Survey.png";
                    }
                    int Count = filterSurvey.Where(x => x.UserSurveyStatus == BusinessEntites.UserSurveyStatus.Release).Count();
                    return Ok(new { Code = 200, Status = true, Message = "Success", Data = myCollectionList ,TotolOpen= Count });

                }

                else
                {
                    return Ok(new { Code = 200, Message = "Something Went Wrong", MessageCode = 2 });
                }
            }
            else
            {
                return Ok(new { Code = 200, Message = "Token is Invalid", MessageCode = 2 });
            }
        }
        [HttpGet, Route("StaticBannerSurvey")]
        public IActionResult StaticBanner()
        {
            var myCollectionList = new BusinessEntites.MySurveyColection();
            bool IsAuth = User.Identity.IsAuthenticated;
            List<MySurveyDataListEntites> modellist = new List<MySurveyDataListEntites>();
            if (IsAuth != false)
            {
                long UserId = Convert.ToInt64(User.Identity.Name);
                bool userActive = _common.ValidUser(UserId);
                if (userActive == false)
                {
                    return Ok(new { Code = 201, Status = false, Message = "Invalid User, Please create new account.", MessageCode = 2 });
                }
                if (ModelState.IsValid)
                {
                    myCollectionList = _Survey.BanneSurvey(UserId.ToString());
                    modellist = myCollectionList.MySurveyDataListEntites;
                    List<BusinessEntites.MySurveyDataListEntites> filterSurvey = new List<MySurveyDataListEntites>();
                    modellist.ForEach(x =>
                    {
                        x.Surveycount = modellist.Where(y => y.SurveyId == x.SurveyId && y.Survey_Type == BusinessEntites.SurveyType.Static).Count();
                    });
                    filterSurvey.AddRange(modellist.Where(x => x.Survey_Type == BusinessEntites.SurveyType.Static

        && ((x.CreatedOn.Value.Date != AppHelper.CurrentDateTime.Date && x.UserSurveyStatus == BusinessEntites.UserSurveyStatus.Release)
        || (x.CreatedOn.Value.Date == AppHelper.CurrentDateTime.Date && x.UserSurveyStatus != BusinessEntites.UserSurveyStatus.Release)
         || (x.CreatedOn.Value.Date != AppHelper.CurrentDateTime.Date && x.UserSurveyStatus != BusinessEntites.UserSurveyStatus.Release)
        || (x.Surveycount == 1)))
            .OrderByDescending(x => x.CreatedOn)
            .GroupBy(x => x.SurveyId)
            .Select(x => x.FirstOrDefault()));
                    filterSurvey.AddRange(modellist.Where(x => x.Survey_Type == SurveyType.Dynamic));
                    filterSurvey = filterSurvey.OrderBy(z => z.UserSurveyStatus).Select(z => z).ToList();
                    int Srno = 1;
                    if (filterSurvey != null)
                    {
                        filterSurvey.ForEach(X =>
                        {
                            X.SNo = Srno;
                            Srno++;
                            if ((X.SurveyStatus == Convert.ToInt32(SurveyStatus.Live) && X.FilterStatus == Convert.ToInt32(FilterStatus.InComplete)) || (X.SurveyStatus == Convert.ToInt32(SurveyStatus.Paused) || X.FilterStatus == Convert.ToInt32(FilterStatus.Pause)))
                            {
                                //X.Color = "Grey";
                                X.SurveyStatusText = X.UserSurveyStatus.EnumDescription();
                                X.URL = X.UserSurveyStatus == BusinessEntites.UserSurveyStatus.Release ? X.URL : "";
                                X.PointsStatusText = X.UserSurveyStatus == BusinessEntites.UserSurveyStatus.Completed ? Survey_User_Point_Status.Qualified.EnumDescription()
                                : X.UserSurveyStatus == BusinessEntites.UserSurveyStatus.Credited ? Survey_User_Point_Status.Credited.EnumDescription()
                                : (X.UserSurveyStatus == BusinessEntites.UserSurveyStatus.Terminated && X.TerminateRewards == 0) ? Survey_User_Point_Status.NotQualified.EnumDescription()
                                 : (X.UserSurveyStatus == BusinessEntites.UserSurveyStatus.Terminated && X.TerminateRewards != 0) ? Survey_User_Point_Status.Qualified.EnumDescription()
                                 : (X.UserSurveyStatus == BusinessEntites.UserSurveyStatus.QuotaFull && X.QuotaFullRewards == 0) ? Survey_User_Point_Status.NotQualified.EnumDescription()
                                  : (X.UserSurveyStatus == BusinessEntites.UserSurveyStatus.QuotaFull && X.QuotaFullRewards != 0) ? Survey_User_Point_Status.Qualified.EnumDescription()
                                 : Survey_User_Point_Status.Pending.EnumDescription();
                            }
                            else
                            {
                                // X.Color = "Brown";
                                X.SurveyStatusText = "Closed";
                                X.URL = "";
                                X.PointsStatusText = X.UserSurveyStatus == BusinessEntites.UserSurveyStatus.Completed ? Survey_User_Point_Status.Qualified.EnumDescription()
                                : X.UserSurveyStatus == BusinessEntites.UserSurveyStatus.Credited ? Survey_User_Point_Status.Credited.EnumDescription()
                                : (X.UserSurveyStatus == BusinessEntites.UserSurveyStatus.Terminated && X.TerminateRewards == 0) ? Survey_User_Point_Status.NotQualified.EnumDescription()
                                 : (X.UserSurveyStatus == BusinessEntites.UserSurveyStatus.Terminated && X.TerminateRewards != 0) ? Survey_User_Point_Status.Qualified.EnumDescription()
                                 : (X.UserSurveyStatus == BusinessEntites.UserSurveyStatus.QuotaFull && X.QuotaFullRewards == 0) ? Survey_User_Point_Status.NotQualified.EnumDescription()
                                  : (X.UserSurveyStatus == BusinessEntites.UserSurveyStatus.QuotaFull && X.QuotaFullRewards != 0) ? Survey_User_Point_Status.Qualified.EnumDescription()
                                   : (X.UserSurveyStatus == BusinessEntites.UserSurveyStatus.Release && X.SurveyStatus == Convert.ToInt32(SurveyStatus.Close)) ? "Not Attempted"
                                   : (X.UserSurveyStatus == BusinessEntites.UserSurveyStatus.InProgress && X.SurveyStatus == Convert.ToInt32(SurveyStatus.Close)) ? "Not Qualified"
                                  : Survey_User_Point_Status.Pending.EnumDescription();
                            }
                        });
                    }
                    foreach (var item in filterSurvey)
                    {
                        if (!string.IsNullOrEmpty(item.URL))
                        {
                            Uri myUri = new Uri(item.URL);
                            string param1 = HttpUtility.ParseQueryString(myUri.Query).Get("tforid");
                            string param2 = HttpUtility.ParseQueryString(myUri.Query).Get("utype");
                            string Log_EncryptedId1 = param1.EncryptID();
                            string Log_EncryptedId2 = param2.EncryptID();
                            item.URL = AppHelper.replaceQueryString(item.URL, "tforid", Log_EncryptedId1);
                            item.URL = AppHelper.replaceQueryString(item.URL, "utype", Log_EncryptedId2);
                            item.URL = item.URL + "&t=" + item.Survey_Type.ToString().EncryptID();
                        }
                    }
                    myCollectionList.MySurveyDataListEntites = filterSurvey;
                    int Count = filterSurvey.Where(x => x.UserSurveyStatus == BusinessEntites.UserSurveyStatus.Release).Count();
                    return Ok(new { Code = 200, Status = true, Message = "Success", Data = myCollectionList, TotolOpen = Count });

                }

                else
                {
                    return Ok(new { Code = 200, Message = "Something Went Wrong", MessageCode = 2 });
                }
            }
            else
            {
                return Ok(new { Code = 200, Message = "Token is Invalid", MessageCode = 2 });
            }
        }
        [HttpGet, Route("surveyStatus")]
        public IActionResult surveyStatus()
        {
            List<SurveyStatusEntites> list = new List<SurveyStatusEntites>();
            list.Add(new SurveyStatusEntites{id= "Pending", Name= "Pending" });
            list.Add(new SurveyStatusEntites { id = "InCompleted", Name = "InCompleted" });
            list.Add(new SurveyStatusEntites { id = "Taken", Name = "Taken" });
                list.Add(new SurveyStatusEntites { id = "Closed", Name = "Closed" });
            return Ok(new { Code = 200, Status = true, Message = "Success", Data = list});
        }
        [HttpGet, Route("QuestionCategory")]
        public IActionResult QuestionCategory()
        {
            var Result = _IQuestion.ProfileQuestionCategory(21);
            var list = (from res in Result
                        select new
                        {
                            id = res.categoryId,
                            Name = res.categoryName
                        }).ToList();

            return Ok(new { Code = 200, Status = true, Message = "Success", Data = list });
        }

    }
}