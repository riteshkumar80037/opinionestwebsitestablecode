﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using BusinessAccessLayer.InterfaceDeclaration;
using BusinessAccessLayer.ServiceDefination;
using BusinessEntites;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace OpinionestWebApi.Controllers
{
    [Microsoft.AspNetCore.Cors.EnableCors("AllowOrigin")]
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class QuestionAnswerController : Controller
    {
        IQuestion _IQuestion;
        SqlConnection _Conn;
        IConfiguration _iconfiguration;
        ICommon _common;
        public const int RecordsPerPage = 15;
        public QuestionAnswerController(IConfiguration iconfiguration)
        {
            _iconfiguration = iconfiguration;
            string Connection = _iconfiguration.GetSection("ConnectionStrings").GetSection("DefaultConnection").Value;
            _Conn = new SqlConnection(Connection);
            _IQuestion = new QuestionServicesDefination(_Conn);
            _common = new CommonServicesDefination(_Conn);
        }
        [HttpPost, Route("SaveQuestionAnswer")]
        public IActionResult Index(OptionalRequestEntites obj)
        {
            bool IsAuth = User.Identity.IsAuthenticated;
            if (IsAuth != false)
            {
                long UserId = Convert.ToInt64(User.Identity.Name);
                bool userActive = _common.ValidUser(UserId);
                if (userActive == false)
                {
                    return Ok(new { Code = 201, Status = false, Message = "Invalid User, Please create new account.", MessageCode = 2 });
                }
                if(String.IsNullOrEmpty(obj.ProfileAnswerId))
                {
                    return Ok(new { Code = 201, Status = false, Message = "Please select the atleast one option.", MessageCode = 2 });
                }
                if (!String.IsNullOrEmpty(obj.ProfileAnswerId))
                {
                    
                    var Result = _IQuestion.SaveAnswer(obj, UserId, obj.Mandatory);
                    if (Result>0)
                    {
                        return Ok(new { Code = 200, Status = true, Message = Result + " Points Added in your Account ", MessageCode = 1 });
                    }
                    else if(Result==-2)
                    {
                        return Ok(new { Code = 201, Status = false, Message = "Question already attended.", MessageCode = 2 });
                    }
                    else if (Result == -3)
                    {
                        return Ok(new { Code = 201, Status = false, Message = "Same answer already exists.", MessageCode = 2 });
                    }
                    else
                    {
                        return Ok(new { Code = 201, Status = false, Message = "Something wrong please Try again.", MessageCode = 2 });
                    }
                }
                else
                {
                    return Ok(new { Code = 201, Status = false, Message = "Please Choose atleast one answer", MessageCode = 2 });
                }
            }
            else
            {
                return Ok(new { Code = 200, Message = "Token is Invalid", MessageCode = 2 });
            }

        }
        [HttpGet, Route("GetQuestion")]
        public IActionResult Mandatory(bool MandatoryType)
        {
            OptionalRequestEntites obj = new OptionalRequestEntites();
            QuestionResponse Result = new QuestionResponse();
            bool IsAuth = User.Identity.IsAuthenticated;
            if (IsAuth != false)
            {
                long UserId = Convert.ToInt64(User.Identity.Name);
                bool userActive = _common.ValidUser(UserId);
                if (userActive == false)
                {
                    return Ok(new { Code = 201, Status = false, Message = "Invalid User, Please create new account.", MessageCode = 2 });
                }

                Result = _IQuestion.OptionalQuestionGet(obj, UserId, MandatoryType);
                
                if (Result != null)
                {
                    return Ok(new { Code = 200, Status = true, Message = "Data Fetch Successfully !", Data = Result, MessageCode = 1 });
                }
                else
                {
                    return Ok(new { Code = 201, Status = false, Message = "Something wrong please Try again.", MessageCode = 2 });
                }
            }
            else
            {
                return Ok(new { Code = 200, Message = "Token is Invalid", MessageCode = 2 });
            }

        }
        [HttpGet, Route("GetCategoryQuestion")]
        public IActionResult GetCategoryQuestion(long categoryId)
        {
            OptionalRequestEntites obj = new OptionalRequestEntites();
            obj.GPCID = categoryId;
            QuestionResponse Result = new QuestionResponse();
            bool IsAuth = User.Identity.IsAuthenticated;
            if (IsAuth != false)
            {
                long UserId = Convert.ToInt64(User.Identity.Name);
                bool userActive = _common.ValidUser(UserId);
                if (userActive == false)
                {
                    return Ok(new { Code = 201, Status = false, Message = "Invalid User, Please create new account.", MessageCode = 2 });
                }

                Result = _IQuestion.OptionalQuestionGet(obj, UserId, false);

                if (Result != null)
                {
                    return Ok(new { Code = 200, Status = true, Message = "Data Fetch Successfully !", Data = Result, MessageCode = 1 });
                }
                else
                {
                    return Ok(new { Code = 201, Status = false, Message = "Something wrong please Try again.", MessageCode = 2 });
                }
            }
            else
            {
                return Ok(new { Code = 200, Message = "Token is Invalid", MessageCode = 2 });
            }

        }
        [HttpGet, Route("ProfilequestionsOptional")]
        public IActionResult ProfilequestionsOptional(int? pageindex)
        {
            List<ProfileOptionalQuestionResponse> Result = new List<ProfileOptionalQuestionResponse>();
            bool IsAuth = User.Identity.IsAuthenticated;
            if (IsAuth != false)
            {
                long UserId = Convert.ToInt64(User.Identity.Name);
                bool userActive = _common.ValidUser(UserId);
                if (userActive == false)
                {
                    return Ok(new { Code = 201, Status = false, Message = "Invalid User, Please create new account.", MessageCode = 2 });
                }
                Result = _IQuestion.ProfileOptionalQuestionGet(UserId);
                var list = Result.Skip((pageindex-1 ?? 0) * RecordsPerPage).Take(RecordsPerPage).ToList();

                if (Result != null)
                {
                    return Ok(new { Code = 200, Status = true, Message = "Data Fetch Successfully !", Data = list, MessageCode = 1,TotalRecords = Result.Count() });
                }
                else
                {
                    return Ok(new { Code = 201, Status = false, Message = "Something wrong please Try again.", MessageCode = 2 });
                }
            }
            else
            {
                return Ok(new { Code = 200, Message = "Token is Invalid", MessageCode = 2 });
            }

        }
        [HttpGet, Route("ProfileQuestionDetail")]
        public IActionResult ProfileQuestionDetail()
        {
             bool IsAuth = User.Identity.IsAuthenticated;
            if (IsAuth != false)
            {
                long UserId = Convert.ToInt64(User.Identity.Name);
                bool userActive = _common.ValidUser(UserId);
                if (userActive == false)
                {
                    return Ok(new { Code = 201, Status = false, Message = "Invalid User, Please create new account.", MessageCode = 2 });
                }
                var Result = _IQuestion.ProfileQuestionCount(UserId);
               
                if (Result != null)
                {
                    return Ok(new { Code = 200, Status = true, Message = "Data Fetch Successfully !", Data = Result, MessageCode = 1 });
                }
                else
                {
                    return Ok(new { Code = 201, Status = false, Message = "Something wrong please Try again.", MessageCode = 2 });
                }
            }
            else
            {
                return Ok(new { Code = 200, Message = "Token is Invalid", MessageCode = 2 });
            }

        }
        [HttpGet, Route("OptionalQuestionCategory")]
        public IActionResult OptionalQuestionCategory(int? pageIndex, int pageSize)
        {
            bool IsAuth = User.Identity.IsAuthenticated;
            if (IsAuth != false)
            {
                long UserId = Convert.ToInt64(User.Identity.Name);
                bool userActive = _common.ValidUser(UserId);
                if (userActive == false)
                {
                    return Ok(new { Code = 201, Status = false, Message = "Invalid User, Please create new account.", MessageCode = 2 });
                }
                var Result = _IQuestion.ProfileQuestionCategory(UserId) ;
                var Total = Result.Count();
                Result= Result.Skip((pageIndex - 1 ?? 0) * pageSize).Take(pageSize).ToList();
                if (Result != null)
                {
                    return Ok(new { Code = 200, Status = true, Message = "Data Fetch Successfully !", Data = Result,total= Total, MessageCode = 1 });
                }
                else
                {
                    return Ok(new { Code = 201, Status = false, Message = "Something wrong please Try again.", MessageCode = 2 });
                }
            }
            else
            {
                return Ok(new { Code = 200, Message = "Token is Invalid", MessageCode = 2 });
            }

        }
        [HttpGet, Route("OptionalQuestionTotal")]
        public IActionResult OptionalQuestionTotal()
        {
            bool IsAuth = User.Identity.IsAuthenticated;
            if (IsAuth != false)
            {
                long UserId = Convert.ToInt64(User.Identity.Name);
                bool userActive = _common.ValidUser(UserId);
                if (userActive == false)
                {
                    return Ok(new { Code = 201, Status = false, Message = "Invalid User, Please create new account.", MessageCode = 2 });
                }
                var Result = _IQuestion.OptionalTotal(UserId);
                
                if (Result != null)
                {
                    return Ok(new { Code = 200, Status = true, Message = "Data Fetch Successfully !", Data = Result, MessageCode = 1 });
                }
                else
                {
                    return Ok(new { Code = 201, Status = false, Message = "Something wrong please Try again.", MessageCode = 2 });
                }
            }
            else
            {
                return Ok(new { Code = 200, Message = "Token is Invalid", MessageCode = 2 });
            }

        }
        [HttpGet, Route("MandatoryQuestionTotal")]
        public IActionResult MandatoryQuestionTotal()
        {
            bool IsAuth = User.Identity.IsAuthenticated;
            if (IsAuth != false)
            {
                long UserId = Convert.ToInt64(User.Identity.Name);
                bool userActive = _common.ValidUser(UserId);
                if (userActive == false)
                {
                    return Ok(new { Code = 201, Status = false, Message = "Invalid User, Please create new account.", MessageCode = 2 });
                }
                var Result = _IQuestion.MandatoryTotal(UserId);

                if (Result != null)
                {
                    return Ok(new { Code = 200, Status = true, Message = "Data Fetch Successfully !", Data = Result, MessageCode = 1 });
                }
                else
                {
                    return Ok(new { Code = 201, Status = false, Message = "Something wrong please Try again.", MessageCode = 2 });
                }
            }
            else
            {
                return Ok(new { Code = 200, Message = "Token is Invalid", MessageCode = 2 });
            }

        }

    }
}