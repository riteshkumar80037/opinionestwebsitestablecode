﻿using BusinessAccessLayer.InterfaceDeclaration;
using BusinessAccessLayer.ServiceDefination;
using BusinessEntites;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;

namespace OpinionestWebApi.Controllers
{
    [Microsoft.AspNetCore.Cors.EnableCors("AllowOrigin")]
    [Route("api/[controller]")]
    [ApiController]
    public class HomeController : ControllerBase
    {
        SqlConnection _Conn;
        IConfiguration _iconfiguration;
        IHome _Home;
        ILogin _Login;
        IOffer _Offer;
        ICommon _common;

        public HomeController(IConfiguration iconfiguration)
        {
            _iconfiguration = iconfiguration;
            string Connection = _iconfiguration.GetSection("ConnectionStrings").GetSection("DefaultConnection").Value;
            _Conn = new SqlConnection(Connection);
            _Home = new HomeServicesDefination(_Conn);
            _Login = new LoginServicesDefination(_Conn);
            _Offer = new OfferServiceDefination(_Conn);
            _common = new CommonServicesDefination(_Conn);
        }
        [HttpGet, Route("BlogList")]
        public IActionResult GetBlogList(int pageindex, int pagesize)
        {
            var myList = new List<BusinessEntites.BlogEntites>();
            try
            {
                myList = _Home.Bloglist(pageindex, pagesize);
                if (myList.Count >= 1)
                {
                    return Ok(new { Code = 200, Status = true, Message = "Success", Data = myList });
                }
                else
                {
                    return Ok(new { Code = 401, Status = false, Message = "Something wrong please check again.", Data = "" });
                }

            }
            catch (Exception e)
            {
                return Ok(new { Code = 401, Status = false, Message = e.Message, Data = "" });
            }

        }
        [HttpPut, Route("BlogDetails")]
        public IActionResult GetBlogDetails(int blogId)
        {
            var myList = new List<BusinessEntites.BlogEntites>();
            try
            {
                myList = _Home.BlogDetails(blogId);
                if (myList.Count >= 1)
                {
                    return Ok(new { Code = 200, Status = true, Message = "Success", Data = myList });
                }
                else
                {
                    return Ok(new { Code = 401, Status = false, Message = "Something wrong please check again.", Data = "" });
                }

            }
            catch (Exception e)
            {
                return Ok(new { Code = 401, Status = false, Message = e.Message, Data = "" });
            }

        }
        [HttpGet, Route("BlogDetail")]
        public IActionResult BlogDetail(string header)
        {

            try
            {
                //var Data = _Home.BlogDetails(Id);
                var Data = _Home.BlogDetails(header);
                if (Data != null)
                {
                    return Ok(new { Code = 200, Status = true, Message = "Success", Data = Data, MessageCode = 1 });
                }
                else
                {
                    return Ok(new { Code = 401, Status = false, Message = "Something wrong please check again.", MessageCode = 1 });
                }

            }
            catch (Exception e)
            {
                return Ok(new { Code = 401, Status = false, Message = e.Message, Data = "" });
            }

        }
        [HttpGet, Route("DailyFeeds")]
        [Authorize]
        public IActionResult DailyFeeds(int pageIndex, int pageSize,int? Categoryid=0,bool? IsStart=false,string text="")
        {
            bool IsAuth = User.Identity.IsAuthenticated;
            var myList = new List<BusinessEntites.DailyFeedModelList>();
            try
            {

                if (IsAuth != false)
                {
                    long UserId = Convert.ToInt64(User.Identity.Name);
                    bool userActive = _common.ValidUser(UserId);
                    if (userActive == false)
                    {
                        return Ok(new { Code = 201, Status = false, Message = "Invalid User, Please create new account.", MessageCode = 2 });
                    }
                    int Pagesize = 10;
                    var Data = _Home.DailyFeedlist(UserId, pageIndex, Pagesize, Categoryid, IsStart, text);
                    myList = Data.Item1;
                    int total = Data.Item2;
                    int toatlPoint = Data.Item3;
                    if (myList.Count >= 1)
                    {
                        return Ok(new { Code = 200, Message = "Success", Data = myList, totalCount = total, toatlPoints = toatlPoint });
                    }

                    else
                    {
                        return Ok(new { Code = 201, Message = "Not records found", Data = "" });
                    }
                }
                else
                {
                    return Ok(new { Code = 201, Message = "Token is Invalid", MessageCode = 2 });
                }

            }
            catch (Exception e)
            {
                return Ok(new { Code = 401, Status = false, Message = e.Message, Data = "" });
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="sliderType"></param>
        /// <returns></returns>
        [HttpGet, Route("Slider")]
        [Authorize]
        public IActionResult Slider(int sliderType)
        {
            bool IsAuth = User.Identity.IsAuthenticated;
            var _sliders = new List<BusinessEntites.Slider>();
            try
            {

                if (IsAuth != false)
                {
                    _sliders = _Home.SliderList(sliderType);
                    if (_sliders.Count >= 1)
                    {
                        return Ok(new { Code = 200, Message = "Success", Data = _sliders });
                    }

                    else
                    {
                        return Ok(new { Code = 201, Message = "No records found", Data = "" });
                    }
                }
                else
                {
                    return Ok(new { Code = 201, Message = "Token is Invalid", MessageCode = 2 });
                }

            }
            catch (Exception e)
            {
                return Ok(new { Code = 401, Status = false, Message = e.Message, Data = "" });
            }
        }

        /// <summary>
        /// DailyFeedsDetails
        /// </summary>
        /// <param name="feedId"></param>
        /// <returns></returns>
        [HttpGet, Route("DailyFeedsDetails/{feedId}")]
        [Authorize]
        public IActionResult DailyFeedsDetails(int feedId)
        {
            bool IsAuth = User.Identity.IsAuthenticated;
            var myList = new BusinessEntites.DailyFeedDetails();
            try
            {
                if (IsAuth != false)
                {

                    long UserId = Convert.ToInt64(User.Identity.Name);
                    bool userActive = _common.ValidUser(UserId);
                    if (userActive == false)
                    {
                        return Ok(new { Code = 201, Status = false, Message = "Invalid User, Please create new account.", MessageCode = 2 });
                    }
                    myList = _Home.DailyFeedDetails(feedId);
                    return myList != null
                        ? Ok(new { Code = 200, Message = "Success", Data = myList })
                        : Ok(new { Code = 201, Message = "Not records found", Data = "" });
                }
                else
                {
                    return Ok(new { Code = 201, Message = "Token is Invalid", MessageCode = 2 });
                }

            }
            catch (Exception e)
            {
                return Ok(new { Code = 401, Status = false, Message = e.Message, Data = "" });
            }
        }

        /// <summary>
        /// Referral Details
        /// </summary>
        /// <param name="feedId"></param>
        /// <returns></returns>
        [HttpGet, Route("ReferralList")]
        [Authorize]
        public IActionResult ReferralList(int pageNo, int pageSize)
        {
            bool IsAuth = User.Identity.IsAuthenticated;
            var myList = new List<BusinessEntites.ReferralDetailList>();
            try
            {
                if (IsAuth != false)
                {
                    long UserId = Convert.ToInt64(User.Identity.Name);
                    bool userActive = _common.ValidUser(UserId);
                    if (userActive == false)
                    {
                        return Ok(new { Code = 201, Status = false, Message = "Invalid User, Please create new account.", MessageCode = 2 });
                    }
                    myList = _Home.ReferralList(UserId, pageNo, pageSize);
                    var TotalPoint = myList.Sum(x => x.Points);
                    var count = myList.Count();

                    if (myList.Count >= 1)
                    {
                        return Ok(new { Code = 200, Message = "Success", Data = myList,totalPoint= TotalPoint,totalCount= count });
                    }

                    else
                    {
                        return Ok(new { Code = 201, Message = "No records found", Data = "" });
                    }
                }
                else
                {
                    return Ok(new { Code = 201, Message = "Token is Invalid", MessageCode = 2 });
                }

            }
            catch (Exception e)
            {
                return Ok(new { Code = 401, Status = false, Message = e.Message, Data = "" });
            }
        }
        [HttpGet, Route("GetHomePoints")]
        public IActionResult GetPoints()
        {


            try
            {
                var TotalPoints = _Home.HomePageTotalPoints();

                if (TotalPoints >= 0)
                {
                    return Ok(new { Code = 200, Message = "Success", Data = TotalPoints, MessageCode = 1 });
                }

                else
                {
                    return Ok(new { Code = 201, Message = "No records found", Data = "", MessageCode = 2 });
                }


            }
            catch (Exception e)
            {
                return Ok(new { Code = 401, Status = false, Message = e.Message, Data = "" });
            }
        }

        [HttpPost, Route("ContactFormSubmission")]
        public IActionResult ContactFormSubmission(ContactUsEntities model)
        {
            try
            {
                string SendGridKey = _iconfiguration.GetSection("SendGridKey").Value;
                var result = _Home.ContactFormSubmission(model, SendGridKey);
                if (result != "Error")
                {
                    return Ok(new { Code = 200, Status = true, Message = "Your Query Submitted Successfully , We will contact you soon", Data = result });
                }
                else
                {
                    return Ok(new { Code = 401, Status = false, Message = "Something wrong please check again.", Data = "" });
                }

            }
            catch (Exception e)
            {
                return Ok(new { Code = 401, Status = false, Message = e.Message, Data = "" });
            }

        }

        [HttpGet, Route("CustomerReviews")]
        public IActionResult CustomerReviews(int pageindex, int pagesize)
        {
            var myList = new List<BusinessEntites.CustomerReview>();
            try
            {
                myList = _Home.ReviewList(pageindex, pagesize);
                if (myList.Count >= 1)
                {
                    return Ok(new { Code = 200, Status = true, Message = "Success", Data = myList });
                }
                else
                {
                    return Ok(new { Code = 401, Status = false, Message = "Something wrong please check again.", Data = "" });
                }

            }
            catch (Exception e)
            {
                return Ok(new { Code = 401, Status = false, Message = e.Message, Data = "" });
            }

        }
        [HttpGet, Route("SMSAppLink")]
        public IActionResult SMSAppLink(string PhoneNo)
        {
            var myList = new List<BusinessEntites.CustomerReview>();
            try
            {
                //const string accountSid = "AC9ee3b4a998abd7c251db92a39d94688e";
                //const string authToken = "0c87dc54c61f44c1a7b5fdef8338863f";
                //TwilioClient.Init(accountSid, authToken);
                //List<Uri> _uri = new List<Uri>();
                //Uri _first = new Uri("https://climacons.herokuapp.com/clear.png");
                //_uri.Add(_first);
                //var to = new PhoneNumber(PhoneNo);
                //var message = MessageResource.Create(
                //  to,
                //  from: new PhoneNumber("+91-9812107106"),
                //  body: "https://play.google.com/store/apps/details?id=com.opinionestmobile.opinionestmobile",
                //  mediaUrl: _uri);
                //Console.WriteLine(message.Sid);
                if (1==1)
                {
                    return Ok(new { Code = 200, Status = true, Message = "We have stored your information once we have app ready we will send it to you", Data = myList });
                }
                else
                {
                    return Ok(new { Code = 401, Status = false, Message = "Something wrong please check again.", Data = "" });
                }

            }
            catch (Exception e)
            {
                return Ok(new { Code = 401, Status = false, Message = e.Message, Data = "" });
            }

        }
        [HttpPost, Route("EmailAppLink")]
        public IActionResult EmailAppLink(SendEmailLink email)
        {
            var myList = new List<BusinessEntites.CustomerReview>();
            try
            {
                string SendGridKey = _iconfiguration.GetSection("SendGridKey").Value;
                int Resutl = _Login.SendAppLine(SendGridKey, email.EmailId);
                if (Resutl > 0)
                {
                    return Ok(new { Code = 200, Status = true, Message = "Link send to user email ,Please check", Data = myList });
                }
                else
                {
                    return Ok(new { Code = 401, Status = false, Message = "Something wrong please check again.", Data = "" });
                }

            }
            catch (Exception e)
            {
                return Ok(new { Code = 401, Status = false, Message = e.Message, Data = "" });
            }

        }



        /// <summary>
        /// GetPointsValue
        /// </summary>
        /// <param name="paymentGatewayId"></param>
        /// <returns></returns>
        [HttpGet, Route("TotalCashback")]
        public IActionResult TotalCashback()
        {

            long userId = 0;

            var pointValue = _Offer.TotalCashback(userId);

            if (pointValue >= 0)
            {
                return Ok(new { Code = 200, Status = true, Message = "Success", Data = pointValue });
            }
            else
            {
                return Ok(new { Code = 200, Status = false, Message = "No records are found.", Data = "" });
            }



        }
        [HttpGet, Route("mobileFeature")]
        public IActionResult mobileFeature(string code)
        {
            var data = _Home.mobilefeatures(code);
            return Ok(new { Code = 200, Status = true, Message = "Success", Data = data });

        }
        
    }
}