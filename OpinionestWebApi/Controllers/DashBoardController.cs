﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using BusinessAccessLayer.InterfaceDeclaration;
using BusinessAccessLayer.ServiceDefination;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace OpinionestWebApi.Controllers
{
    [Microsoft.AspNetCore.Cors.EnableCors("AllowOrigin")]
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class DashBoardController : Controller
    {
        IDashboard _IDashBoard;
        SqlConnection _Conn;
        IConfiguration _iconfiguration;
        ICommon _common;
        public DashBoardController(IConfiguration iconfiguration)
        {
            _iconfiguration = iconfiguration;
            string Connection = _iconfiguration.GetSection("ConnectionStrings").GetSection("DefaultConnection").Value;
            _Conn = new SqlConnection(Connection);
            _IDashBoard = new DashBoardServicesDefination(_Conn);
            _common = new CommonServicesDefination(_Conn);
        }
        [HttpGet, Route("DashBoard")]
        public IActionResult Index()
        {
            bool IsAuth = User.Identity.IsAuthenticated;
            if (IsAuth != false)
            {
                long UserId = Convert.ToInt64(User.Identity.Name);
                bool userActive = _common.ValidUser(UserId);
                if(userActive==false)
                {
                    return Ok(new { Code = 201, Status = false, Message = "Invalid User, Please create new account.", MessageCode = 2 });
                }
                var Result = _IDashBoard.GetDashboard(UserId);
                if (Result != null)
                {
                    return Ok(new { Code = 200, Status = true, Message = "Data Fetch Successfully !", Data = Result, MessageCode = 1 });
                }
                else
                {
                    return Ok(new { Code = 201, Status = false, Message = "Something wrong please Try again.", MessageCode = 2 });
                }
            }
            else
            {
                return Ok(new { Code = 200, Message = "Token is Invalid", MessageCode = 2 });
            }
        }

    }
}