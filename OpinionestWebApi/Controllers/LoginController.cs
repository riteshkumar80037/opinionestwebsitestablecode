﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using BusinessAccessLayer.InterfaceDeclaration;
using BusinessAccessLayer.ServiceDefination;
using BusinessEntites;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace OpinionestWebApi.Controllers
{
    [Microsoft.AspNetCore.Cors.EnableCors("AllowOrigin")]
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : Controller
    {
        ILogin _Login;
        SqlConnection _Conn;
        IConfiguration _iconfiguration;
        public LoginController(IConfiguration iconfiguration)
        {
            _iconfiguration = iconfiguration;
            string Connection = _iconfiguration.GetSection("ConnectionStrings").GetSection("DefaultConnection").Value;    
            _Conn = new SqlConnection(Connection);
            _Login = new LoginServicesDefination(_Conn);
        }
        [HttpPost]
        public IActionResult Index(UseLoginEntites _Log)
        {
            LoginEntites Obj = new LoginEntites();
            bool popup = false;
            Obj = _Login.Login(_Log,out popup);
            if(Obj.UserId>0)
            {
                return Ok(new { Code = 200, Data = Obj,LoginPopUp= popup, Message = "Logged in Successfully !",MessageCode=1 });
            }
            else
            {
                long userId = _Login.LoginEmail(_Log);
                if(userId==1)
                {
                    return Ok(new { Code = 201, Message = "The email address you entered isn't connected to an account.Create a new Opinionest account", MessageCode = 2 });
                    
                }
                else if (userId> 1)
                {
                    return Ok(new { Code = 201, Message = "The Password that you've entered is incorrect. Enter correct password or try forget password to reset your password.", MessageCode = 2 });

                }
                return Ok(new { Code = 201,  Message = "Your account is not verified .", MessageCode = 2 });
            }
            
        }
    }
}