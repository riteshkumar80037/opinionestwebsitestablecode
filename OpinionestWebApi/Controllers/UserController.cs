﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using BusinessAccessLayer.InterfaceDeclaration;
using BusinessAccessLayer.ServiceDefination;
using BusinessEntites;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace OpinionestWebApi.Controllers
{
    [Microsoft.AspNetCore.Cors.EnableCors("AllowOrigin")]
    [Route("api/[controller]")]
    [ApiController]
   // [Authorize]
    public class UserController : Controller
    {
        Iuser _IUser;
        SqlConnection _Conn;
        IConfiguration _iconfiguration;
        ICommon _common;
        public UserController(IConfiguration iconfiguration)
        {
            _iconfiguration = iconfiguration;
            string Connection = _iconfiguration.GetSection("ConnectionStrings").GetSection("DefaultConnection").Value;
            _Conn = new SqlConnection(Connection);
            _IUser = new UserServicesDefination(_Conn);
            _common = new CommonServicesDefination(_Conn);
        }

        [HttpPost, Route("editprofile")]
        public IActionResult editprofile(EditUserEntites _User)
        {
            bool IsAuth = User.Identity.IsAuthenticated;
            if(IsAuth!=false)
            {
                long UserId = Convert.ToInt64(User.Identity.Name);
                bool userActive = _common.ValidUser(UserId);
                if (userActive == false)
                {
                    return Ok(new { Code = 201, Status = false, Message = "Invalid User, Please create new account.", MessageCode = 2 });
                }
                _User.UserId = UserId;
               
                    if (String.IsNullOrEmpty(_User.Pincode))
                    {
                        return Ok(new { Code = 201, Message = "Pincode is Requried", MessageCode = 2 });
                    }                                     
                    if (_User.Gender <= 0 || _User.Gender == null)
                    {
                        return Ok(new { Code = 201, Message = "Gender is Requried", MessageCode = 2 });
                    }
                    if (_User.PhoneNumber.Length<10)
                    {
                        return Ok(new { Code = 201, Message = "Mobile Number should  be minimum then 10 digit", MessageCode = 2 });
                    }
                if (_User.PhoneNumber.Length > 15)
                {
                    return Ok(new { Code = 201, Message = "Mobile Number can not be greater then 15 digit", MessageCode = 2 });
                }
                if (_User.Pincode.Length > 10)
                    {
                        return Ok(new { Code = 201, Message = "Pin code Can not greater then 6 digit", MessageCode = 2 });
                    }
                    if (_User.DateOfBirth == null)
                    {
                        return Ok(new { Code = 201, Message = "DOB is Requried", MessageCode = 2 });
                    }
                    if (String.IsNullOrEmpty(_User.FirstName))
                    {
                        return Ok(new { Code = 201, Message = "First Name is Requried", MessageCode = 2 });
                    }
                    if (String.IsNullOrEmpty(_User.LastName))
                    {
                        return Ok(new { Code = 201, Message = "Last Name is Requried", MessageCode = 2 });
                    }
                    else
                    {
                        _User.FullName = _User.FirstName + " " + _User.LastName;
                        var Result = _IUser.editprofile(_User);
                        if (Result == -1)
                        {
                            return Ok(new { Code = 201, Status = true, Message = "Another User exists with the same name and age", MessageCode = 2 });
                        }
                       else if (Result == 0)
                        {
                            return Ok(new { Code = 200, Status = true, Message = "Profile saved successfully !", Data = _User });
                        }
                        else
                        {
                            return Ok(new { Code = 401, Status = false, Message = "Something wrong please check again.", Data = "" });
                        }
                    }
               
            }
            else
            {
                return Ok(new { Code = 200, Message = "Token is Invalid", MessageCode = 2 });
            }            
        }

        [HttpGet,Route("GetProfile")]
        public IActionResult GetProfile()
        {
            bool IsAuth = User.Identity.IsAuthenticated;
            if (IsAuth != false)
            {
                long UserId = Convert.ToInt64(User.Identity.Name);
                bool userActive = _common.ValidUser(UserId);
                if (userActive == false)
                {
                    return Ok(new { Code = 201, Status = false, Message = "Invalid User, Please create new account.", MessageCode = 2 });
                }
                if (ModelState.IsValid)
                {
                    var Result = _IUser.GetUserProfile(UserId);
                    if (Result !=null)
                    {
                        return Ok(new { Code = 200, Status = true, Message = "", Data = Result, MessageCode = 1 });
                    }
                    else
                    {
                        return Ok(new { Code = 201, Status = false, Message = "Something wrong please Try again.", MessageCode = 2 });
                    }
                }
                else
                {
                    return Ok(new { Code = 201, Status = false, Message = "Something wrong please Try again.", MessageCode = 2 });
                }
            }
            else
            {
                return Ok(new { Code = 200, Message = "Token is Invalid", MessageCode = 2 });
            }
        }
        [HttpGet, Route("GetProfilePercentage")]
        public IActionResult GetProfilePercentage()
        {
            bool IsAuth = User.Identity.IsAuthenticated;
            if (IsAuth != false)
            {
                long UserId = Convert.ToInt64(User.Identity.Name);
                bool userActive = _common.ValidUser(UserId);
                if (userActive == false)
                {
                    return Ok(new { Code = 201, Status = false, Message = "Invalid User, Please create new account.", MessageCode = 2 });
                }
                if (ModelState.IsValid)
                {
                    var Result = _IUser.GetUserProfilePercentage(UserId);
                    if (Result != null)
                    {
                        return Ok(new { Code = 200, Status = true, Message = "", Data = Result, MessageCode = 1 });
                    }
                    else
                    {
                        return Ok(new { Code = 201, Status = false, Message = "Something wrong please Try again.", MessageCode = 2 });
                    }
                }
                else
                {
                    return Ok(new { Code = 201, Status = false, Message = "Something wrong please Try again.", MessageCode = 2 });
                }
            }
            else
            {
                return Ok(new { Code = 200, Message = "Token is Invalid", MessageCode = 2 });
            }
        }

        [HttpGet, Route("Notification/Count")]
        public IActionResult GetUserNotificationCount()
        {
            bool IsAuth = User.Identity.IsAuthenticated;
            if (IsAuth != false)
            {
                long UserId = Convert.ToInt64(User.Identity.Name);
                bool userActive = _common.ValidUser(UserId);
                if (userActive == false)
                {
                    return Ok(new { Code = 201, Status = false, Message = "Invalid User, Please create new account.", MessageCode = 2 });
                }
                if (ModelState.IsValid)
                {
                    var Result = _IUser.GetUserNotificationCount(UserId);
                    var Data = new
                    {
                        count = Result.Item1,
                        userName = Result.Item2,
                        FullName = Result.Item3,
                        Notification=Result.Item4
                    };
                    if (Result != null)
                    {
                        return Ok(new { Code = 200, Status = true, Message = "", Data = Data, MessageCode = 1 });
                    }
                    else
                    {
                        return Ok(new { Code = 201, Status = false, Message = "Something wrong please Try again.", MessageCode = 2 });
                    }
                }
                else
                {
                    return Ok(new { Code = 201, Status = false, Message = "Something wrong please Try again.", MessageCode = 2 });
                }
            }
            else
            {
                return Ok(new { Code = 200, Message = "Token is Invalid", MessageCode = 2 });
            }
        }

        [HttpGet, Route("Notifications")]
        public IActionResult GetUserNotifications()
        {
            bool IsAuth = User.Identity.IsAuthenticated;
            if (IsAuth != false)
            {
                long UserId = Convert.ToInt64(User.Identity.Name);
                bool userActive = _common.ValidUser(UserId);
                if (userActive == false)
                {
                    return Ok(new { Code = 201, Status = false, Message = "Invalid User, Please create new account.", MessageCode = 2 });
                }
                if (ModelState.IsValid)
                {
                    var Result = _IUser.GetUserNotification(UserId);
                    if (Result != null)
                    {
                        return Ok(new { Code = 200, Status = true, Message = "", Data = Result, MessageCode = 1 });
                    }
                    else
                    {
                        return Ok(new { Code = 201, Status = false, Message = "No Record Found.", MessageCode = 2 });
                    }

                }
                else
                {
                    return Ok(new { Code = 201, Status = false, Message = "Something wrong please Try again.", MessageCode = 2 });
                }

            }
            else
            {
                return Ok(new { Code = 200, Message = "Token is Invalid", MessageCode = 2 });
            }
        }
        [HttpGet, Route("GetReferealLink")]
        public IActionResult GetReferealLink()
        {
            bool IsAuth = User.Identity.IsAuthenticated;
            if (IsAuth != false)
            {
                long UserId = Convert.ToInt64(User.Identity.Name);
                bool userActive = _common.ValidUser(UserId);
                if (userActive == false)
                {
                    return Ok(new { Code = 201, Status = false, Message = "Invalid User, Please create new account.", MessageCode = 2 });
                }
                if (ModelState.IsValid)
                {
                    var Result = _IUser.UserReferal(UserId);
                    if (Result != null)
                    {
                        return Ok(new { Code = 200, Status = true, Message = "", Data = Result, MessageCode = 1 });
                    }
                    else
                    {
                        return Ok(new { Code = 201, Status = false, Message = "Something wrong please Try again.", MessageCode = 2 });
                    }
                }
                else
                {
                    return Ok(new { Code = 201, Status = false, Message = "Something wrong please Try again.", MessageCode = 2 });
                }
            }
            else
            {
                return Ok(new { Code = 200, Message = "Token is Invalid", MessageCode = 2 });
            }
        }
        [HttpPost, Route("ConsentPrivacy")]
        public IActionResult ConsentPrivacy(ConsentPrivacyEntity Obj)
        {
            bool IsAuth = User.Identity.IsAuthenticated;
            if (IsAuth != false)
            {
                Obj.UserID = Convert.ToInt64(User.Identity.Name);
                if (String.IsNullOrEmpty(Obj.PinCode))
                {
                    return Ok(new { Code = 201, Message = "Pincode is Requried", MessageCode = 2 });
                }
                if (Obj.Policy!=1 && Obj.Tearm!=1)
                {
                    return Ok(new { Code = 201, Message = "Please check Tearm and Policy", MessageCode = 2 });
                }
                if (Obj.CountryId<=0)
                {
                    return Ok(new { Code = 201, Message = "Country is Requried", MessageCode = 2 });
                }
                if (Obj.Gender <= 0 || Obj.Gender==null)
                {
                    return Ok(new { Code = 201, Message = "Gender is Requried", MessageCode = 2 });
                }
                if (Obj.Dob == null)
                {
                    return Ok(new { Code = 201, Message = "DOB is Requried", MessageCode = 2 });
                }
                else
                {
                    var Result = _IUser.ConsentPrivacy(Obj);
                    
                    if (Result == -1)
                    {
                        return Ok(new { Code = 201, Status = true, Message = "Sorry Another User with the same Date of Birth,ZIP Code and Name exists.", MessageCode = 2 });
                    }
                    else if (Result == 1)
                    {
                        return Ok(new { Code = 200, Status = true, Message = "Thank you for Registering with us", MessageCode = 1 });
                    }
                    else
                    {
                        return Ok(new { Code = 201, Status = true, Message = "Something wrong please Try again.", MessageCode = 2 });
                    }
                }
            }
            else
            {
                return Ok(new { Code = 200, Message = "Token is Invalid", MessageCode = 2 });
            }
        }

        [HttpPut, Route("readnotification/{notificationId}")]
        public IActionResult ReadNotification(int notificationId)
        {
            bool IsAuth = User.Identity.IsAuthenticated;
            if (IsAuth != false)
            {
                long UserId = Convert.ToInt64(User.Identity.Name);
                bool userActive = _common.ValidUser(UserId);
                if (userActive == false)
                {
                    return Ok(new { Code = 201, Status = false, Message = "Invalid User, Please create new account.", MessageCode = 2 });
                }

                if (ModelState.IsValid)
                {
                    var Result = _IUser.ReadNotification(UserId, notificationId);
                    if (Result == 1)
                    {
                        return Ok(new { Code = 200, Status = true, Message = "Notification Read Successfully !", Data = Result });
                    }
                    else
                    {
                        return Ok(new { Code = 401, Status = false, Message = "Something wrong please check again.", Data = "" });
                    }
                }
                else
                {
                    return BadRequest(new { Code = 401, Status = false, Message = "Something wrong please check again.", Data = "" });
                }
            }
            else
            {
                return Ok(new { Code = 200, Message = "Token is Invalid", MessageCode = 2 });
            }
        }

        [HttpGet, Route("GetGUID")]
        public IActionResult GetGUID()
        {
            bool IsAuth = User.Identity.IsAuthenticated;
            if (IsAuth != false)
            {
                long UserId = Convert.ToInt64(User.Identity.Name);
                bool userActive = _common.ValidUser(UserId);
                if (userActive == false)
                {
                    return Ok(new { Code = 201, Status = false, Message = "Invalid User, Please create new account.", MessageCode = 2 });
                }
                if (ModelState.IsValid)
                {
                    var Result = _IUser.GetGUID(UserId);
                    if (Result != null)
                    {
                        return Ok(new { Code = 200, Status = true, Message = "", Data = Result, MessageCode = 1 });
                    }
                    else
                    {
                        return Ok(new { Code = 201, Status = false, Message = "Something wrong please Try again.", MessageCode = 2 });
                    }
                }
                else
                {
                    return Ok(new { Code = 201, Status = false, Message = "Something wrong please Try again.", MessageCode = 2 });
                }
            }
            else
            {
                return Ok(new { Code = 200, Message = "Token is Invalid", MessageCode = 2 });
            }
        }
        [HttpPost, Route("saveDeviceId")]
        public IActionResult saveDeviceId(SaveNotification _Notification)
        {
            bool IsAuth = User.Identity.IsAuthenticated;
            if (IsAuth != false)
            {
                long UserId = Convert.ToInt64(User.Identity.Name);
                bool userActive = _common.ValidUser(UserId);
                if (userActive == false)
                {
                    return Ok(new { Code = 201, Status = false, Message = "Invalid User, Please create new account.", MessageCode = 2 });
                }
                if (ModelState.IsValid)
                {
                    var Result = _IUser.postSaveNotificationDevice(_Notification, UserId);
                    if (Result > 0)
                    {
                        return Ok(new { Code = 200, Status = true, Message = "Data Saved Successfully !", MessageCode = 1 });
                    }
                    else
                    {
                        return Ok(new { Code = 401, Status = false, Message = "Something wrong please check again.", Data = "", MessageCode = 2 });
                    }
                }
                else
                {
                    return BadRequest(new { Code = 401, Status = false, Message = "Something wrong please check again.", Data = "", MessageCode = 2 });
                }
            }
            else
            {
                return Ok(new { Code = 200, Message = "Token is Invalid", MessageCode = 2 });
            }
        }
        [HttpPost, Route("saveReferalCode")]
        public IActionResult saveReferalCode(SaveReferalCode _referalCode)
        {
            bool IsAuth = User.Identity.IsAuthenticated;
            if (IsAuth != false)
            {
                long UserId = Convert.ToInt64(User.Identity.Name);
                bool userActive = _common.ValidUser(UserId);
                if (userActive == false)
                {
                    return Ok(new { Code = 201, Status = false, Message = "Invalid User, Please create new account.", MessageCode = 2 });
                }
                if (ModelState.IsValid)
                {
                    var Result = _IUser.postSaveReferalCode(_referalCode, UserId);
                    string MessageText = "";
                    if(Result==0)
                    {
                        MessageText = "You can not use self generated Referral code";
                    }
                   else if (Result == 1)
                    {
                        MessageText = "Referral code successfull applied";
                    }
                    else if (Result == -1)
                    {
                        MessageText = "Referral code is not valid";
                    }
                    else if (Result == 2)
                    {
                        MessageText = "You have already applied a referral code";
                    }
                    else if (Result == 3)
                    {
                        MessageText = "Sorry! You can not user referral code who refered you.";
                    }
                    else if (Result == 4)
                    {
                        MessageText = "Sorry! You can not use the referral code after 48 hours from the registration. ";
                    }
                    else if (Result == 5)
                    {
                        MessageText = "You have already used a referral code in Old account.";
                    }
                    else if (Result == 5)
                    {
                        MessageText = "You have already used a referral code in Old account.";
                    }
                    else if (Result == 6)
                    {
                        MessageText = "Sorry, you have exceeded per day referral limit.";
                    }
                    if (Result !=null)
                    {
                        return Ok(new { Code = 200, Status = true, Message = MessageText, MessageCode = 1 });
                    }
                    else
                    {
                        return Ok(new { Code = 401, Status = false, Message = "Something wrong please check again.", Data = "", MessageCode = 2 });
                    }
                }
                else
                {
                    return BadRequest(new { Code = 401, Status = false, Message = "Something wrong please check again.", Data = "", MessageCode = 2 });
                }
            }
            else
            {
                return Ok(new { Code = 200, Message = "Token is Invalid", MessageCode = 2 });
            }
        }
        [HttpGet,Route("notificationCategory")]
        public IActionResult notificationCategory()
        {
            bool IsAuth = User.Identity.IsAuthenticated;
            if (IsAuth != false)
            {
                long UserId = Convert.ToInt64(User.Identity.Name);

                if (ModelState.IsValid)
                {
                    var Result = _IUser.notificationCategory();
                    if (Result.Count() >=0)
                    {
                        return Ok(new { Code = 200, Status = true, Message = "Notification category fetched !", Data = Result });
                    }
                    else
                    {
                        return Ok(new { Code = 401, Status = false, Message = "Something wrong please check again.", Data = "" });
                    }
                }
                else
                {
                    return BadRequest(new { Code = 401, Status = false, Message = "Something wrong please check again.", Data = "" });
                }
            }
            else
            {
                return Ok(new { Code = 200, Message = "Token is Invalid", MessageCode = 2 });
            }
        }
        [HttpGet, Route("spinQuestionGet")]
        public IActionResult spinQuestionGet()
        {
            bool IsAuth = User.Identity.IsAuthenticated;
            if (IsAuth != false)
            {
                long UserId = Convert.ToInt64(User.Identity.Name);
                bool userActive = _common.ValidUser(UserId);
                if (userActive == false)
                {
                    return Ok(new { Code = 201, Status = false, Message = "Invalid User, Please create new account.", MessageCode = 2 });
                }
                if (ModelState.IsValid)
                {
                    var Result = _IUser.getSpinQuestion();
                    if (Result.Count() >= 0)
                    {
                        return Ok(new { Code = 200, Status = true, Message = "Notification category fetched !", Data = Result });
                    }
                    else
                    {
                        return Ok(new { Code = 401, Status = false, Message = "Something wrong please check again.", Data = "" });
                    }
                }
                else
                {
                    return BadRequest(new { Code = 401, Status = false, Message = "Something wrong please check again.", Data = "" });
                }
            }
            else
            {
                return Ok(new { Code = 200, Message = "Token is Invalid", MessageCode = 2 });
            }
        }
        [HttpPost, Route("spinQuestionSave")]
        public IActionResult spinQuestionSave(SpinQuestionSave spinQuestionSave)
        {
            bool IsAuth = User.Identity.IsAuthenticated;
            if (IsAuth != false)
            {
                long UserId = Convert.ToInt64(User.Identity.Name);
                bool userActive = _common.ValidUser(UserId);
                if (userActive == false)
                {
                    return Ok(new { Code = 201, Status = false, Message = "Invalid User, Please create new account.", MessageCode = 2 });
                }
                if (ModelState.IsValid)
                {
                    spinQuestionSave.UserId = UserId;
                    var Result = _IUser.saveSnipQuestion(spinQuestionSave);
                    if (Result >= 0)
                    {
                        return Ok(new { Code = 200, Status = true, Message = "Save data Successfully !" });
                    }
                    else
                    {
                        return Ok(new { Code = 401, Status = false, Message = "Something wrong please check again.", Data = "" });
                    }
                }
                else
                {
                    return BadRequest(new { Code = 401, Status = false, Message = "Something wrong please check again.", Data = "" });
                }
            }
            else
            {
                return Ok(new { Code = 200, Message = "Token is Invalid", MessageCode = 2 });
            }
        }
        [HttpPost, Route("NotificationConfiguration")]
        public IActionResult NotificationConfiguration(NotificationConfiguration notificationConfiguration)
        {
            bool IsAuth = User.Identity.IsAuthenticated;
            if (IsAuth != false)
            {
                long UserId = Convert.ToInt64(User.Identity.Name);
                bool userActive = _common.ValidUser(UserId);
                if (userActive == false)
                {
                    return Ok(new { Code = 201, Status = false, Message = "Invalid User, Please create new account.", MessageCode = 2 });
                }
                if (ModelState.IsValid)
                {
                    notificationConfiguration.UserId = UserId;
                    var Result = _IUser.notificationConfiguration(notificationConfiguration);
                    if (Result >= 0)
                    {
                        return Ok(new { Code = 200, Status = true, Message = "Save data Successfully !" });
                    }
                    else
                    {
                        return Ok(new { Code = 401, Status = false, Message = "Something wrong please check again.", Data = "" });
                    }
                }
                else
                {
                    return BadRequest(new { Code = 401, Status = false, Message = "Something wrong please check again.", Data = "" });
                }
            }
            else
            {
                return Ok(new { Code = 200, Message = "Token is Invalid", MessageCode = 2 });
            }
        }
        [HttpGet, Route("spinRandomQuestionGet")]
        public IActionResult spinRandomQuestionGet()
        {
            bool IsAuth = User.Identity.IsAuthenticated;
            if (IsAuth != false)
            {
                long UserId = Convert.ToInt64(User.Identity.Name);

                if (ModelState.IsValid)
                {
                    var Result = _IUser.getSpinRandomQuestion();
                    if (Result.id> 0)
                    {
                        return Ok(new { Code = 200, Status = true, Message = "Question fetch successfully !", Data = Result });
                    }
                    else
                    {
                        return Ok(new { Code = 401, Status = false, Message = "Something wrong please check again.", Data = "" });
                    }
                }
                else
                {
                    return BadRequest(new { Code = 401, Status = false, Message = "Something wrong please check again.", Data = "" });
                }
            }
            else
            {
                return Ok(new { Code = 200, Message = "Token is Invalid", MessageCode = 2 });
            }
        }
        [HttpPost, Route("saveRandowQuestionAns")]
        public IActionResult saveRandowQuestionAns(SpinRandomQueAnsSave spinQuestionSave)
        {
            bool IsAuth = User.Identity.IsAuthenticated;
            if (IsAuth != false)
            {
                long UserId = Convert.ToInt64(User.Identity.Name);
                bool userActive = _common.ValidUser(UserId);
                if (userActive == false)
                {
                    return Ok(new { Code = 201, Status = false, Message = "Invalid User, Please create new account.", MessageCode = 2 });
                }
                if (ModelState.IsValid)
                {
                    spinQuestionSave.UserId = UserId;
                    var Result = _IUser.spinQuestionRandomSave(spinQuestionSave);
                    if (Result > 0)
                    {
                        return Ok(new { Code = 200, Status = true, Message = "Save data Successfully !" });
                    }
                    else
                    {
                        return Ok(new { Code = 401, Status = false, Message = "Something wrong please check again.", Data = "" });
                    }
                }
                else
                {
                    return BadRequest(new { Code = 401, Status = false, Message = "Something wrong please check again.", Data = "" });
                }
            }
            else
            {
                return Ok(new { Code = 200, Message = "Token is Invalid", MessageCode = 2 });
            }
        }
        [HttpGet, Route("notificationRemove")]
        public IActionResult notificationRemove(int NotificationId)
        {
            bool IsAuth = User.Identity.IsAuthenticated;
            if (IsAuth != false)
            {
                long UserId = Convert.ToInt64(User.Identity.Name);
                bool userActive = _common.ValidUser(UserId);
                if (userActive == false)
                {
                    return Ok(new { Code = 201, Status = false, Message = "Invalid User, Please create new account.", MessageCode = 2 });
                }
                if (ModelState.IsValid)
                {
                    var Result = _IUser.notificationRemove(NotificationId);
                    if (Result >= 0)
                    {
                        return Ok(new { Code = 200, Status = true, Message = "Notification remove from star!", Data = Result });
                    }
                    else
                    {
                        return Ok(new { Code = 401, Status = false, Message = "Something wrong please check again.", Data = "" });
                    }
                }
                else
                {
                    return BadRequest(new { Code = 401, Status = false, Message = "Something wrong please check again.", Data = "" });
                }
            }
            else
            {
                return Ok(new { Code = 200, Message = "Token is Invalid", MessageCode = 2 });
            }
        }
        [HttpGet, Route("notificationStar")]
        public IActionResult notificationStar(int NotificationId)
        {
            bool IsAuth = User.Identity.IsAuthenticated;
            if (IsAuth != false)
            {
                long UserId = Convert.ToInt64(User.Identity.Name);
                bool userActive = _common.ValidUser(UserId);
                if (userActive == false)
                {
                    return Ok(new { Code = 201, Status = false, Message = "Invalid User, Please create new account.", MessageCode = 2 });
                }
                if (ModelState.IsValid)
                {
                    var Result = _IUser.notificationStar(NotificationId);
                    if (Result >= 0)
                    {
                        return Ok(new { Code = 200, Status = true, Message = "Notification marked as Star  !", Data = Result });
                    }
                    else
                    {
                        return Ok(new { Code = 401, Status = false, Message = "Something wrong please check again.", Data = "" });
                    }
                }
                else
                {
                    return BadRequest(new { Code = 401, Status = false, Message = "Something wrong please check again.", Data = "" });
                }
            }
            else
            {
                return Ok(new { Code = 200, Message = "Token is Invalid", MessageCode = 2 });
            }
        }
        [HttpPost, Route("notificationReport")]
        public IActionResult notificationReport(NotificationReport notificationReport)
        {
            bool IsAuth = User.Identity.IsAuthenticated;
            if (IsAuth != false)
            {
                long UserId = Convert.ToInt64(User.Identity.Name);
                bool userActive = _common.ValidUser(UserId);
                if (userActive == false)
                {
                    return Ok(new { Code = 201, Status = false, Message = "Invalid User, Please create new account.", MessageCode = 2 });
                }
                if (ModelState.IsValid) 
                {
                    notificationReport.UserId = UserId;
                    var Result = _IUser.notificationReport(notificationReport);
                    if (Result >= 0)
                    {
                        return Ok(new { Code = 200, Status = true, Message = "Notification category fetched !", Data = Result });
                    }
                    else
                    {
                        return Ok(new { Code = 401, Status = false, Message = "Something wrong please check again.", Data = "" });
                    }
                }
                else
                {
                    return BadRequest(new { Code = 401, Status = false, Message = "Something wrong please check again.", Data = "" });
                }
            }
            else
            {
                return Ok(new { Code = 200, Message = "Token is Invalid", MessageCode = 2 });
            }
        }
    }
}