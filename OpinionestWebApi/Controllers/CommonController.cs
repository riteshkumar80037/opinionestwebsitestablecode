﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using BusinessAccessLayer.InterfaceDeclaration;
using BusinessAccessLayer.ServiceDefination;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace OpinionestWebApi.Controllers
{
    [Microsoft.AspNetCore.Cors.EnableCors("AllowOrigin")]
    [Route("api/[controller]")]
    [ApiController]
    public class CommonController : Controller
    {
        ICommon _ICommon;
        SqlConnection _Conn;
        IConfiguration _iconfiguration;
        private readonly IHostingEnvironment hostingEnv;
        private const string ChampionsImageFolder = "Champions";
        public CommonController(IConfiguration iconfiguration, IHostingEnvironment hostingEnv)
        {
            _iconfiguration = iconfiguration;
            string Connection = _iconfiguration.GetSection("ConnectionStrings").GetSection("DefaultConnection").Value;
            _Conn = new SqlConnection(Connection);
            _ICommon = new CommonServicesDefination(_Conn);
            this.hostingEnv = hostingEnv;

        }

        [HttpGet,Route("Country")]
        public IActionResult Index()
        {
            //string Paht =  Path.Combine(hostingEnv.WebRootPath, ChampionsImageFolder);
            var Result = _ICommon.CountryList();
            if (Result != null)
            {
                return Ok(new { Code = 200, Message = "Data Fetch Successfully", Data= Result, MessageCode = 1 });
            }
            else if (Result ==null)
            {
                return Ok(new { Code = 201, Message = "OOPS! Something went wrong", MessageCode = 2 });
            }
            else
            {
                return Ok(new { Code = 201, Message = "OOPS! Something went wrong", MessageCode = 2 });
            }
        }
        
        [HttpGet, Route("State")]
        public IActionResult Index(int CountryId)
        {
            var Result = _ICommon.StateList(CountryId);
            if (Result != null)
            {
                return Ok(new { Code = 200, Message = "Data Fetch Successfully", Data = Result, MessageCode = 1 });
            }
            else if (Result == null)
            {
                return Ok(new { Code = 201, Message = "OOPS! Something went wrong", MessageCode = 2 });
            }
            else
            {
                return Ok(new { Code = 201, Message = "OOPS! Something went wrong", MessageCode = 2 });
            }
        }
    }
}