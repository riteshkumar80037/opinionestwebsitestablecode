﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using BusinessAccessLayer.InterfaceDeclaration;
using BusinessAccessLayer.ServiceDefination;
using CommonMethod.Interface;
using BusinessEntites;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Rest.Lookups.V1;

namespace OpinionestWebApi.Controllers
{
    [Microsoft.AspNetCore.Cors.EnableCors("AllowOrigin")]
    [Route("api/[controller]")]
    [ApiController]
    public class PasswordController : Controller
    {
        Ipassword _Password;
        SqlConnection _Conn;
        CommonMethod.Interface.ICommon _Comm;
        IConfiguration _iconfiguration;
        ILogin _Login;
        public PasswordController(IConfiguration iconfiguration)
        {
            _iconfiguration = iconfiguration;
            string Connection = _iconfiguration.GetSection("ConnectionStrings").GetSection("DefaultConnection").Value;
            _Conn = new SqlConnection(Connection);
            _Password = new PasswordServicesDefination(_Conn);
            _Login = new LoginServicesDefination(_Conn);
        }
        [HttpPost, Route("Recover")]
        public IActionResult Recover(forgetpassword obj)
        {
            try
            {
                string SendGridKey = _iconfiguration.GetSection("SendGridKey").Value;
                int Resutl = _Login.ReGenerateOtp(obj.EmailID, SendGridKey,false );
                if(Resutl==1)
                {
                    return Ok(new { Code = 200, Status = true, Message = "OTP Sent to your Email.", MessageCode = 1 });
                }
                else if(Resutl==-1)
                {
                    return Ok(new { Code = 201, Status = false, Message = "Sorry! Email Id does not Exist.", MessageCode = 2 });
                }
                else
                {
                    return Ok(new { Code = 200, Status = false, Message = "Some Problem To Send Mail. Please Try Again.", MessageCode=2 });
                }
            }
            catch (Exception ex)
            {
                return Ok(new { Code = 200, Status = false, Message = "Some Problem To Send Mail. Please Try Again." , MessageCode =2});
            }
        }
        [HttpGet, Route("mobileOtp")]
        public IActionResult mobileOtp(string countryCode,string mobile)
        {
            if(String.IsNullOrEmpty(countryCode))
            {
                return Ok(new { Code = 201, Message = "Country code can not be null or empty.", MessageCode = 2 });
            }
            if (String.IsNullOrEmpty(mobile))
            {
                return Ok(new { Code = 201, Message = "Mobile Number can not be null or empty.", MessageCode = 2 });
            }
            try
            {
                string accountSid = _iconfiguration.GetSection("accountSid").Value.ToString();
                string authToken = _iconfiguration.GetSection("authToken").Value.ToString();
                string fromNo = _iconfiguration.GetSection("fromNo").Value.ToString();
                int otp = _Login.mobileOtp(mobile, countryCode);
                TwilioClient.Init(accountSid, authToken);
                var type = new List<string> {
            "NationalFormat",
            "PhoneNumber"
        };

                //var phoneNumber = PhoneNumberResource.Fetch(
                //    type: type,
                //    pathPhoneNumber: new Twilio.Types.PhoneNumber("919812107106")
                //);

                //Console.WriteLine(phoneNumber.NationalFormat);
                mobile = countryCode + mobile;
                var message = MessageResource.Create(
                    body: "Your OTP is :" + otp,
                    from: new Twilio.Types.PhoneNumber(fromNo),
                    to: new Twilio.Types.PhoneNumber(mobile)
                );
                return Ok(new { Code = 200, Status = true, Message = "OTP Sent to your number.", MessageCode = 1 });
            }
            catch(Exception ex)
            {
                return Ok(new { Code = 201, Status = true, Message = ex.Message, MessageCode = 2 });
            }
            

        }
    }
}