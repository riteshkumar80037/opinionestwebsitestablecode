﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using BusinessAccessLayer.InterfaceDeclaration;
using BusinessAccessLayer.ServiceDefination;
using CommonMethod;
using BusinessEntites;
using Microsoft.AspNetCore.Identity;

namespace OpinionestWebApi.Controllers
{
    [Microsoft.AspNetCore.Cors.EnableCors("AllowOrigin")]
    [Route("api/[controller]")]
    [ApiController]
    public class ExtraActivitiesController : Controller
    {
        Iuser _IUser;
        IExtraActivity _IExtraActivity;
        ILogin _Login;
        SqlConnection _Conn;
        IConfiguration _iconfiguration;
        public ExtraActivitiesController(IConfiguration iconfiguration)
        {
            _iconfiguration = iconfiguration;
            string Connection = _iconfiguration.GetSection("ConnectionStrings").GetSection("DefaultConnection").Value;
            _Conn = new SqlConnection(Connection);
            _IExtraActivity = new ExtraActivityServicesDefination(_Conn);
            _IUser = new UserServicesDefination(_Conn);
            _Login = new LoginServicesDefination(_Conn);
        }



        [HttpGet, Route("Redirect")]
        public IActionResult Redirect()
        {
            bool status = false;
            var UrlKeyValues = ControllerContext.HttpContext.Request.Query;

            string mailaddress = string.IsNullOrEmpty(UrlKeyValues.LastOrDefault(x => x.Key == "info").Value) ? ""
                  : (UrlKeyValues.LastOrDefault(x => x.Key == "info").Value).ToString();

            string Redirecttype = string.IsNullOrEmpty(UrlKeyValues.LastOrDefault(x => x.Key == "redirect").Value) ? ""
                   : (UrlKeyValues.LastOrDefault(x => x.Key == "redirect").Value).ToString();

            UserEntites usermodel = new UserEntites();
            UseLoginEntites userloginmodel = new UseLoginEntites();
            LoginEntites loginmodel = new LoginEntites();


            if (!string.IsNullOrEmpty(mailaddress) && !string.IsNullOrEmpty(Redirecttype))
            {
                usermodel = _IUser.GetUserDetailsByEmailAddress(mailaddress.DecryptID());
                if (usermodel.UserId != 0)
                {
                    userloginmodel.EmailId = usermodel.EmailAddress;
                    if(usermodel.IsSocial==false)
                    {
                        userloginmodel.Password = usermodel.Password.DecryptID();
                    }
                    
                    userloginmodel.IsSocialLogin = usermodel.IsSocial;
                    bool popup = false;
                    loginmodel = _Login.Login(userloginmodel,out popup);
                    loginmodel.Redirect = Redirecttype.DecryptID();
                }
            }
 
            if (usermodel.UserId!=0)
            {
                return Ok(new { Code = 200, Status = true, Data = loginmodel, Message = "Data saved retrieved :)", MessageCode = 1 });
            }
            else
            {
                return Ok(new { Code = 201, Status = false, Message = "Something went wrong :(", MessageCode = 2 });
            }
        }

        [HttpGet, Route("IpAddress")]
        public IActionResult IpAddress(string Userid, string IpAddress)
        {
            bool status = false;
            long UserId_ = string.IsNullOrEmpty(Userid) ? 0 : Convert.ToInt64(Userid);
            if (UserId_ != 0 && !string.IsNullOrEmpty(IpAddress))
            {
                status = _IExtraActivity.SaveIpAddress(UserId_, IpAddress);
            }
            if (status)
            {
                return Ok(new { Code = 200, Status = true, Message = "Data saved successfully :)", MessageCode = 1 });
            }
            else
            {
                return Ok(new { Code = 201, Status = false, Message = "Something went wrong :(", MessageCode = 2 });
            }
        }
    }
}
