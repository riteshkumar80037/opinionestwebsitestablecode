﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OpinionestWebApi
{
    public class BrandRequirements
    {
        public string displayInstructions { get; set; }
        public string termsAndConditionsInstructions { get; set; }
        public string disclaimerInstructions { get; set; }
        public bool alwaysShowDisclaimer { get; set; }

    }
    public class ImageUrls
    {
        [JsonProperty("80w-326ppi")]
        public Uri The80W326Ppi { get; set; }

        [JsonProperty("130w-326ppi")]
        public Uri The130W326Ppi { get; set; }

        [JsonProperty("200w-326ppi")]
        public Uri The200W326Ppi { get; set; }

        [JsonProperty("278w-326ppi")]
        public Uri The278W326Ppi { get; set; }

        [JsonProperty("300w-326ppi")]
        public Uri The300W326Ppi { get; set; }

        [JsonProperty("1200w-326ppi")]
        public Uri The1200W326Ppi { get; set; }


    }
public class Items
{
    public string utid { get; set; }
    public string rewardName { get; set; }
    public string currencyCode { get; set; }
    public string status { get; set; }
    public string valueType { get; set; }
    public string rewardType { get; set; }
    public bool isWholeAmountValueRequired { get; set; }
    public decimal faceValue { get; set; }
    public DateTime createdDate { get; set; }
    public DateTime lastUpdateDate { get; set; }
    public IList<string> countries { get; set; }
    public IList<string> credentialTypes { get; set; }
    public string redemptionInstructions { get; set; }

}
public class Brands
{
    public string brandKey { get; set; }
    public string brandName { get; set; }
    public string disclaimer { get; set; }
    public string description { get; set; }
    public string shortDescription { get; set; }
    public string terms { get; set; }
    public DateTime createdDate { get; set; }
    public DateTime lastUpdateDate { get; set; }
    public BrandRequirements brandRequirements { get; set; }
    public ImageUrls imageUrls { get; set; }
    public string status { get; set; }
    public IList<Items> items { get; set; }

}
public class Application
{
    public string catalogName { get; set; }
    public IList<Brands> brands { get; set; }

}
    public partial class TangoPost
    {
        [JsonProperty("accountIdentifier")]
        public string AccountIdentifier { get; set; }

        [JsonProperty("amount")]
        public string Amount { get; set; }

        [JsonProperty("campaign")]
        public string Campaign { get; set; }

        [JsonProperty("customerIdentifier")]
        public string CustomerIdentifier { get; set; }

        [JsonProperty("emailSubject")]
        public string EmailSubject { get; set; }

        [JsonProperty("etid")]
        public string Etid { get; set; }

        [JsonProperty("externalRefID")]
        public string ExternalRefId { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("notes")]
        public string Notes { get; set; }

        [JsonProperty("recipient")]
        public Recipient Recipient { get; set; }

        [JsonProperty("sendEmail")]
        public bool SendEmail { get; set; }

        [JsonProperty("sender")]
        public Recipient Sender { get; set; }

        [JsonProperty("utid")]
        public string Utid { get; set; }
    }

    public partial class Recipient
    {
        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("firstName")]
        public string FirstName { get; set; }

        [JsonProperty("lastName")]
        public string LastName { get; set; }
    }
    public class Errors
    {
        public string path { get; set; }
        public string i18nKey { get; set; }
        public string message { get; set; }
        public string invalidValue { get; set; }
        public string constraint { get; set; }

    }
    public class ApplicationResponse
    {
        public DateTime timestamp { get; set; }
        public string requestId { get; set; }
        public string path { get; set; }
        public int httpCode { get; set; }
        public string httpPhrase { get; set; }
        public IList<Errors> errors { get; set; }
        public string status { get; set; }

    }
}
