﻿using DataBaseAccess.Interface;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace DataBaseAccess.Defination
{
    public class ServiceDataAccessMySurvey: IDataAccessMySurvey
    {
        SqlConnection Conn;
        SQLCommonMethod SqlCom;
        public ServiceDataAccessMySurvey(SqlConnection _Conn)
        {
            Conn = new SqlConnection();
            Conn = _Conn;
            SqlCom = new SQLCommonMethod(Conn);
        }

        public DataTable BannerSurvey(string UserId)
        {
            DataTable Dt = new DataTable();
            try
            {
                SqlParameter[] parameter = {
                new SqlParameter("@UserId",UserId)};
                Dt = SqlCom.Datatable_Sp("StaticBannerServe", parameter);
                return Dt;
            }
            catch (Exception Ex)
            {
                return Dt;
            }
        }

        public DataSet MySurveylist(string UserId, int pageindex, int pageSize)
        {
            DataSet Dt = new DataSet();
            try
            {
                SqlParameter[] parameter = {
                new SqlParameter("@pageno",pageindex),
                new SqlParameter("@UserId",UserId),
                new SqlParameter("@PageSize", pageSize)};
            Dt = SqlCom.DataSet_Sp("ServePageData", parameter);
                return Dt;
            }
            catch (Exception Ex)
            {
                return Dt;
            }
        }
    }
}
