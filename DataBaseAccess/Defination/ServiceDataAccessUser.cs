﻿using BusinessEntites;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Linq;
using Insight.Database;

namespace DataBaseAccess.Defination
{
    public class ServiceDataAccessUser : IDataAccessUser
    {
        SqlConnection Conn;
        SQLCommonMethod SqlCom;
        public ServiceDataAccessUser(SqlConnection _Conn)
        {
            Conn = new SqlConnection();
            Conn = _Conn;
            SqlCom = new SQLCommonMethod(Conn);
        }

        public EditUserEntites GetUserProfile(long UserId)
        {
            try

            {
                SqlParameter[] parameter = { new SqlParameter
                ("@UserId",UserId),};
                DataTable Dt = SqlCom.Datatable("select UserId,IsNull(FullName,'N/A') as FullName,FirstName,IsNull(LastName,'N/A') AS LastName , GenderId AS Gender,CONVERT(varchar(12), DateOfBirth,105) as DateOfBirth, EmailAddress, IsNull(PhoneNumber, 'N/A') as PhoneNumber, IsNull(PinCode, 'N/A') as PinCode,IsNull(ProfileImage, '') as ProfileImage,IsNull(CountryMaster.CountryName,'N/A') AS CountryName,IsNull(CountryMaster.CountryId,0) AS CountryId from usermaster left join CountryMaster on CountryMaster.CountryId = usermaster.CountryId where UserId = @UserId", parameter);
                if (Dt.Rows.Count > 0)
                {
                    EditUserEntites _user = new EditUserEntites
                    {
                        CountryName = Dt.Rows[0]["CountryName"].ToString(),
                        DateOfBirth = Dt.Rows[0]["DateOfBirth"] == DBNull.Value ? "N/A" : Dt.Rows[0]["DateOfBirth"].ToString(),
                        FirstName = Dt.Rows[0]["FirstName"].ToString(),
                        FullName = Dt.Rows[0]["FullName"].ToString(),
                        LastName = Dt.Rows[0]["LastName"].ToString(),
                        Gender = Dt.Rows[0]["Gender"]==DBNull.Value?-1:Convert.ToInt32(Dt.Rows[0]["Gender"]),
                        PhoneNumber = Dt.Rows[0]["PhoneNumber"].ToString(),
                        Pincode = Dt.Rows[0]["PinCode"].ToString(),
                        EmailAddress = Dt.Rows[0]["EmailAddress"].ToString(),
                        ProfileImage = Dt.Rows[0]["ProfileImage"].ToString(),
                        UserId = Convert.ToInt64(Dt.Rows[0]["UserId"]),
                        CountryId= Dt.Rows[0]["CountryId"].ToString()

                    };

                    return _user;
                }
                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public Tuple<int, string,string, DailyFeedDetails> GetUserNotificationCount(long UserId)
        {
            int Count = 0;
            string Name = "";
            string FullName = "";
            DailyFeedDetails _noti = new DailyFeedDetails();
            try
               
            {
                SqlParameter[] parameter = { new SqlParameter
                ("@UserId",UserId),};
                DataTable Dt = SqlCom.Datatable("select (select count(1) count from DailyFeeds where UserId = @UserId and IsRead=0) as count,(select substring(firstname, 1, 1) + ' ' + substring(lastname, 1, 1) from usermaster where UserId = @UserId) as UserName,(select fullname from usermaster where UserId = @UserId) as FullName", parameter);
                DataTable Notification = SqlCom.Datatable("select  top 1 NotificationType.IconPath as Icon, *  from DailyFeeds   inner join NotificationType on NotificationType.Id = DailyFeeds.NotificationTypeId where UserId = @UserId order by DailyFeeds.Id desc ", parameter);
                if (Dt.Rows.Count > 0)
                {
                    Count= Convert.ToInt32(Dt.Rows[0][0].ToString());
                    Name = Convert.ToString(Dt.Rows[0][1].ToString());
                    FullName = Convert.ToString(Dt.Rows[0][2].ToString());
                    if(Notification.Rows.Count>0)
                    {

                        _noti.FromDt = Convert.ToDateTime(Notification.Rows[0]["FromDt"]);
                        _noti.Header = Notification.Rows[0]["Header"].ToString();
                        _noti.iconPath = Notification.Rows[0]["Icon"].ToString();
                        _noti.IsRead = Notification.Rows[0]["IsRead"].ToString() == "True" ? "Read" : "Unread";
                        _noti.Message = Notification.Rows[0]["Message"].ToString();
                        _noti.Points = Convert.ToDecimal(Notification.Rows[0]["Points"].ToString());
                        _noti.Time = Notification.Rows[0]["Time"].ToString();
                        _noti.Category = Notification.Rows[0]["Name"].ToString();
                    }
                    return new Tuple<int, string,string, DailyFeedDetails>(Count, Name, FullName, _noti);
                }
                return new Tuple<int, string,string, DailyFeedDetails>(Count, Name, FullName, _noti);
            }
            catch (Exception ex)
            {
                return new Tuple<int, string,string, DailyFeedDetails>(Count, Name, FullName, _noti);
            }
        }
        public IList<UserNotificationEntity> GetUserNotification(long UserId)
        {
            try

            {
                SqlParameter[] parameter = { new SqlParameter
                ("@UserId",UserId),};
                //DataTable Dt = SqlCom.Datatable("select Case when datediff(MINUTE, createdDate, getdate()) < 59  then convert(Varchar(20), datediff(MINUTE, createdDate, getdate())) + ' Min' when datediff(HOUR, createdDate, getdate()) <= 24 then Convert(varchar(20), datediff(HOUR, createdDate, getdate())) + ' Hrs' else convert(varchar(20), datediff(day, createdDate, getdate())) + ' Days'  End as Hrs,Message,fromdt,Header,NotificationType.IconPath,Url,IsNull(Points,'0') as Points from Dailyfeeds inner join NotificationType on NotificationType.Id = DailyFeeds.NotificationTypeId where  IsRead = 0 and UserId = @UserId  order by createdDate desc update Dailyfeeds set IsRead=1 where UserId=@UserId", parameter);
                DataTable Dt = SqlCom.Datatable("select Case when datediff(MINUTE, createdDate, getdate()) < 59  then convert(Varchar(20), datediff(MINUTE, createdDate, getdate())) + ' Min' when datediff(HOUR, createdDate, getdate()) <= 24 then Convert(varchar(20), datediff(HOUR, createdDate, getdate())) + ' Hrs' else convert(varchar(20), datediff(day, createdDate, getdate())) + ' Days'  End as Hrs,Message,fromdt,Header,NotificationType.IconPath,Url,IsNull(Points,'0') as Points from Dailyfeeds inner join NotificationType on NotificationType.Id = DailyFeeds.NotificationTypeId where  IsRead = 0 and UserId = @UserId  order by createdDate desc ", parameter);
                List<UserNotificationEntity> NotificationList=new List<UserNotificationEntity>();
                if (Dt.Rows.Count > 0)
                {
                    NotificationList = (from DataRow dr in Dt.Rows
                                   select new UserNotificationEntity()
                                   {
                                       Message = dr["Message"].ToString(),
                                       CreatedDate = dr["fromdt"].ToString(),
                                       Header= dr["Header"].ToString(),
                                       ImagePath= dr["IconPath"].ToString(),
                                       Url= dr["Url"].ToString(),
                                       Time= dr["Hrs"].ToString(),
                                       Points=dr["Points"].ToString()
                                   }).ToList();
                    return NotificationList;
                }
                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public int UserEdit(EditUserEntites _obj)
        {
            try
            {
                SqlParameter[] parameter = {
            new SqlParameter("@UserId",_obj.UserId),
            new SqlParameter("@Fullname",_obj.FullName),
            new SqlParameter("@FirstName",_obj.FirstName),
            new SqlParameter("@LastName",_obj.LastName),
            new SqlParameter("@Phone",_obj.PhoneNumber),
            new SqlParameter("@Gender",_obj.Gender),
            new SqlParameter("@Image",_obj.ProfileImage),
            new SqlParameter("@DOB",_obj.DateOfBirth),
            new SqlParameter("@PinCode",_obj.Pincode),
            new SqlParameter("@Mode","edit"),
            };
                long i = SqlCom.ExecuteScaler_SP("EditUserProfile", parameter);
                return  Convert.ToInt32(i);
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public UserProfilePercentage GetUserProfilePercentage(long UserID)
        {
            try

            {
                SqlParameter[] parameter = { new SqlParameter
                ("@UserId",UserID),};
                DataTable Dt = SqlCom.Datatable_Sp("Opinionset_Getprofile_Prcentage", parameter);
                if (Dt.Rows.Count > 0)
                {
                    UserProfilePercentage _percentage = new UserProfilePercentage
                    {
                        
                        TotalQuestion = Convert.ToInt32(Dt.Rows[0]["TotalQuestion"]),
                        Attempted = Convert.ToInt32(Dt.Rows[0]["Attempted"]),
                        Percentage = Convert.ToDecimal(Dt.Rows[0]["Percentage"]),
                        TotalPoint = Convert.ToDecimal(Dt.Rows[0]["TotalPoint"]),
                        PointRedeemMessage= Dt.Rows[0]["PointRedeemMessage"].ToString()

                    };

                    return _percentage;
                }
                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public UserReferalEntites GetReferal(long UserId)
        {
            UserReferalEntites _obj = new UserReferalEntites();
            try
            {
                SqlParameter[] parameter = {
            new SqlParameter("@UserId",UserId)
            };
                DataTable Dt = SqlCom.Datatable_Sp("Opinionest_get_referel", parameter);
                if(Dt.Rows.Count>0)
                {
                    _obj.TotalPoint = Convert.ToInt32(Dt.Rows[0]["TotalPoint"]);
                    _obj.TotalReferal = Convert.ToInt32(Dt.Rows[0]["TotalReferal"]);
                    _obj.UniqueCode = Convert.ToString(Dt.Rows[0]["UniqueCode"]);
                    _obj.ReferalExist= Convert.ToBoolean(Dt.Rows[0]["ReferalExist"]);
                    _obj.UserTotalPoints = Convert.ToInt32(Dt.Rows[0]["UserPointDeduct"]);
                }
            }
            catch (Exception ex)
            {
                
            }
            return _obj;
        }

        public long ConsentPrivacy(ConsentPrivacyEntity Obj)
        {
            try
            {
                SqlParameter[] parameter = {
            new SqlParameter("@Privacy",Obj.Policy),
            new SqlParameter("@Tearms",Obj.Tearm),
            new SqlParameter("@UserId",Obj.UserID),
            new SqlParameter("@Dob",Obj.Dob.Date),
            new SqlParameter("@Gender",Obj.Gender),
            new SqlParameter("@CountryId",Obj.CountryId),
            new SqlParameter("@PinCode",Obj.PinCode),
            };
                var I = SqlCom.ExecuteNonQuery_Sp("Opinionest_Update_Policy", parameter);
                return I; 
            }
            catch (Exception ex)
            {

            }
            return 0;
        }


        /// <summary>
        /// Read Notification
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="notificationId"></param>
        /// <returns></returns>
        public int ReadNotification(long userId, int notificationId)
        {
            try
            {
                SqlParameter[] parameter = {
                new SqlParameter("@userId", userId),
                new SqlParameter("@notificationId", notificationId)
            };
                long i = SqlCom.ExecuteNonQuery("Update DailyFeeds set IsRead = 1 Where Id = @notificationId and UserId = @userId", parameter);
                return Convert.ToInt32(i);
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public string GetGUID(long UserId)
        {
            Guid _guid = Guid.NewGuid();
            try
            {
                SqlParameter[] parameter = {
                new SqlParameter("@UserId", UserId),
                new SqlParameter("@GUID", _guid.ToString())
            };
                long i = SqlCom.ExecuteNonQuery("insert into RegisterUserGuid(UserID,guid) values(@UserId,@GUID)", parameter);
                return _guid.ToString();
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        public int postSaveNotification(SaveNotification obj, long UserId)
        {
            
            try
            {
                SqlParameter[] parameter = {
                new SqlParameter("@UserId", UserId),
                new SqlParameter("@DeviceToken", obj.DeviceToken)
            };
                long i = SqlCom.ExecuteScaler_SP("Usp_Opinionset_SaveNotificationToken", parameter);
                return  Convert.ToInt32(i);
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public int postSavereferalCode(SaveReferalCode obj, long UserId)
        {
            try
            {
                SqlParameter[] parameter = {
                new SqlParameter("@UserId", UserId),
                new SqlParameter("@referalCode", obj.ReferalCode)
            };
                long i = SqlCom.ExecuteScaler_SP("Usp_SaveReferalCode1", parameter);
                return Convert.ToInt32(i);
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public UserEntites GetUserDetailsByEmailAddress(string Emailaddress)
        {
            UserEntites userEntites = new UserEntites();
            try
            {
                using (SqlConnection Db = new SqlConnection(Conn.ConnectionString))
                {
                    userEntites = Conn.QuerySql<UserEntites>(@"select * from usermaster 
                            where emailaddress=@emailaddress",new
                    {
                        emailaddress=Emailaddress.Trim()
                    }).FirstOrDefault();
                }
            }
            catch(Exception ex)
            {

            }
            return userEntites;
        }

        public DataTable getNotificationCategroy()
        {
            DataTable dt = SqlCom.Datatable("select id,name as categoryName from NotificationType where Id not in(2,4) ");
            return
                  dt;
        }

        public DataTable getSpinQuestion()
        {
            DataTable dt = SqlCom.Datatable("select id,Question  from SpinQuestion ");
            return
                  dt;
        }

        public long SaveSpinQuestion(SpinQuestionSave spinQuestionSave)
        {
            try
            {
                SqlParameter[] parameter = {
                new SqlParameter("@UserId", spinQuestionSave.UserId),
                new SqlParameter("@SpinQuestionId", spinQuestionSave.SpinQuestionId)
            };
                long i = SqlCom.ExecuteScaler("insert into SpinQuestionSave(SpinQuestionId,UserId) values(@SpinQuestionId,@UserId)", parameter);
                return Convert.ToInt32(i);
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public long notificationConfiguration(NotificationConfiguration notificationConfiguration)
        {
            try
            {
                SqlParameter[] parameter = {
                new SqlParameter("@userId", notificationConfiguration.UserId),
                new SqlParameter("@active", notificationConfiguration.IsActive)
            };
                long i = SqlCom.ExecuteNonQuery_Sp("usp_NotificationConfiguration", parameter);
                return Convert.ToInt32(i);
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public DataTable getRandomSpinQuestion()
        {
            DataTable dt = SqlCom.Datatable(" SELECT TOP 1 Id,Question FROM SpinRandomQuestion where Isactive=1 ORDER BY NEWID() ");
            return
                  dt;
        }

        public DataTable getRandomSpinQuestionAns(int QuestionId)
        {
            SqlParameter[] parameter = {
                new SqlParameter("@Questionid",QuestionId),

            };
            DataTable dt = SqlCom.Datatable(" select id as ansId,answer,QuestionId from [dbo].[SpinRandomQuestionAns] where Isactive=1 and QuestionId=@QuestionId ", parameter);
            return
                  dt;
        }

        public long spinQuestionRandomSave(SpinRandomQueAnsSave spinRandomQueAnsSave)
        {
            try
            {
                SqlParameter[] parameter = {
                new SqlParameter("@Questionid", spinRandomQueAnsSave.QuestionId),
                new SqlParameter("@AnswerId", spinRandomQueAnsSave.ansId),
                new SqlParameter("@Answer", spinRandomQueAnsSave.answer),
                new SqlParameter("@UserId", spinRandomQueAnsSave.UserId)
            };
                long i = SqlCom.ExecuteNonQuery("insert into SpinRandomQueSave(Questionid,AnswerId,Answer,UserId) values(@Questionid,@AnswerId,@Answer,@UserId)", parameter);
                return Convert.ToInt32(i);
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public int notificationRemove(int NotificationId)
        {
            try
            {
                SqlParameter[] parameter = {
                new SqlParameter("@Id",NotificationId),
            };
                long i = SqlCom.ExecuteNonQuery("update DailyFeeds set IsStart=0  where Id=@Id", parameter);
                return Convert.ToInt32(i);
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public int notificationStar(int NotificationId)
        {
            try
            {
                SqlParameter[] parameter = {
                new SqlParameter("@Id",NotificationId),
            };
                long i = SqlCom.ExecuteNonQuery("update DailyFeeds set IsStart=1 where Id=@Id", parameter);
                return Convert.ToInt32(i);
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public int notificationReport(NotificationReport notificationReport)
        {
            try
            {
                SqlParameter[] parameter = {
                new SqlParameter("@UserId",notificationReport.UserId),
                 new SqlParameter("@NotificationId",notificationReport.NotificationId),
                  new SqlParameter("@report",notificationReport.report),
            };
                long i = SqlCom.ExecuteNonQuery("insert into NotificationReport(UserId,NotificationId,report) values (@UserId,@NotificationId,@report)", parameter);
                return Convert.ToInt32(i);
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public Tuple<int, long> postSavereferalCodeMobile(string obj, string email)
        {
            int status = 0;
            long userId = 0;
            try
            {
                SqlParameter[] parameter = {
                new SqlParameter("@EmailId", email),
                new SqlParameter("@referalCode", obj)
            };
                DataTable dt = SqlCom.Datatable_Sp("Usp_SaveReferalCodeMobile", parameter);
                if(dt.Rows.Count>0)
                {
                    status = Convert.ToInt32(dt.Rows[0][0].ToString());
                    userId = Convert.ToInt64(dt.Rows[0][1].ToString());
                }
                return new Tuple<int, long>(status, userId);
            }
            catch (Exception ex)
            {
                return new Tuple<int, long>(status, userId);
            }
        }

        public int postSavereferalCodeMobilePoints(string obj, string email)
        {
            
            try
            {
                SqlParameter[] parameter = {
                new SqlParameter("@EmailId", email),
                new SqlParameter("@referalCode", obj)
            };
                long i = SqlCom.ExecuteScaler_SP("Usp_SaveReferalCodeMobilePointsSave", parameter);
                
                return Convert.ToInt32(i);
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        public DataTable TangoProcess()
        {

            DataTable dt = SqlCom.Datatable("select FirstName,LastName,EmailAddress,  Id,Payment_Type as Type,Fk_UserId,MobileNo,Point,RequestedAmount,Isnull(PGId,'NA') as PGId ,CreatedDate from PaymentCookies inner join UserMaster on UserMaster.UserID=PaymentCookies.Fk_UserId where Status=1 and IsActive=1 and IsVerify=1 ", null);
            return
                  dt;
        }

        public int PaymentGatewaysLogs(PaymentGatewaysLogs paymentGatewaysLogs)
        {
            SqlParameter[] parameter = {
                new SqlParameter("@Fk_PaymentgatewayId",paymentGatewaysLogs.Fk_PaymentgatewayId),
                new SqlParameter("@Fk_UserId",paymentGatewaysLogs.Fk_UserId),
                new SqlParameter("@Request_Type",paymentGatewaysLogs.Request_Type),
                new SqlParameter("@Points",paymentGatewaysLogs.Points),
                new SqlParameter("@Amount",paymentGatewaysLogs.RequestedAmount),
                new SqlParameter("@RequestJson",paymentGatewaysLogs.RequestJson),
                new SqlParameter("@ResponseJson",paymentGatewaysLogs.ResponseJson),
                new SqlParameter("@ResponseStatus",paymentGatewaysLogs.ResponseStatus),
                new SqlParameter("@PaymentCookiesId",paymentGatewaysLogs.paymentCookiesId),

            };
            long i = SqlCom.ExecuteNonQuery_Sp("UspPaymentGatewaysLogs", parameter);
            return Convert.ToInt32(i);
        }

        public int PaymentGatewaysErrorLogs(PaymentGatewaysErrorLogs paymentGatewaysLogs)
        {
            SqlParameter[] parameter = {
                new SqlParameter("@Fk_PaymentgatewayId",paymentGatewaysLogs.Fk_PaymentgatewayId),
                new SqlParameter("@Fk_UserId",paymentGatewaysLogs.Fk_UserId),
                new SqlParameter("@Request_Type",paymentGatewaysLogs.Request_Type),
                new SqlParameter("@Points",paymentGatewaysLogs.Points),
                new SqlParameter("@Amount",paymentGatewaysLogs.RequestedAmount),
                new SqlParameter("@RequestJson",paymentGatewaysLogs.RequestJson),
                new SqlParameter("@ResponseJson",paymentGatewaysLogs.ResponseJson),
                new SqlParameter("@ResponseStatus",paymentGatewaysLogs.ResponseStatus),
                new SqlParameter("@PaymentCookiesId",paymentGatewaysLogs.paymentCookiesId),
                  new SqlParameter("@Error",paymentGatewaysLogs.Error),

            };
            long i = SqlCom.ExecuteNonQuery_Sp("UspPaymentGatewaysErrorLogs", parameter);
            return Convert.ToInt32(i);
        }

    }
}
