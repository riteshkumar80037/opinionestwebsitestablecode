﻿using DataBaseAccess.Interface;
using Insight.Database;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace DataBaseAccess.Defination
{
    public class ServiceDataAccessExtraActivity : IDataAccessExtraActivity
    {
        SqlConnection Conn;
        SQLCommonMethod SqlCom;
        public ServiceDataAccessExtraActivity(SqlConnection _Conn)
        {
            Conn = new SqlConnection();
            Conn = _Conn;
            SqlCom = new SQLCommonMethod(Conn);
        }

        public bool MailOpenSite(long Id)
        {
            bool status = false;
            try
            {
                using (SqlConnection Db = new SqlConnection(Conn.ConnectionString))
                {
                    //int count = Conn.QuerySql<int>("exec MailOpenCount @Tforid", new
                    //{
                    //    Tforid = Id
                    //}).FirstOrDefault();
                    int count = Conn.ExecuteSql("insert into SurveyTempSurvey (UserId) values(1)");
                    status = count > 0 ? true : false;
                }
            }
            catch (Exception ex)
            {

            }
            return status;
        }

        public bool MailOpenCint(string date)
        {
            bool status = false;
            using (SqlConnection Db = new SqlConnection(Conn.ConnectionString))
            {
                // int count=  Conn.ExecuteSql(@"update Cint_Survey.dbo.SurveyCintEmails set SurveyEmailOpens=coalesce(SurveyEmailOpens,0)+1 where Convert(date,SentOn)='" + date + "'");
                int count = Conn.ExecuteSql("insert into SurveyTempSurvey (UserId) values(2)");
                status = count > 1 ? true : false;
            }
            return status;
        }

        public bool SaveIpAddress(long UserId,string IpAddress)
        {
            bool status = false;
            try
            {
                using (SqlConnection Db = new SqlConnection(Conn.ConnectionString))
                {
                    string Ipaddress = Conn.QuerySql<string>(@"select IpAddress from usermaster where userid=@Userid", new
                    {
                        Userid = UserId
                    }).FirstOrDefault();
                    if(string.IsNullOrEmpty(Ipaddress))
                    {
                        int count = Conn.ExecuteSql(@"update usermaster set IpAddress=@IpAddress where userid=@userid", new
                        {
                            userid = UserId,
                            IpAddress = IpAddress
                        });
                        status = count > 0 ? true : false;
                    }

                }
            }
            catch(Exception ex)
            {

            }
            return status;
        }

       
    }
}
