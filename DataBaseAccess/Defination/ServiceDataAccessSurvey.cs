﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using BusinessEntites;
using DataBaseAccess.Interface;

namespace DataBaseAccess.Defination
{
    public class ServiceDataAccessSurvey:IDataAccessSurvey
    {
        SqlConnection Conn;
        SQLCommonMethod SqlCom;
        public ServiceDataAccessSurvey(SqlConnection _Conn)
        {
            Conn = new SqlConnection();
            Conn = _Conn;
            SqlCom = new SQLCommonMethod(Conn);
        }

        public int addForumQuestion(QuestionEntites questionEntites)
        {
            long i = 0;
            foreach (var v in questionEntites.QuestionList)
            {
                try
                {
                    SqlParameter[] parameter = {
                new SqlParameter("@Type", v.Type),
                new SqlParameter("@Mandatory", v.Mandatory),
                new SqlParameter("@Logical", v.Logical),
                new SqlParameter("@Question", v.Question),
                 new SqlParameter("@SerialNo", v.SerialNo),
                 new SqlParameter("@IfYes1", v.IfYes1),
                 new SqlParameter("@IfNo1", v.IfNo1),
                 new SqlParameter("@DependentYes1", v.DependentYes1),
                 new SqlParameter("@userId", questionEntites.UserId),
                 new SqlParameter("@ForumId", questionEntites.FourmId),
                };
                     i = SqlCom.ExecuteScaler_SP("Sp_Fourm_Question", parameter);
                      foreach(var v1 in v.AnswerListEntites)
                    {
                        SqlParameter[] parameterAns = {             
                 new SqlParameter("@QuestionId", i),
                 new SqlParameter("@Answer", v1.Answer),
                 new SqlParameter("@UserId", questionEntites.UserId),
                 new SqlParameter("@SurveyId", questionEntites.FourmId),
                };
                        long j = SqlCom.ExecuteNonQuery_Sp("Sp_Fourm_Answer", parameterAns);

                    }
                    
                }
                catch (Exception ex)
                {
                    return 0;
                }
                
            }
            return Convert.ToInt32(i);
        }

        public int addSurvey(ForumCreate forumCreate)
        {
            try
            {
                SqlParameter[] parameter = {
                new SqlParameter("@UserId", forumCreate.UserId),
                new SqlParameter("@SurveyName", forumCreate.SurveyName),
                new SqlParameter("@SurveyDescrition", forumCreate.SurveyDescription),
                new SqlParameter("@SurveyLimit", forumCreate.SurveyLimit),
            };
                long i = SqlCom.ExecuteScaler_SP("Sp_Fourm_Create", parameter);
                return Convert.ToInt32(i);
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public DataTable FromsCount(long UserId, long Countryid)
        {
            DataTable Dt = new DataTable();
            try
            {
                SqlParameter[] parameter = {
                new SqlParameter("@UserId",UserId)};
                Dt = SqlCom.Datatable_Sp("Usp_SurveyCount", parameter);
                return Dt;

            }
            catch (Exception Ex)
            {
                return Dt;
            }
        }

        public DataTable MySurveylist(long UserId)
        {
            //String Query = "select  case when  Status=1 then 'Pending' when status=-1 then 'Rejected' When Status = 1 then 'Pause' else 'Approved'  END as Status,isnull(Remarks, 'N/A') as Remarks,Date,SurveyName,SurveyDescription,Id,IsNull(SurveyLimit, 0) as SurveyLimit,(select count(*) as TotalClick from(select Distinct UserId, SurveyId from SurveyUserAnswer where SurveyId = SurveyDetailsStart.Id) as TotalClick) as TotalClick from[dbo].[SurveyDetailsStart]where UserId = @UserId ORDER BY id" ;
            //String Query = "select  case when  Status=1 then 'Pending' when status=-1 then 'Rejected' When Status = 1 then 'Pause' else 'Approved'  END as Status,isnull(Remarks, 'N/A') as Remarks,Date,SurveyName,SurveyDescription,Id,IsNull(SurveyLimit, 0) as SurveyLimit,(select count(*) as TotalClick from(select Distinct UserId, SurveyId from SurveyUserAnswer where SurveyId = SurveyDetailsStart.Id) as TotalClick) as TotalClick from[dbo].[SurveyDetailsStart]where UserId = @UserId ORDER BY id";
            String Query = @"With Query as (
                    select  case when  Status=1 then 'Pending' when status=-1 then 'Rejected' When Status = 2 then 'Approved' else 'Released' 
                    END as Status,isnull(Remarks, 'N/A') as Remarks,Date,SurveyName,SurveyDescription,Id,IsNull(SurveyLimit, 0)
                    as SurveyLimit,(select count(*) as TotalClick from(select Distinct UserId, SurveyId from SurveyUserAnswer
                    where SurveyId = SurveyDetailsStart.Id) as TotalClick) as TotalClick,IsNull(SurveyDetailsStart.Url,'N/A') as Url  from[dbo].[SurveyDetailsStart] where
                    UserId = @UserId
	                    )
	                Select Row_Number() over (order by Query.iD desc) AS RowNo, Query.Status,Query.Remarks,Query.Date,Query.SurveyName,
	                Query.SurveyDescription,Query.Id,Query.SurveyLimit,Convert(varchar(20),Query.TotalClick)+'/'+ Convert(varchar(20),Query.SurveyLimit) 
	                as Limit,Query.TotalClick,(Query.TotalClick*100/Query.SurveyLimit) as Percentage,Query.Url 
	                from Query";
            DataTable Dt = new DataTable();
            try
            {
                SqlParameter[] parameter = {
                new SqlParameter("@UserId",UserId), };
                Dt = SqlCom.Datatable(Query, parameter);
                return Dt;

            }
            catch (Exception Ex)
            {
                return Dt;
            }
        }
        public DataTable OtherSurveylist(long UserId,int CountryId)
        {
            DataTable Dt = new DataTable();
            try
            {
                SqlParameter[] parameter = {
                new SqlParameter("@UserId",UserId),
                new SqlParameter("@CountryId",CountryId), };
                Dt = SqlCom.Datatable_Sp("Usp_GetOherSurvey", parameter);
                return Dt;

            }
            catch (Exception Ex)
            {
                return Dt;
            }
        }

        public DataTable SurveryAnswerResutl(long QuestionId)
        {
            DataTable Dt = new DataTable();
            try
            {
                SqlParameter[] parameter = {
                new SqlParameter("@QuestionId",QuestionId) };
                Dt = SqlCom.Datatable_Sp("Usp_GetSurveyQuestionCount", parameter);
                return Dt;

            }
            catch (Exception Ex)
            {
                return Dt;
            }
        }

        public DataTable SurveyData(long surveyId)
        {
            DataTable Dt = new DataTable();
            try
            {
                SqlParameter[] parameter = {
                new SqlParameter("@SurveyId",surveyId)};
                Dt = SqlCom.Datatable_Sp("Usp_SurveyQuestionDetails", parameter);
                return Dt;

            }
            catch (Exception Ex)
            {
                return Dt;
            }
        }
    }
}
