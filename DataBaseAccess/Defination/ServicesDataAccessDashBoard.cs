﻿using DataBaseAccess.Interface;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace DataBaseAccess.Defination
{
    public class ServicesDataAccessDashBoard : IDataAccessDashBoard
    {
        SqlConnection Conn;
        SQLCommonMethod SqlCom;
        public ServicesDataAccessDashBoard(SqlConnection _Conn)
        {
            Conn = new SqlConnection();
            Conn = _Conn;
            SqlCom = new SQLCommonMethod(Conn);
        }
        public DataSet GetDashBorad(long UserId)
        {
            try
            {
                SqlParameter[] parameter = {
                    new SqlParameter ("@UserId",UserId)
                };
                return SqlCom.DataSet_Sp("Opinionest_Dashboard_Points", parameter);

            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
