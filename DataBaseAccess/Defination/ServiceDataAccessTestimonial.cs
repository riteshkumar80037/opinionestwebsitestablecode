﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using BusinessEntites;
using DataBaseAccess.Interface;

namespace DataBaseAccess.Defination
{
   public class ServiceDataAccessTestimonial: IDataAccessTestimonial
    {
        SqlConnection Conn;
        SQLCommonMethod SqlCom;

        public ServiceDataAccessTestimonial(SqlConnection _Conn)
        {
            Conn = new SqlConnection();
            Conn = _Conn;
            SqlCom = new SQLCommonMethod(Conn);
        }
        public DataTable GetTestimoniallist()
        {
            DataTable Dt = new DataTable();
            try
            {
                SqlParameter[] parameter = {};
                Dt = SqlCom.Datatable_Sp("USP_getTestimonialList");
                return Dt;
            }
            catch (Exception Ex)
            {
                return Dt;
            }
        }
        public int AddTestimonial(GetTestimonialEntites getTestimonial)
        {
            try
            {
                SqlParameter[] parameter = {
                new SqlParameter("@userid",getTestimonial.UserId),
                new SqlParameter("@message",getTestimonial.Message),
                new SqlParameter("@image",getTestimonial.Image)};
                var I =SqlCom.ExecuteNonQuery_Sp("USP_InsertTestimonial", parameter);
                return Convert.ToInt32(I);
            }
            catch (Exception Ex)
            {
                return -1;
            }
        }
    }
}
