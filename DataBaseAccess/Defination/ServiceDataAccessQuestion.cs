﻿using BusinessEntites;
using DataBaseAccess.Interface;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace DataBaseAccess.Defination
{
   public class ServiceDataAccessQuestion: IDataAccessQuestion
    {
        SqlConnection Conn;
        SQLCommonMethod SqlCom;
        public ServiceDataAccessQuestion(SqlConnection _Conn)
        {
            Conn = new SqlConnection();
            Conn = _Conn;
            SqlCom = new SQLCommonMethod(Conn);
        }

        public DataTable GetOptionalQuestion(OptionalRequestEntites obj, long UserId, bool MandatoryType)
        {
            try
            {
                SqlParameter[] parameter = {
                    new SqlParameter ("@UserId",UserId),
                    new SqlParameter ("@IsMandatory",MandatoryType),
                    new SqlParameter ("@GPCID",obj.GPCID)
                    
                };
                return SqlCom.Datatable_Sp("Proc_GetOptionalQuestionsAnswers", parameter);

            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public long SaveAnswer(OptionalRequestEntites obj, long UserId, bool MandatoryType)
        {
            long i = 0;
            try
            {
                SqlParameter[] parameter = {
                    new SqlParameter ("@UserID",UserId),
                    new SqlParameter ("@GPCID",obj.GPCID),
                    new SqlParameter ("@ProfileQuestionsID",obj.ProfileQuestionId),
                    new SqlParameter ("@ProfileAnswerID",obj.ProfileAnswerId),
                    new SqlParameter ("@ProfileAnswers",""),
                    new SqlParameter ("@MandatoryType",MandatoryType)
                };
                 i= SqlCom.ExecuteScaler_SP("sp_newanswer2", parameter);
                return i;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public DataTable GetProfileOptionalQuestion(long UserId)
        {
            try
            {
                SqlParameter[] parameter = {
                    new SqlParameter ("@UserId",UserId)
                };
                return SqlCom.Datatable_Sp("sp_ProfileCategory", parameter);

            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public long SavePoints(long UserId, int PointType, long QuestionId, long ForId)
        {
            long P = 0;
            long i = 0;
            try
            {
                SqlParameter[] parameter = {
                    new SqlParameter ("@ProfileQuestionsId",QuestionId)
                    
                };
                P = SqlCom.ExecuteScaler("select Points from  [dbo].[ProfileQuestions] where ProfileQuestionsId=@ProfileQuestionsId", parameter);
                SqlParameter[] Saveparameter = {
                    new SqlParameter ("@UserId",UserId),
                    new SqlParameter ("@PointType",PointType),
                    new SqlParameter ("@Points",P),
                    new SqlParameter ("@ForId",ForId),
                    new SqlParameter ("@Transection_Date",System.DateTime.Now),

                };
                i = SqlCom.ExecuteNonQuery(@"INSERT INTO Point_Transection
                                                                  (Point
                                                                  ,Point_Type
                                                                   ,Fk_Userid
                                                                  ,Trans_Date
                                                               ,For_Id)
                                                                         VALUES
                                                                  (@Points
                                                                  ,@PointType
                                                                   ,@UserId
                                                                   ,@Transection_Date
                                                                   ,@ForId

                                                                  );", Saveparameter);
                if(i==1)
                {
                    return P;
                }
                else
                {
                    return 0;
                }
                
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public DataTable GetProfileQuestionCount(long UserId)
        {
            try
            {
                SqlParameter[] parameter = {
                    new SqlParameter ("@UserId",UserId)
                };
                return SqlCom.Datatable_Sp("opinionset_Profile_Question_Count", parameter);

            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public DataTable GetOptionalQuestionCategory(long UserId)
        {
            try
            {
                SqlParameter[] parameter = {
                    new SqlParameter ("@UserId",UserId)
                };
                return SqlCom.Datatable_Sp("sp_OptionQuestionCategory", parameter);

            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public DataTable GetQuestionTotal(long userid, int type)
        {
            try
            {
                SqlParameter[] parameter = {
                    new SqlParameter ("@UserId",userid),
                    new SqlParameter ("@Type",type),
                };
                return SqlCom.Datatable_Sp("Get_Mandatory_Total_Pending", parameter);

            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
