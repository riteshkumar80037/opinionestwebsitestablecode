﻿using DataBaseAccess.Interface;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace DataBaseAccess.Defination
{
   public class ServicesDataAccessCommon : IDataAccessCommon
    {
        SqlConnection Conn;
        SQLCommonMethod SqlCom;

        public ServicesDataAccessCommon(SqlConnection _Conn)
        {
            Conn = new SqlConnection();
            Conn = _Conn;
            SqlCom = new SQLCommonMethod(Conn);
        }
        public DataTable StateData(long CountryId)
        {
            DataTable Dt = new DataTable();
            try
            {
                SqlParameter[] parameter = {
                new SqlParameter("CountryId",CountryId) };
                Dt = SqlCom.Datatable("select StateName,StateId,CountryId from [dbo].[StateMaster] where CountryId=@CountryId  and IsActive<>0 and IsDelete<>0", parameter);
                return Dt;

            }
            catch (Exception Ex)
            {
                return Dt;
            }
        }

        public DataTable CountryData()
        {
            DataTable Dt = new DataTable();
            try
            {
                SqlParameter[] parameter = {};
                Dt = SqlCom.Datatable("select CountryID,CountryName,SortName from [dbo].[CountryMaster] where IsActive<>0 and IsDelete<>0 and countryName is not null", parameter);
                return Dt;

            }
            catch (Exception Ex)
            {
                return Dt;
            }
        }

        public bool ValidUser(long userID)
        {
            DataTable Dt = new DataTable();
            try
            {
                SqlParameter[] parameter = {
                new SqlParameter("@userid",userID) };
                long i = SqlCom.ExecuteScaler("select UserID FROM UserMaster  where UserID=@userid and IsActive=1 and IsDelete=1", parameter);
                if(i>0)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception Ex)
            {
                return false;
            }
        }

        public bool mobileNoValidate(string mobileNo)
        {
            DataTable Dt = new DataTable();
            try
            {
                SqlParameter[] parameter = {
                new SqlParameter("@PhoneNumber",mobileNo) };
                long i = SqlCom.ExecuteScaler("select UserID FROM UserMaster  where PhoneNumber=@PhoneNumber and IsActive=1 and IsDelete=1 and IsVerify=1", parameter);
                if (i > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception Ex)
            {
                return false;
            }
        }
    }
}
