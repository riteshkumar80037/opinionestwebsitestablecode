﻿using BusinessEntites;
using DataBaseAccess.Interface;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace DataBaseAccess.Defination
{
    public class ServiceDataAccessOffer: IDataAccessOffer
    {
        SqlConnection Conn;
        SQLCommonMethod SqlCom;

        public ServiceDataAccessOffer(SqlConnection _Conn)
        {
            Conn = new SqlConnection();
            Conn = _Conn;
            SqlCom = new SQLCommonMethod(Conn);
        }

        public DataTable OfferList(long UserId,string offerType, int? pageindex, int pagesize)
        {
            DataTable Dt = new DataTable();
            try
            {
                SqlParameter[] parameter = {
                new SqlParameter("@USERID",UserId),
                new SqlParameter("@OFFERTYPE",offerType),
                new SqlParameter("@startRowIndex",pageindex),
                new SqlParameter("@pageSize",pagesize), };
                Dt = SqlCom.Datatable_Sp("GETOFFERLIST", parameter);
                return Dt;

            }
            catch (Exception Ex)
            {
                return Dt;
            }
        }

        /// <summary>
        /// Update User Points
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public string UpdatePoints(PointsUpdateInputModel model)   
        {
            string result = string.Empty;
            try
            {
                SqlParameter[] parameter = {
                    new SqlParameter ("@USERID",model.UserId),
                    new SqlParameter ("@DED_Amount",model.DeductionPoints),
                    new SqlParameter ("@FK_OFFER_ID",model.OfferId),
                    new SqlParameter ("@payment_type",2),
                    new SqlParameter ("@Fk_PaymentGetwayId",model.Fk_PaymentGetwayId),
                    new SqlParameter ("@utId",model.utid),
                };
                result = SqlCom.Datatable_Sp("USP_UPDATE_USER_POINTS", parameter).Rows[0][0].ToString();
                return result;
            }
            catch (Exception ex)
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Update User Points
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public long MarkOfferFavourite(MarkOfferFavouriteInputModel model)
        {
            long i = 0;
            try
            {
                SqlParameter[] parameter = {
                    new SqlParameter ("@FK_USERID",model.UserId),
                    new SqlParameter ("@IS_FAVOURITE",model.IsFavourite),
                    new SqlParameter ("@FK_OFFER_ID",model.OfferId),
                    new SqlParameter ("@TangoId",model.TangoFavtId),
                };
                i = SqlCom.ExecuteScaler_SP("USP_MARK_OFFER_FAVOURITE", parameter);
                return i;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        /// <summary>
        /// Offer History Details
        /// </summary>
        /// <param name="UserId"></param>
        /// <param name="offerType"></param>
        /// <param name="pageindex"></param>
        /// <param name="pagesize"></param>
        /// <returns></returns>
        public DataTable OfferHistoryDetails(long UserId, string offerType, int? pageindex, int pagesize)
        {
            DataTable Dt = new DataTable();
            try
            {
                SqlParameter[] parameter = {
                new SqlParameter("@USERID",UserId),
                new SqlParameter("@OFFER_TYPE",offerType),
                new SqlParameter("@STARTROWINDEX",pageindex),
                new SqlParameter("@PAGESIZE",pagesize), };
                Dt = SqlCom.Datatable_Sp("GET_OFFER_TRANSACTION_DETAILS_USER_WISE", parameter);
                return Dt;

            }
            catch (Exception Ex)
            {
                return Dt;
            }
        }

        /// <summary>
        /// UserRedeemRequestModel
        /// </summary>
        /// <param name="CountryId"></param>
        /// <returns></returns>
        public DataTable CashVoucherDetails(long CountryId)
        {
            DataTable Dt = new DataTable();
            try
            {
                SqlParameter[] parameter = {
                new SqlParameter("@CountryId",CountryId) };
                Dt = SqlCom.Datatable_Sp("GetUserPaymentGatewayDetails", parameter);
                return Dt;

            }
            catch (Exception Ex)
            {
                return Dt;
            }
        }

        /// <summary>
        /// UserRedeemRequestModel
        /// </summary>
        /// <param name="CountryId"></param>
        /// <returns></returns>
        public DataTable CheckUserAccountLinked(long userId, long paymentGatewayId)
        {
            DataTable Dt = new DataTable();
            try
            {
                SqlParameter[] parameter = {
                new SqlParameter("@userId",userId),
                new SqlParameter("@paymentGatewayId",paymentGatewayId)
                };
                Dt = SqlCom.Datatable("select LinkedAddress,FK_PaymentGatewayId [PaymentGatewayId],Isverify  from UserLinkedEwallettbl where Fk_UserId=@userId and FK_PaymentGatewayId=@paymentGatewayId", parameter);
                return Dt;

            }
            catch (Exception Ex)
            {
                return Dt;
            }
        }

        /// <summary>
        /// Link User Wallet
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public long LinkUserWallet(UserLinkWalletInputModel model)
        {
            long i = 0;
            try
            {
                SqlParameter[] parameter = {
                    new SqlParameter ("@Fk_UserId",model.UserId),
                    new SqlParameter ("@FK_PaymentGatewayId",model.PaymentGatewayId),
                    new SqlParameter ("@LinkedAddress",string.IsNullOrEmpty(model.MobileNumber) ? model.EmailAddress : model.MobileNumber)
                };
                i = SqlCom.ExecuteScaler_SP("LinkUserWallet", parameter);
                return i;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        /// <summary>
        /// GetUniqueNoForPaymentGateway
        /// </summary>
        /// <param name="PaymentGateway"></param>
        /// <returns></returns>
        public string GetUniqueNoForPaymentGateway(string PaymentGateway)
        {
            string result = string.Empty;
            try
            {
                
                SqlParameter[] parameter = {
                    new SqlParameter ("@PaymentGatewayPrefix",PaymentGateway)
                };
                result = SqlCom.Datatable("select dbo.GenrateUniqueNoFor_PaymentGateways(@PaymentGatewayPrefix)", parameter).Rows[0][0].ToString();
                return result;
            }
            catch (Exception ex)
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Save Payment Gateway Logs
        /// </summary>
        /// <param name="model"></param>
        public long SavePaymentGatewayLogs(PaytmApiModel model)
        {
            long i = 0;
            try
            {
                SqlParameter[] parameter = {
                    new SqlParameter ("@PaymentGatewayId",model.Id),
                    new SqlParameter ("@UserId",model.UserId),
                    new SqlParameter ("@Points",model.Points),
                    new SqlParameter ("@Request_Type",model.Request_Type),
                    new SqlParameter ("@Amount",model.Amount),
                    new SqlParameter ("@RequestJson",model.RequestJson),
                    new SqlParameter ("@ResponseJson",model.ResponseJson),
                    new SqlParameter ("@ResponseStatus",model.ResponseStatus),
                    new SqlParameter ("@PaytmCookiesId",model.PaytmCookiedId),
                    new SqlParameter ("@RequestedOn",Convert.ToDateTime(DateTime.Now, System.Globalization.CultureInfo.GetCultureInfo("en-GB").DateTimeFormat))
                };
                i = Convert.ToInt64(SqlCom.Datatable_Sp("usp_update_user_payment_gateway_log", parameter).Rows[0][0].ToString());
                return i;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        /// <summary>
        /// GetPointsValue
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public decimal GetPointsValue(long userId)
        {
            decimal i = 0;
            try
            {
                SqlParameter[] parameter = {
                    new SqlParameter ("@UserId",userId),
                };
                i = Convert.ToDecimal(SqlCom.Datatable("select PointValue from pointmaster where CountryId = (select CountryId from UserMaster where UserId = @UserId)", parameter).Rows[0][0].ToString());
                return i;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        /// <summary>
        /// TotalCashback
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public long TotalCashback(long userId)
        {
            long i = 0;
            try
            {
                SqlParameter[] parameter = {
                    new SqlParameter ("@UserId", userId),
                };
                i = Convert.ToInt64(SqlCom.Datatable_Sp("usp_get_totalcashback", parameter).Rows[0][0].ToString());
                return i;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public Tuple<string, decimal> convertPointsToCurrency(ConvertPointToCurrency obj, long UserId)
        {
            long i = 0;
            try
            {
                SqlParameter[] parameter = {
                    new SqlParameter ("@UserId", UserId),
                    new SqlParameter ("@Points", obj.Points),
                    new SqlParameter ("@currency", obj.Currency)
                };
                DataTable dt = SqlCom.Datatable_Sp("GetPointToCurrency", parameter);
               if(dt.Rows.Count>0)
                {
                    var points = Convert.ToDecimal(dt.Rows[0]["Points"].ToString());
                    var Message = dt.Rows[0]["Currency"].ToString();
                    return new Tuple<string, decimal>(Message, points);
                }
                return new Tuple<string, decimal>("", -2);
            }
            catch (Exception ex)
            {
                return new Tuple<string, decimal>("", -2);
            }
        }

        public DataTable OfferUserData(long UserId)
        {
            DataTable Dt = new DataTable();
            try
            {
                SqlParameter[] parameter = {
                new SqlParameter("@UserID",UserId)};
                Dt = SqlCom.Datatable_Sp("OfferUserData", parameter);
                return Dt;

            }
            catch (Exception Ex)
            {
                return Dt;
            }
        }

        public DataTable offerfevt(long UserId)
        {
            DataTable dt = new DataTable();
            
            try
            {
                SqlParameter[] parameter = {
                    new SqlParameter ("@UserId", UserId),
                };
                 dt = (SqlCom.Datatable("select TangoId from OFFER_USER_DETAILS where FK_USERID=@UserId and IS_FAVOURITE=1", parameter));
                return dt;
            }
            catch (Exception ex)
            {
                return dt;
            }
        }

        public long deletePointTransction(long TranID)
        {
            long ID = 0;

            try
            {
                SqlParameter[] parameter = {
                    new SqlParameter ("@Pk_id", TranID),
                };
                ID = (SqlCom.ExecuteScaler("delete from User_Point_Deductiontbl where Pk_id=@Pk_id", parameter));
                return ID;
            }
            catch (Exception ex)
            {
                return ID;
            }
        }

        public string tanglLogs(tangoLogs tangoLogs)
        {
            long ID = 0;

            try
            {
                SqlParameter[] parameter = {
                    new SqlParameter ("@UserId", tangoLogs.UserId),
                    new SqlParameter ("@Status", tangoLogs.Status),
                    new SqlParameter ("@RequestJson", tangoLogs.RequestJson),
                    new SqlParameter ("@ResponseJson", tangoLogs.ResponseJson),
                };
                ID = (SqlCom.ExecuteNonQuery("insert into TangoLogs(UserId,Status,RequestJson,ResponseJson) values(@UserId,@Status,@RequestJson ,@ResponseJson )", parameter));
                return ID.ToString();
            }
            catch (Exception ex)
            {
                return ID.ToString();
            }
        }

        public void DailyFeedSave(DailyFeedEntites dailyFeedEntites)
        {
            long ID = 0;

            try
            {
                SqlParameter[] parameter = {
                    new SqlParameter ("@Header", dailyFeedEntites.Header),
                    new SqlParameter ("@UserId", dailyFeedEntites.USERiD),
                    new SqlParameter ("@Message", dailyFeedEntites.Message),
                    new SqlParameter ("@IsActive", true),
                    new SqlParameter ("@IsDeleted", false),
                    new SqlParameter ("@Points", dailyFeedEntites.Points),
                    new SqlParameter ("@IsRead", false),
                    new SqlParameter ("@NotificationTypeId", 31),
                    new SqlParameter ("@OfferIcon", dailyFeedEntites.IconPath),
                    new SqlParameter ("@CreatedDate", System.DateTime.Now),
                    new SqlParameter ("@FromDt", System.DateTime.Now),
                    new SqlParameter ("@Todt", System.DateTime.Now),
                };
                ID = (SqlCom.ExecuteNonQuery("insert into DailyFeeds(Header,UserId,Message,IsActive,IsDeleted,Points,IsRead,NotificationTypeId,OfferIcon,CreatedDate,FromDt,Todt) values(@Header,@UserId,@Message ,@IsActive,@IsDeleted ,@Points,@IsRead,@NotificationTypeId,@OfferIcon,@CreatedDate,@FromDt,@Todt)", parameter));
                
            }
            catch (Exception ex)
            {
                
            }
        }

        public bool CheckExistVefication(long UserId, string phone)
        {
            DataTable dt = new DataTable();

            try
            {
                SqlParameter[] parameter = {
                    new SqlParameter ("@UserId", UserId),
                    new SqlParameter ("@LinkedAddress", phone),
                };
                dt = (SqlCom.Datatable("select top 1 * from UserLinkedEwallettbl  where Fk_UserId=@UserId and LinkedAddress=@LinkedAddress", parameter));
                if(dt.Rows.Count>0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public long SavePaytmVerifyNo(long userID,string Number)
        {
            try
            {
                SqlParameter[] parameter = {
                    new SqlParameter ("@Fk_UserId", userID),
                     new SqlParameter ("@LinkedAddress", Number),
                     new SqlParameter ("@Isverify", 1),
                };
                var result = SqlCom.ExecuteNonQuery("delete from  UserLinkedEwallettbl where Fk_UserId=@Fk_UserId; insert into UserLinkedEwallettbl(Fk_UserId,LinkedAddress,[Isverify ] ) values (@Fk_UserId,@LinkedAddress,@Isverify)", parameter);
            }
            catch(Exception ex)
            {

            }
            return 1;
        }

        public long PaymentCookiesSave(PaymentCookies paymentCookies)
        {
            try
            {
                SqlParameter[] parameter = {
                    new SqlParameter ("@Fk_UserId", paymentCookies.Fk_UserId),
                     new SqlParameter ("@MobileNo", paymentCookies.MobileNo),
                     new SqlParameter ("@Point", paymentCookies.Point),
                     new SqlParameter ("@RequestedAmount", paymentCookies.RequestedAmount),
                     new SqlParameter ("@PointsRedeem", paymentCookies.PointsRedeem),
                     new SqlParameter ("@PGId", paymentCookies.PGId),
                      new SqlParameter ("@Payment_Type", paymentCookies.payment_type),
                       new SqlParameter ("@Fk_PaymentGateway_Id", paymentCookies.Fk_PaymentGetwayId),
                };
                var result = SqlCom.ExecuteScaler_SP("USP_PaymentCookies_Save", parameter);

                return result;

            }
            catch (Exception ex)
            {
                return 0;
            }

        }

        public DataTable GetPaymentCookiesPending()
        {
            DataTable Dt = new DataTable();
            try
            {
                Dt = SqlCom.Datatable("select Id,Fk_UserId,MobileNo,RequestedAmount,PGId,Point from PaymentCookies where Status=1", null);
            }
            catch(Exception ex)
            {

            }
            return Dt;
        }

        public void SavePaytmTransactionUpdatation(long userid, long PaymentCookiesId, long PaymentGatewayLogId, long Points, decimal Amount)
        {
            
            try
            {
                SqlParameter[] parameter = {
                    new SqlParameter ("@UserId",userid),
                    new SqlParameter ("@PaymentCookiesId",PaymentCookiesId),
                    new SqlParameter ("@Points",Points),
                    new SqlParameter ("@PaymentGatewayLogId",PaymentGatewayLogId),
                    new SqlParameter ("@Amount",Amount),
                };
                 SqlCom.Datatable_Sp("SavePaytmTransaction", parameter);
                
            }
            catch (Exception ex)
            {
                
            }
        }

        public DataTable GetPaymentCookiesPendingPick()
        {
            DataTable Dt = new DataTable();
            try
            {
                Dt = SqlCom.Datatable("select Id,Fk_UserId,MobileNo,RequestedAmount,PGId,Point from PaymentCookies where Status=0; update PaymentCookies set Status=1 where  Status=0;", null);
            }
            catch (Exception ex)
            {

            }
            return Dt;
        }
    }
}
