﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using BusinessEntites;


namespace DataBaseAccess.Defination
{
    public class ServiceDataAccessLogin : IDataAccessLogin
    {
        SqlConnection Conn;
        SQLCommonMethod SqlCom;

        public ServiceDataAccessLogin(SqlConnection _Conn)
        {
            Conn = new SqlConnection();
            Conn = _Conn;
            SqlCom = new SQLCommonMethod(Conn);
        }
        public DataSet UserRegistation(UserEntites _obj)
        {
            try
            {

                SqlParameter[] parameter = {
           new SqlParameter("@EmailAddress",_obj.EmailAddress),new SqlParameter("@MacAddress",_obj.MacAddress),
           new SqlParameter("@UserTypeId",_obj.UserTypeId),new SqlParameter("@FirstName",_obj.FirstName),
           new SqlParameter("@LastName",_obj.LastName),new SqlParameter("@FullName",_obj.FullName),
           new SqlParameter("@Gender",_obj.Gender),new SqlParameter("@DateOfBirth",null),
           new SqlParameter("@PhoneNumber",_obj.PhoneNumber),new SqlParameter("@Inviter_Id",_obj.Inviter_Id),
           new SqlParameter("@Partner_Id",_obj.Partner_Id),new SqlParameter("@Partner_Params",_obj.Partner_Params),
           new SqlParameter("@Password",_obj.Password),new SqlParameter("@IsSocial",_obj.IsSocial),
           new SqlParameter("@ProfileImage",_obj.ProfileImage),new SqlParameter("@UserStateId",_obj.UserStateId),
           new SqlParameter("@IsVerify",_obj.IsVerify),new SqlParameter("@IsActive",_obj.IsActive),
           new SqlParameter("@IsDelete",_obj.IsDelete),new SqlParameter("@UserId",_obj.UserId),
           new SqlParameter("@IsMandatory",_obj.IsMandatory),new SqlParameter("@CountryID",_obj.CountryID),
           new SqlParameter("@StateId",_obj.StateID),new SqlParameter("@PinCode",_obj.Pincode),
           new SqlParameter("@FacebookId",_obj.FacebookId),new SqlParameter("@GooglePlusId",_obj.GooglePlusId),
           new SqlParameter("@CreatedOn",System.DateTime.UtcNow),new SqlParameter("@CreatedBy",1),
           new SqlParameter("@IpAddress",_obj.IPAddress),
           new SqlParameter("@DisplayName",_obj.DisplayName),new SqlParameter("@RegisteredName",_obj.RegisteredName),
           new SqlParameter("@DynamicStaticLink",_obj.DynamicStaticLink),new SqlParameter("@IsBackToRegister",_obj.IsBackToRegister),
           new SqlParameter("@IsProceedToOtherOffersRewards",_obj.IsProceedToOtherOffersRewards),new SqlParameter("@MacValue",1),new SqlParameter("@OTP",_obj.OTP),

                                     };


                DataSet Ds = SqlCom.DataSet_Sp("USP_USER_REGISTATION", parameter);
                return Ds;
            }
            catch (Exception Ex)
            {
                return null;
            }
        }
        public long UserEmailSubscrible(long UserId, bool EmailSubscrible, bool OfferEmailSubscription, bool? Cookies)
        {
            try
            {
                SqlParameter[] parameter = { new SqlParameter
                ("@UserID",UserId),
                new SqlParameter("@EmailSubscrible",EmailSubscrible),
                new SqlParameter ("@offerEmailUnsubscrib",OfferEmailSubscription),
                new SqlParameter ("@Cookies",Cookies)};
                return SqlCom.ExecuteNonQuery_Sp("uSP_EmailUnsubscrib", parameter);

            }
            catch (Exception Ex)
            {
                return -1;
            }
        }
        public DataTable GetUserEmailSubscrible(long UserId)
        {
            DataTable Dt = new DataTable();
            try
            {
                SqlParameter[] parameter = { new SqlParameter
                ("@UserID",UserId),};
                Dt = SqlCom.Datatable_Sp("USP_GetEmailSubscription", parameter);
                return Dt;

            }
            catch (Exception Ex)
            {
                return Dt;
            }
        }
        public DataSet UserPassword(string Email, string Where)
        {
            //EditUserEntites _obj = new UserEntiEditUserEntitestes();
            try
            {
                SqlParameter[] parameter = {
            new SqlParameter("@where",Where),
            new SqlParameter("@mode","select"),
            };
                DataSet Ds = SqlCom.DataSet_Sp("CRUDUsers", parameter);
                return Ds;
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        public DataTable Login(UseLoginEntites Obj, int UserType)
        {
            DataTable Dt = new DataTable();
            try
            {
                SqlParameter[] parameter = {
                new SqlParameter("@EmailId",Obj.EmailId),
                new SqlParameter("@UserType",UserType),
                new SqlParameter("@IsSocial",Obj.IsSocialLogin),
                new SqlParameter("@Password",Obj.Password),
                new SqlParameter("@facebookId",Obj.facebookId),
                new SqlParameter("@gmailId",Obj.gmailId),};
                Dt = SqlCom.Datatable_Sp("Usp_Login", parameter);
                return Dt;

            }
            catch (Exception Ex)
            {
                return Dt;
            }
        }

        public int DeleteUserAccount(long UserId)
        {
            try
            {
                SqlParameter[] parameter = { new SqlParameter
                ("@UserID",UserId)};
                long i = SqlCom.ExecuteNonQuery_Sp("uSP_DeleteUserAccount", parameter);
                return Convert.ToInt32(i);
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        public int OTPVerifiy(int Otp, string EmailId, int Minutes)
        {
            try
            {
                SqlParameter[] parameter = { new SqlParameter
                ("@EmailId",EmailId),};
                DataTable Dt = SqlCom.Datatable("select OTP,OtpStartTime from usermaster where EmailAddress=@EmailId and IsActive=1", parameter);
                if (Dt.Rows.Count > 0)
                {
                    int LastOtp = Convert.ToInt32(Dt.Rows[0]["OTP"]);
                    DateTime ExpireTime = Convert.ToDateTime(Dt.Rows[0]["OtpStartTime"]);
                    ExpireTime = ExpireTime.AddMinutes(Minutes);
                    if (Otp != LastOtp)
                    {
                        return -1;
                    }
                    else if (ExpireTime < System.DateTime.Now)
                    {
                        return -2;
                    }
                    else
                    {
                        SqlParameter[] parameter1 = { new SqlParameter
                        ("@EmailId",EmailId),};
                        SqlCom.ExecuteNonQuery("update usermaster set IsVerify=1 where EmailAddress=@EmailId and IsActive=1", parameter1);

                        return 1;
                    }
                }
                return 0;

            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public int RegenerateOtp(string OTP, string EmailId)
        {
            try
            {
                SqlParameter[] parameter = {
                    new SqlParameter("@EmailAddress",EmailId),
                   new SqlParameter("@Otp",OTP),
                new SqlParameter("@Datetime",System.DateTime.Now)};
                var i = SqlCom.ExecuteNonQuery("update usermaster set OTPStartTime=@Datetime,OTP=@Otp where EmailAddress=@EmailAddress and IsActive=1", parameter);
                return Convert.ToInt32(i);
            }
            catch (Exception ex)
            {
                return 0;
            }
        }


        public int resetPassword(resetpassword _obj)
        {
            try
            {
                SqlParameter[] parameter = { new SqlParameter
                ("@EmailId",_obj.emailId),};
                DataTable Dt = SqlCom.Datatable("select OTP,OtpStartTime from usermaster where EmailAddress=@EmailId and IsActive=1", parameter);
                if (Dt.Rows.Count > 0)
                {
                    int LastOtp = Convert.ToInt32(Dt.Rows[0]["OTP"]);
                    DateTime ExpireTime = Convert.ToDateTime(Dt.Rows[0]["OtpStartTime"]);
                    ExpireTime = ExpireTime.AddMinutes(_obj.minutes);
                    if (_obj.otp != LastOtp)
                    {
                        return -1;
                    }
                    else if (ExpireTime < System.DateTime.Now)
                    {
                        return -2;
                    }
                    else
                    {
                        SqlParameter[] parameter1 = { new SqlParameter
                        ("@EmailId",_obj.emailId),
                        new SqlParameter("@Password",_obj.password)};
                        SqlCom.ExecuteNonQuery("update usermaster set Password=@Password where EmailAddress=@EmailId and IsActive=1", parameter1);

                        return 1;
                    }
                }
                return -3;

            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public int CheckUser(string emailid)
        {
            long id = 0;
            try
            {
                SqlParameter[] parameter = { new SqlParameter
                ("@EmailId",emailid),};
                id = SqlCom.ExecuteScaler("select 1 from usermaster where EmailAddress=@EmailId and IsActive=1 and IsDelete=1", parameter);
                if (id>0)
                {
                    return Convert.ToInt32( id);
                }
                return 0;

            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public int CheckUserMobile(string mobileNo)
        {
            long id = 0;
            try
            {
                SqlParameter[] parameter = { new SqlParameter
                ("@PhoneNumber",mobileNo),};
                id = SqlCom.ExecuteScaler("select 1 from usermaster where PhoneNumber=@PhoneNumber and IsActive=1 and IsDelete=1", parameter);
                if (id > 0)
                {
                    return Convert.ToInt32(id);
                }
                return 0;

            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public int mobileOpt(string OTP, string mobile)
        {
            try
            {
                SqlParameter[] parameter = {
                    new SqlParameter("@mobile",mobile),
                   new SqlParameter("@Otp",OTP),
                new SqlParameter("@Datetime",System.DateTime.Now)};
                var i = SqlCom.ExecuteNonQuery("update usermaster set OTPStartTime=@Datetime,OTP=@Otp where PhoneNumber=@mobile and IsActive=1", parameter);
                return Convert.ToInt32(i);
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public long LoginEmail(UseLoginEntites Obj)
        {
            try
            {
                SqlParameter[] parameter1 = {
                    new SqlParameter("@EmailId",Obj.EmailId)};
                var j = SqlCom.ExecuteScaler("SELECT 1 FROM usermaster WHERE ( emailaddress = @EmailId OR PhoneNumber = @EmailId OR FullName = @EmailId ) ", parameter1);
                if(j==0)
                {
                    return 1;
                }
                else
                {
                    SqlParameter[] parameter = {
                    new SqlParameter("@EmailId",Obj.EmailId)};
                    var i = SqlCom.ExecuteScaler("SELECT UserId FROM usermaster WHERE ( emailaddress = @EmailId OR PhoneNumber = @EmailId OR FullName = @EmailId ) AND isactive = 1   AND isdelete = 1 AND IsVerify = 1  ", parameter);
                    return Convert.ToInt32(i);
                }
               
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        public DataSet UserRegistationMobile(UserEntites _obj)
        {
            try
            {

                SqlParameter[] parameter = {
           new SqlParameter("@EmailAddress",_obj.EmailAddress),new SqlParameter("@MacAddress",_obj.MacAddress),
           new SqlParameter("@UserTypeId",_obj.UserTypeId),new SqlParameter("@FirstName",_obj.FirstName),
           new SqlParameter("@LastName",_obj.LastName),new SqlParameter("@FullName",_obj.FullName),
           new SqlParameter("@Gender",_obj.Gender),new SqlParameter("@DateOfBirth",null),
           new SqlParameter("@PhoneNumber",_obj.PhoneNumber),new SqlParameter("@Inviter_Id",_obj.Inviter_Id),
           new SqlParameter("@Partner_Id",_obj.Partner_Id),new SqlParameter("@Partner_Params",_obj.Partner_Params),
           new SqlParameter("@Password",_obj.Password),new SqlParameter("@IsSocial",_obj.IsSocial),
           new SqlParameter("@ProfileImage",_obj.ProfileImage),new SqlParameter("@UserStateId",_obj.UserStateId),
           new SqlParameter("@IsVerify",_obj.IsVerify),new SqlParameter("@IsActive",_obj.IsActive),
           new SqlParameter("@IsDelete",_obj.IsDelete),new SqlParameter("@UserId",_obj.UserId),
           new SqlParameter("@IsMandatory",_obj.IsMandatory),new SqlParameter("@CountryID",_obj.CountryID),
           new SqlParameter("@StateId",_obj.StateID),new SqlParameter("@PinCode",_obj.Pincode),
           new SqlParameter("@FacebookId",_obj.FacebookId),new SqlParameter("@GooglePlusId",_obj.GooglePlusId),
           new SqlParameter("@CreatedOn",System.DateTime.UtcNow),new SqlParameter("@CreatedBy",1),
           new SqlParameter("@IpAddress",_obj.IPAddress),
           new SqlParameter("@DisplayName",_obj.DisplayName),new SqlParameter("@RegisteredName",_obj.RegisteredName),
           new SqlParameter("@DynamicStaticLink",_obj.DynamicStaticLink),new SqlParameter("@IsBackToRegister",_obj.IsBackToRegister),
           new SqlParameter("@IsProceedToOtherOffersRewards",_obj.IsProceedToOtherOffersRewards),new SqlParameter("@MacValue",1),new SqlParameter("@OTP",_obj.OTP),

                                     };


                DataSet Ds = SqlCom.DataSet_Sp("USP_USER_REGISTATION", parameter);
                return Ds;
            }
            catch (Exception Ex)
            {
                return null;
            }
        }

        public string userName(string emailId)
        {
            long id = 0;
            try
            {
                SqlParameter[] parameter = { new SqlParameter
                ("@EmailAddress",emailId),};
                DataTable dt = SqlCom.Datatable("select FullName from usermaster where EmailAddress=@EmailAddress and IsActive=1 and IsDelete=1", parameter);
                if (dt.Rows.Count > 0)
                {
                    return dt.Rows[0]["FullName"].ToString();
                }
                return "";

            }
            catch (Exception ex)
            {
                return "";
            }
        }
    }
}
