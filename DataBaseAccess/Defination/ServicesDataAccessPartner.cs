﻿using BusinessEntites;
using DataBaseAccess.Interface;
using Insight.Database;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace DataBaseAccess.Defination
{
    public class ServicesDataAccessPartner : IDataAccessPartner
    {

        SqlConnection Conn;
        SQLCommonMethod SqlCom;
        public ServicesDataAccessPartner(SqlConnection _Conn)
        {
            Conn = new SqlConnection();
            Conn = _Conn;
            SqlCom = new SQLCommonMethod(Conn);
        }
        public long GetPartnerId(string PartnerRef)
        {
            long PartnerId = 0;
            try
            {
                using (SqlConnection Db = new SqlConnection(Conn.ConnectionString))
                {
                    PartnerId = Db.QuerySql<long>(@"select Pk_Id  from RegReferPartner where UniqueNo=@UniqueNo", new
                    {
                        UniqueNo = PartnerRef
                    }).FirstOrDefault();
                }

            }
            catch (Exception ex)
            {

            }
            return PartnerId;

        }

        public PartnerEntities PartnerDetails(long PartnerId)
        {
            PartnerEntities Model = new PartnerEntities();
            try
            {
                using (Conn)
                {
                    Model = Conn.QuerySql<PartnerEntities>(@"SELECT  
                                                                   [VerificationUrl]
                                                              FROM [RegReferPartner] where PK_Id=@Id and IsDelete=1", new
                    {
                        Id = PartnerId
                    }).FirstOrDefault();

                    if (Model != null)
                    {
                        Model.URLKeyMapList = GetPartnerKeys(PartnerId);
                    }

                }
            }
            catch (Exception ex)
            {

            }
            return Model;
        }

        public List<PartnerKeyRelationEntities> GetPartnerKeys(long _PartnerId)
        {
            List<PartnerKeyRelationEntities> ModelList = new List<PartnerKeyRelationEntities>();
            try
            {
                using (Conn)
                {
                    ModelList = Conn.QuerySql<PartnerKeyRelationEntities>(@"SELECT row_number() over (order by PK_Id) as Row_No,[PK_Id] as Id
                                          ,[Fk_PartnerId] as PartnerId 
                                          ,[Query_Get_Key]
                                          ,[Query_Replace_ValueKey]
                                      FROM [RegReferPartner_UrlKeyRel]
                                      where isdelete=1 and Fk_PartnerId=@Id", new
                    {
                        Id = _PartnerId
                    }).ToList();

                }
            }
            catch (Exception ex)
            {
                
            }
            return ModelList;
        }
    }
}
