﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using BusinessEntites;
using DataBaseAccess.Interface;

namespace DataBaseAccess.Defination
{
    public class ServiceDataAccessHome : IDataAccessHome
    {
        SqlConnection Conn;
        SQLCommonMethod SqlCom;

        public ServiceDataAccessHome(SqlConnection _Conn)
        {
            Conn = new SqlConnection();
            Conn = _Conn;
            SqlCom = new SQLCommonMethod(Conn);
        }
        public DataTable GetBloglist(string Mode, int pageindex, int pagesize)
        {
            DataTable Dt = new DataTable();
            try
            {
                SqlParameter[] parameter = {
                new SqlParameter("@Mode",Mode),
                new SqlParameter("@startRowIndex",pageindex),
                new SqlParameter("@pageSize",pagesize), };
                Dt = SqlCom.Datatable_Sp("Usp_GetBlog", parameter);
                return Dt;

            }
            catch (Exception Ex)
            {
                return Dt;
            }
        }
        public DataTable GetBlogDetails(int blogId, int pageindex, int pagesize)
        {
            DataTable Dt = new DataTable();
            try
            {
                SqlParameter[] parameter = {
                new SqlParameter("@Id",blogId),
                new SqlParameter("@startRowIndex",pageindex),
                new SqlParameter("@pageSize",pagesize), };
                Dt = SqlCom.Datatable_Sp("Usp_GetBlog", parameter);
                return Dt;

            }
            catch (Exception Ex)
            {
                return Dt;
            }
        }

        public DataSet GetDailyFeed(long UserID, int pageNo, int pageSize, int? categoryId, bool? IsStart = false, string text="")
        {
            DataSet Ds = new DataSet();
            try
            {
                SqlParameter[] parameter = {                
                new SqlParameter("@Fk_Userid",UserID),
                new SqlParameter("@page",pageNo),
                new SqlParameter("@size",pageSize),
                new SqlParameter("@categoryid",categoryId),
                new SqlParameter("@IsStart",IsStart),
                new SqlParameter("@text",text),
                };

                Ds = SqlCom.DataSet_Sp("Usp_GetDailyFeeds", parameter);
                return Ds;

            }
            catch (Exception Ex)
            {
                return Ds;
            }
        }

        public DataTable BlogDetails(long Id)
        {
            DataTable Dt = new DataTable();
            try
            {
                //select replace(message,'src="','src="https://admin.opinionest.com') from Blogs where BlogsId = 31
                SqlParameter[] parameter = {
                new SqlParameter("@BlogsId",Id) };
                Dt = SqlCom.Datatable_Sp("Sp_BlogsDeatils", parameter);
                return Dt;

            }
            catch (Exception Ex)
            {
                return Dt;
            }

        }

        /// <summary>
        /// Slider List from DB
        /// </summary>
        /// <param name="_sliderType"></param>
        /// <returns></returns>
        public DataTable SliderList(int _sliderType)
        {
            DataTable Dt = new DataTable();
            try
            {
                SqlParameter[] parameter = {
                new SqlParameter("@Slider_Type", _sliderType) };
                Dt = SqlCom.Datatable("select FIRST_HEADER [FirstHeader],SECOND_HEADER [SecondHeader],[FILE_NAME] [FileName],FILE_PATH [FilePath],FILE_TYPE [FileType],SLIDER_TYPE [SliderType] from [dbo].[SLIDER] where SLIDER_TYPE=@Slider_Type", parameter);
                return Dt;

            }
            catch (Exception Ex)
            {
                return Dt;
            }

        }

        /// <summary>
        /// Daily Feed Details
        /// </summary>
        /// <param name="feedId"></param>
        /// <returns></returns>
        public DataTable DailyFeedDetails(int feedId)
        {
            DataTable Dt = new DataTable();
            try
            {
                SqlParameter[] parameter = {
                new SqlParameter("@Feed_Id",feedId) };
                Dt = SqlCom.Datatable(@"select DailyFeeds.Id,Header,UserId,Message,FromDt,IsNull(isCredit,1) as isCredit,Todt,cast(Time as varchar) Time,Isnull(Createdby,0) Createdby,CreatedDate,Isnull(ModifiedBy,0) ModifiedBy
                ,isnull(ModifiedDate,CreatedDate) ModifiedDate,IsActive,isnull(IsDeleted,0) IsDeleted,ISNULL( DailyFeeds.OfferIcon,NotificationType.IconPath)  as IconPath,
				NotificationType.Name as Category,Status,Class,NotificationCount,Link,isnull(filter_id,0) filter_id,BatchId,Points,IsRead from [dbo].[DailyFeeds] left join     
                        NotificationType on NotificationType.Id = DailyFeeds.NotificationTypeId where DailyFeeds.Id=@Feed_Id", parameter);
                return Dt;

            }
            catch (Exception Ex)
            {
                return Dt;
            }

        }

        /// <summary>
        /// Referral Details
        /// </summary>
        /// <param name="inviterId"></param>
        /// <returns></returns>
        public DataTable ReferralList(long inviterId)
        {
            DataTable Dt = new DataTable();
            try
            {
                SqlParameter[] parameter = {
                new SqlParameter("@Inviter_ID",inviterId) };
                Dt = SqlCom.Datatable(@"select isnull(usermaster.firstname,'')+ ' '+isnull(usermaster.LastName,'') as Name ,substring(usermaster.firstname,0,2)+substring(usermaster.lastName,0,2) as shortName,case when usermaster.IsVerify=0 then 'Accepted' else 'Verified' end Status,((select isnull(sum(point),0) from Point_Transection where Fk_Userid=@Inviter_ID and For_Id=usermaster.UserID and Point_Type=8)) Points,convert(varchar(20),CreatedOn,106) as CreatedDate from usermaster where inviter_id=@Inviter_ID", parameter);
                return Dt;

            }
            catch (Exception Ex)
            {
                return Dt;
            }

        }

        public long HomePageTotalPoints()
        {
           
            try
            {
                long Points = SqlCom.ExecuteScaler_SP(@"Usp_GetTotalHomePoints", null);
                return Points;

            }
            catch (Exception Ex)
            {
                return 0;
            }
        }

        public string ContactFormSubmission(ContactUsEntities model)
        {
            try
            {
                SqlParameter[] parameter1 = 
                {
                    new SqlParameter("@Contact_Name",model.ContactName),
                    new SqlParameter("@Contact_Email",model.ContactEmail),
                    new SqlParameter("@Category_Id",model.CategoryId),
                    new SqlParameter("@Query",model.Query),
                    new SqlParameter("@ContactNo",model.ContactNo)
                };

                string result = SqlCom.Datatable(@"Declare @Ref_No varchar(500) SET @Ref_No = (CAST(CAST(RAND() * (1000000+(DATEPART(ss, GETDATE()) * 100)) AS INT) AS VARCHAR))
                insert into ContactUs(Contact_Name,Contact_Email,Category_Id,Query,Reference_Number,ContactNo) values(@Contact_Name,@Contact_Email,@Category_Id,@Query,@Ref_No,@ContactNo)
				declare @id int
				select @id = SCOPE_IDENTITY()  
				select Reference_Number from ContactUs where Id= @id"
                , parameter1).Rows[0][0].ToString();
                return result;
            }
            catch (Exception Ex)
            {
                return "Error";
            }
        }

        /// <summary>
        /// Review List
        /// </summary>
        /// <returns></returns>
        public DataTable ReviewList(int pageindex, int pagesize)
        {
            DataTable Dt = new DataTable();
            try
            {
                SqlParameter[] parameter = {
                new SqlParameter("@startRowIndex",pageindex),
                new SqlParameter("@pageSize",pagesize), };
                Dt = SqlCom.Datatable_Sp("Usp_GetReviews", parameter);
                return Dt;

            }
            catch (Exception Ex)
            {
                return Dt;
            }

        }

        public DataTable BlogsTag(long blogsId)
        {
            DataTable Dt = new DataTable();
            try
            {
                SqlParameter[] parameter = {
                new SqlParameter("@BlogsID",blogsId) };
                Dt = SqlCom.Datatable("select Pk_Id,ISNULL(Tag,'N/A') as Tag,ISNULL(link,'N/A') as Link from BlogTags Where Fk_BlogId=@BlogsID and isactive=1", parameter);
                return Dt;

            }
            catch (Exception Ex)
            {
                return Dt;
            }
        }

        public DataTable BlogDetails(string header)
        {
            DataTable Dt = new DataTable();
            try
            {
                //select replace(message,'src="','src="https://admin.opinionest.com') from Blogs where BlogsId = 31
                SqlParameter[] parameter = {
                new SqlParameter("@Header",header) };
                Dt = SqlCom.Datatable_Sp("Sp_BlogsDeatils", parameter);
                return Dt;

            }
            catch (Exception Ex)
            {
                return Dt;
            }
        }

        public DataTable mobileFetatures(string code)
        {
            DataTable Dt = new DataTable();
            try
            {
                //select replace(message,'src="','src="https://admin.opinionest.com') from Blogs where BlogsId = 31
                SqlParameter[] parameter = {
                new SqlParameter("@code",code) };
                Dt = SqlCom.Datatable("select * from MobileFeatures where Code=@code ", parameter);
                return Dt;
            }

            catch (Exception Ex)
            {
                return Dt;
            }
        }
    }
}
