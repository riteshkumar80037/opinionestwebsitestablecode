﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace DataBaseAccess
{
    public class SQLCommonMethod
    {
        SqlConnection Connection;
        SqlDataAdapter Sda;
        SqlCommand Scmd;
        DataTable Dt;
        DataSet Ds;
        long i = 0;
        public SQLCommonMethod(SqlConnection Conn)
        {
            Connection = Conn;
            if(Connection.State==ConnectionState.Open)
            {
                Connection.Close();
            }
        }
        public DataTable Datatable(string Query, SqlParameter[] Parameter = null)
        {
            Dt = new DataTable();
            try
            {
                Sda = new SqlDataAdapter(Query, Connection);
                if (Parameter != null)
                {
                    foreach (var v in Parameter)
                    {
                        Sda.SelectCommand.Parameters.AddWithValue(v.ParameterName, v.Value);
                    }
                }
                Sda.Fill(Dt);

                return Dt;
            }
            catch (Exception Ex)
            {
                return Dt;
            }
        }
        public long ExecuteNonQuery(string Query, SqlParameter[] Parameter = null)
        {
            try
            {
                Scmd = new SqlCommand(Query, Connection);
                if (Parameter != null)
                {
                    foreach (var v in Parameter)
                    {
                        Scmd.Parameters.AddWithValue(v.ParameterName, v.Value == null ? "" : v.Value);
                    }
                }
                if (Connection.State == ConnectionState.Closed)
                {
                    Connection.Open();
                }
                i = Scmd.ExecuteNonQuery();
                Connection.Close();
                return i;
            }
            catch (Exception ex)
            {
                return -1;

            }
        }
        public long ExecuteScaler(string Query, SqlParameter[] Parameter = null)
        {
            try
            {
                Scmd = new SqlCommand(Query, Connection);
                if (Parameter != null)
                {
                    foreach (var v in Parameter)
                    {
                        Scmd.Parameters.AddWithValue(v.ParameterName, v.Value == null ? "" : v.Value);
                    }
                }
                if(Connection.State==ConnectionState.Closed)
                {
                    Connection.Open();
                }
               
                i = Convert.ToInt64(Scmd.ExecuteScalar());
                Connection.Close();
                return i;
            }
            catch (Exception ex)
            {
                return -1;
            }
        }
        public DataTable Datatable_Sp(string Query, SqlParameter[] Parameter = null)
        {
            Dt = new DataTable();
            try
            {
                Sda = new SqlDataAdapter(Query, Connection);
                Sda.SelectCommand.CommandType = CommandType.StoredProcedure;
                if (Parameter != null)
                {
                    foreach (var v in Parameter)
                    {
                        Sda.SelectCommand.Parameters.AddWithValue(v.ParameterName, v.Value);
                    }
                }
                Sda.Fill(Dt);
                return Dt;
            }
            catch (Exception Ex)
            {
                return Dt;
            }
        }
        public DataSet DataSet_Sp(string Query, SqlParameter[] Parameter = null)
        {
            Ds = new DataSet();
            try
            {
                Sda = new SqlDataAdapter(Query, Connection);
                Sda.SelectCommand.CommandType = CommandType.StoredProcedure;
                if (Parameter != null)
                {
                    foreach (var v in Parameter)
                    {
                        Sda.SelectCommand.Parameters.AddWithValue(v.ParameterName, v.Value);
                    }
                }
                Sda.Fill(Ds);
                return Ds;
            }
            catch (Exception Ex)
            {
                return Ds;
            }
        }

        public long ExecuteNonQuery_Sp(string Query, SqlParameter[] Parameter = null)
        {
            try
            {
                Scmd = new SqlCommand(Query, Connection);
                Scmd.CommandType = CommandType.StoredProcedure;
                if (Parameter != null)
                {
                    foreach (var v in Parameter)
                    {
                        Scmd.Parameters.AddWithValue(v.ParameterName, v.Value == null ? "" : v.Value);
                    }
                }

                if (Connection.State == ConnectionState.Closed)
                {
                    Connection.Open();
                }
                i = Scmd.ExecuteNonQuery();
                Connection.Close();
                return i;
            }
            catch (Exception ex)
            {
                return -1;

            }
        }
        public long ExecuteScaler_SP(string Query, SqlParameter[] Parameter = null)
        {
            try
            {
                Scmd = new SqlCommand(Query, Connection);
                Scmd.CommandType = CommandType.StoredProcedure;
                if (Parameter != null)
                {
                    foreach (var v in Parameter)
                    {
                        Scmd.Parameters.AddWithValue(v.ParameterName, v.Value == null ? "" : v.Value);
                    }
                }
                if (Connection.State == ConnectionState.Closed)
                {
                    Connection.Open();
                }
                
                    i = Convert.ToInt32(Scmd.ExecuteScalar());
                
                Connection.Close();
                return i;
            }
            catch (Exception ex)
            {
                return -1;
            }
        }
    }
}
