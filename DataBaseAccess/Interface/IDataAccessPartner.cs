﻿using BusinessEntites;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataBaseAccess.Interface
{
    public interface IDataAccessPartner
    {
        long GetPartnerId(string PartnerRef);

        PartnerEntities PartnerDetails(long PartnerId);
    }
}
