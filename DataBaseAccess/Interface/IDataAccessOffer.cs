﻿using BusinessEntites;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;


namespace DataBaseAccess.Interface
{
    public interface IDataAccessOffer
    {
        DataTable OfferList(long UserId, string offerType, int? pageindex, int pagesize);

        /// <summary>
        /// Update User Points
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        string UpdatePoints(PointsUpdateInputModel model);   

        /// <summary>
        /// Mark Offer Favourite
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        long MarkOfferFavourite(MarkOfferFavouriteInputModel model);

        /// <summary>
        /// Offer History Details
        /// </summary>
        /// <param name="UserId"></param>
        /// <param name="offerType"></param>
        /// <param name="pageindex"></param>
        /// <param name="pagesize"></param>
        /// <returns></returns>
        DataTable OfferHistoryDetails(long UserId, string offerType, int? pageindex, int pagesize);

        /// <summary>
        /// CashVoucherDetails
        /// </summary>
        /// <param name="CountryId"></param>
        /// <returns></returns>
        DataTable CashVoucherDetails(long CountryId);

        /// <summary>
        /// CashVoucherDetails
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="paymentGatewayId"></param>
        /// <returns></returns>
        DataTable CheckUserAccountLinked(long userId, long paymentGatewayId);

        /// <summary>
        /// Link User Wallet
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        long LinkUserWallet(UserLinkWalletInputModel model);

        /// <summary>
        /// GetUniqueNoForPaymentGateway
        /// </summary>
        /// <param name="PaymentGateway"></param>
        /// <returns></returns>
        string GetUniqueNoForPaymentGateway(string PaymentGateway);

        /// <summary>
        /// SavePaymentGatewayLogs
        /// </summary>
        /// <param name="model"></param>
        long SavePaymentGatewayLogs(PaytmApiModel model);
        long SavePaytmVerifyNo(long userID,string Number);
        void SavePaytmTransactionUpdatation(long userid, long PaymentCookiesId, long PaymentGatewayLogId, long Points, decimal Amount);

        /// <summary>
        /// GetPointsValue
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        decimal GetPointsValue(long userId);

        /// <summary>
        /// TotalCashback
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        long TotalCashback(long userId);
        Tuple<string, decimal> convertPointsToCurrency(ConvertPointToCurrency obj, long UserId);
        DataTable OfferUserData(long UserId);
        DataTable offerfevt(long UserId);
        long deletePointTransction(long TranID);
        string tanglLogs(tangoLogs tangoLogs);
        void DailyFeedSave(DailyFeedEntites dailyFeedEntites);
        bool CheckExistVefication(long UserId, string phone);
        long PaymentCookiesSave(PaymentCookies paymentCookies);
        DataTable GetPaymentCookiesPending();
        DataTable GetPaymentCookiesPendingPick();
    }
}
