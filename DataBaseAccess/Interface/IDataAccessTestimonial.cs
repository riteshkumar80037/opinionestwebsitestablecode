﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using BusinessEntites;

namespace DataBaseAccess.Interface
{
    public interface IDataAccessTestimonial
    {
        DataTable GetTestimoniallist();
        int AddTestimonial(GetTestimonialEntites getTestimonial);
    }
}
