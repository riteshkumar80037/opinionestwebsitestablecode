﻿using BusinessEntites;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace DataBaseAccess.Interface
{
   public interface IDataAccessQuestion
    {
        long SaveAnswer(OptionalRequestEntites obj,long UserId, bool MandatoryType);
        DataTable GetOptionalQuestion(OptionalRequestEntites obj, long UserId, bool MandatoryType);
        DataTable GetProfileOptionalQuestion(long UserId);
        long SavePoints(long UserId, int PointType, long QuestionId, long ForId);
        DataTable GetProfileQuestionCount(long UserId);
        DataTable GetOptionalQuestionCategory(long UserId);
        DataTable GetQuestionTotal(long userid, int type);


    }
}
