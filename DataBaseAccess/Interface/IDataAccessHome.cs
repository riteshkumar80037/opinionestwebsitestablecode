﻿using BusinessEntites;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace DataBaseAccess.Interface
{
    public interface IDataAccessHome
    {
        DataTable GetBloglist(string Mode, int pageindex, int pagesize);
        DataTable BlogsTag(long blogsId);
        DataTable GetBlogDetails(int blogId, int pageindex, int pagesize);
        DataSet GetDailyFeed(long UserID, int pageNo, int pageSize, int? categoryId, bool? IsStart , string text);
        DataTable BlogDetails(long Id);
        DataTable BlogDetails(string header);
        DataTable mobileFetatures(string code);
        /// <summary>
        /// to get slider list on the basis of slider type
        /// </summary>
        /// <param name="_sliderType"></param>
        /// <returns></returns>
        DataTable SliderList(int _sliderType);

        /// <summary>
        /// Daily Feed Details
        /// </summary>
        /// <param name="feedId"></param>
        /// <returns></returns>
        DataTable DailyFeedDetails(int feedId);

        /// <summary>
        /// Referral List Details
        /// </summary>
        /// <param name="inviterId)"></param>
        /// <returns></returns>
        DataTable ReferralList(long inviterId);
        long HomePageTotalPoints();

        /// <summary>
        /// Contact Form Submission
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        string ContactFormSubmission(ContactUsEntities model);

        /// <summary>
        /// Review List
        /// </summary>
        /// <param name="pageindex"></param>
        /// <param name="pagesize"></param>
        /// <returns></returns>
        DataTable ReviewList(int pageindex, int pagesize);
    }
}
