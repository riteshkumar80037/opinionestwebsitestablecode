﻿using BusinessEntites;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace DataBaseAccess.Interface
{
    public interface IDataAccessSurvey
    {
        DataTable MySurveylist(long UserId);
        DataTable OtherSurveylist(long UserId,int CountryId);
        DataTable FromsCount(long UserId, long Countryid);
        DataTable SurveyData(long surveyId);
        DataTable SurveryAnswerResutl(long QuestionId);
        int addSurvey(ForumCreate forumCreate);
        int addForumQuestion(QuestionEntites questionEntites);
    }
}
