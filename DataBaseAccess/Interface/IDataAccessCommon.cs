﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace DataBaseAccess.Interface
{
  public  interface IDataAccessCommon
    {
        DataTable CountryData();
        DataTable  StateData(long CountryId);
        bool ValidUser(long userID);
        bool mobileNoValidate(string mobileNo);
    }
}
