﻿using System;
using System.Collections.Generic;
using System.Text;


namespace DataBaseAccess.Interface
{
  public  interface IDataAccessExtraActivity
    {
        bool MailOpenSite(long Id);
        bool MailOpenCint(string date);
        bool SaveIpAddress(long userid, string Ipaddress);


    }
}
