﻿using BusinessEntites;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace DataBaseAccess.Defination
{
   public interface IDataAccessLogin
    {
        DataSet UserRegistation(UserEntites Obj);
        long UserEmailSubscrible(long UserId, bool EmailSubscrible, bool OfferEmailSubscription, bool? Cookies);
        DataTable GetUserEmailSubscrible(long UserId);
        DataSet UserPassword(string Email,string Where);
        DataTable Login(UseLoginEntites Obj, int UserType);
        long LoginEmail(UseLoginEntites Obj);
        int DeleteUserAccount(long UserId);
        int OTPVerifiy(int Otp, string EmailId, int Minutes);
        int resetPassword(resetpassword _obj);
        int RegenerateOtp(string OTP, string EmailId);
        int CheckUser(string emailid);
        int CheckUserMobile(string mobileNo);
        int mobileOpt(string OTP, string mobile);
        DataSet UserRegistationMobile(UserEntites Obj);
        string userName(string emailId);
        
    }
}
