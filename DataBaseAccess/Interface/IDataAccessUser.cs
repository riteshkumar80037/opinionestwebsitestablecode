﻿using BusinessEntites;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace DataBaseAccess.Defination
{
    public interface IDataAccessUser
    {
      int UserEdit(EditUserEntites _obj);
      EditUserEntites GetUserProfile(long UserId);
      IList<UserNotificationEntity> GetUserNotification(long UserId);
      Tuple<int,string, string, DailyFeedDetails> GetUserNotificationCount(long UserId);
        UserProfilePercentage GetUserProfilePercentage(long UserID);
        UserReferalEntites GetReferal(long UserId);
        long ConsentPrivacy(ConsentPrivacyEntity Obj);

        /// <summary>
        /// Read Notification
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="notificationId"></param>
        /// <returns></returns>
        int ReadNotification(long userId, int notificationId);
        string GetGUID(long UserId);
        int postSaveNotification(SaveNotification obj, long UserId);
        int postSavereferalCode(SaveReferalCode code, long UserId);
        Tuple<int, long> postSavereferalCodeMobile(string obj, string email);

        UserEntites GetUserDetailsByEmailAddress(string EmailAddress);
        DataTable getNotificationCategroy();
        DataTable getSpinQuestion();
        DataTable getRandomSpinQuestion();
        DataTable getRandomSpinQuestionAns(int QuestionId);
        int notificationRemove(int NotificationId);
        int notificationStar(int NotificationId);
        long SaveSpinQuestion(SpinQuestionSave spinQuestionSave);
        long notificationConfiguration(NotificationConfiguration notificationConfiguration);
        long spinQuestionRandomSave(SpinRandomQueAnsSave spinRandomQueAnsSave);
        int notificationReport(NotificationReport notificationReport);
        int postSavereferalCodeMobilePoints(string obj, string email);
        DataTable TangoProcess();
        int PaymentGatewaysLogs(PaymentGatewaysLogs paymentGatewaysLogs);
        int PaymentGatewaysErrorLogs(PaymentGatewaysErrorLogs paymentGatewaysLogs);

    }
}
