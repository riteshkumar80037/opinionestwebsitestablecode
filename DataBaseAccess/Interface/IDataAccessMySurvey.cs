﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace DataBaseAccess.Interface
{
   public interface IDataAccessMySurvey
    {
        DataSet MySurveylist(string UserId, int pageindex,int pageSize);
        DataTable BannerSurvey(string UserId);
    }
}
