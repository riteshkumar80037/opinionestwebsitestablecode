﻿using CommonMethod;
using DataBaseAccess;
using DataBaseAccess.Defination;
using Newtonsoft.Json;
using System;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using System.Threading;

/// <summary>
/// Summary description for PaymentGatewaysAPI
/// </summary>
public class PaymentGatewaysAPI
{
    //PaytmApiModel gatewaymodel = new PaytmApiModel();
    //public PaytmApiModel GetPaytmUserStatus(string PhoneNo,long paymentgatewayId, string paytmUniqueNumber, long UserId)
    //{
    //    PaytmApiModel model = new PaytmApiModel();
    //    model.Id = paymentgatewayId;
    //    model.merchantguid = GlobalConstant.PaytmMerchantGuid;
    //    model.AesKey = GlobalConstant.PaytmAesKey;
    //    model.saleswalletid = GlobalConstant.Paytmsaleswalletid;
    //    model.orderid = Repo.GetUniqueNoForPaymentGateway((long)PaymentGatways.PayTM) ;
    //    model.Phone = PhoneNo;
    //    model.postData = "{\"request\":{\"requestType\":\"VERIFY\",\"merchantGuid\":\"" + model.merchantguid + "\",\"merchantOrderId\":\"" + model.orderid + "\",\"salesWalletGuid\":\"" + model.saleswalletid + "\",\"salesWalletName\":\"PayTM\",\"payeeEmailId\":null,\"payeePhoneNumber\":\"" + model.Phone + "\",\"payeeSsoId\":null,\"appliedToNewUsers\":\"N\",\"amount\":\"1\",\"currencyCode\":\"INR\",\"callbackURL\":\"http:////" + AppHelper.SiteURL+"//survey//paytmAPITest.aspx\"},\"metadata\":\"Testing Data\",\"ipAddress\":\"127.0.0.1\",\"operationType\":\"SALES_TO_USER_CREDIT\",\"platformName\":\"PayTM\"}";
    //    model.checksum = paytm.CheckSum.generateCheckSumByJson(model.AesKey, model.postData);
    //    model.PaytmTransectionUri = " https://trust.paytm.in/wallet-web/asyncSalesToUserCredit";
    //    //string uri = "https://trust-uat.paytm.in/wallet-web/salesToUserCredit";//Syncronious API
    //    GetPaytmResonse(ref model,GatewayRequestType.Verify.ToString());
    //    gatewaymodel.Id = paymentgatewayId;
    //    gatewaymodel.UserId = UserId;
    //    gatewaymodel.Request_Type = (long)GatewayRequestResponse.VerifyRequestResponse;
    //    gatewaymodel.RequestJson = model.postData;
    //    gatewaymodel.ResponseJson = model.response;
    //    gatewaymodel.ResponseStatus = string.IsNullOrEmpty(model.responseverify.statusMessage)  ? model.responseverify.status : model.responseverify.statusMessage;
    //    Repo.SavePaymentGatewayLogs(gatewaymodel);
    //    Thread.Sleep(9000);//This line used to make differece between calling get api again .
    //    GetTransectionStatus(ref model, GatewayRequestType.Verify.ToString());
    //    gatewaymodel.Request_Type = (long)GatewayRequestResponse.VerifyTransectionStatus;
    //    gatewaymodel.RequestJson = model.postData;
    //    gatewaymodel.ResponseJson = model.response;
    //    gatewaymodel.ResponseStatus = string.IsNullOrEmpty(model.responseverify.statusMessage) ? model.responseverify.status : model.responseverify.statusMessage;
    //    Repo.SavePaymentGatewayLogs(gatewaymodel);
    //    //model = GetPaytmTransStatus(model);
    //    model.statusMessage = gatewaymodel.ResponseStatus;
    //    return model;
    //}

    //public PaytmApiModel GetPaytmTransStatus(PaytmApiModel model,UserRedeemRequestModel offermodel)
    //{
    //    PaytmApiModel modelTrans = new PaytmApiModel();
    //    modelTrans.merchantguid = AppHelper.PaytmMerchantGuid;
    //    modelTrans.AesKey = AppHelper.PaytmAesKey;
    //    modelTrans.saleswalletid = AppHelper.PaytmSaleswalletId;
    //    modelTrans.orderid = "Track-"+DateTime.Now.Ticks.ToString();
    //    modelTrans.Phone = model.Phone;
    //    modelTrans.postData = "{\"request\":{\"requestType\":null,\"merchantGuid\":\"" + modelTrans.merchantguid + "\",\"merchantOrderId\":\"" + modelTrans.orderid + "\",\"salesWalletGuid\":\"" + modelTrans.saleswalletid + "\",\"salesWalletName\":\"PayTM\",\"payeeEmailId\":null,\"payeePhoneNumber\":\"" + modelTrans.Phone + "\",\"payeeSsoId\":null,\"appliedToNewUsers\":\"N\",\"amount\":\"" +offermodel.RequestNumbers+ "\",\"currencyCode\":\"INR\",\"callbackURL\":\"http:////" + AppHelper.SiteURL+"//survey//paytmAPITest.aspx\"},\"metadata\":\"Testing Data\",\"ipAddress\":\"127.0.0.1\",\"operationType\":\"SALES_TO_USER_CREDIT\",\"platformName\":\"PayTM\"}";
    //    modelTrans.checksum = paytm.CheckSum.generateCheckSumByJson(modelTrans.AesKey, modelTrans.postData);
    //    modelTrans.PaytmTransectionUri = " https://trust.paytm.in/wallet-web/asyncSalesToUserCredit";
    //    //string uri = "https://trust-uat.paytm.in/wallet-web/salesToUserCredit";//Syncronious API
    //     GetPaytmResonse(ref modelTrans, GatewayRequestType.Verify.ToString());
    //    gatewaymodel.Id = offermodel.PaymentGatewayId;
    //    gatewaymodel.UserId = SessionVariables.User_Id;
    //    gatewaymodel.Request_Type = (long)GatewayRequestResponse.PaymentRequestRespone;
    //    gatewaymodel.RequestJson = modelTrans.postData;
    //    gatewaymodel.ResponseJson = modelTrans.response;
    //    gatewaymodel.Points = offermodel.Points;
    //    gatewaymodel.Amount = offermodel.RequestNumbers;
    //    gatewaymodel.ResponseStatus = string.IsNullOrEmpty(modelTrans.responseverify.statusMessage) ? modelTrans.responseverify.status : modelTrans.responseverify.statusMessage;
    //    Repo.SavePaymentGatewayLogs(gatewaymodel);
    //    Thread.Sleep(9000);//This line used to make differece between calling get api again .
    //    GetTransectionStatus( ref modelTrans, GatewayRequestType.Transection.ToString());
    //    gatewaymodel.Request_Type = (long)GatewayRequestResponse.PaymentTransectionResponse;
    //    gatewaymodel.RequestJson = modelTrans.postData;
    //    gatewaymodel.ResponseJson = modelTrans.response;
    //    gatewaymodel.ResponseStatus =modelTrans.responsepay.txnList!=null? modelTrans.responsepay.txnList[0].message: modelTrans.responseverify.statusMessage;
    //    gatewaymodel.Points = offermodel.Points;
    //    gatewaymodel.Amount = offermodel.RequestNumbers;
    //    gatewaymodel.IsTransAmount = true;
    //    Repo.SavePaymentGatewayLogs(gatewaymodel);
    //    //model = GetPaytmTransStatus(model);
    //    modelTrans.statusMessage = gatewaymodel.ResponseStatus;
    //    return modelTrans;
    //}
    //public void GetPaytmResonse(ref PaytmApiModel model,string RequestType)
    //{

    //    HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(model.PaytmTransectionUri);
    //    webRequest.Method = "POST";
    //    webRequest.Accept = "application/json";
    //    webRequest.ContentType = "application/json";
    //    webRequest.Headers.Add("mid", model.merchantguid);
    //    webRequest.Headers.Add("checksumhash", model.checksum); ;
    //    webRequest.ContentLength = model.postData.Length;
    //    try
    //    {
    //        using (StreamWriter requestWriter2 = new StreamWriter(webRequest.GetRequestStream()))
    //        {
    //            requestWriter2.Write(model.postData);
    //        }

    //        //  This actually does the request and gets the response back;

    //        string responseData = string.Empty;

    //        using (StreamReader responseReader = new StreamReader(webRequest.GetResponse().GetResponseStream()))
    //        {
    //            model.response = responseReader.ReadToEnd();
    //            if (RequestType == GatewayRequestType.Verify.ToString())
    //            {
    //                model.responseverify = JsonConvert.DeserializeObject<ValidationStatusResponse>(model.response);
    //            }
    //            else if (RequestType == GatewayRequestType.Transection.ToString())
    //            {
    //                model.responsepay = JsonConvert.DeserializeObject<TransectionList>(model.response);
    //                if(model.responsepay.txnList==null)
    //                {
    //                    model.responseverify = JsonConvert.DeserializeObject<ValidationStatusResponse>(model.response);
    //                }
    //            }
    //        }
    //    }
    //    catch (WebException web)
    //    {
    //        HttpWebResponse res = web.Response as HttpWebResponse;
    //        Stream s = res.GetResponseStream();
    //        using (StreamReader sr = new StreamReader(s))
    //        {
    //            model.statusMessage = sr.ReadToEnd();
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        LogErrorToLogFile.WriteError(ex);
    //    }
    //}

    //public PaytmApiModel GetTransectionStatus(ref PaytmApiModel model,string Requesttype)
    //{
    //    model.merchantguid = AppHelper.PaytmMerchantGuid;
    //    model.AesKey = AppHelper.PaytmAesKey;
    //    //model.PaytmTransectionUri = "https://trust-uat.paytm.in/wallet-web/txnStatusList";
    //    model.PaytmTransectionUri = " https://trust.paytm.in/wallet-web/txnStatusList";
    //    //string PostStatusdata = "{\r\n\"request\": {\r\n\"requestType\":\"merchanttxnId\",\r\n\"txnType\":\"SALES_TO_USER_CREDIT\",\r\n\"txnId\": \"Track-636918902227399894\"\r\n\"mId\" : \"1b6eafb0-ed13-4b65-b834-86427fdb8052\"\r\n},\r\n\"ipAddress\": \"127.0.0.1\",\r\n\"platformName\": \"PayTM\",\r\n\"operationType\": \"CHECK_TXN_STATUS\",\r\n\u201Cchannel\u201D:\u201D\u201D,\r\n\u201Cversion\u201D:\u201D\u201D\r\n}";
    //    //model.postData = "{\"request\":{\"requestType\":\"merchanttxnid\",\"txnType\":\"SALES_TO_USER_CREDIT\",\"txnId\":\"Track-636921440932903473\",\"mId\":\"TrackO30770234457239\"},\"ipAddress\":\"127.0.0.1\",\"operationType\":\"CHECK_TXN_STATUS\",\"platformName\":\"PayTM\"}";
    //    model.postData = "{\"request\":{\"requestType\":\"merchanttxnid\",\"txnType\":\"SALES_TO_USER_CREDIT\",\"txnId\":\"" + model.orderid + "\",\"mId\":\"TrackO70456283268961\"},\"ipAddress\":\"127.0.0.1\",\"operationType\":\"CHECK_TXN_STATUS\",\"platformName\":\"PayTM\"}";
    //    model.checksum = paytm.CheckSum.generateCheckSumByJson(model.AesKey, model.postData);
    //    GetPaytmResonse(ref model, Requesttype);
    //    return model;
    //}
}