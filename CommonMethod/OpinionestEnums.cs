﻿using System;
using System.ComponentModel;

namespace CommonMethod
{
    public class OpinionestEnums
    {
    }
    public enum SocialLoginType
    {
        Facebook = 1,
        Googleplus = 2
    }

    public enum PaymentType
    {
        Cash = 1,
        Voucher = 2
    }
    public enum PaymentGatewayMaster
    {
        PayTM = 2,
        Tango = 8
    }
    public enum RegisteredBy
    {
        Website = 1,
        Api = 2
    }

    public enum StatusEnums
    {
        Approved = 1,
        Rejected = 2,

    }

    public enum SurveyType
    {
        Static = 1,
        Dynamic = 2
    }

    public enum PolicyStatus
    {
        PolicyStatusTrue = 1,
        PolicyStatusFalse = 0
    }

    public enum Relinkwallet
    {
        Yes = 1,
        No = 0
    }

    public enum CRUDEnum
    {
        Add = 1,
        Edit = 2,
        Delete = 3,
        Select = 4
    }

    public enum QualityControl
    {

    }
    public enum WalletLinkType
    {
        Phone = 1,
        Email = 2
    }

    public enum WalletLinkMessage
    {
        [Description("Wallet is Successfully Linked")]
        Success = 1,
        [Description("Wallet is Already Linked")]
        AlreadyLinked = 2
    }

    public enum PointRedeemType
    {
        None = 0,
        Cash = 1,
        Vouchers = 2
    }
    public enum Point_Types
    {
        /// <summary>
        /// 
        /// </summary>
        [Description("Referral SignUp Bonus")]
        Referrals_SignUpBonus = 1,
        [Description("Mandatory Question Complete")]
        Referral_User_MandatoryQue_Cmpt = 2,
        [Description("Referral Mandatory Question Complete bonus")]
        Referral_User_MandatoryQue_Comission = 3,
        [Description("Optional Question")]
        User_Optional_Question = 4,
        [Description("Survey Complete")]
        User_Survey_Completes = 5,
        [Description("Referrals Life Time Points")]
        Referrals_LifeTime = 6,
        [Description("Survey Share Points")]
        Survey_Share = 7,
        [Description("Survey Create Points")]
        Survey_Create = 8,
        [Description("Poll of the day")]
        PolloftheDay = 9,
        [Description("Points Redeem")]
        PointRedeem = 10,
        [Description("Rewards")]
        Rewards = 11,
        [Description("ComplimentaryPoints")]
        ComplimentaryPoints = 12
    }

    public enum Point_Deduction_Types
    {
        ByPaymentateway = 1,
        ByVouchers = 2
    }
    public enum QualityControlIP
    {
        None = 0,
        IpCheck = 1
    }
    public enum Survey_User_Point_Status
    {
        [Description("-")]
        Pending = 1,
        [Description("Qualified")]
        Qualified = 2,
        [Description("NotQualified")]
        NotQualified = 3,
        [Description("Credited")]
        Credited = 4
    }

    public enum Gender
    {
        Male = 1,
        Female = 0
    }
    public enum ProjectMode
    {
        Testing = 1,
        Live = 2
    }
    public enum UserType
    {
        SuperAdmin = 1,
        Admin = 2,
        Public = 3,
        Client = 4,
        Vennder = 5,
        Customer = 6,
        SurveyBetaUser = 7,
        Facebook = 8,
        Google = 9
    }
    public enum UserState
    {
        Inactive = 1,
        Active = 2,
        Unsubscribed = 3,
        NotVerified = 4
    }
    public enum Duration
    {
        Hours = 12,
        Minutes = 60,
        Seconds = 60
    }

    public enum SqlCRUDOpt_Type
    {
        add,
        edit,
        delete,
        select
    }

    public enum PaymentGatways
    {
        PayPal = 1,
        PayTM = 2,
        Freecharge = 3,
        MoneyinBankAccount = 4,
        AmazonVouchers = 5,
        ShopperStop = 6
    }
    public enum VouchersStatus//this enum is used when admin approves the vouchers
    {
        Approved = 1,
        Required = 2,
        NoData = 3
    }


    public enum AnswerType
    {
        Dropdown = 1,
        TextBox = 2,
        CheckBox = 3,
        RadioButton = 4,
        MultiSelect = 5,
        MatrixRadioButton = 6,
        MatrixCheckBox = 7,
    }

    public enum SurveyUserType
    {
        SiteUser = 1,
        Vendor = 2,
        StaticSurveyUser = 3
    }

    public enum FilterOption
    {
        UploadPanelist = 1,
        ChoosePanelistWithCriteria = 2
    }

    public enum FilterStatus
    {
        Open = 1,
        InComplete = 2,
        Close = 3,
        Pause = 4
    }

    public enum SurveyStatus
    {
        Open = 1,
        Live = 2,
        Close = 3,
        Paused = 4,
        Resume = 5,
        IpError = 6,
        AlreadyGiven = 7
    }

    public enum UserSurveyStatus
    {
        None = 0,
        [Description("Pending")]
        Release = 1,
        [Description("-")]
        InProcess = 2,
        [Description("Taken")]
        Completed = 3,
        [Description("Taken")]
        QuotaFull = 4,
        [Description("Taken")]
        Terminated = 5,
        InCompleted = 6,
        [Description("Taken")]
        Credited = 7

    }

    public enum UserSurveyMesssage
    {
        None = 0,
        [Description("Survey is Closed !")]
        Closed = 3,
        [Description("Survey have been paused,Please try after some time!")]
        Paused = 4,
        [Description("Unknown Profile !")]
        WrongIp = 6,
        [Description("You have already given this survey!!")]
        AlreadyGiven = 5,
        [Description("Oh Ho, We are really sorry, for the best experience this survey can only be accessed through mobile.")]
        WrongDeviceDesktop = 7,
        [Description("Oh Ho, We are really sorry, for the best experience this survey can only be accessed through desktop.")]
        WrongDeviceMobile = 8,
        [Description("Oh Ho, We are really sorry, for the best experience this survey can only be accessed through tablet.")]
        WrongDeviceTablet = 9,
        UnknownRequest = 2,
        [Description("Oh Ho, We are really sorry, but you have exhausted your per day survey limit.")]
        ValidDayLimit = 10,
        [Description("Oh Ho, We are really sorry, but you have exhausted your per month survey limit.")]
        ValidMonthLimit = 11

    }
    public enum VendorStatus
    {
        Open = 1,
        InComplete = 2,
        Close = 3,
        Pause = 4
    }

    public enum UserSurveyVisitType
    {
        Website = 1,
        Email = 2,
        Facebook = 3

    }
    /// <summary>
    /// This enum is used to get the Way by which action is taken
    /// </summary>
    public enum ActionbyType
    {
        WebApplication = 1,
        CronJob = 2,
        API = 3
    }

    public enum PointRedeemStatus
    {
        Pending = 1,
        Approved = 2,
        Failed = 3,
        Rejected_ByAdmin = 4
    }

    public enum PaymenGatewayPrefix
    {
        [Description("ppl")]
        Paypal = 1,
        [Description("ptm")]
        Paytm = 2

    }

    public enum GatewayRequestType
    {
        Verify = 1,
        Transection = 2
    }


    public enum PaytmStatusMessage
    {
        Success = 1,
        Faliure = 2
    }
    public enum GatewayRequestResponse
    {
        VerifyRequestResponse = 1,
        VerifyTransectionStatus = 2,
        PaymentRequestRespone = 3,
        PaymentTransectionResponse = 4
    }

    public enum MailType
    {
        Registration=1,
        SiteSurvey=2,
        CintSurvey=3
    }

    public enum Redirects
    {
        Dashboard=1,
        Mysurvey=2,
        Unsubscribe=3
    }
}
