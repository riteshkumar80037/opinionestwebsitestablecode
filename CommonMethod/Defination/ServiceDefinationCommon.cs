﻿using BusinessEntites;
using CommonMethod.Interface;
using DataBaseAccess;
using Newtonsoft.Json;
using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace CommonMethod.Defination
{
    public class ServiceDefinationCommon : ICommon
    {
        SqlConnection Con;
        public ServiceDefinationCommon(SqlConnection _Con = null)
        {
            Con = new SqlConnection();
            Con = _Con;
        }

        public string DecryptID(string id)
        {
            string DecryptID = " 0";
            try
            {
                DecryptID = new EncryptMethod().DecryptFromHex(id, AppHelper.EncryptKeyValue);
            }
            catch (Exception ex)
            {
                //LogErrorToLogFile.WriteError(ex);
            }
            return DecryptID;

        }

        public string EncryptID(string id)
        {
            string EncryptedID = string.Empty;
            try
            {
                EncryptedID = new EncryptMethod().EncryptToHex(id.ToString(), AppHelper.EncryptKeyValue);
            }
            catch
            {

            }
            return EncryptedID;
        }

        public Country_By_Ip_Entites GetCountryByIp(string IpAddress)
        {
            Country_By_Ip_Entites Model = new Country_By_Ip_Entites();
            try
            {
                //string GetIpAddress = GetIPAddressByWeb();
                string GetIpAddress = IpAddress;
                IPDataIPstackEntites IPinfo = new IPDataIPstackEntites();
                string strResponse = new WebClient().DownloadString("http://api.ipstack.com/" + GetIpAddress + "?access_key=4136d7077bb6ccba7f59a059e86b1208");
                if (strResponse == null || strResponse == "") return Model;
                IPinfo = JsonConvert.DeserializeObject<IPDataIPstackEntites>(strResponse);

                //Get all IP infos
                //var IPinfo = IPK.GetIpInfo(ip);
                Model.IP = IPinfo.ip.ToString();
                Model.country = IPinfo.country_name;
                Model.countryCode = IPinfo.country_code;
                Model.zip = IPinfo.zip;
                Model.countryCode = IPinfo.location.calling_code;
                Model.country_flag = IPinfo.location.country_flag;

                return Model;
            }
            catch (Exception ex)
            {
                // LogErrorToLogFile.WriteError(ex);
                return Model;

            }
        }

        public int GetCountryCode(string Ipaddress)
        {
            try
            {

                Country_By_Ip_Entites Model = new Country_By_Ip_Entites();
                Country_By_Ip_Entites Model1 = new Country_By_Ip_Entites();
                Model = GetCountryByIp(Ipaddress);
                DataTable Dt = new DataTable();
                SQLCommonMethod Sql = new SQLCommonMethod(Con);
                SqlParameter[] parameter = {

                                   new SqlParameter("@CountryName",Model.country)  ,
                                   new SqlParameter("@SortName",Model.countryCode),
                                     };
                Dt = Sql.Datatable_Sp("GetCountryIdBasedOnName", parameter);

                if (Dt.Rows.Count > 0)
                {
                    Model1.CountryId = Convert.ToInt32(Dt.Rows[0]["CountryId"]);
                }
                return Model1.CountryId;
            }
            catch (Exception ex)
            {
                // LogErrorToLogFile.WriteError(ex);
                return 0;
            }
        }

        public bool SendEmailThroughSendGrid(SendGridEntity obj)
        {
            Execute(obj).Wait();
            return true;
        }
        private async Task Execute(SendGridEntity entity)
        {
            var client = new SendGridClient("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");
            var from = new EmailAddress("respondents@opinionest.com", "Opinionest");
            var to = new EmailAddress(entity.emailId, entity.Name);
            var htmlContent = entity.body;
            var msg = MailHelper.CreateSingleEmail(from, to, entity.subject, "", htmlContent);
            var response = await client.SendEmailAsync(msg);
        }
        public bool SendEmail(EmailEntites obj)
        {
            bool MailStatus = true;
            SendGrid.Response response = null;
            try
            {
                string MailContent = "";

                if (obj.Type == 1)
                {
                    if(obj.CountryId== 10)
                    {
                        MailContent = System.IO.File.ReadAllText(@"C:\Api2\Registration\RegisteredMailVerificationKoria.ehtml");
                    }
                    else
                    {
                    MailContent = System.IO.File.ReadAllText(@"C:\Api2\Registration\RegisteredMailVerification.ehtml");
                    }
                }
                else if (obj.Type == 2)
                {
                    if (obj.CountryId == 10)
                    {
                        MailContent = System.IO.File.ReadAllText(@"C:\Api2\Registration\RegenerateOtpKoria.ehtml");
                    }
                    else
                    {
                        MailContent = System.IO.File.ReadAllText(@"C:\Api2\Registration\RegenerateOtp.ehtml");
                    }
                }
                else if (obj.Type == 3)
                {
                    MailContent = System.IO.File.ReadAllText(@"C:\Api2\Registration\ContactUsMailTest.ehtml");
                }
                else if (obj.Type == 4)
                {
                    MailContent = System.IO.File.ReadAllText(@"C:\Api2\Registration\Applink.ehtml");
                }
                else if (obj.Type == 5)
                {
                    MailContent = System.IO.File.ReadAllText(@"C:\Api2\Registration\Paytm.ehtml");
                    MailContent = MailContent.Replace("_PaytmMessage_", obj.OTP);
                }
                else if (obj.Type == 6)
                {
                    MailContent = System.IO.File.ReadAllText(@"C:\Api2\Registration\forgotpassword.ehtml");
                }
                else if (obj.Type == 7)
                {
                    MailContent = System.IO.File.ReadAllText(@"C:\Api2\Registration\regenerateotp.ehtml");
                }
                else if (obj.Type == 8)
                {
                    MailContent = System.IO.File.ReadAllText(@"C:\Api2\Registration\mailer.ehtml");
                    MailContent = MailContent.Replace("_welcome_", obj.FirstName);    
                }

                //if (obj.Type == 3)
                //{
                //    MailContent = MailContent.Replace("___query___", obj.Query);

                //}
                //else
                //{
                //    MailContent = MailContent.Replace("__ActivationURL__", obj.OTP);
                //    MailContent = MailContent.Replace("__ContactUs__", "");
                //    MailContent = MailContent.Replace("___username___", obj.FirstName);
                //    MailContent = MailContent.Replace("__domainName__", "");
                //    MailContent = MailContent.Replace("__Query___", obj.Query);
                //}

                MailContent = MailContent.Replace("_OTP_", obj.OTP);
                MailContent = MailContent.Replace("__ContactUs__", "");
                MailContent = MailContent.Replace("_username_", obj.FirstName);
                MailContent = MailContent.Replace("__domainName__", "");
                MailContent = MailContent.Replace("__Query__", obj.Query);
                MailContent = MailContent.Replace("_appLink_", obj.OTP);
                MailContent = MailContent.Replace("_otp_", obj.OTP);

                var to = obj.Type == 3 ? new EmailAddress("help@opinionest.com", "Opinionest") : new EmailAddress(obj.UserEmail, obj.FirstName);
                var from = new EmailAddress("respondents@opinionest.com", "Opinionest");
                var client = new SendGridClient(obj.client);
                var plainTextContent = "";

                var msg = MailHelper.CreateSingleEmail(from, to, obj.subject, plainTextContent, MailContent);
                response = client.SendEmailAsync(msg).Result;
                MailStatus = true;

            }
            catch (Exception ex)
            {
                MailStatus = false;
            }
            return MailStatus;
        }

        public CountryFlag countryInfo(string ip)
        {
            CountryFlag countryFlag = new CountryFlag();
            try
            {

                Country_By_Ip_Entites Model = new Country_By_Ip_Entites();
                Country_By_Ip_Entites Model1 = new Country_By_Ip_Entites();
                Model = GetCountryByIp(ip);
                countryFlag.countryCode = "+" + Model.countryCode;
                countryFlag.CountryName = Model.country;
                countryFlag.flagPath = Model.country_flag;
               
                return countryFlag;
            }
            catch (Exception ex)
            {
                // LogErrorToLogFile.WriteError(ex);
                return countryFlag;
            }
        }
    }
}
