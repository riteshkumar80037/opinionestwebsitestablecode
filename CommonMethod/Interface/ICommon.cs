﻿using BusinessEntites;
using System;
using System.Collections.Generic;
using System.Text;

namespace CommonMethod.Interface
{

   public interface ICommon
    {
        Country_By_Ip_Entites GetCountryByIp(string IpAddress);
        int GetCountryCode(string Ipaddress);
        string EncryptID(string id);
        string DecryptID(string id);
        bool SendEmail(EmailEntites obj);
        bool SendEmailThroughSendGrid(SendGridEntity obj);
        CountryFlag countryInfo(string ip);
    }
}
