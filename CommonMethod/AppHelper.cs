﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace CommonMethod
{
   public static class AppHelper
    {
        public static string EncryptKeyValue = "P@%5w0r]>|<?^S*G";

        public static string OpinionestCintUrlForSurveySet = "https://apisurvey1.opinionest.com/api/SetSurvey";

        public static string OpinionestCintUrlForSurveyGet = "https://apisurvey1.opinionest.com/api/GetSurvey/GetSurveyData";

        public static string OpinionestCintUrlForClientLink = "https://apisurvey1.opinionest.com/api/GetSurvey/GetSurveyUrl";

        public static string OpinionestCintSurveyCallBackStatusUrl = "https://apisurvey1.opinionest.com/api/GetSurvey/GetSurevyCallBackStatus";

        public static string OpinionestCintSurveyMailOpenUrl = "https://apisurvey1.opinionest.com/api/MailAndOther/GetMailOpen";

        public static DateTime CurrentDateTime
        {
            get
            {
                return DateTime.Now;
            }
        }

        public static string GetEnumDescription(Enum ProfessionalProfile)
        {
            FieldInfo fi = ProfessionalProfile.GetType().GetField(ProfessionalProfile.ToString());
            DescriptionAttribute[] attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);
            if (attributes.Length > 0)
            {
                return attributes[0].Description;
            }
            else
            {
                return ProfessionalProfile.ToString();
            }
        }
        public static string replaceQueryString(string request, string key, string value)
        {
            var uri = new Uri(request);
            var qs = HttpUtility.ParseQueryString(uri.Query);
            qs.Set(key, value);
            var uriBuilder = new UriBuilder(uri);
            uriBuilder.Query = qs.ToString();
            var newUri = uriBuilder.Uri;
            return newUri.ToString();
        }

        public static Dictionary<string, string> ConvertParam_Dict(string uri, bool IsAddQuestionSign_Pre = false)
        {
            if (!IsAddQuestionSign_Pre)
            {
                uri = "?" + uri;
            }
            var matches = Regex.Matches(uri, @"[\?&](([^&=]+)=([^&=#]*))", RegexOptions.Compiled);
            return matches.Cast<Match>().ToDictionary(
                m => Uri.UnescapeDataString(m.Groups[2].Value.ToLower()),
                m => Uri.UnescapeDataString(m.Groups[3].Value)
            );
        }
    }

}
