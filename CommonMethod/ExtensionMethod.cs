﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace CommonMethod
{
    public static class ExtensionMethod
    {
        public static T CloneJson<T>(this T source)
        {
            // Don't serialize a null object, simply return the default for that object
            if (Object.ReferenceEquals(source, null))
            {
                return default(T);
            }

            // initialize inner objects individually
            // for example in default constructor some list property initialized with some values,
            // but in 'source' these items are cleaned -
            // without ObjectCreationHandling.Replace default constructor values will be added to result
            var deserializeSettings = new JsonSerializerSettings { ObjectCreationHandling = ObjectCreationHandling.Replace };

            return JsonConvert.DeserializeObject<T>(JsonConvert.SerializeObject(source), deserializeSettings);
        }

        public static bool IsNumber(this String Value)
        {
            Regex regex = new Regex(@"^[0-9]*$");
            if (regex.IsMatch(Value))
            { return true; }
            else
            { return false; }
        }

        public static string EnumDescription(this Enum Value)
        {
            return AppHelper.GetEnumDescription(Value);
        }

        public static bool IsNumbers(String Value)
        {
            Regex regex = new Regex(@"^[0-9]*$");
            if (regex.IsMatch(Value))
            { return true; }
            else
            { return false; }
        }


        public static bool IsDateTime(this String Value)
        {
            DateTime OutputValue;
            return DateTime.TryParse(Value, out OutputValue);
        }

        public static bool IsDecimal(this String Value)
        {
            Decimal OutputValue;
            return Decimal.TryParse(Value, out OutputValue);
        }

        public static bool IsEmailId(this String Value)
        {
            Regex regex = new Regex(@"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?");
            return regex.IsMatch(Value);
        }

        public static bool IsValidCharacters(this String Value)
        {
            Regex regex = new Regex(@"^[a-zA-Z0-9._&@!\s-]*$");
            return regex.IsMatch(Value);
        }



        /// <summary>
        /// Set Billing Address or Shipping Address 
        /// </summary>
        /// <param name="FullAddressString"></param>
        /// <param name="StartHtml"></param>
        /// <param name="EndHtml"></param>
        /// <returns></returns>
        public static string SetAddress(this string FullAddressString, string StartHtml, string EndHtml)
        {
            string Address = string.Empty;
            try
            {
                List<string> TempAddress = new List<string>(FullAddressString.Split(new string[] { "\n" }, StringSplitOptions.RemoveEmptyEntries));

                foreach (var item in TempAddress)
                {
                    Address += StartHtml + item + EndHtml;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Address;
        }

        /// <summary>
        /// Remove Extra Space between words
        /// </summary>
        /// <param name="Value"></param>
        /// <returns></returns>
        public static string RemoveSpaces(this String Value)
        {
            RegexOptions options = RegexOptions.None;
            Regex regex = new Regex(@"[ ]{2,}", options);
            return regex.Replace(Value.Trim(), @" ");
        }

        public static bool IsValidPassword(this String Value)
        {
            Regex regex = new Regex(@"((?=.*\d)(?=.*[a-z]|[A-Z])(?!.*\s).{8,20})");
            return regex.IsMatch(Value);
        }
        public static bool IsXLFile(this String Value)
        {
            Regex regex = new Regex(@"^.*\.(xls|xlsx)$");
            return regex.IsMatch(Value);

        }
        public static bool IsCSVFile(this String Value)
        {
            Regex regex = new Regex(@"^.*\.(csv)$");
            return regex.IsMatch(Value);

        }
        public static bool IsTXTFile(this String Value)
        {
            Regex regex = new Regex(@"^.*\.(txt)$");
            return regex.IsMatch(Value);

        }

        public static string HTMLEncode(this String value)
        {
            return HttpUtility.HtmlEncode(value);
        }

        public static string HTMLDecode(this String value)
        {
            return HttpUtility.HtmlDecode(value);
        }

        public static string GetUploadedFileName(this String Value)
        {
            string FileName = Value;
            if (FileName.Contains(':'))
            {
                FileName = FileName.Substring(FileName.LastIndexOf('\\') + 1);
            }
            return FileName;

        }

        public static DateTime ConvertToDate(this String value)
        {
            String[] strDateSplit = value.Split('/');
            DateTime Date = new DateTime(Convert.ToInt32(strDateSplit[2]), Convert.ToInt32(strDateSplit[0]), Convert.ToInt32(strDateSplit[1]));
            return Date;
        }

        public static DateTime ToUTC_to_IST(this DateTime utcdate)
        {
            return TimeZoneInfo.ConvertTimeFromUtc(utcdate,
    TimeZoneInfo.FindSystemTimeZoneById("India Standard Time"));

        }



        public static int TableSkipRecord(this int value, int? CurrentPageSize)
        {
            if (CurrentPageSize.HasValue)
            {
                value = value == 0 ? 0 : (value - 1) * (CurrentPageSize.Value);
            }
            return value;
        }


        public static string EncryptID(this string id)
        {
            string EncryptedID = string.Empty;
            try
            {
                EncryptedID = new EncryptMethod().EncryptToHex(id.ToString(), AppHelper.EncryptKeyValue);
            }
            catch
            {

            }
            return EncryptedID;
        }

        public static string DecryptID(this String id)
        {
            string DecryptID = " 0";
            try
            {
                DecryptID = new EncryptMethod().DecryptFromHex(id, AppHelper.EncryptKeyValue);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return DecryptID;
        }

        //public static string DecryptTemp(this String id)
        //{
        //    string DecryptID = "0";
        //    try
        //    {

        //        DecryptID = new Cryptology().DecryptNumber(id);

        //    }
        //    catch
        //    {

        //    }
        //    return DecryptID;
        //}


    }

    public static class LinqExtension
    {
        public static IQueryable<TSource> SearchBy<TSource, TKey>(this IQueryable<TSource> source, Expression<Func<TSource, TKey>> keySelector, TKey low, TKey high) where TKey : IComparable<TKey>
        {
            Expression key = Expression.Invoke(keySelector, keySelector.Parameters.ToArray());
            Expression lowerBound = Expression.GreaterThanOrEqual(key, Expression.Constant(low));
            Expression upperBound = Expression.LessThanOrEqual(key, Expression.Constant(high));
            Expression and = Expression.AndAlso(lowerBound, upperBound);
            Expression<Func<TSource, bool>> lambda = Expression.Lambda<Func<TSource, bool>>(and, keySelector.Parameters);
            return source.Where(lambda);
        }
    }
}
