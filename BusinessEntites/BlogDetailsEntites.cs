﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessEntites
{
   public class BlogDetailsEntites
    {
        public long BlogsId { get; set; }
        public string Header { get; set; }
        public string SubHeader { get; set; }
        public string Message { get; set; }
        public string Image { get; set; }
        public string AltText { get; set; }
        public string NewHeader { get; set; }
        public List<BlogEntites> otherlist { get; set; }
        public List<blogsTag> blogsTag { get; set; }
    }
    public class Rate
    {
        public string to { get; set; }
        public string from { get; set; }
        public double rate { get; set; }
    }
}
