﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessEntites
{
   public class CountryEntites
    {
        public long CountryId { get; set; }
        public string CountryName { get; set; }
        public string CountryCode { get; set; }
    }
}
