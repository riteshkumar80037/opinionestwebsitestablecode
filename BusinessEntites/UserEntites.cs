﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BusinessEntites
{
   public class UserEntites :CommanEntites
    {
        public long UserId { get; set; }
        public int SrNo { get; set; }
        [Required]
        public string FullName { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        public string DisplayName { get; set; }
        public string RegisteredName { get; set; }
        public bool DynamicStaticLink { get; set; }
        public string UserName { get; set; }
        [Required]
        [EmailAddress]
        public string EmailAddress { get; set; }
        public long UserStateId { get; set; }
        public long UserTypeId { get; set; }
        public string PhoneNumber { get; set; }
        public string UniqueNo { get; set; }
        public int? Gender { get; set; }
        public DateTime DateOfBirth { get; set; }
        [Required]
        public string Password { get; set; }
        public long CountryID { get; set; }
        public string CountryName { get; set; }
        public long QualityControlId { get; set; }
        public long StateID { get; set; }
        public string Pincode { get; set; }
        public string FacebookId { get; set; }
        public string GooglePlusId { get; set; }
        public string ProfileImage { get; set; }
        public bool IsBackToRegister { get; set; }
        public bool IsProceedToOtherOffersRewards { get; set; }
        public long Inviter_Id { get; set; }
        public long Partner_Id { get; set; }
        public string Partner_Params { get; set; }
        public bool EmailUnsubscrib { get; set; }
        [Required]
        public int PolicyStatus { get; set; }
        public bool IsMandatory { get; set; }
        public string MacAddress { get; set; }
        public int MacValue { get; set; }
        public string IPAddress { get; set; }
        public string OTP { get; set; }
    }
    public class PaymentCookiesaEntity
    {
        public long Id { get; set; }
        public long Fk_UserId { get; set; }
        public string MobileNo { get; set; }
        public int Point { get; set; }
        public decimal RequestedAmount { get; set; }
        public string PGId { get; set; }
        public DateTime CreatedDate { get; set; }
        public string EmailAddress { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Type { get; set; }
    }
}
