﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessEntites
{
   public class EmailEntites
    {
        public long UserId { get; set; }
        public string UserEmail { get; set; }
        public string FirstName { get; set; }
        public string ApiKey { get; set; }
        public string OTP { get; set; }
        public string CompanyName { get; set; }
        public string sendemail { get; set; }
        public string pwd { get; set; }
        public string smtp { get; set; }
        public int port { get; set; }
        public string domainName { get; set; }
        public string logo { get; set; }
        public string client { get; set; }
        public string subject { get; set; }
        public int Type { get; set; }
        public string Query { get; set; }
        public long CountryId { get; set; }
    }
    public class SendGridEntity
    {
        public string emailId { get; set; }
        public string Name { get; set; }
        public string subject { get; set; }
        public string body { get; set; }
    }
}
