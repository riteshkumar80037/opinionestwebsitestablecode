﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BusinessEntites
{
   public class RegistationEntites
    {
        [System.ComponentModel.DataAnnotations.Required]
        public string FullName { get; set; }
       
        
        
        public string EmailAddress { get; set; }
        
        public string Password { get; set; }
        [Required]
        public int PolicyStatus { get; set; }
        public string MacAddress { get; set; }
       
        public string IPAddress { get; set; }

        public long PartnerId { get; set; }

        public string PartnerParams { get; set; }
       
        public string mobileNo { get; set; }
       
        public string CountrCode { get; set; }
        public string referalCode { get; set; }


    }
}
