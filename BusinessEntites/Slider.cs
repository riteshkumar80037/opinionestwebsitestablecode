﻿namespace BusinessEntites
{
    public class Slider
    {
        public string FirstHeader { get; set; }
        public string SecondHeader { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public string FileType { get; set; }
        public int SliderType { get; set; }
    }
}
