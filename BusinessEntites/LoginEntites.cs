﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessEntites
{
   public class LoginEntites
    {
        public string Token { get; set; }
        public long UserId { get; set; }
        public string EmailId { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public string ProfilePic { get; set; }
        public int TotalPoints { get; set; }
        public bool IsMandatory { get; set; }
        public string Redirect { get; set; }

    }
}
