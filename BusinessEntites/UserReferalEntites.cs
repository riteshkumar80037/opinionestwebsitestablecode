﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BusinessEntites
{
   public class UserReferalEntites
    {
        public int TotalPoint { get; set; }
        public int TotalReferal { get; set; }
        public string UniqueCode { get; set; }
        public bool ReferalExist { get; set; }
        public int UserTotalPoints { get; set; }
    }
    public class ForumCreate
    {
        public int? Id { get; set; }
        public long? UserId { get; set; }
        public Nullable<int> Status { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        [Required(ErrorMessage = "Survey name is required")]
        public string SurveyName { get; set; }
        [Required(ErrorMessage = "Survey description is required")]
        public string SurveyDescription { get; set; }
        public Nullable<int> SurveyLimit { get; set; }
        public Nullable<int> RoleId { get; set; }
    }
    public class QuestionEntites
    {
        public Int32 FourmId { get; set; }
        public Int64? UserId { get; set; }
        public List<QuestionList> QuestionList { get; set; }
    }
    public class QuestionList
    {
        public Int32? QuestionId { get; set; }
        public bool? IsOptional { get; set; }
        public int? IsQuestionFound { get; set; }
        public int? Type { get; set; }
        public bool? Mandatory { get; set; }
        public bool? Logical { get; set; }
        public string Question { get; set; }
        public int? SerialNo { get; set; }
        public int? IfYes1 { get; set; }
        public int? IfNo1 { get; set; }
        public string DependentYes1 { get; set; }
        public List<AnswerEntites> AnswerListEntites { get; set; }
    }
    public class AnswerEntites
    {
        public int? AnswerId { get; set; }
        public int? QuestionId { get; set; }
        public string Answer { get; set; }
        public string IfYes { get; set; }
        public string IfNo { get; set; }
    }
}
