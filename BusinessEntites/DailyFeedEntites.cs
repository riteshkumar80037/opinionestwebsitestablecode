﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessEntites
{
    public class DailyFeedEntites
    {
        public long Id { get; set; }
        public string Header { get; set; }
        public string Message { get; set; }
        public string agoTime { get; set; }
        public string Date { get; set; }
        public string IconPath { get; set; }
        public string Url { get; set; }
        public string Points { get; set; }
        public string Status { get; set; }
        public string USERiD { get; set; }
        
    }
    
    public class DailyFeedModelList
    {
        public string Date { get; set; }
        public List<DailyFeedEntites> _List { get; set; }
    }

    public class DailyFeedDetails
    {
        public int Id { get; set; }
        public string Header { get; set; }
        public int UserId { get; set; }
        public string Message { get; set; }
        public DateTime FromDt { get; set; }
        public DateTime Todt { get; set; }
        public string Time { get; set; }
        public string Category { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public string iconPath { get; set; }
        public string Status { get; set; }
        public string Class { get; set; }
        public string NotificationCount { get; set; }
        public string Link { get; set; }
        public long filter_id { get; set; }
        public string BatchId { get; set; }
        public decimal Points { get; set; }
        public string IsRead { get; set; }
    }

    public class ReferralDetails
    {
        public string Name { get; set; }
        public string shortName { get; set; }
        public double Points { get; set; }
        public string Status { get; set; }
        public string CreatedDate { get; set; }
    }
    public class ReferralDetailList
    {
        public string Name { get; set; }
        public string shortName { get; set; }
        public double Points { get; set; }
        public string Status { get; set; }
        public string CreatedDate { get; set; }
        public string color { get; set; }
    }
    public class mobileFeature
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public bool Status { get; set; }
    }
}
