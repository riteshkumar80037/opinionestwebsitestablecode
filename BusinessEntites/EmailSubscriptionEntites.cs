﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessEntites
{
    public class EmailSubscriptionEntites
    {
        public bool EmailSubscrible { get; set; }
        public bool OfferEmailSubscription { get; set; }
        public bool Cookies { get; set; }
    }
    public class emailSubscription
    {
        public bool UserEmailSubscrible { get; set; }
        public bool OfferEmailSubscription { get; set; }
        public bool Cookies { get; set; }
    }
    public class CountryFlag
    {
        public string flagPath { get; set; }
        public string countryCode { get; set; }
        public string CountryName { get; set; }
    }
}
