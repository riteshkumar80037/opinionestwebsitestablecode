﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace BusinessEntites
{
    public class MySurveyDataListEntites
    {
        public int SNo { get; set; }

        public long SurveyId { get; set; }

        public int RewardPoints { get; set; }

        public int TerminateRewards { get; set; }

        public int QuotaFullRewards { get; set; }

        public string SurveyNo { get; set; }

        public string SurveyName { get; set; }

        public int SurveyStatus { get; set; }

        public int FilterStatus { get; set; }

        public UserSurveyStatus UserSurveyStatus { get; set; }

        public string SurveyStatusText { get; set; }
        public string SurveyAttedmptedText { get; set; }

        public string PointsStatusText { get; set; }

        public string Category { get; set; }
        public int CategoryId { get; set; }

        public string URL { get; set; }
        public string ImagePath { get; set; }
        public string LeftDate { get; set; }
        public long Surveycount { get; set; }
        public SurveyType Survey_Type { get; set; }

        public System.DateTime? CreatedOn { get; set; }
    }
    public class MySurveyColection
    {
        public int TotalRecords { get; set; }
        public int TotalRewardPoints { get; set; }
        public List<MySurveyDataListEntites> MySurveyDataListEntites { get; set; }

    }
    public enum SurveyType
    {
        Static = 1,
        Dynamic = 2
    }
    public enum UserSurveyStatus
    {
        None = 0,
        [Description("Pending")]
        Release = 1,
        [Description("InCompleted")]
        InProgress = 2,
        [Description("Taken")]
        Completed = 3,
        [Description("Taken")]
        QuotaFull = 4,
        [Description("Taken")]
        Terminated = 5,
        [Description("InCompleted")]
        InCompleted = 6,
        [Description("Taken")]
        Credited = 7,
        [Description("Taken")]
        NoSurveyURL = 8,
        [Description("Taken")]
        SecurityRedirectUrl = 9,
        [Description("Taken")]
        DuplicateRedirectUrl = 10,
        [Description("Taken")]
        TentativeComplete = 11,
        [Description("Taken")]
        QualityRejected = 12,
    }
}
