﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessEntites
{
   public class Profile_Question_Count
    {
        public int TotalQuestion { get; set; }
        public int TotalAttemped { get; set; }
        public int BalancePoint { get; set; }
        public int TotalEarn { get; set; }
    }
}
