﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessEntites
{
   public class PartnerEntities
    {
        public long Id { get; set; }

        public string Email { get; set; }

        public string ContactNo { get; set; }

        public string CompanyName { get; set; }

        public string VerificationUrl { get; set; }

        public List<PartnerKeyRelationEntities> URLKeyMapList { get; set; }

    }
}
