﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BusinessEntites
{
   public class UseLoginEntites
    {
        [Required]
        public string EmailId { get; set; }
       
        public string Password { get; set; }

        public bool IsSocialLogin { get; set; }
        public string facebookId { get; set; }
        public string gmailId { get; set; }
    }
}
