﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessEntites
{
    public class ContactUsEntities
    {
        public string ContactName { get; set; }
        public string ContactEmail { get; set; }
        public int CategoryId { get; set; }
        public string Query { get; set; }
        public string ContactNo { get; set; }
    }
    public class SendEmailLink
    {
        
        public string EmailId { get; set; }
        public int OTP { get; set; }

    }
    public class forgetpassword
    {

        public string EmailID { get; set; }

    }
    public class resetpassword
    {

        public string emailId { get; set; }
        public int otp { get; set; }
        public int minutes { get; set; }
        public string password { get; set; }

    }
}
