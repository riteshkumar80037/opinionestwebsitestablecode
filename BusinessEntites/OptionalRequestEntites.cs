﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessEntites
{
    public class OptionalRequestEntites
    {
        public long ProfileQuestionId { get; set; }
        public string ProfileAnswerId { get; set; }
        
        public long  GPCID { get; set; }
        public bool Mandatory { get; set; }
    }
}
