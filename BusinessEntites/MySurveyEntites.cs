﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessEntites
{
    public class MySurveyEntites
    {
        public long SNo { get; set; }
        public long SurveyId { get; set; }
        public string Title { get; set; }
        public string Category { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }
        public string Remarks { get; set; }
        public long Points { get; set; }
        public long SurveyPoint { get; set; }
        public long SurveyLimit { get; set; }
        public Int64 TotalClicks { get; set; }
        public string Url { get; set; }
        public string Limit { get; set; }
        public string Percentage { get; set; }
    }
    public class FormsCount
    {
        public int MySurvey { get; set; }
        public int OtherSurvey { get; set; }
    }
}
