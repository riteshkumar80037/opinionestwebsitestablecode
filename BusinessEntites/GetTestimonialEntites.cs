﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessEntites
{
    public class GetTestimonialEntites
    {
        public string Message { get; set; }
        public string Image { get; set; }
        public int UserId { get; set; }
    }
}
