﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessEntites
{
   public class SurveyResult
    {
        public int SurveyId { get; set; }
        public string  SurveyName { get; set; }
        public List<SurveyQuestion> SurveyQuestions { get; set; }
    }
     public class SurveyQuestion
    {
        public int QuestionId { get; set; }
        public string Question { get; set; }
       public List<AnswerDetails> AnswerDetails { get; set; }
    }
    public class AnswerDetails
    {
        public string Answer { get; set; }
        public int Count { get; set; }
    }
}
