﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessEntites
{
   public class UserProfilePercentage
    {
        public int TotalQuestion { get; set; }
        public int Attempted {   get; set; }
        public decimal Percentage { get; set; }
        public decimal TotalPoint { get; set; }
        public string PointRedeemMessage { get; set; }
    }
}
