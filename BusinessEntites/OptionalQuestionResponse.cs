﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessEntites
{
  public  class OptionalQuestionResponse
    {
        public long ProfileQuestionId { get; set; }
        public string ProfileQuestion { get; set; }
        public string Type { get; set; }
        public string GPCName { get; set; }
        public long GPCID { get; set; }
        public bool Required { get; set; }
        public int Points { get; set; }
        public int ProfileComplete { get; set; }
    }

    public class OptionalAnswerResponse
    {
        public long ProfileAnswerId { get; set; }
        public string ProfileAnswer { get; set; }
    }

    public class QuestionResponse
    {
        public OptionalQuestionResponse Question { get; set; }
        public List<OptionalAnswerResponse> Answer { get; set; }
    }
    public class ProfileOptionalQuestionResponse
    {
        public string GPCName { get; set; }
        public string GPCDescription { get; set; }
        public long GPCID { get; set; }
        public string Color { get; set; }
        public string Title { get; set; }
    }
    public class QuestionTotal
    {
        public int Total { get; set; }
        public int Complete { get; set; }
        public int Pending { get; set; }
    }

}
