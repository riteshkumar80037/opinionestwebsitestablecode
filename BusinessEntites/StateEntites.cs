﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessEntites
{
  public  class StateEntites
    {
        public long StateId { get; set; }
        public string StateName { get; set; }
        public long CountyId { get; set; }
    }
}
