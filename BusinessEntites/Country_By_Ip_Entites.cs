﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessEntites
{
   public class Country_By_Ip_Entites
    {
        public int UserId { get; set; }
        public string country { get; set; }
        public string countryCode { get; set; }
        public string timezone { get; set; }
        public string status { get; set; }
        public string regionName { get; set; }
        public string region { get; set; }
        public string query { get; set; }
        public string org { get; set; }
        public string zip { get; set; }
        public string lon { get; set; }
        public string isp { get; set; }
        public string city { get; set; }
        public string AS { get; set; }
        public string IP { get; set; }
        public string lat { get; set; }
        public string languages { get; set; }
        public int CountryId { get; set; }
        public string calling_code { get; set; }
        public string country_flag { get; set; }
    }
}
