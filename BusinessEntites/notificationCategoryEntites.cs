﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessEntites
{
   public  class notificationCategoryEntites
    {
        public int id { get; set; }
        public string categoryName { get; set; }
    }
    public class SpinQuestion
    {
        public int Id { get; set; }
        public string Question { get; set; }
    }
    public class SpinRandomQuestion
    {
        public int id { get; set; }
        public string Question { get; set; }
        public List<SpinRandomQueAns> answers { get; set; }
    }
    public class SpinQuestionSave
    {
        public int SpinQuestionId { get; set; }
        public long UserId { get; set; }
    }
    public class NotificationConfiguration
    {
        public long UserId { get; set; }
        public bool IsActive { get; set; }
    }
    public class NotificationReport
    {
        public long NotificationId { get; set; }
        public string  report { get; set; }
        public long? UserId { get; set; }
    }
    public class SpinRandomQueAns
    {
        public int ansId { get; set; }
        public string answer { get; set; }
    }
    public class SpinRandomQueAnsSave
    {
        public int ansId { get; set; }
        public string answer { get; set; }
        public int QuestionId { get; set; }
        public long UserId { get; set; }
    }
}
