﻿using System;

namespace BusinessEntites
{
    public class CommanEntites
    {
        public System.DateTime? CreatedOn { get; set; }
        public System.Int64 CreatedBy { get; set; }
        public System.DateTime? ModifiedOn { get; set; }
        public System.Int64 ModifiedBy { get; set; }
        public System.Boolean? IsVerify { get; set; }
        public System.Boolean? IsProfileUpdate { get; set; }
        public System.Boolean? IsActive { get; set; }
        public System.Boolean? IsDelete { get; set; }
        public System.String mode { get; set; }
        public System.String where { get; set; }
        public bool IsSocial { get; set; }
        public int TotalCount { get; set; }


    }
}
