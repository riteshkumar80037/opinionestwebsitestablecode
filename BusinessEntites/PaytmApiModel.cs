﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessEntites
{
    public class PaytmApiModel: UserModel
    {
        public string merchantguid { get; set; }
        public string AesKey { get; set; }  // 16 digits Merchant Key or Aes Key
        public string saleswalletid { get; set; }
        public string orderid { get; set; }
        public string postData { get; set; }
        public string checksum { get; set; }
        public string PaytmTransectionUri { get; set; }
        public string PaytmTransectionStatusUri
        {
            get;
            set;
        }
        public string Phone { get; set; }
        public object type { get; set; }
        public object requestGuid { get; set; }
        public string status { get; set; }
        public string statusCode { get; set; }
        public string statusMessage { get; set; }
        public TransectionList responsepay { get; set; }
        public ValidationStatusResponse responseverify { get; set; }
        public string metadata { get; set; }
        public string response { get; set; }

        public long Request_Type { get; set; }

        public string RequestJson { get; set; }

        public string ResponseJson { get; set; }

        public string ResponseStatus { get; set; }

        public bool IsTransAmount { get; set; }

        public long Points { get; set; }

        public float Amount { get; set; }
        public long PaytmCookiedId { get; set; }
    }
   public class PaytmResponseMail
    {
        public long UserId { get; set; }
        public decimal Amount { get; set; }
        public string Mobile { get; set; }
    }

    public class TransectionList
    {
        public List<TransectionStatusResponse> txnList { get; set; }
    }
    public class ResponsePaytmonAfterPay
    {
        public string walletSysTransactionId { get; set; }
        public bool status { get; set; }
    }
    public class ValidationStatusResponse
    {
        public string type { get; set; }
        public string requestGuid { get; set; }
        public string orderId { get; set; }
        public string status { get; set; }
        public string statusCode { get; set; }
        public string statusMessage { get; set; }
        public Response response { get; set; }
        public string metadata { get; set; }
        public string message { get; set; }
    }
    public class Response
    {
        public object walletSysTransactionId { get; set; }
        public object destination { get; set; }
        public string beneficiaryName { get; set; }
    }

    public class TransectionStatusResponse
    {
        public object txnGuid { get; set; }
        public object txnAmount { get; set; }
        public int status { get; set; }
        public string message { get; set; }
        public object txnErrorCode { get; set; }
        public object ssoId { get; set; }
        public string txnType { get; set; }
        public string merchantOrderId { get; set; }
        public object pgTxnId { get; set; }
        public object pgRefundId { get; set; }
        public object cashbackTxnId { get; set; }
        public bool isLimitPending { get; set; }
        public object mId { get; set; }
    }
}
