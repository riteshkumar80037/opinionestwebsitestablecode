﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessEntites
{
 public   class JSONSurveyResponse
    {
        public System.Net.HttpStatusCode Status { get; set; }

        public string Message { get; set; }

        public List<MySurveyDataListEntites> Data { get; set; }
    }
}
