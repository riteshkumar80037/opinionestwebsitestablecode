﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessEntites
{
    public class Offer
    {
        public int OfferId { get; set; }
        public string UtId { get; set; }
        public string RewardName { get; set; }
        public string Currency { get; set; }
        public string Denomination { get; set; }
        public int MinRange { get; set; }
        public string MaxRange { get; set; }
        public string Country { get; set; }
        public int IsFavourite { get; set; }
        public decimal AvailablePoints { get; set; }
        public string ImagePath { get; set; }
        public string Disclaimer { get; set; }
        public string PointsRequried { get; set; }
    }

    public class CommonOfferInputModel
    {
        public int OfferId { get; set; }
        public long UserId { get; set; }
        public int Fk_PaymentGetwayId { get; set; }
    }
    public class OfferFavirout
    {
        
        
        public string TangoId { get; set; }
    }

    public class PointsUpdateInputModel: CommonOfferInputModel
    {
        public decimal DeductionPoints { get; set; }
        public string utid { get; set; }
        public string logoPath { get; set; }
    }
    public class tangoLogs 
    {
        public long UserId { get; set; }
        public string Status { get; set; }
        public string RequestJson { get; set; }
        public string ResponseJson { get; set; }
    }
    public class MarkOfferFavouriteInputModel: CommonOfferInputModel
    {
        public int IsFavourite { get; set; }
        public string TangoFavtId { get; set; }
    }

    public class OfferHistoryDetails
    {
        public string FullName { get; set; }
        public string EmailAddress { get; set; }
        public int DeductedPoint { get; set; }
        public int RemainingPoints { get; set; }
        public int TotalPoints { get; set; }
        public string OfferType { get; set; }
        public string RewardName { get; set; }
        public string Utid { get; set; }
        public string Currency { get; set; }
        public string Country { get; set; }
        public int IsFavourite { get; set; }
        public string ReceivedDate { get; set; }
    }

    public class UserRedeemRequestModel
    {
        public long PaymentGatewayId { get; set; }
        public string Details { get; set; }
        public long Points { get; set; }
        public long PointTypes { get; set; }
        public string PaymentGatewayToken { get; set; }
        public string FileName { get; set; }
        public float RequestNumbers { get; set; }
    }

    public class UserAccountLinkedStatus
    {
        public string LinkedAdress { get; set; }
        public long PaymentGatewayId { get; set; }
        public bool Isverify { get; set; }
    }
    public class PaymentCookiesPending
    {
        public long Id { get; set; }
        public long Fk_UserId { get; set; }
        public string MobileNo { get; set; }
        public decimal RequestedAmount { get; set; }
        public int Point { get; set; }
        public string PGId { get; set; }
        public string email { get; set; }
        public int Type { get; set; }
        public long Fk_PaymentgatewayId { get; set; }

    }

    public class UserLinkWalletInputModel
    {
        public long UserId { get; set; }
        public long PaymentGatewayId { get; set; }
        public string EmailAddress { get; set; }
        public string MobileNumber { get; set; }
    }
    public class ConvertPointToCurrency
    {
        public long Points { get; set; }
        public string Currency { get; set; }

    }

    public class PaymentCookies
    {
        public long Fk_UserId { get; set; }
        public string MobileNo { get; set; }
        public int Point { get; set; }
        public decimal RequestedAmount { get; set; }
        public int PointsRedeem { get; set; }
        public string PGId { get; set; }
        public int payment_type { get; set; }
        public int Fk_PaymentGetwayId { get; set; }

    }
    public class PaymentGatewaysLogs
    {
        public long Fk_PaymentgatewayId { get; set; }
        public long Fk_UserId { get; set; }
        public long Request_Type { get; set; }
        public long Points { get; set; }
        public string RequestJson { get; set; }
        public string ResponseJson { get; set; }
        public string ResponseStatus { get; set; }
        public decimal RequestedAmount { get; set; }
        public long paymentCookiesId { get; set; }
    }
    public class PaymentGatewaysErrorLogs
    {
        public long Fk_PaymentgatewayId { get; set; }
        public long Fk_UserId { get; set; }
        public long Request_Type { get; set; }
        public long Points { get; set; }
        public string RequestJson { get; set; }
        public string ResponseJson { get; set; }
        public string ResponseStatus { get; set; }
        public decimal RequestedAmount { get; set; }
        public long paymentCookiesId { get; set; }
        public string Error { get; set; }
    }
}
