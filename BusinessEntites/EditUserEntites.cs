﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BusinessEntites
{
   public class EditUserEntites
    {
        
        public long UserId { get; set; }
        
        public string FullName { get; set; }
        
        public string FirstName { get; set; }
        
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string ProfileImage { get; set; }
        public string CountryId { get; set; }
        public string CountryName { get; set; }
        public int Gender { get; set; }
        public string DateOfBirth { get; set; }
        public string Pincode { get; set; }
        public string EmailAddress { get; set; }
    }
    public class UserNotificationEntity
    {
        public string Message { get; set; }
        public string CreatedDate { get; set; }
        public string Header { get; set; }
        public string Url { get; set; }
        public string Time { get; set; }
        public string ImagePath { get; set; }
        public string Points { get; set; }
    }
    public class ConsentPrivacyEntity
    {
        public long UserID { get; set; }
        [Required]
        public int Tearm { get; set; }
        [Required]
        public int Policy { get; set; }
        public DateTime Dob { get; set; }
        public int   Gender { get; set; }
        public string PinCode { get; set; }
        [Required]
        public long CountryId { get; set; }
    }
    public class SaveNotification
    {
        public string DeviceToken { get; set; }
        
    }
    public class SaveReferalCode
    {
        [Required]
        public string ReferalCode { get; set; }

    }
}
