﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessEntites
{
    public class TestimonialEntites
    {
        public int Id { get; set; }
        public string Country { get; set; }
        public string UserName { get; set; }
        public string Message { get; set; }
        public string Image { get; set; }
        public DateTime CreatedDate { get; set; }
        public int UserId { get; set; }
    }
}
