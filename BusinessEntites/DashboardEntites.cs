﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessEntites
{
   public class DashboardEntites
    {
        public int TotalPoints { get; set; }
        public int OpenSurvey { get; set; }
        public int Forum { get; set; }
        public int ReferalPoint { get; set; }
        public int DailyFeeds { get; set; }
        public bool PopupStatus { get; set; }

        public List<BlogEntites> Blogs { get; set; }
    }
}
