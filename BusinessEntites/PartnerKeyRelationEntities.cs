﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessEntites
{
  public  class PartnerKeyRelationEntities
    {
        public long Id { get; set; }

        public string Query_Get_Key { get; set; }

        public string Query_Replace_ValueKey { get; set; }

        public long PartnerId { get; set; }
    }
}
