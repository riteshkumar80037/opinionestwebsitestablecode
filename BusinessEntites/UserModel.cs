﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessEntites
{
    public class UserModel
    {
        public long Id { get; set; }
        public long UserId { get; set; }
        public int SrNo { get; set; }
        public string FullName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string DisplayName { get; set; }
        public string RegisteredName { get; set; }
        public bool DynamicStaticLink { get; set; }
        public string UserName { get; set; }
        public string EmailAddress { get; set; }
        public long UserStateId { get; set; }
        public long UserTypeId { get; set; }
        public string PhoneNumber { get; set; }
        public string UniqueNo { get; set; }
        public bool Gender { get; set; }
        public string DateOfBirth { get; set; }
        public string Password { get; set; }
        public long CountryID { get; set; }
        public string CountryName { get; set; }
        public long QualityControlId { get; set; }
        public long StateID { get; set; }
        public string Pincode { get; set; }
        public string FacebookId { get; set; }
        public string GooglePlusId { get; set; }
        public string ProfileImage { get; set; }
        public bool IsBackToRegister { get; set; }
        public bool IsProceedToOtherOffersRewards { get; set; }
        public long Inviter_Id { get; set; }
        public long Partner_Id { get; set; }
        public string Partner_Params { get; set; }
        public bool EmailUnsubscrib { get; set; }
        public int PolicyStatus { get; set; }
        public bool IsMandatory { get; set; }
        public string MacAddress { get; set; }

        public string ReferrelStatus { get; set; }
        #region ClientDetails
        public long ClientDetailId { get; set; }
        public string ClientLink { get; set; }
        public string QueryGetKey { get; set; }
        public string QuerySetKey { get; set; }
        #endregion
        #region VendorDetails
        public long VendorDetailsId { get; set; }

        public long SurveyId { get; set; }

        public long ProjectId { get; set; }

        public string URL { get; set; }

        public string CompleteLink { get; set; }

        public string QuotaFullLink { get; set; }

        public string TerminateLink { get; set; }

        public string CompleteLinkReplaceVal { get; set; }

        public string QuotaFullLinkReplaceVal { get; set; }

        public string TerminateLinkReplaceVal { get; set; }

        public bool IsApplicableforRewards { get; set; }
        #endregion
        #region Comman
        public System.DateTime? CreatedOn { get; set; }
        public System.Int64 CreatedBy { get; set; }
        public System.DateTime? ModifiedOn { get; set; }
        public System.Int64 ModifiedBy { get; set; }
        public System.Boolean? IsVerify { get; set; }
        public System.Boolean? IsProfileUpdate { get; set; }
        public System.Boolean? IsActive { get; set; }
        public System.Boolean? IsDelete { get; set; }
        public System.String mode { get; set; }
        public System.String where { get; set; }
        public bool IsSocial { get; set; }
        #endregion
    }
}
