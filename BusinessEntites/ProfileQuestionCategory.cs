﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessEntites
{
   public class ProfileQuestionCategory
    {
        public long categoryId { get; set; }
        public string categoryName { get; set; }
        public int TotalQuestion { get; set; }
        public int Points { get; set; }
        public string IconPath { get; set; }
        public int AttendQuestion { get; set; }
        public int EarnedPoint { get; set; }
        public int Percantage { get; set; }

    }
}
