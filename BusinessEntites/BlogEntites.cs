﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessEntites
{
    public class BlogEntites: CommanEntites
    {
        public int Id { get; set; }
        public string Header { get; set; }
        public string UserName { get; set; }
        public string Message { get; set; }
        public string Image { get; set; }
        public DateTime ToDate { get; set; }
        public DateTime FromDate { get; set; }
        public string SubHeader { get; set; }
        public string Tag { get; set; }
        public string AltText { get; set; }
        public int UserId { get; set; }
        public bool isTop { get; set; }
        public string OriginalName { get; set; }
        public List<blogsTag> BlogsTag { get; set; }
        public string NewHeader { get; set; }
        /// <summary>
        ///1 - TOP
        ///2 - LEFT
        ///3 - RIGHT SMALL TOP
        ///4 - RIGHT SMALL LEFT
        ///5 - BOTTOM FIRST
        ///6 - BOTTOM SECOND
        /// </summary>
        public int ImageType { get; set; }
    }
    public class blogsTag
    {
        public int TagId { get; set; }
        public string TagName { get; set; }
        public string Link { get; set; }
    }

    public class CustomerReview
    {
        public int ReviewId { get; set; }
        public string Reviews { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CustomerName { get; set; }
        public string CustomerImage { get; set; }
        public string CustomerLocation { get; set; }
    }
}
