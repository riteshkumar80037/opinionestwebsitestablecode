﻿using BusinessAccessLayer.InterfaceDeclaration;
using BusinessEntites;
using DataBaseAccess.Defination;
using DataBaseAccess.Interface;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace BusinessAccessLayer.ServiceDefination
{
    public class QuestionServicesDefination : IQuestion
    {
        SqlConnection Conn;
        IDataAccessQuestion _DalQuestion;
        public QuestionServicesDefination(SqlConnection _Conn)
        {
            Conn = _Conn;
            _DalQuestion = new ServiceDataAccessQuestion(Conn);
           
        }
        public QuestionResponse OptionalQuestionGet(OptionalRequestEntites obj, long UserId, bool MandatoryType)
        {
            QuestionResponse _Obj = new QuestionResponse();
            try
            {
                //if(obj.ProfileAnswerId>0)
                //{
                //    int i = _DalQuestion.SaveAnswer(obj, UserId, MandatoryType);
                //}
                DataTable Dt = _DalQuestion.GetOptionalQuestion(obj, UserId, MandatoryType);
                if(Dt.Rows.Count>0)
                {
                    OptionalQuestionResponse _Que = new OptionalQuestionResponse
                    {
                        Type=Dt.Rows[0]["AnswerType"].ToString(),
                        GPCID=Convert.ToInt64(Dt.Rows[0]["GPCID"].ToString()),
                        GPCName= Dt.Rows[0]["GPCName"].ToString(),
                        ProfileQuestion= Dt.Rows[0]["Questions"].ToString(),
                        ProfileQuestionId= Convert.ToInt64(Dt.Rows[0]["ProfileQuestionsId"].ToString()),
                        Required= MandatoryType
                    };
                    _Obj.Question = _Que;
                    List<OptionalAnswerResponse> _ans = new List<OptionalAnswerResponse>();
                    foreach(DataRow dr in Dt.Rows)
                    {
                        _ans.Add(new OptionalAnswerResponse
                        {
                            ProfileAnswer= dr["ProfileAnswers"].ToString(),
                            ProfileAnswerId = Convert.ToInt64(dr["ProfileAnswerID"].ToString()),
                        });

                    }
                    _Obj.Answer = _ans;

                }
                return _Obj;
            }
            catch(Exception ex)
            {
                return null;
            }
        }

        public long SaveAnswer(OptionalRequestEntites obj, long UserId, bool MandatoryType)
        {
            long i = 0;
            long Point = 0;
            try
            {
                if(!String.IsNullOrEmpty(obj.ProfileAnswerId))
                {
                    i = _DalQuestion.SaveAnswer(obj, UserId, MandatoryType);
                    if(!String.IsNullOrEmpty(obj.ProfileAnswerId) && MandatoryType==true && i>0)
                    {
                        Point = _DalQuestion.SavePoints(UserId, Convert.ToInt32(CommonMethod.Point_Types.Referral_User_MandatoryQue_Cmpt), obj.ProfileQuestionId, i);
                    }
                   else if  (!String.IsNullOrEmpty(obj.ProfileAnswerId) && MandatoryType == false && i>0)
                    {
                        Point = _DalQuestion.SavePoints(UserId, Convert.ToInt32(CommonMethod.Point_Types.User_Optional_Question), obj.ProfileQuestionId, i);
                    }
                    else
                    {
                        Point = i;
                    }

                }
                return Point;

            }
            catch (Exception Ex)
            {

            }

            return i;
        }

        public List<ProfileOptionalQuestionResponse> ProfileOptionalQuestionGet(long UserId)
        {
            List<ProfileOptionalQuestionResponse> _Obj = new List<ProfileOptionalQuestionResponse>();
            try
            {
                DataTable Dt = _DalQuestion.GetProfileOptionalQuestion(UserId);
                
                if (Dt.Rows.Count > 0)
                {
                    foreach (DataRow Dr in Dt.Rows)
                    {
                        ProfileOptionalQuestionResponse Ob = new ProfileOptionalQuestionResponse();
                        Ob.GPCName = Dr["GPCName"].ToString();
                        Ob.GPCDescription = Dr["GPCDescription"].ToString();
                        Ob.GPCID = Convert.ToInt32(Dr["GPCID"]);
                        int GivenAns = Convert.ToInt32(Dr["AnsGiven"]);
                        int Total = Convert.ToInt32(Dr["Total"]);
                        if (GivenAns == 0 && Total == 0)
                        {
                            Ob.Color = "#f89606";
                            Ob.Title = "No questions are available!!";
                        }
                        else if (GivenAns == Total)
                        {
                            Ob.Color = "#27d2ea";
                            Ob.Title = "All answers are given!!";
                        }
                        
                        else
                        {
                            Ob.Color = "#ffffff";
                            Ob.Title = "Questions are remaining!!";
                        }
                        _Obj.Add(Ob);
                    }

                }
                return _Obj;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public Profile_Question_Count ProfileQuestionCount(long UserID)
        {
            Profile_Question_Count _obj = new Profile_Question_Count();
            try
            {
                DataTable _dt = _DalQuestion.GetProfileQuestionCount(UserID);
                if(_dt.Rows.Count>0)
                {
                    _obj.BalancePoint = Convert.ToInt32(_dt.Rows[0]["TotalQuestion"]);
                    _obj.TotalAttemped = Convert.ToInt32(_dt.Rows[0]["TotalAttemped"]);
                    _obj.TotalEarn = Convert.ToInt32(_dt.Rows[0]["TotalEarn"]);
                    _obj.TotalQuestion = Convert.ToInt32(_dt.Rows[0]["TotalQuestion"]);
                }

            }
            catch(Exception Ex)
            {

            }
            return _obj;
        }

        public List<ProfileQuestionCategory> ProfileQuestionCategory(long UserID)
        {
            List<ProfileQuestionCategory> _obj = new List<ProfileQuestionCategory>();
            try
            {
                DataTable _dt = _DalQuestion.GetOptionalQuestionCategory(UserID);
                if (_dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in _dt.Rows)
                    {
                        _obj.Add(new ProfileQuestionCategory
                        {
                            AttendQuestion=Convert.ToInt32(dr["AttendQuestion"]),
                            EarnedPoint = Convert.ToInt32(dr["AttendQuestion"]),
                            categoryId = Convert.ToInt64(dr["GPCID"]),
                            categoryName = dr["GPCName"].ToString(),
                            IconPath = dr["GPCDescription"].ToString(),
                            Percantage = Convert.ToInt32(dr["Percantage"]),
                            Points = Convert.ToInt16(dr["Points"]),
                            TotalQuestion=Convert.ToInt32(dr["TotalQuestion"])

                        });

                    }
                }

            }
            catch (Exception Ex)
            {

            }
            return _obj;
        }

        public QuestionTotal OptionalTotal(long userid)
        {
            QuestionTotal _question = new QuestionTotal();
            try
            {
                DataTable _dt = _DalQuestion.GetQuestionTotal(userid,0);
                if(_dt.Rows.Count>0)
                {

                    _question.Total = Convert.ToInt32(_dt.Rows[0]["Total"]);
                    _question.Complete = Convert.ToInt32(_dt.Rows[0]["Complete"]);
                    _question.Pending = _question.Total - _question.Complete;
                }
            }
            catch(Exception ex)
            {

            }
            return _question;
        }

        public QuestionTotal MandatoryTotal(long userid)
        {
            QuestionTotal _question = new QuestionTotal();
            try
            {
                DataTable _dt = _DalQuestion.GetQuestionTotal(userid,1);
                if (_dt.Rows.Count > 0)
                {

                    _question.Total = Convert.ToInt32(_dt.Rows[0]["Total"]);
                    _question.Complete = Convert.ToInt32(_dt.Rows[0]["Complete"]);
                    _question.Pending = _question.Total - _question.Complete;
                }
            }
            catch (Exception ex)
            {

            }
            return _question;
        }
    }
}
