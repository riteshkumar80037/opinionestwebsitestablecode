﻿using BusinessAccessLayer.InterfaceDeclaration;
using BusinessEntites;
using CommonMethod;
using CommonMethod.Defination;
using CommonMethod.Interface;
using DataBaseAccess.Defination;
using DataBaseAccess.Interface;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Text.RegularExpressions;
namespace BusinessAccessLayer.ServiceDefination
{
    public class SurveyServicesDefination:ISurvey
    {
        SqlConnection Conn;
        IDataAccessSurvey _Survey;
        CommonMethod.Interface.ICommon _Comm;

        public SurveyServicesDefination(SqlConnection _Conn)
        {
            Conn = _Conn;
            _Survey = new ServiceDataAccessSurvey(Conn);
            _Comm = new ServiceDefinationCommon(Conn);
        }

        public int addForumQuestion(QuestionEntites questionEntites)
        {
            return _Survey.addForumQuestion(questionEntites);
        }

        public int addSurvey(ForumCreate forumCreate)
        {
            return _Survey.addSurvey(forumCreate);
        }

        public FormsCount FromsCount(long UserId)
        {
            FormsCount forms = new FormsCount();
            try
            {
                DataTable DT = _Survey.FromsCount(UserId,0);
                
                if(DT.Rows.Count>0)
                {
                    forms.MySurvey = Convert.ToInt32(DT.Rows[0][1]);
                    forms.OtherSurvey = Convert.ToInt32(DT.Rows[0][0]);
                }
                return forms;
            }
            catch(Exception ex)
            {

            }
            return forms;
        }
        public List<MySurveyEntites> MySurveylist(long UserId)
        {
            DataTable DT = _Survey.MySurveylist(UserId);
            List<MySurveyEntites> List = new List<MySurveyEntites>();
            foreach (DataRow Dr in DT.Rows)
            {
                MySurveyEntites obj = new MySurveyEntites();
                obj.Title = Dr["SurveyName"].ToString();
                obj.Description = Dr["SurveyDescription"].ToString();
                obj.Status = Dr["Status"].ToString();
                obj.SurveyLimit = Convert.ToInt32(Dr["SurveyLimit"]);
                obj.SNo = Convert.ToInt64(Dr["RowNo"]);
                obj.TotalClicks = Convert.ToInt64(Dr["TotalClick"]);
                obj.Remarks = Dr["Remarks"].ToString();
                obj.SurveyId = Convert.ToInt32(Dr["Id"]);
                obj.Url = Dr["Url"].ToString();
                obj.Limit = Dr["Limit"].ToString();
                obj.Percentage = Dr["Percentage"].ToString();
                List.Add(obj);
            }
            return List;
        }
        public List<MySurveyEntites> OtherSurveylist(long UserId,int CountryId)
        {
            DataTable DT = _Survey.OtherSurveylist(UserId,CountryId);
            List<MySurveyEntites> List = new List<MySurveyEntites>();
            foreach (DataRow Dr in DT.Rows)
            {
                MySurveyEntites obj = new MySurveyEntites();
                obj.Title = Dr["SurveyName"].ToString();
                obj.Description = Dr["SurveyDescription"].ToString();
                obj.Status = Dr["Status"].ToString();
                obj.Points = Convert.ToInt64(Dr["TotalEarn"]);
                obj.Remarks = Dr["Remarks"].ToString();
                obj.SurveyId = Convert.ToInt32(Dr["Id"]);
                obj.Url = Dr["Url"].ToString();
                obj.SNo = Convert.ToInt64(Dr["RowNo"]);
                List.Add(obj);
            }
            return List;
        }

        public SurveyResult ViewSurveyResult(long UserId, long Surveyid)
        {
            SurveyResult _obj = new SurveyResult();
            DataTable SurveyData = _Survey.SurveyData(Surveyid);
            if(SurveyData.Rows.Count>0)
            {
                _obj.SurveyId = Convert.ToInt32(SurveyData.Rows[0]["Id"]);
                _obj.SurveyName = Convert.ToString(SurveyData.Rows[0]["SurveyName"]);
                List<SurveyQuestion> _listSurveryQuestion = new List<SurveyQuestion>();
                foreach(DataRow Dr in SurveyData.Rows)
                {
                    SurveyQuestion _objQuestion = new SurveyQuestion();
                    List<AnswerDetails> _answerResult = new List<AnswerDetails>();
                    _objQuestion.Question = Dr["Question"].ToString();
                    _objQuestion.QuestionId=Convert.ToInt32( Dr["QuestionId"].ToString());

                    DataTable SurveyAnswerResutl = _Survey.SurveryAnswerResutl(_objQuestion.QuestionId);
                    foreach(DataRow answerRow in SurveyAnswerResutl.Rows)
                    {
                        AnswerDetails _asnrestul = new AnswerDetails();
                        _asnrestul.Answer = answerRow["answer"].ToString();
                        _asnrestul.Count = Convert.ToInt32( answerRow["TotalAttend"].ToString());
                        _answerResult.Add(_asnrestul);
                    }
                    _objQuestion.AnswerDetails = _answerResult;
                    _listSurveryQuestion.Add(_objQuestion);
                }
                _obj.SurveyQuestions = _listSurveryQuestion;
            }
            
            return _obj;
        }
    }
}
