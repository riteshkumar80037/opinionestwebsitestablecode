﻿using BusinessAccessLayer.InterfaceDeclaration;
using BusinessEntites;
using DataBaseAccess.Defination;
using DataBaseAccess.Interface;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace BusinessAccessLayer.ServiceDefination
{
   public class DashBoardServicesDefination : IDashboard
    {
        SqlConnection Conn;
        IDataAccessDashBoard _dbDashBorad;
        public DashBoardServicesDefination(SqlConnection _Conn)
        {
            Conn = _Conn;
            _dbDashBorad = new ServicesDataAccessDashBoard(Conn);
        }
        public DashboardEntites GetDashboard(long UserId)
        {
            DashboardEntites _Obj= new DashboardEntites();
            bool Pop = false;
            try
            {
               DataSet Ds= _dbDashBorad.GetDashBorad(UserId);
                if(Ds.Tables[0].Rows.Count>0)
                {
                    Pop = Convert.ToBoolean(Ds.Tables[0].Rows[0]["TearmofUser"]);
                    
                    _Obj = new DashboardEntites
                    {
                        Forum =Convert.ToInt32(Ds.Tables[0].Rows[0]["TOTALFORMS"]),
                        DailyFeeds = Convert.ToInt32(Ds.Tables[0].Rows[0]["TOTALDAILYFEEDS"]),
                        OpenSurvey = Convert.ToInt32(Ds.Tables[0].Rows[0]["TOTALSURVEY"]),
                        ReferalPoint = Convert.ToInt32(Ds.Tables[0].Rows[0]["TOTALREFEREAL"]),
                        TotalPoints = Convert.ToInt32(Ds.Tables[0].Rows[0]["TOTALPOINTS"]),
                        PopupStatus = Pop
                    };
                }
                if(Ds.Tables[1].Rows.Count>0)
                {
                    List<BlogEntites> _bloglist = new List<BlogEntites>();
                    foreach(DataRow dr in Ds.Tables[1].Rows)
                    {
                        BlogEntites _blog = new BlogEntites
                        {
                            Id = Convert.ToInt32(dr["BlogsId"].ToString()),
                            AltText = dr["AltText"].ToString(),
                            SubHeader = dr["SubHeader"].ToString(),
                            Image = dr["Image"].ToString(),
                            Message = dr["Message"].ToString(),
                            Header = dr["Header"].ToString(),
                            NewHeader= dr["NewHeader"].ToString(),
                        };
                        _bloglist.Add(_blog);

                    }
                    
                    _Obj.Blogs = _bloglist;

                }
                
                return _Obj;
            }
            catch(Exception ex)
            {
                return null;
            }
        }
    }
}
