﻿using BusinessAccessLayer.InterfaceDeclaration;
using BusinessEntites;
using CommonMethod;
using CommonMethod.Defination;
using CommonMethod.Interface;
using DataBaseAccess.Defination;
using DataBaseAccess.Interface;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Text.RegularExpressions;

namespace BusinessAccessLayer.ServiceDefination
{
    public class TestimonialServicesDefination : ITestimonial
    {
        SqlConnection Conn;
        IDataAccessTestimonial _DaTestimonial;
        CommonMethod.Interface.ICommon _Comm;

        public TestimonialServicesDefination(SqlConnection _Conn)
        {
            Conn = _Conn;
            _DaTestimonial = new ServiceDataAccessTestimonial(Conn);
            _Comm = new ServiceDefinationCommon(Conn);
        }
        public List<TestimonialEntites> GetTestimonialList()
        {
            TestimonialEntites obj = new TestimonialEntites();
            DataTable DT = _DaTestimonial.GetTestimoniallist();
            List<TestimonialEntites> List = new List<TestimonialEntites>();
            foreach (DataRow Dr in DT.Rows)
            {
                TestimonialEntites Testimonial = new TestimonialEntites();
                Testimonial.UserId = Convert.ToInt32(Dr["UserId"]);
                Testimonial.Country = Dr["CountryName"].ToString();
                Testimonial.Image = Dr["Image"].ToString();
                Testimonial.Message = Dr["Message"].ToString();
                Testimonial.UserName = Dr["FullName"].ToString();
                Testimonial.CreatedDate = Convert.ToDateTime(Dr["createddate"]);
                //if (HttpContext.Current.Request.IsLocal)
                //{
                //    blog.Image = WebConfigurationManager.AppSettings["LocalPath"] + "Admin/BlogImage/" + Dr["Image"].ToString();
                //}
                //else
                //{
                //    blog.Image = WebConfigurationManager.AppSettings["Path"] + "Admin/BlogImage/" + Dr["Image"].ToString();
                //}
                ////blog.Image = Server.MapPath("~/Admin/BlogImage/" + Dr["Image"].ToString());
                List.Add(Testimonial);
            }
            return List;
        }
        public int AddTestimonial(GetTestimonialEntites getTestimonial)
        {
            var result = _DaTestimonial.AddTestimonial(getTestimonial);
            return result;
        }
    }
}
