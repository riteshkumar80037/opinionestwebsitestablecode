﻿using BusinessAccessLayer.InterfaceDeclaration;
using BusinessEntites;
using CommonMethod;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;

namespace BusinessAccessLayer.ServiceDefination
{
    public class SurveyAPI1ServicesDefination:ISurveyAPI1
    {

        public JSONResponseModel<string> SaveAPI1Surveys(long UserId, string DbName, string SiteUrl)
        {
            JSONResponseModel<string> JsonResponse = new JSONResponseModel<string>();
            try
            {
                string OpinionestCintUrl = AppHelper.OpinionestCintUrlForSurveySet;
                HttpWebRequest WebReq = (HttpWebRequest)WebRequest.Create(string.Format(OpinionestCintUrl + "?UserId=" + UserId + "&DbName=" + DbName + "&SiteUrl=" + SiteUrl));

                WebReq.Method = "GET";

                HttpWebResponse WebResp = (HttpWebResponse)WebReq.GetResponse();

                string jsonString;
                using (Stream stream = WebResp.GetResponseStream())   //modified from your code since the using statement disposes the stream automatically when done
                {
                    StreamReader reader = new StreamReader(stream, System.Text.Encoding.UTF8);
                    jsonString = reader.ReadToEnd();
                }
              
                JsonResponse = JsonConvert.DeserializeObject<JSONResponseModel<string>>(jsonString);
            }
            catch (Exception ex)
            {

            }
            return JsonResponse;
        }

        public List<MySurveyDataListEntites> GetAPISurveys(long UserId)
        {
            List<MySurveyDataListEntites> CintSurveyList = new List<MySurveyDataListEntites>();
            string OpinionestCintUrl = AppHelper.OpinionestCintUrlForSurveyGet;
            HttpWebRequest WebReq = (HttpWebRequest)WebRequest.Create(string.Format(OpinionestCintUrl + "?UserId=" + UserId));

            WebReq.Method = "GET";

            HttpWebResponse WebResp = (HttpWebResponse)WebReq.GetResponse();

            string jsonString;
            using (Stream stream = WebResp.GetResponseStream())   //modified from your code since the using statement disposes the stream automatically when done
            {
                StreamReader reader = new StreamReader(stream, System.Text.Encoding.UTF8);
                jsonString = reader.ReadToEnd();
            }
            JSONSurveyResponse SurveyResponse = new JSONSurveyResponse();
            SurveyResponse = JsonConvert.DeserializeObject<JSONSurveyResponse>(jsonString);
            return CintSurveyList = SurveyResponse.Data;
        }
    }
}
