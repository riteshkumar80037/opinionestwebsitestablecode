﻿using BusinessAccessLayer.InterfaceDeclaration;
using BusinessEntites;
using CommonMethod;
using DataBaseAccess.Defination;
using DataBaseAccess.Interface;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Net;
using System.Text;

namespace BusinessAccessLayer.ServiceDefination
{
    public class ParnterServicesDefination:IPartner
    {
        SqlConnection Conn;
        IDataAccessPartner _dbPartner;
        public ParnterServicesDefination(SqlConnection _Conn)
        {
            Conn = _Conn;
            _dbPartner = new ServicesDataAccessPartner(Conn);
        }
        public long GetPartnerId(string PartnerRef)
        {
            return _dbPartner.GetPartnerId(PartnerRef);

        }

        public void PartnerVerification(long PartnerId, string PartnerParms)
        {
            string Partner_VerficationURL = "";
            PartnerEntities model = new PartnerEntities();
            model = _dbPartner.PartnerDetails(PartnerId);
            Dictionary<string, string> ParmStrList = AppHelper.ConvertParam_Dict(PartnerParms);
            foreach (var item in model.URLKeyMapList)
            {
                if (ParmStrList.ContainsKey(item.Query_Get_Key))
                {
                    Partner_VerficationURL = model.VerificationUrl.Replace(item.Query_Replace_ValueKey, ParmStrList[item.Query_Get_Key]);
                }
            }
            if(!string.IsNullOrEmpty(Partner_VerficationURL))
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(Partner_VerficationURL);
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                     
                }
            }
        }
    }
}
