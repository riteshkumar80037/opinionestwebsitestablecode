﻿using BusinessAccessLayer.GeneralServices;
using BusinessAccessLayer.InterfaceDeclaration;
using BusinessEntites;
using CommonMethod;
using CommonMethod.Defination;
using CommonMethod.Interface;
using DataBaseAccess.Defination;
using DataBaseAccess.Interface;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace BusinessAccessLayer.ServiceDefination
{
    public class HomeServicesDefination : IHome
    {
        SqlConnection Conn;
        IDataAccessHome _DaHome;
        CommonMethod.Interface.ICommon _Comm;

        public HomeServicesDefination(SqlConnection _Conn)
        {
            Conn = _Conn;
            _DaHome = new ServiceDataAccessHome(Conn);
            _Comm = new ServiceDefinationCommon(Conn);
        }

        public List<BlogEntites> Bloglist(int pageindex, int pagesize)
        {
            BlogEntites obj = new BlogEntites();
            DataTable DT = _DaHome.GetBloglist("all", pageindex, pagesize);
            List<BlogEntites> List = new List<BlogEntites>();
            foreach (DataRow Dr in DT.Rows)
            {
                BlogEntites blog = new BlogEntites();
                blog.Id = Convert.ToInt32(Dr["BlogsId"]);
                blog.Header = Dr["Header"].ToString();
                blog.SubHeader = Dr["SubHeader"].ToString();
                blog.Message = Dr["Message"].ToString();
                blog.Message = blog.Message.Substring(0, 500);
                blog.UserName = Dr["FullName"].ToString();
                blog.FromDate = Convert.ToDateTime(Dr["CreatedDate"]);
                blog.TotalCount = Convert.ToInt32(Dr["TotalCount"]);
                blog.ImageType = Convert.ToInt32(Dr["ImageType"]);
                blog.isTop = Convert.ToBoolean(Dr["isTop"]);
                blog.NewHeader = Dr["NewHeader"].ToString();
                DataTable BlogsTag = _DaHome.BlogsTag(blog.Id);
                if(BlogsTag.Rows.Count>0)
                {
                    List<blogsTag> list = new List<blogsTag>();
                    foreach(DataRow drblogs in BlogsTag.Rows)
                    {
                        list.Add(new blogsTag
                        {
                            Link= drblogs["Link"].ToString(),
                            TagName= drblogs["Tag"].ToString(),
                            TagId= Convert.ToInt32(drblogs["Pk_Id"])
                    });
                    }
                    blog.BlogsTag = list;
                }
                //if (HttpContext.Current.Request.IsLocal)
                //{
                //    blog.Image = WebConfigurationManager.AppSettings["LocalPath"] + "Admin/BlogImage/" + Dr["Image"].ToString();
                //}
                //else
                //{
                //    blog.Image = WebConfigurationManager.AppSettings["Path"] + "Admin/BlogImage/" + Dr["Image"].ToString();
                //}
                blog.Image = Dr["Image"].ToString();
                List.Add(blog);
            }
            return List;
        }
        public List<BlogEntites> BlogDetails(int blogId)
        {
            BlogEntites obj = new BlogEntites();
            DataTable DT = _DaHome.GetBlogDetails(blogId, 0, 0);
            List<BlogEntites> List = new List<BlogEntites>();
            foreach (DataRow Dr in DT.Rows)
            {
                BlogEntites blog = new BlogEntites();
                blog.Id = Convert.ToInt32(Dr["BlogsId"]);
                blog.Header = Dr["Header"].ToString();
                blog.Message = Dr["Message"].ToString();
                blog.UserName = Dr["FullName"].ToString();
                blog.FromDate = Convert.ToDateTime(Dr["CreatedDate"]);
                //if (HttpContext.Current.Request.IsLocal)
                //{
                //    blog.Image = WebConfigurationManager.AppSettings["LocalPath"] + "Admin/BlogImage/" + Dr["Image"].ToString();
                //}
                //else
                //{
                //    blog.Image = WebConfigurationManager.AppSettings["Path"] + "Admin/BlogImage/" + Dr["Image"].ToString();
                //}
                ////blog.Image = Server.MapPath("~/Admin/BlogImage/" + Dr["Image"].ToString());
                List.Add(blog);
            }
            return List;
        }

        public Tuple<List<DailyFeedModelList>, int,int> DailyFeedlist(long UserId, int pageNo, int pageSize, int? categoryId=0, bool? IsStart = false, string text="")
        {

            List<DailyFeedModelList> List = new List<DailyFeedModelList>();
            List<DailyFeedEntites> _obj = new List<DailyFeedEntites>();
            int current = 0;
            int totalCount = 0;
            int totalPoint = 0;
            DateTime Date = System.DateTime.Now.Date;
            DataSet Ds = _DaHome.GetDailyFeed(UserId,pageNo,pageSize, categoryId, IsStart, text);
            totalCount = Convert.ToInt32(Ds.Tables[1].Rows[0][0]);
            totalPoint = Convert.ToInt32(Ds.Tables[2].Rows[0][0]);
            if (Ds.Tables[0].Rows.Count>0)
            {
                _obj = ConvertDatatableToList.Convert<DailyFeedEntites>(Ds.Tables[0]);
            }

            var Datefilter = _obj.Select(x => x.Date).Distinct().ToList();
            if (Datefilter!=null)
            {
                foreach(var v in Datefilter)
                {
                    List.Add(new DailyFeedModelList
                    {
                        Date = v.ToString(),
                        _List= _obj.Where(x=>x.Date==v).ToList()
                    }); ;
                }
            }
            return new Tuple<List<DailyFeedModelList>, int,int>(List, totalCount,totalPoint);
        }

        public BlogDetailsEntites BlogDetails(long Id)
        {
            BlogDetailsEntites _Obj = new BlogDetailsEntites();
            try
            {
                DataTable Dt = _DaHome.BlogDetails(Id);
                DataTable SimlerLIst = _DaHome.GetBloglist("all",1,10);
                if (Dt.Rows.Count > 0)
                {
                    _Obj.BlogsId = Dt.Rows[0]["BlogsId"] == null ? 0 : Convert.ToInt64(Dt.Rows[0]["BlogsId"]);
                    _Obj.Header = Dt.Rows[0]["Header"] == null ? "" : Dt.Rows[0]["Header"].ToString();
                    _Obj.Message = Dt.Rows[0]["Message"] == null ? "" : Dt.Rows[0]["Message"].ToString();
                    _Obj.Image = Dt.Rows[0]["Image"] == null ? "" : Dt.Rows[0]["Image"].ToString();
                    _Obj.AltText = Dt.Rows[0]["AltText"] == null ? "" : Dt.Rows[0]["AltText"].ToString();
                    _Obj.SubHeader = Dt.Rows[0]["SubHeader"] == null ? "" : Dt.Rows[0]["SubHeader"].ToString();
                    DataTable BlogsTag = _DaHome.BlogsTag(_Obj.BlogsId);
                    if (BlogsTag.Rows.Count > 0)
                    {
                        List<blogsTag> list = new List<blogsTag>();
                        foreach (DataRow drblogs in BlogsTag.Rows)
                        {
                            list.Add(new blogsTag
                            {
                                Link = drblogs["Link"].ToString(),
                                TagName = drblogs["Tag"].ToString(),
                                TagId = Convert.ToInt32(drblogs["Pk_Id"])
                            });
                        }
                        _Obj.blogsTag = list;
                    }
                }
                List<BlogEntites> List = new List<BlogEntites>();
                foreach (DataRow Dr in SimlerLIst.Rows)
                {
                    BlogEntites blog = new BlogEntites();
                    blog.Id = Convert.ToInt32(Dr["BlogsId"]);
                    blog.Header = Dr["Header"].ToString();
                    blog.SubHeader = Dr["SubHeader"].ToString();
                    blog.Message = Dr["Message"].ToString();
                    blog.Message = blog.Message.Substring(0, 500);
                    blog.UserName = Dr["FullName"].ToString();
                    blog.FromDate = Convert.ToDateTime(Dr["CreatedDate"]);
                    blog.TotalCount = Convert.ToInt32(Dr["TotalCount"]);
                    blog.ImageType = Convert.ToInt32(Dr["ImageType"]);                    
                    blog.Image = Dr["Image"].ToString();
                    List.Add(blog);
                }
                List = List.Where(x => x.Id != Id).Take(4).ToList();
                _Obj.otherlist = List;
            }
            catch (Exception ex)
            {

            }

            return _Obj;

        }

        public List<Slider> SliderList(int _sliderType)
        {
           List<Slider> _Obj = new List<Slider>();
            try
            {
                DataTable _dtSlider = _DaHome.SliderList(_sliderType);
                if (_dtSlider.Rows.Count > 0)
                {
                    _Obj = ConvertDatatableToList.Convert<Slider>(_dtSlider);
                }
            }
            catch (Exception ex)
            {

            }

            return _Obj;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="feedId"></param>
        /// <returns></returns>
        public DailyFeedDetails DailyFeedDetails(int feedId)
        {
            DailyFeedDetails _Obj = new DailyFeedDetails();
            try
            {
                DataTable Dt = _DaHome.DailyFeedDetails(feedId);
                if (Dt.Rows.Count > 0)
                {
                    //_Obj = ConvertDatatableToList.Convert<DailyFeedDetails>(Dt).FirstOrDefault();
                    _Obj.FromDt = Convert.ToDateTime(Dt.Rows[0]["FromDt"]);
                    _Obj.Header = Dt.Rows[0]["Header"].ToString();
                    _Obj.iconPath = Dt.Rows[0]["IconPath"].ToString();
                    _Obj.IsRead =Dt.Rows[0]["IsRead"].ToString()=="True"?"Read":"Unread";
                    _Obj.Message= Dt.Rows[0]["Message"].ToString();
                    _Obj.Points = Convert.ToDecimal( Dt.Rows[0]["Points"].ToString());
                    _Obj.Time = Dt.Rows[0]["Time"].ToString();
                    _Obj.Category = Dt.Rows[0]["Category"].ToString();
                    _Obj.Status = Convert.ToBoolean(Dt.Rows[0]["isCredit"].ToString())==true ? "Credited" : "Pending";
                    if(_Obj.Category=="Offer" || _Obj.Category == "Paytm")
                    {
                        _Obj.Status = "Debited";
                    }
                }
            }
            catch (Exception ex)
            {
            }
            return _Obj;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="inviterId"></param>
        /// <returns></returns>
        public List<ReferralDetailList> ReferralList(long UserId, int pageNo, int pageSize)
        {
            var skip = pageSize * (pageNo - 1);
            List<ReferralDetails> _Obj = new List<ReferralDetails>();
            List<ReferralDetailList> _ObjList = new List<ReferralDetailList>();
            try
            {
                DataTable Dt = _DaHome.ReferralList(UserId);
                if (Dt.Rows.Count > 0)
                {
                    _Obj = ConvertDatatableToList.Convert<ReferralDetails>(Dt);
                }
                foreach(var v in _Obj)
                {
                    ReferralDetailList referralDetailList = new ReferralDetailList();
                    referralDetailList = new ReferralDetailList
                    {
                        CreatedDate=v.CreatedDate,
                        Name=v.Name,
                        Points=v.Points,
                        shortName=v.shortName
                
                    };
                    if (v.Points!=0 && v.Status== "Verified")
                    {
                        referralDetailList.Status = "Referral Successful";
                        referralDetailList.color = "Green";
                    }
                    if (v.Points == 0 && v.Status == "Verified")
                    {
                        referralDetailList.Status = "Survey Pending";
                        referralDetailList.color = "Yellow";
                    }
                    else  if (v.Points == 0 && v.Status == "Accepted")
                    {
                        referralDetailList.Status = "Verification Pending";
                       referralDetailList.color = "Red";
                    }

                    _ObjList.Add(referralDetailList);
                }
            }
            catch (Exception ex)
            {
            }
            return _ObjList.Skip(skip).Take(pageSize).ToList();
        }

        public long HomePageTotalPoints()
        {
           
            try
            {
                long Points = _DaHome.HomePageTotalPoints();
                return Points;


            }
            catch (Exception ex)
            {
                return 0;
            }
            
        }

        /// <summary>
        /// Contact Form Submission
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public string ContactFormSubmission(ContactUsEntities model, string clientkey)
        {
            try
            {
                string result = _DaHome.ContactFormSubmission(model);
                EmailEntites _email = new EmailEntites();
                _email.UserEmail = model.ContactEmail;
                _email.FirstName = model.ContactName;
                _email.subject = "Contact Us";
                _email.Type = 3;
                _email.client = clientkey;
                _email.Query = "Name: "  + model.ContactName + "<br/>" + "Email Id: "+ model.ContactEmail + "<br/>" + "Contact No: " + model.ContactNo + "<br/>" + model.Query + "<br/>" + "Ticket No: " + result;
                var Resutl = _Comm.SendEmail(_email);
                return result;
            }
            catch (Exception ex)
            {
                return "Error";
            }
        }

        /// <summary>
        /// Review List
        /// </summary>
        /// <param name="pageindex"></param>
        /// <param name="pagesize"></param>
        /// <returns></returns>
        public List<CustomerReview> ReviewList(int pageindex, int pagesize)
        {
            List<CustomerReview> _Obj = new List<CustomerReview>();
            try
            {
                DataTable _dtReview = _DaHome.ReviewList(pageindex, pagesize);
                if (_dtReview.Rows.Count > 0)
                {
                    _Obj = ConvertDatatableToList.Convert<CustomerReview>(_dtReview);
                }
            }
            catch (Exception ex)
            {

            }

            return _Obj;

        }

        public BlogDetailsEntites BlogDetails(string header)
        {
            BlogDetailsEntites _Obj = new BlogDetailsEntites();
            try
            {
                DataTable Dt = _DaHome.BlogDetails(header);
                DataTable SimlerLIst = _DaHome.GetBloglist("all", 1, 10);
                if (Dt.Rows.Count > 0)
                {
                    _Obj.BlogsId = Dt.Rows[0]["BlogsId"] == null ? 0 : Convert.ToInt64(Dt.Rows[0]["BlogsId"]);
                    _Obj.Header = Dt.Rows[0]["Header"] == null ? "" : Dt.Rows[0]["Header"].ToString();
                    _Obj.Message = Dt.Rows[0]["Message"] == null ? "" : Dt.Rows[0]["Message"].ToString();
                    _Obj.Image = Dt.Rows[0]["Image"] == null ? "" : Dt.Rows[0]["Image"].ToString();
                    _Obj.AltText = Dt.Rows[0]["AltText"] == null ? "" : Dt.Rows[0]["AltText"].ToString();
                    _Obj.SubHeader = Dt.Rows[0]["SubHeader"] == null ? "" : Dt.Rows[0]["SubHeader"].ToString();
                    DataTable BlogsTag = _DaHome.BlogsTag(_Obj.BlogsId);
                    if (BlogsTag.Rows.Count > 0)
                    {
                        List<blogsTag> list = new List<blogsTag>();
                        foreach (DataRow drblogs in BlogsTag.Rows)
                        {
                            list.Add(new blogsTag
                            {
                                Link = drblogs["Link"].ToString(),
                                TagName = drblogs["Tag"].ToString(),
                                TagId = Convert.ToInt32(drblogs["Pk_Id"])
                            });
                        }
                        _Obj.blogsTag = list;
                    }
                }
                List<BlogEntites> List = new List<BlogEntites>();
                foreach (DataRow Dr in SimlerLIst.Rows)
                {
                    BlogEntites blog = new BlogEntites();
                    blog.Id = Convert.ToInt32(Dr["BlogsId"]);
                    blog.Header = Dr["Header"].ToString();
                    blog.SubHeader = Dr["SubHeader"].ToString();
                    blog.Message = Dr["Message"].ToString();
                    blog.Message = blog.Message.Substring(0, 500);
                    blog.UserName = Dr["FullName"].ToString();
                    blog.FromDate = Convert.ToDateTime(Dr["CreatedDate"]);
                    blog.TotalCount = Convert.ToInt32(Dr["TotalCount"]);
                    blog.ImageType = Convert.ToInt32(Dr["ImageType"]);
                    blog.Image = Dr["Image"].ToString();
                    blog.NewHeader = Dr["NewHeader"].ToString();
                    List.Add(blog);
                }
                List = List.Where(x => x.NewHeader != header).Take(4).ToList();
                _Obj.otherlist = List;
            }
            catch (Exception ex)
            {

            }

            return _Obj;
        }

        public mobileFeature mobilefeatures(string code)
        {
            mobileFeature mobileFeature = new mobileFeature();
            DataTable dt = _DaHome.mobileFetatures(code);
            if(dt.Rows.Count>0)
            {
                mobileFeature.Id = Convert.ToInt32(dt.Rows[0]["Id"].ToString());
                mobileFeature.Code = Convert.ToString(dt.Rows[0]["Code"].ToString());
                mobileFeature.Status = Convert.ToBoolean(dt.Rows[0]["Status"].ToString());
            }
            return mobileFeature;

        }
    }
}
