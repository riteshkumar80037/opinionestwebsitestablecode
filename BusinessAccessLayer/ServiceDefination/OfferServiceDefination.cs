﻿using BusinessAccessLayer.GeneralServices;
using BusinessAccessLayer.InterfaceDeclaration;
using BusinessEntites;
using CommonMethod;
using CommonMethod.Defination;
using DataBaseAccess.Defination;
using DataBaseAccess.Interface;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;

namespace BusinessAccessLayer.ServiceDefination
{
    public class OfferServiceDefination: IOffer
    {
        SqlConnection Conn;
        IDataAccessOffer _DaOffer;
        CommonMethod.Interface.ICommon _Comm;
        ILogin _Login;
        Iuser _IUser;

        public OfferServiceDefination(SqlConnection _Conn)
        {
            Conn = _Conn;
            _DaOffer = new ServiceDataAccessOffer(Conn);
            _Comm = new ServiceDefinationCommon(Conn);
            _Login = new LoginServicesDefination(_Conn);
            _IUser = new UserServicesDefination(_Conn);
        }

        public List<Offer> OfferList(long UserId, string offerType, int? pageindex, int pagesize)
        {
            List<Offer> _Obj = new List<Offer>();
            try
            {
                DataTable _dtReferral = _DaOffer.OfferList(UserId, offerType, pageindex, pagesize);
                if (_dtReferral.Rows.Count > 0)
                {
                    _Obj = ConvertDatatableToList.Convert<Offer>(_dtReferral);
                }
            }
            catch (Exception ex)
            {

            }

            return _Obj;

        }

        /// <summary>
        /// Update User Points
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public string UpdatePoints(PointsUpdateInputModel model)   
        {
            string result = string.Empty;
            try
            {
                result = _DaOffer.UpdatePoints(model);
            }
            catch (Exception Ex)
            {

            }
            return result;
        }

        /// <summary>
        /// Mark Offer Favourite
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public long MarkOfferFavourite(MarkOfferFavouriteInputModel model)
        {
            long i = 0;
            try
            {
                i = _DaOffer.MarkOfferFavourite(model);
            }
            catch (Exception Ex)
            {

            }
            return i;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="UserId"></param>
        /// <param name="pageindex"></param>
        /// <param name="pagesize"></param>
        /// <returns></returns>
        public List<OfferHistoryDetails> OfferHistoryDetails(long UserId, string offerType, int? pageindex, int pagesize)
        {
            List<OfferHistoryDetails> _Obj = new List<OfferHistoryDetails>();
            try
            {
                DataTable _dtOffer = _DaOffer.OfferHistoryDetails(UserId, offerType, pageindex, pagesize);
                if (_dtOffer.Rows.Count > 0)
                {
                    _Obj = ConvertDatatableToList.Convert<OfferHistoryDetails>(_dtOffer);
                }
            }
            catch (Exception ex)
            {

            }

            return _Obj;

        }

        /// <summary>
        /// CashVoucherDetails
        /// </summary>
        /// <param name="CountryId"></param>
        /// <returns></returns>
        public List<UserRedeemRequestModel> CashVoucherDetails(long CountryId)
        {
            List<UserRedeemRequestModel> _Obj = new List<UserRedeemRequestModel>();
            try
            {
                DataTable _dtReferral = _DaOffer.CashVoucherDetails(CountryId);
                if (_dtReferral.Rows.Count > 0)
                {
                    _Obj = ConvertDatatableToList.Convert<UserRedeemRequestModel>(_dtReferral);
                }
            }
            catch (Exception ex)
            {

            }

            return _Obj;

        }

        /// <summary>
        /// CheckUserAccountLinked
        /// </summary>
        /// <param name="CountryId"></param>
        /// <returns></returns>
        public List<UserAccountLinkedStatus> CheckUserAccountLinked(long userId, long paymentGatewayId)
        {
            List<UserAccountLinkedStatus> _Obj = new List<UserAccountLinkedStatus>();
            try
            {
                DataTable _dtReferral = _DaOffer.CheckUserAccountLinked(userId, paymentGatewayId);
                if (_dtReferral.Rows.Count > 0)
                {
                    _Obj = ConvertDatatableToList.Convert<UserAccountLinkedStatus>(_dtReferral);
                }
            }
            catch (Exception ex)
            {

            }

            return _Obj;

        }

        /// <summary>
        /// Link User Wallet
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public long LinkUserWallet(UserLinkWalletInputModel model)
        {
            long i = 0;
            try
            {
                i = _DaOffer.LinkUserWallet(model);
            }
            catch (Exception Ex)
            {

            }
            return i;
        }

        /// <summary>
        /// GetUniqueNoForPaymentGateway
        /// </summary>
        /// <param name="PaymentGatewayId"></param>
        /// <returns></returns>
        public string GetUniqueNoForPaymentGateway(long PaymentGatewayId)
        {
            string result = string.Empty;
            try
            {
                PaymenGatewayPrefix PaymentGateway = (PaymenGatewayPrefix)PaymentGatewayId;
                result = _DaOffer.GetUniqueNoForPaymentGateway(PaymentGateway.ToString());
            }
            catch (Exception Ex)
            {

            }
            return result;
        }

        /// <summary>
        /// Get Paytm User Status
        /// </summary>
        /// <param name="PhoneNo"></param>
        /// <param name="paymentgatewayId"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public PaytmApiModel GetPaytmUserStatus(string PhoneNo, long paymentgatewayId, long UserId)
        {
            long lastId = 0;
            PaytmApiModel gatewaymodel = new PaytmApiModel();
            PaytmApiModel model = new PaytmApiModel();
            model.Id = paymentgatewayId;
            model.merchantguid = GlobalConstant.PaytmMerchantGuid;
            model.AesKey = GlobalConstant.PaytmAesKey;
            model.saleswalletid = GlobalConstant.Paytmsaleswalletid;
            model.orderid = GetUniqueNoForPaymentGateway((long)PaymentGatways.PayTM);
            model.Phone = PhoneNo;
            model.postData = "{\"request\":{\"requestType\":\"VERIFY\",\"merchantGuid\":\"" + model.merchantguid + "\",\"merchantOrderId\":\"" + model.orderid + "\",\"salesWalletGuid\":\"" + model.saleswalletid + "\",\"salesWalletName\":\"PayTM\",\"payeeEmailId\":null,\"payeePhoneNumber\":\"" + model.Phone + "\",\"payeeSsoId\":null,\"appliedToNewUsers\":\"N\",\"amount\":\"1\",\"currencyCode\":\"INR\",\"callbackURL\":\"http:////" + GlobalConstant.SiteURL + "//survey//paytmAPITest.aspx\"},\"metadata\":\"Testing Data\",\"ipAddress\":\"127.0.0.1\",\"operationType\":\"SALES_TO_USER_CREDIT\",\"platformName\":\"PayTM\"}";
            model.checksum = paytm.CheckSum.generateCheckSumByJson(model.AesKey, model.postData);
            model.PaytmTransectionUri = " https://trust.paytm.in/wallet-web/asyncSalesToUserCredit";
            //string uri = "https://trust-uat.paytm.in/wallet-web/salesToUserCredit";//Syncronious API
            GetPaytmResonse(ref model, GatewayRequestType.Verify.ToString());
            gatewaymodel.Id = paymentgatewayId;
            gatewaymodel.UserId = UserId;
            gatewaymodel.Request_Type = (long)GatewayRequestResponse.VerifyRequestResponse;
            gatewaymodel.RequestJson = model.postData;
            gatewaymodel.ResponseJson = model.response;
            gatewaymodel.ResponseStatus = string.IsNullOrEmpty(model.responseverify.statusMessage) ? model.responseverify.status : model.responseverify.statusMessage;
            SavePaymentGatewayLogs(gatewaymodel,ref lastId);
            Thread.Sleep(9000);//This line used to make differece between calling get api again .
            GetTransectionStatus(ref model, GatewayRequestType.Verify.ToString());
            gatewaymodel.Request_Type = (long)GatewayRequestResponse.VerifyTransectionStatus;
            gatewaymodel.RequestJson = model.postData;
            gatewaymodel.ResponseJson = model.response;
            gatewaymodel.ResponseStatus = string.IsNullOrEmpty(model.responseverify.statusMessage) ? model.responseverify.status : model.responseverify.statusMessage;
            SavePaymentGatewayLogs(gatewaymodel,ref lastId);
            //model = GetPaytmTransStatus(model);
            model.statusMessage = gatewaymodel.ResponseStatus;
            if(model.statusMessage=="SUCCESS")
            {
                _DaOffer.SavePaytmVerifyNo(gatewaymodel.UserId, PhoneNo);
            }
            return model;
        }

        public void GetPaytmResonse(ref PaytmApiModel model, string RequestType)
        {
            TransectionStatusResponse TranstatusRes = new TransectionStatusResponse();
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(model.PaytmTransectionUri);
            webRequest.Method = "POST";
            webRequest.Accept = "application/json";
            webRequest.ContentType = "application/json";
            webRequest.Headers.Add("mid", model.merchantguid);
            webRequest.Headers.Add("checksumhash", model.checksum); ;
            webRequest.ContentLength = model.postData.Length;
            try
            {
                using (StreamWriter requestWriter2 = new StreamWriter(webRequest.GetRequestStream()))
                {
                    requestWriter2.Write(model.postData);
                }

                //  This actually does the request and gets the response back;

                string responseData = string.Empty;

                using (StreamReader responseReader = new StreamReader(webRequest.GetResponse().GetResponseStream()))
                {
                    model.response = responseReader.ReadToEnd();
                    if (RequestType == GatewayRequestType.Verify.ToString())
                    {
                        var respo = JsonConvert.DeserializeObject<ValidationStatusResponse>(model.response);
                        model.responseverify = respo;
                        if (model.responseverify.status == null && model.responseverify.statusCode == null)
                        {
                            model.responsepay = JsonConvert.DeserializeObject<TransectionList>(model.response);
                            if (model.responsepay != null)
                            {
                                TranstatusRes = model.responsepay.txnList.Select(x => x).FirstOrDefault();
                                model.responseverify.statusMessage = TranstatusRes.message;
                            }
                        }
                    }
                    else if (RequestType == GatewayRequestType.Transection.ToString())
                    {
                        model.responsepay = JsonConvert.DeserializeObject<TransectionList>(model.response);
                        if (model.responsepay.txnList == null)
                        {
                            model.responseverify = JsonConvert.DeserializeObject<ValidationStatusResponse>(model.response);
                        }
                    }
                }
            }
            catch (WebException web)
            {
                HttpWebResponse res = web.Response as HttpWebResponse;
                Stream s = res.GetResponseStream();
                using (StreamReader sr = new StreamReader(s))
                {
                    model.statusMessage = sr.ReadToEnd();
                }
            }
            catch (Exception ex)
            {

            }
        }

        public PaytmApiModel GetTransectionStatus(ref PaytmApiModel model, string Requesttype)
        {
            model.merchantguid = GlobalConstant.PaytmMerchantGuid;
            model.AesKey = GlobalConstant.PaytmAesKey;
            //model.PaytmTransectionUri = "https://trust-uat.paytm.in/wallet-web/txnStatusList";
            model.PaytmTransectionUri = " https://trust.paytm.in/wallet-web/txnStatusList";
            //string PostStatusdata = "{\r\n\"request\": {\r\n\"requestType\":\"merchanttxnId\",\r\n\"txnType\":\"SALES_TO_USER_CREDIT\",\r\n\"txnId\": \"Track-636918902227399894\"\r\n\"mId\" : \"1b6eafb0-ed13-4b65-b834-86427fdb8052\"\r\n},\r\n\"ipAddress\": \"127.0.0.1\",\r\n\"platformName\": \"PayTM\",\r\n\"operationType\": \"CHECK_TXN_STATUS\",\r\n\u201Cchannel\u201D:\u201D\u201D,\r\n\u201Cversion\u201D:\u201D\u201D\r\n}";
            //model.postData = "{\"request\":{\"requestType\":\"merchanttxnid\",\"txnType\":\"SALES_TO_USER_CREDIT\",\"txnId\":\"Track-636921440932903473\",\"mId\":\"TrackO30770234457239\"},\"ipAddress\":\"127.0.0.1\",\"operationType\":\"CHECK_TXN_STATUS\",\"platformName\":\"PayTM\"}";
            model.postData = "{\"request\":{\"requestType\":\"merchanttxnid\",\"txnType\":\"SALES_TO_USER_CREDIT\",\"txnId\":\"" + model.orderid + "\",\"mId\":\"TrackO70456283268961\"},\"ipAddress\":\"127.0.0.1\",\"operationType\":\"CHECK_TXN_STATUS\",\"platformName\":\"PayTM\"}";
            model.checksum = paytm.CheckSum.generateCheckSumByJson(model.AesKey, model.postData);
            GetPaytmResonse(ref model, Requesttype);
            return model;
        }

        /// <summary>
        /// Save Payment Gateway Logs
        /// </summary>
        /// <param name="model"></param>
        public void SavePaymentGatewayLogs(PaytmApiModel model, ref long lastId)
        {
            lastId= _DaOffer.SavePaymentGatewayLogs(model);
        }

        /// <summary>
        /// Send User Point Redeem Request
        /// </summary>
        /// <param name="PointsRedeem"></param>
        /// <param name="RequestedAmount"></param>
        /// <param name="PGId"></param>
        /// <param name="mobileNumber"></param>
        /// <param name="Points"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public bool SendUserPointRedeemRequest(long PointsRedeem, string RequestedAmount, string PGId, string mobileNumber, long UserId, long Points)
        {
            UserRedeemRequestModel model = new UserRedeemRequestModel();
            model.Points = Points;
            model.RequestNumbers = string.IsNullOrEmpty(RequestedAmount) ? 0 : float.Parse(RequestedAmount);
            model.PaymentGatewayId = string.IsNullOrEmpty(PGId) ? 0 : Convert.ToInt64(PGId.DecryptID());
            PaytmApiModel modelpaytm = new PaytmApiModel();
            modelpaytm.Phone = mobileNumber;
            GetPaytmTransStatus(modelpaytm, model, UserId);
            return true;
        }

        public PaytmApiModel GetPaytmTransStatus(PaytmApiModel model, UserRedeemRequestModel offermodel, long UserId)
        {
            long lastId = 0;
            PaytmApiModel gatewaymodel = new PaytmApiModel();
            PaytmApiModel modelTrans = new PaytmApiModel();
            modelTrans.merchantguid = GlobalConstant.PaytmMerchantGuid;
            modelTrans.AesKey = GlobalConstant.PaytmAesKey;
            modelTrans.saleswalletid = GlobalConstant.Paytmsaleswalletid;
            modelTrans.orderid = "Track-" + DateTime.Now.Ticks.ToString();
            modelTrans.Phone = model.Phone;
            modelTrans.postData = "{\"request\":{\"requestType\":null,\"merchantGuid\":\"" + modelTrans.merchantguid + "\",\"merchantOrderId\":\"" + modelTrans.orderid + "\",\"salesWalletGuid\":\"" + modelTrans.saleswalletid + "\",\"salesWalletName\":\"PayTM\",\"payeeEmailId\":null,\"payeePhoneNumber\":\"" + modelTrans.Phone + "\",\"payeeSsoId\":null,\"appliedToNewUsers\":\"N\",\"amount\":\"" + offermodel.RequestNumbers + "\",\"currencyCode\":\"INR\",\"callbackURL\":\"http:////" + GlobalConstant.SiteURL + "//survey//paytmAPITest.aspx\"},\"metadata\":\"Testing Data\",\"ipAddress\":\"127.0.0.1\",\"operationType\":\"SALES_TO_USER_CREDIT\",\"platformName\":\"PayTM\"}";
            modelTrans.checksum = paytm.CheckSum.generateCheckSumByJson(modelTrans.AesKey, modelTrans.postData);
            modelTrans.PaytmTransectionUri = " https://trust.paytm.in/wallet-web/asyncSalesToUserCredit";
            //string uri = "https://trust-uat.paytm.in/wallet-web/salesToUserCredit";//Syncronious API
            GetPaytmResonse(ref modelTrans, GatewayRequestType.Verify.ToString());
            gatewaymodel.Id = offermodel.PaymentGatewayId;
            gatewaymodel.UserId = UserId;
            gatewaymodel.Request_Type = (long)GatewayRequestType.Verify;
            gatewaymodel.RequestJson = modelTrans.postData;
            gatewaymodel.ResponseJson = modelTrans.response;
            gatewaymodel.Points = offermodel.Points;
            gatewaymodel.Amount = offermodel.RequestNumbers;
            gatewaymodel.ResponseStatus = string.IsNullOrEmpty(modelTrans.responseverify.statusMessage) ? modelTrans.responseverify.status : modelTrans.responseverify.statusMessage;

            if (gatewaymodel.ResponseStatus.ToLower() == "ACCEPTED".Trim().ToLower())
            {
                SavePaymentGatewayLogs(gatewaymodel, ref lastId);
            }
            else
            {
                PaymentGatewaysErrorLogs paymentGatewaysLog = new PaymentGatewaysErrorLogs
                {
                    Fk_PaymentgatewayId = (int)PaymentGatewayMaster.PayTM,
                    Fk_UserId = UserId,
                    Points = offermodel.Points,
                    RequestJson = gatewaymodel.RequestJson,
                    Request_Type = gatewaymodel.Request_Type,
                    ResponseJson = gatewaymodel.response,
                    ResponseStatus = gatewaymodel.ResponseStatus,
                    RequestedAmount = Convert.ToDecimal(gatewaymodel.Amount),
                    paymentCookiesId = model.PaytmCookiedId,
                    Error = ""
                };
                _IUser.paymentGetwayErrorLogs(paymentGatewaysLog);

            }

            Thread.Sleep(9000);//This line used to make differece between calling get api again .
            GetTransectionStatus(ref modelTrans, GatewayRequestType.Transection.ToString());
            gatewaymodel.Request_Type = (long)GatewayRequestType.Transection;
            gatewaymodel.RequestJson = modelTrans.postData;
            gatewaymodel.ResponseJson = modelTrans.response;
            gatewaymodel.ResponseStatus = modelTrans.responsepay.txnList != null ? modelTrans.responsepay.txnList[0].message : modelTrans.responseverify.statusMessage;
            gatewaymodel.Points = offermodel.Points;
            gatewaymodel.Amount = offermodel.RequestNumbers;
            gatewaymodel.IsTransAmount = true;

            gatewaymodel.PaytmCookiedId = model.PaytmCookiedId;


            //model = GetPaytmTransStatus(model);
            modelTrans.statusMessage = gatewaymodel.ResponseStatus;
            if (modelTrans.statusMessage == "SUCCESS")
            {
                SavePaymentGatewayLogs(gatewaymodel, ref lastId);
                //decimal amout = Convert.ToDecimal(offermodel.RequestNumbers);
                //_DaOffer.SavePaytmTransactionUpdatation(UserId, model.PaytmCookiedId,lastId, offermodel.Points, amout);
            }
            else
            {
                PaymentGatewaysErrorLogs paymentGatewaysLog = new PaymentGatewaysErrorLogs
                {
                    Fk_PaymentgatewayId = (int)PaymentGatewayMaster.PayTM,
                    Fk_UserId = UserId,
                    Points = offermodel.Points,
                    RequestJson = gatewaymodel.RequestJson,
                    Request_Type = gatewaymodel.Request_Type,
                    ResponseJson = gatewaymodel.ResponseJson,
                    ResponseStatus = gatewaymodel.ResponseStatus,
                    RequestedAmount = Convert.ToDecimal(gatewaymodel.Amount),
                    paymentCookiesId = gatewaymodel.PaytmCookiedId,
                    Error = ""
                };
                _IUser.paymentGetwayErrorLogs(paymentGatewaysLog);
            }
            return modelTrans;
        }

        /// <summary>
        /// GetPointsValue
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public decimal GetPointsValue(long userId)
        {
            return _DaOffer.GetPointsValue(userId);
        }

        /// <summary>
        /// TotalCashback
        /// </summary>
        /// <param name="CountryId"></param>
        /// <returns></returns>
        public long TotalCashback(long userId)
        {
            return _DaOffer.TotalCashback(userId);
        }

        
        Tuple<string, decimal> IOffer.ConvertPointToCurrency(ConvertPointToCurrency model,long UserId)
        {
            return _DaOffer.convertPointsToCurrency(model, UserId);
        }

        public Tuple<string, string, string, List<OfferFavirout>> offerUserDetails(long useriId)
        {
            List<Offer> _Obj = new List<Offer>();
            List<OfferFavirout> _Favt = new List<OfferFavirout>();
            try
            {
                DataTable _dtReferral = _DaOffer.OfferUserData(useriId);
                DataTable _dtfavt = _DaOffer.offerfevt(useriId);

                if (_dtfavt.Rows.Count > 0)
                {
                    _Favt = ConvertDatatableToList.Convert<OfferFavirout>(_dtfavt);
                }

                if (_dtReferral.Rows.Count > 0)
                {
                    string points = _dtReferral.Rows[0]["points"].ToString();
                    string pointvalue = _dtReferral.Rows[0]["pointvalue"].ToString();
                    string country = _dtReferral.Rows[0]["country"].ToString();
                    return new Tuple<string, string, string, List<OfferFavirout>>(points, pointvalue, country, _Favt);
                }
                return new Tuple<string, string, string, List<OfferFavirout>>("", "", "", _Favt);
            }
            catch (Exception ex)
            {

            }

             return new Tuple<string, string, string, List<OfferFavirout>>("", "", "", _Favt);
        }

        public int DeletePointTransaction(long TransId)
        {
            return Convert.ToInt32( _DaOffer.deletePointTransction(TransId));
        }

        public string tangoLogs(tangoLogs model)
        {
            return (_DaOffer.tanglLogs(model));
        }

        public void DailyFeedSave(DailyFeedEntites dailyFeedEntites)
        {
            _DaOffer.DailyFeedSave(dailyFeedEntites);
        }

        public bool CheckExistVefication(long UserId, string phone)
        {
            return _DaOffer.CheckExistVefication(UserId, phone);
        }

        public long PaymentCookiesSave(PaymentCookies paymentCookies)
        {
            return _DaOffer.PaymentCookiesSave(paymentCookies);
        }

        public List<PaytmResponseMail> SendUserPointRedeemRequest()
        {
            List<PaytmResponseMail> retunlist = new List<PaytmResponseMail>();
            try
            {
                DataTable dt = _DaOffer.GetPaymentCookiesPending();
                List<PaymentCookiesPending> list = ConvertDatatableToList.Convert<PaymentCookiesPending>(dt);
                foreach (var v in list)
                {
                    //UserRedeemRequestModel model = new UserRedeemRequestModel();
                    //model.Points = Points;
                    //model.RequestNumbers = string.IsNullOrEmpty(RequestedAmount) ? 0 : float.Parse(RequestedAmount);
                    //model.PaymentGatewayId = string.IsNullOrEmpty(PGId) ? 0 : Convert.ToInt64(PGId.DecryptID());
                    //PaytmApiModel modelpaytm = new PaytmApiModel();
                    //modelpaytm.Phone = mobileNumber;
                    //GetPaytmTransStatus(modelpaytm, model, UserId);
                    //    return true;
                    UserRedeemRequestModel model = new UserRedeemRequestModel();
                    model.Points = v.Point;
                    model.RequestNumbers =  (float)v.RequestedAmount;
                    model.PaymentGatewayId = string.IsNullOrEmpty(v.PGId) ? 0 : Convert.ToInt64(v.PGId.DecryptID());
                    PaytmApiModel modelpaytm = new PaytmApiModel();
                    modelpaytm.Phone = v.MobileNo;
                    modelpaytm.PaytmCookiedId = v.Id;
                    PaytmApiModel paytmApiModel= GetPaytmTransStatus(modelpaytm, model, v.Fk_UserId);
                    if (paytmApiModel.statusMessage == "SUCCESS")
                    {
                        retunlist.Add(new PaytmResponseMail
                        {Amount= v.RequestedAmount,
                        UserId= v.Fk_UserId

                        });
                    }
                }
                return retunlist;

            }
            catch(Exception ex)
            {
                return retunlist;
            }
        }

        public List<PaytmResponseMail> SendUserPointRedeemRequestPick()
        {
            List<PaytmResponseMail> retunlist = new List<PaytmResponseMail>();
            try
            {
                DataTable dt = _DaOffer.GetPaymentCookiesPendingPick();
                List<PaymentCookiesPending> list = ConvertDatatableToList.Convert<PaymentCookiesPending>(dt);
                foreach (var v in list)
                {                   
                        retunlist.Add(new PaytmResponseMail
                        {
                            Amount = v.RequestedAmount,
                            UserId = v.Fk_UserId,
                            Mobile=v.MobileNo
                        });                    
                }
                return retunlist;

            }
            catch (Exception ex)
            {
                return retunlist;
            }
        }
        public List<PaytmResponseMail> SendUserPointRedeemRequest(PaymentCookiesPending paymentCookiesPending, string email, string key)
        {
            List<PaytmResponseMail> retunlist = new List<PaytmResponseMail>();
            try
            {
                //DataTable dt = _DaOffer.GetPaymentCookiesPending();
                //List<PaymentCookiesPending> list = ConvertDatatableToList.Convert<PaymentCookiesPending>(dt);
                List<PaymentCookiesPending> list = new List<PaymentCookiesPending>();
                list.Add(paymentCookiesPending);
                foreach (var v in list)
                {

                    try
                    {
                        UserRedeemRequestModel model = new UserRedeemRequestModel();
                        model.Points = v.Point;
                        model.RequestNumbers = (float)v.RequestedAmount;
                        model.PaymentGatewayId = string.IsNullOrEmpty(v.PGId) ? 0 : Convert.ToInt64(v.PGId.DecryptID());
                        PaytmApiModel modelpaytm = new PaytmApiModel();
                        modelpaytm.Phone = v.MobileNo;
                        modelpaytm.PaytmCookiedId = v.Id;
                        PaytmApiModel paytmApiModel = GetPaytmTransStatus(modelpaytm, model, v.Fk_UserId);
                        if (paytmApiModel.statusMessage == "SUCCESS")
                        {
                            retunlist.Add(new PaytmResponseMail
                            {
                                Amount = v.RequestedAmount,
                                UserId = v.Fk_UserId

                            });
                            string message = "Successfully Amount Credited in Below user Account";

                            message += "<br/>";
                            message += "UserId=" + v.email + " Amount=" + v.RequestedAmount;

                            _Login.sendEmail(email, "Paytm Amount Credited Status:" + paytmApiModel.statusMessage, message, key);
                        }
                        else
                        {
                            //PaymentGatewaysErrorLogs paymentGatewaysLog = new PaymentGatewaysErrorLogs
                            //{
                            //    Fk_PaymentgatewayId = (int)PaymentGatewayMaster.PayTM,
                            //    Fk_UserId = v.Fk_UserId,
                            //    Points = v.Point,
                            //    RequestJson = paytmApiModel.RequestJson,
                            //    Request_Type = v.Type,
                            //    ResponseJson = paytmApiModel.response,
                            //    ResponseStatus = paytmApiModel.statusMessage,
                            //    RequestedAmount = v.RequestedAmount,
                            //    paymentCookiesId = v.Id,
                            //    Error = ""
                            //};
                            //_IUser.paymentGetwayErrorLogs(paymentGatewaysLog);

                            string message = "Paytm Amount Credited failed";
                            message += "<br/>";
                            message += "UserId=" + v.email + " Amount=" + v.RequestedAmount;
                            _Login.sendEmail(email, "Paytm Amount Credited Status:" + paytmApiModel.statusMessage, message, key);

                        }
                    }
                    catch (Exception ex)
                    {
                        string message = "There is some issue with Paytm Amount Transfer";

                        message += "<br/>";
                        message += "UserId=" + v.email + " Amount=" + v.RequestedAmount + " Error:" + ex.Message;

                        _Login.sendEmail(email, "Paytm Amount Credited Failed", message, key);
                        PaymentGatewaysErrorLogs paymentGatewaysLog = new PaymentGatewaysErrorLogs
                        {
                            Fk_PaymentgatewayId = (int)PaymentGatewayMaster.PayTM,
                            Fk_UserId = v.Fk_UserId,
                            Points = v.Point,
                            RequestJson = "",
                            Request_Type = v.Type,
                            ResponseJson = "",
                            ResponseStatus = "",
                            RequestedAmount = v.RequestedAmount,
                            paymentCookiesId = v.Id,
                            Error = ex.Message
                        };
                        _IUser.paymentGetwayErrorLogs(paymentGatewaysLog);

                    }
                }
                return retunlist;

            }
            catch (Exception ex)
            {

                return retunlist;
            }
        }

    }
}
