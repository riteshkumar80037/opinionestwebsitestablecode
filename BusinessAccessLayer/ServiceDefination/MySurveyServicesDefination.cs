﻿using BusinessAccessLayer.InterfaceDeclaration;
using BusinessEntites;
using CommonMethod.Defination;
using DataBaseAccess.Defination;
using DataBaseAccess.Interface;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.Text;

namespace BusinessAccessLayer.ServiceDefination
{
    public class MySurveyServicesDefination : IMySurvey
    {
        SqlConnection Conn;
        IDataAccessMySurvey _Survey;
        CommonMethod.Interface.ICommon _Comm;
        ISurveyAPI1 _SurveyAPI1;
        public MySurveyServicesDefination(SqlConnection _Conn)
        {
            Conn = _Conn;
            _Survey = new ServiceDataAccessMySurvey(Conn);
            _SurveyAPI1 = new SurveyAPI1ServicesDefination();
            _Comm = new ServiceDefinationCommon(Conn);
        }
        public MySurveyColection MySurveylist(string UserId, int pageindex, int pageSize)
        {
            DataSet DT = _Survey.MySurveylist(UserId, pageindex, pageSize);
            MySurveyColection MyCollection = new MySurveyColection();
            List<MySurveyDataListEntites> List = new List<MySurveyDataListEntites>();
            List<MySurveyDataListEntites> SurveyAPI1List = new List<MySurveyDataListEntites>();
            if (DT.Tables.Count > 0)
            {
                foreach (DataRow Dr in DT.Tables[0].Rows)
                {
                    MySurveyDataListEntites obj = new MySurveyDataListEntites();
                    obj.SurveyId = Convert.ToInt32(Dr["SurveyId"].ToString());
                    obj.SurveyNo = Dr["SurveyNo"].ToString();
                    obj.Survey_Type = Convert.ToInt32(Dr["Survey_Type"].ToString()) == 1 ? SurveyType.Static : SurveyType.Dynamic;
                    obj.SurveyStatus = Convert.ToInt32(Dr["SurveyStatus"].ToString());
                    obj.FilterStatus = Convert.ToInt32(Dr["FilterStatus"].ToString());
                    obj.URL = Dr["URL"].ToString();
                    obj.UserSurveyStatus = GetSurveyStatus(Convert.ToInt32(Dr["UserSurveyStatus"].ToString()));
                    obj.SurveyName = Dr["SurveyName"].ToString();
                    obj.Category = Dr["Category"].ToString();
                    obj.RewardPoints = Convert.ToInt32(Dr["RewardPoints"].ToString());
                    obj.TerminateRewards = Convert.ToInt32(Dr["TerminateRewards"].ToString());
                    obj.QuotaFullRewards = Convert.ToInt32(Dr["QuotaFullRewards"].ToString());
                    obj.CreatedOn = Convert.ToDateTime(Dr["CreatedOn"].ToString());
                    obj.CategoryId = Convert.ToInt32(Dr["Fk_CategoryId"].ToString());
                    obj.LeftDate = "";
                    obj.ImagePath = "https://api2.opinionest.com/Uplodaed_Documents/Survey.png";
                    List.Add(obj);
                }
            }
            //********************SurveyAPI1*****************************

            JSONResponseModel<string> responsave = _SurveyAPI1.SaveAPI1Surveys(Convert.ToInt64(UserId), "Opinionest2_0_Live", "https://message.opinionest.com/");
            if (responsave.Status == HttpStatusCode.OK)
            {
                SurveyAPI1List = _SurveyAPI1.GetAPISurveys(Convert.ToInt64(UserId));
                if (SurveyAPI1List != null)
                {
                    List.AddRange(SurveyAPI1List);
                }
            }
            //*********************************
            MyCollection.MySurveyDataListEntites = List;
            if (DT.Tables.Count > 0)
            {
                MyCollection.TotalRecords = Convert.ToInt32(DT.Tables[1].Rows[0]["RowCounts"].ToString());
                MyCollection.TotalRewardPoints = Convert.ToInt32(DT.Tables[2].Rows[0]["Total_poits"].ToString());
            }
            return MyCollection;
        }

        public UserSurveyStatus GetSurveyStatus(int Id)
        {
            switch (Id)
            {
                case 0:
                    return UserSurveyStatus.None;

                case 1:
                    return UserSurveyStatus.Release;

                case 2:
                    return UserSurveyStatus.InProgress;

                case 3:
                    return UserSurveyStatus.Completed;

                case 4:
                    return UserSurveyStatus.QuotaFull;

                case 5:
                    return UserSurveyStatus.Terminated;

                case 6:
                    return UserSurveyStatus.InCompleted;

                case 7:
                    return UserSurveyStatus.Credited;
                case 8:
                    return UserSurveyStatus.NoSurveyURL;
                case 9:
                    return UserSurveyStatus.SecurityRedirectUrl;
                case 10:
                    return UserSurveyStatus.DuplicateRedirectUrl;
                case 11:
                    return UserSurveyStatus.TentativeComplete;
                case 12:
                    return UserSurveyStatus.QualityRejected;
                default:
                    return UserSurveyStatus.None;
            }
        }

        public MySurveyColection BanneSurvey(string UserId)
        {
            DataTable DT = _Survey.BannerSurvey(UserId);
            MySurveyColection MyCollection = new MySurveyColection();
            List<MySurveyDataListEntites> List = new List<MySurveyDataListEntites>();
            foreach (DataRow Dr in DT.Rows)
            {
                MySurveyDataListEntites obj = new MySurveyDataListEntites();
                obj.SurveyId = Convert.ToInt32(Dr["SurveyId"].ToString());
                obj.SurveyNo = Dr["SurveyNo"].ToString();
                obj.Survey_Type = Convert.ToInt32(Dr["Survey_Type"].ToString()) == 1 ? SurveyType.Static : SurveyType.Dynamic;
                obj.SurveyStatus = Convert.ToInt32(Dr["SurveyStatus"].ToString());
                obj.FilterStatus = Convert.ToInt32(Dr["FilterStatus"].ToString());
                obj.URL = Dr["URL"].ToString();
                obj.UserSurveyStatus = GetSurveyStatus(Convert.ToInt32(Dr["UserSurveyStatus"].ToString()));
                obj.SurveyName = Dr["SurveyName"].ToString();
                obj.Category = Dr["Category"].ToString();
                obj.RewardPoints = Convert.ToInt32(Dr["RewardPoints"].ToString());
                obj.TerminateRewards = Convert.ToInt32(Dr["TerminateRewards"].ToString());
                obj.QuotaFullRewards = Convert.ToInt32(Dr["QuotaFullRewards"].ToString());
                obj.CreatedOn = Convert.ToDateTime(Dr["CreatedOn"].ToString());
                obj.ImagePath = Dr["Image"].ToString();
                obj.LeftDate = Dr["Leftdays"].ToString();
                List.Add(obj);
            }
            MyCollection.MySurveyDataListEntites = List;
            MyCollection.TotalRecords = 0;
            MyCollection.TotalRewardPoints = 0;
            return MyCollection;
        }
    }
}
