﻿using BusinessAccessLayer.InterfaceDeclaration;
using BusinessEntites;
using CommonMethod;
using CommonMethod.Defination;
using CommonMethod.Interface;
using DataBaseAccess.Defination;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;

namespace BusinessAccessLayer.ServiceDefination
{
    public class LoginServicesDefination : ILogin
    {
        SqlConnection Conn;
        IDataAccessLogin _DaLogin;
        IPartner _Partner;
        IDataAccessUser _DaUser;
        IDashboard dashboard;
        CommonMethod.Interface.ICommon _Comm;
        private string Key = "CNUnwR05TRtiO6kCDbTtvddLmBD6E9rR";
        public LoginServicesDefination(SqlConnection _Conn)
        {
            Conn = _Conn;
            _DaLogin = new ServiceDataAccessLogin(Conn);
            _Comm = new ServiceDefinationCommon(Conn);
            _Partner = new ParnterServicesDefination(Conn);
            _DaUser = new ServiceDataAccessUser(Conn);
            dashboard = new DashBoardServicesDefination(Conn);
        }
        public Tuple<Int64, int, int> UserRegistation(RegistationEntites _Reg, int MacValue,string SendGridKey)
        {
            int CheckUserExist = 0;
            int CheckMacAddressExist = 0;
            Int64 UserId = 0;
            UserEntites _obj = new UserEntites();
            //***************Get Parnter Id****************
            if(!string.IsNullOrEmpty(_Reg.PartnerParams) && _Reg.PartnerParams!="undefined")
            {
                _Reg.PartnerParams = _Reg.PartnerParams.Replace('~', '&');
                var UrlKeyValues = _Reg.PartnerParams.Trim()
               .Split('&')
               .Select(x => x.Split('='))
               .ToDictionary(x => x[0], x => x[1]);
                string PartnerRef = string.IsNullOrEmpty(UrlKeyValues.LastOrDefault(x => x.Key == "partner").Value) ? ""
                      : (UrlKeyValues.LastOrDefault(x => x.Key == "partner").Value).ToString();
                _Reg.PartnerId = _Partner.GetPartnerId(PartnerRef);
            }
            //********************#**********************
            Random generator = new Random();
            String RanderNo = generator.Next(0, 999999).ToString("D6");
            try
            {
                string FirstNAME = "", LastName = "";

                string[] Name = _Reg.FullName.Split(' ');
                if(Name.Length>1)
                {
                    int i = 0;
                    foreach (var v in Name)
                    {
                        if (i == 0)
                        {
                            FirstNAME = v;
                        }
                        else
                        {
                            LastName = LastName + " " + v;
                        }
                        i++;

                    }
                }
                else
                {
                    FirstNAME = Name[0];
                }
                _obj.Password = _Comm.EncryptID(_Reg.Password);
                _obj.MacAddress = _Reg.MacAddress;
                _obj.Gender = null;
                _obj.EmailAddress = _Reg.EmailAddress;
                _obj.MacValue = MacValue;
                _obj.FullName = _Reg.FullName;
                _obj.FirstName = FirstNAME;
                _obj.MacValue = 1;
                _obj.LastName = LastName;
                _obj.PolicyStatus = _Reg.PolicyStatus;
                _obj.MacAddress = _Reg.MacAddress;
                _obj.UserTypeId = (Int64)UserType.Public;
                _obj.Inviter_Id = 0;
                _obj.Partner_Id = _Reg.PartnerId;
                _obj.Partner_Params = _Reg.PartnerParams;
                _obj.IsSocial = false;
                _obj.ProfileImage = "";
                _obj.UserStateId = (Int64)UserState.NotVerified;
                _obj.IsVerify = false;
                _obj.IsActive = true;
                _obj.IsDelete = true;
                _obj.IsMandatory = false;
                _obj.OTP = RanderNo;
                _obj.CountryID = _Comm.GetCountryCode(_Reg.IPAddress);
                _obj.IPAddress = _Reg.IPAddress;
                DataSet Ds = _DaLogin.UserRegistation(_obj);
                if (Ds.Tables.Count > 0)
                {
                    CheckUserExist = Convert.ToInt32(Ds.Tables[0].Rows[0][0]);
                    CheckMacAddressExist = Convert.ToInt32(Ds.Tables[1].Rows[0][0]);
                    UserId = Convert.ToInt32(Ds.Tables[2].Rows[0][0]);
                    _obj.UserId = UserId;
                }
                if(CheckUserExist==0 && CheckMacAddressExist==0)
                {
                    EmailEntites _email = new EmailEntites();
                    _email.UserEmail = _Reg.EmailAddress;
                    _email.OTP = RanderNo;
                    _email.UserId = _obj.UserId;
                    _email.FirstName = _obj.FullName;
                    _email.subject = "We’re glad you said YES!";
                    _email.client = SendGridKey;
                    _email.Type = 1;
                    _email.CountryId = _obj.CountryID;
                    var Resutl = _Comm.SendEmail(_email);
                
                }
                

                return new Tuple<Int64, int, int>(UserId, CheckUserExist, CheckMacAddressExist);


            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public int UserEmailSubscrible(long UserId, bool EmailSubscrible, bool OfferEmailSubscription, bool? Cookies)
        {
            try
            {
                var I= _DaLogin.UserEmailSubscrible(UserId, EmailSubscrible,OfferEmailSubscription, Cookies);
                return Convert.ToInt32(I);
            }
            catch (Exception ex)
            {
                return -1;
            }

        }

        public EmailSubscriptionEntites GetUserEmailSubscrible(long UserId)
        {
            DataTable DT = _DaLogin.GetUserEmailSubscrible(UserId);
            EmailSubscriptionEntites obj = new EmailSubscriptionEntites();
            foreach (DataRow Dr in DT.Rows)
            {
                obj.EmailSubscrible = Convert.ToBoolean(Dr["EmailUnsubscrib"].ToString());
                obj.OfferEmailSubscription = Convert.ToBoolean(Dr["offerEmailUnsubscrib"].ToString());
                obj.Cookies = Convert.ToBoolean(Dr["Cookies"].ToString());
            }
            return obj;
        }

        public LoginEntites Login(UseLoginEntites Obj, out bool popup)
        {
            popup = false;
            LoginEntites _obj = new LoginEntites();
            try
            {
                Obj.Password = _Comm.EncryptID(Obj.Password);
                var pass = _Comm.DecryptID("57945FEB02B9FCB0DE10834DA556C54E");
               int Type = (int)UserType.Public;

                DataTable Dt = _DaLogin.Login(Obj, Type);
                if (Dt.Rows.Count > 0)
                {
                    _obj.UserId = Convert.ToInt64( Dt.Rows[0]["UserID"]);
                    _obj.EmailId = Dt.Rows[0]["EmailAddress"].ToString();
                    _obj.LastName = Dt.Rows[0]["LastName"]==null?"":Dt.Rows[0]["LastName"].ToString();
                    _obj.FirstName = Dt.Rows[0]["FirstName"]==null?"": Dt.Rows[0]["FirstName"].ToString();
                    //_obj.IsMandatory = Dt.Rows[0]["IsMandatory"]==null?false:Convert.ToBoolean(Dt.Rows[0]["IsMandatory"]);
                    //_obj.ProfilePic = Dt.Rows[0]["ProfileImage"]==null?"":Dt.Rows[0]["ProfileImage"].ToString();
                   var Data= dashboard.GetDashboard(_obj.UserId);
                    popup = Data.PopupStatus;
                }
                //JWT Token
                //Below for generate JWT Token
                var tokenHandler = new JwtSecurityTokenHandler();
                var key = Encoding.ASCII.GetBytes(Key);
                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(new Claim[]
                    {
                    new Claim(ClaimTypes.Name, _obj.UserId.ToString())
                    }),
                    Expires = DateTime.Now.AddDays(7),
                    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
                };
                var token = tokenHandler.CreateToken(tokenDescriptor);
                _obj.Token = tokenHandler.WriteToken(token);
                _obj.ExpiryDate = tokenDescriptor.Expires;

                
                return _obj;
            }
            catch(Exception Ex)
            {
                return _obj;
            }
        }

        public int DeleteAccount(long UserId)
        {
            try
            {
                return _DaLogin.DeleteUserAccount(UserId);
            }
            catch(Exception ex)
            {
                return 0;
            }
        }

        public int OTPVerifiy(int Otp, string EmailId, int Minutes, string SendGridKey)
        {
            try
            {
                var check = _DaLogin.CheckUser(EmailId);
                if(check>0)
                {
                    int i= _DaLogin.OTPVerifiy(Otp, EmailId, Minutes);
                    string name = _DaLogin.userName(EmailId);
                    if(i==1)
                    {
                        EmailEntites _email = new EmailEntites();
                        _email.UserEmail =EmailId;
                        _email.OTP = "";
                        _email.UserId = 0;
                        _email.FirstName = name;
                        _email.subject = "Welcome Opinionest!";
                        _email.client = SendGridKey;
                        _email.Type = 8;
                        _email.CountryId = 0;
                        _Comm.SendEmail(_email);
                    }
                    return i;
                }
                else
                {
                    return -2;
                }
                
            }
            catch(Exception Ex)
            {
                return 0;
            }
        }

        public int ReGenerateOtp(string EmailId, string SendGridKey, bool regenerate)
        {
            var check = _DaLogin.CheckUser(EmailId);
            int Data = 0;
            if (check > 0)
            {
                Random generator = new Random();
                String RanderNo = generator.Next(0, 999999).ToString("D6");
                 Data = _DaLogin.RegenerateOtp(RanderNo, EmailId);
                try
                {
                    EmailEntites _email = new EmailEntites();
                    _email.UserEmail = EmailId;
                    _email.OTP = RanderNo;
                    _email.subject = regenerate==true?"New OTP!": "Forgot password OTP!";
                    _email.client = SendGridKey;
                    _email.Type = regenerate == true ? 7:6;
                    var Resutl = _Comm.SendEmail(_email);
                }
                catch (Exception ex)
                {

                }
            }
            else
            {
                return -1;
            }

            return Data;
        }

        public int SendAppLine(string sendGridKey, string email)
        {
            try
            {
                Random generator = new Random();
                String RanderNo = generator.Next(0, 999999).ToString("D6");
                EmailEntites _email = new EmailEntites();
                _email.UserEmail = email;
                _email.OTP = "https://play.google.com/store/apps/details?id=com.opinionestmobile.opinionestmobile";
                _email.UserId = 0;
                _email.FirstName ="";
                _email.subject = "Opinionest AppLink";
                _email.client = sendGridKey;
                _email.Type = 4;
                var Resutl = _Comm.SendEmail(_email);
               // _DaLogin.RegenerateOtp(RanderNo, email);
                if (Resutl == true)
                    return 1;
            }
            catch(Exception ex)
            {

            }
            return 0;
        }

        public int ForgotPassword(string EmailId, string SendGridKey)
        {
            Random generator = new Random();
            String RanderNo = generator.Next(0, 999999).ToString("D6");
            int Data = _DaLogin.RegenerateOtp(RanderNo, EmailId);
            try
            {
                EmailEntites _email = new EmailEntites();
                _email.UserEmail = EmailId;
                _email.OTP = RanderNo;
                _email.subject = "Forgot Password OTP";
                _email.client = SendGridKey;
                _email.Type = 2;
                var Resutl = _Comm.SendEmail(_email);
            }
            catch (Exception ex)
            {

            }

            return Data;
        }

        public int ResetPassword(resetpassword obj)
        {
            try
            {
                obj.password= _Comm.EncryptID(obj.password);
                return _DaLogin.resetPassword(obj);
            }
            catch (Exception Ex)
            {
                return 0;
            }
        }

        public bool sendEmail(string emailId, string Subject, string message, string SendGridKey)
        {
            EmailEntites _email = new EmailEntites();
            _email.UserEmail = emailId;
            _email.OTP = message;
            _email.UserId = 0;
            _email.FirstName = "Piyush";
            _email.subject = Subject;
            _email.client = SendGridKey;
            _email.Type = 5;
            _email.CountryId = 2;
            _email.Query = Subject;
            var Resutl = _Comm.SendEmail(_email);
            return Resutl;
        }

        public int mobileOtp(string mobileNo,string Code)
        {
            var check = _DaLogin.CheckUserMobile(mobileNo);
            int Data = 0;
            if (check > 0)
            {
                Random generator = new Random();
                String RanderNo = generator.Next(0, 999999).ToString("D6");
                if(!String.IsNullOrEmpty(Code))
                {
                    Data = _DaLogin.mobileOpt(Code, mobileNo);
                    return Convert.ToInt32(Code);
                }
                else
                {
                    Data = _DaLogin.mobileOpt(RanderNo, mobileNo);
                    return Convert.ToInt32(RanderNo);
                }
                
                
                
            }
            else
            {
                return -1;
            }

           
        }

        public long LoginEmail(UseLoginEntites Obj)
        {
            return _DaLogin.LoginEmail(Obj);
        }

        public Tuple<long, int, int,int,string> UserMobileRegistation(RegistationEntites _Reg, int MacValue, string SendGridKey)
        {
               if(!String.IsNullOrEmpty(_Reg.referalCode))
            {
                var Referal = _DaUser.postSavereferalCodeMobile(_Reg.referalCode, _Reg.EmailAddress);
                if (Referal.Item1 == 1 && _Reg.referalCode != "")
                {
                    int CheckUserExist = 0;
                    int CheckMacAddressExist = 0;
                    Int64 UserId = 0;
                    UserEntites _obj = new UserEntites();
                    //***************Get Parnter Id****************
                    if (!string.IsNullOrEmpty(_Reg.PartnerParams) && _Reg.PartnerParams != "undefined")
                    {
                        _Reg.PartnerParams = _Reg.PartnerParams.Replace('~', '&');
                        var UrlKeyValues = _Reg.PartnerParams.Trim()
                       .Split('&')
                       .Select(x => x.Split('='))
                       .ToDictionary(x => x[0], x => x[1]);
                        string PartnerRef = string.IsNullOrEmpty(UrlKeyValues.LastOrDefault(x => x.Key == "partner").Value) ? ""
                              : (UrlKeyValues.LastOrDefault(x => x.Key == "partner").Value).ToString();
                        _Reg.PartnerId = _Partner.GetPartnerId(PartnerRef);
                    }
                    //********************#**********************
                    Random generator = new Random();
                    String RanderNo = generator.Next(0, 999999).ToString("D6");
                    try
                    {
                        string FirstNAME = "", LastName = "";

                        string[] Name = _Reg.FullName.Split(' ');
                        if (Name.Length > 1)
                        {
                            int i = 0;
                            foreach(var v in Name)
                            {
                                if(i==0)
                                {
                                    FirstNAME =v;
                                }
                                else
                                {
                                    LastName = LastName+" "+ v;
                                }
                                i++;
                                
                            }
                          
                        }
                        else
                        {
                            FirstNAME = Name[0];
                        }
                        _obj.Password = _Comm.EncryptID(_Reg.Password);
                        _obj.MacAddress = _Reg.MacAddress;
                        _obj.Gender = null;
                        _obj.EmailAddress = _Reg.EmailAddress;
                        _obj.MacValue = MacValue;
                        _obj.FullName = _Reg.FullName;
                        _obj.FirstName = FirstNAME;
                        _obj.MacValue = 1;
                        _obj.LastName = LastName;
                        _obj.PolicyStatus = _Reg.PolicyStatus;
                        _obj.MacAddress = _Reg.MacAddress;
                        _obj.UserTypeId = (Int64)UserType.Public;
                        _obj.Inviter_Id = 0;
                        _obj.Partner_Id = _Reg.PartnerId;
                        _obj.Partner_Params = _Reg.PartnerParams;
                        _obj.IsSocial = false;
                        _obj.ProfileImage = "";
                        _obj.UserStateId = (Int64)UserState.NotVerified;
                        _obj.IsVerify = false;
                        _obj.IsActive = true;
                        _obj.IsDelete = true;
                        _obj.IsMandatory = false;
                        _obj.OTP = RanderNo;
                        _obj.CountryID = _Comm.GetCountryCode(_Reg.IPAddress);
                        _obj.IPAddress = _Reg.IPAddress;
                        _obj.PhoneNumber = _Reg.mobileNo;
                        _obj.Inviter_Id = Referal.Item2;
                        DataSet Ds = _DaLogin.UserRegistationMobile(_obj);
                        if (Ds.Tables.Count > 0)
                        {
                            CheckUserExist = Convert.ToInt32(Ds.Tables[0].Rows[0][0]);
                            CheckMacAddressExist = Convert.ToInt32(Ds.Tables[1].Rows[0][0]);
                            UserId = Convert.ToInt32(Ds.Tables[2].Rows[0][0]);
                            _obj.UserId = UserId;
                        }
                        if (CheckUserExist == 0 && CheckMacAddressExist == 0)
                        {
                            EmailEntites _email = new EmailEntites();
                            _email.UserEmail = _Reg.EmailAddress;
                            _email.OTP = _obj.OTP;
                            _email.UserId = _obj.UserId;
                            _email.FirstName = _obj.FullName;
                            _email.subject = "We’re glad you said YES!";
                            _email.client = SendGridKey;
                            _email.Type = 1;
                            _email.CountryId = _obj.CountryID;
                            var Resutl = _Comm.SendEmail(_email);
                            //_Reg.mobileNo = _Reg.CountrCode + _Reg.mobileNo;
                            mobileOtp(_Reg.mobileNo, _obj.OTP);
                            _DaUser.postSavereferalCodeMobilePoints(_Reg.referalCode, _Reg.EmailAddress);
                        }


                        return new Tuple<Int64, int, int, int,string>(UserId, CheckUserExist, CheckMacAddressExist, Referal.Item1, RanderNo);


                    }
                    catch (Exception ex)
                    {
                        return null;
                    }
                }
                else
                {
                    return new Tuple<long, int, int, int,string>(0, 0, 0, Referal.Item1,"");
                }
            }
            else
            {
               
                    int CheckUserExist = 0;
                    int CheckMacAddressExist = 0;
                    Int64 UserId = 0;
                    UserEntites _obj = new UserEntites();
                    //***************Get Parnter Id****************
                    if (!string.IsNullOrEmpty(_Reg.PartnerParams) && _Reg.PartnerParams != "undefined")
                    {
                        _Reg.PartnerParams = _Reg.PartnerParams.Replace('~', '&');
                        var UrlKeyValues = _Reg.PartnerParams.Trim()
                       .Split('&')
                       .Select(x => x.Split('='))
                       .ToDictionary(x => x[0], x => x[1]);
                        string PartnerRef = string.IsNullOrEmpty(UrlKeyValues.LastOrDefault(x => x.Key == "partner").Value) ? ""
                              : (UrlKeyValues.LastOrDefault(x => x.Key == "partner").Value).ToString();
                        _Reg.PartnerId = _Partner.GetPartnerId(PartnerRef);
                    }
                    //********************#**********************
                    Random generator = new Random();
                    String RanderNo = generator.Next(0, 999999).ToString("D6");
                    try
                    {
                        string FirstNAME = "", LastName = "";

                        string[] Name = _Reg.FullName.Split(' ');
                        if (Name.Length > 1)
                        {
                        int i = 0;
                        foreach (var v in Name)
                        {
                            if (i == 0)
                            {
                                FirstNAME = v;
                            }
                            else
                            {
                                LastName = LastName + " " + v;
                            }
                            i++;

                        }

                    }
                    else
                        {
                            FirstNAME = Name[0];
                        }
                        _obj.Password = _Comm.EncryptID(_Reg.Password);
                        _obj.MacAddress = _Reg.MacAddress;
                        _obj.Gender = null;
                        _obj.EmailAddress = _Reg.EmailAddress;
                        _obj.MacValue = MacValue;
                        _obj.FullName = _Reg.FullName;
                        _obj.FirstName = FirstNAME;
                        _obj.MacValue = 1;
                        _obj.LastName = LastName;
                        _obj.PolicyStatus = _Reg.PolicyStatus;
                        _obj.MacAddress = _Reg.MacAddress;
                        _obj.UserTypeId = (Int64)UserType.Public;
                        _obj.Inviter_Id = 0;
                        _obj.Partner_Id = _Reg.PartnerId;
                        _obj.Partner_Params = _Reg.PartnerParams;
                        _obj.IsSocial = false;
                        _obj.ProfileImage = "";
                        _obj.UserStateId = (Int64)UserState.NotVerified;
                        _obj.IsVerify = false;
                        _obj.IsActive = true;
                        _obj.IsDelete = true;
                        _obj.IsMandatory = false;
                        _obj.OTP = RanderNo;
                        _obj.CountryID = _Comm.GetCountryCode(_Reg.IPAddress);
                        _obj.IPAddress = _Reg.IPAddress;
                        _obj.PhoneNumber = _Reg.mobileNo;
                       // _obj.Inviter_Id = Referal.Item2;
                        DataSet Ds = _DaLogin.UserRegistationMobile(_obj);
                        if (Ds.Tables.Count > 0)
                        {
                            CheckUserExist = Convert.ToInt32(Ds.Tables[0].Rows[0][0]);
                            CheckMacAddressExist = Convert.ToInt32(Ds.Tables[1].Rows[0][0]);
                            UserId = Convert.ToInt32(Ds.Tables[2].Rows[0][0]);
                            _obj.UserId = UserId;
                        }
                        if (CheckUserExist == 0 && CheckMacAddressExist == 0)
                        {
                            EmailEntites _email = new EmailEntites();
                            _email.UserEmail = _Reg.EmailAddress;
                            _email.OTP = RanderNo;
                            _email.UserId = _obj.UserId;
                            _email.FirstName = _obj.FullName;
                            _email.subject = "We’re glad you said YES!";
                            _email.client = SendGridKey;
                            _email.Type = 1;
                            _email.CountryId = _obj.CountryID;
                            var Resutl = _Comm.SendEmail(_email);
                            //_Reg.mobileNo = _Reg.CountrCode + _Reg.mobileNo;
                            mobileOtp(_Reg.mobileNo, RanderNo);
                        }


                        return new Tuple<Int64, int, int, int,string>(UserId, CheckUserExist, CheckMacAddressExist,0, RanderNo);


                    }
                    catch (Exception ex)
                    {
                        return null;
                    }
                
            }
           
        }

        public CountryFlag countryInfo(string ip)
        {
            return  _Comm.countryInfo(ip);
        }
    }
}
