﻿using BusinessAccessLayer.InterfaceDeclaration;
using BusinessEntites;
using DataBaseAccess.Defination;
using DataBaseAccess.Interface;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace BusinessAccessLayer.ServiceDefination
{
   public class CommonServicesDefination:ICommon
    {
        SqlConnection Conn;
        IDataAccessCommon _dbCommon;
        public CommonServicesDefination(SqlConnection _Conn)
        {
            Conn = _Conn;
            _dbCommon = new ServicesDataAccessCommon(Conn);
        }

        public List<CountryEntites> CountryList()
        {
            List<CountryEntites> _list = new List<CountryEntites>();
            try
            {
                DataTable Dt = _dbCommon.CountryData();
                if(Dt.Rows.Count>0)
                {
                    foreach(DataRow dr in Dt.Rows)
                    {
                        _list.Add(new CountryEntites
                        {
                            CountryCode = dr["SortName"].ToString(),
                            CountryName = dr["CountryName"].ToString(),
                            CountryId = Convert.ToInt64(dr["CountryID"].ToString()),

                        }) ;
                    }

                }
            }
            catch(Exception ex)
            {

            }

            return _list;
        }

        public bool MobileNumberUnique(string mobileNo)
        {
            return _dbCommon.mobileNoValidate(mobileNo);
        }

        public List<StateEntites> StateList(long CountryId)
        {
            List<StateEntites> _list = new List<StateEntites>();
            try
            {
                DataTable Dt = _dbCommon.StateData(CountryId);
                if (Dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in Dt.Rows)
                    {
                        _list.Add(new StateEntites
                        {
                            StateId = Convert.ToInt64(dr["StateId"].ToString()),
                            StateName = dr["StateName"].ToString(),
                            CountyId = Convert.ToInt64(dr["CountryId"].ToString()),

                        });
                    }

                }
            }
            catch (Exception ex)
            {

            }

            return _list;
        }

        public bool ValidUser(long userId)
        {
            return _dbCommon.ValidUser(userId);
        }
    }
}
