﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using BusinessAccessLayer.InterfaceDeclaration;
using CommonMethod;
using DataBaseAccess.Defination;
using DataBaseAccess.Interface;

namespace BusinessAccessLayer.ServiceDefination
{
    public class ExtraActivityServicesDefination : IExtraActivity
    {
        SqlConnection Conn;
        IDataAccessExtraActivity _dbExtraActivity;
        public ExtraActivityServicesDefination(SqlConnection _Conn)
        {
            Conn = _Conn;
            _dbExtraActivity = new ServiceDataAccessExtraActivity(Conn);
        }
        public bool MailOpen(string value)
        {
            bool status = false;
            string[] Queryvalue = (value.DecryptID()).Split('-');
            int length_queryvalue = Queryvalue == null ? 0 : Queryvalue.Length;
            if (length_queryvalue > 1)
            {
                MailType mailType = Queryvalue[0].ToString().ToLower() == MailType.CintSurvey.ToString().ToLower()
                    ? MailType.CintSurvey : MailType.SiteSurvey;
                if (mailType == MailType.CintSurvey)
                {
                    string FileTime = Queryvalue[1].ToString();
                    DateTime Dt = DateTime.FromFileTime(Convert.ToInt64(FileTime));
                    string Date = Dt.ToString("yyyy-MM-dd");
                    status = _dbExtraActivity.MailOpenCint(Date);
                }
                else
                {
                    long Tforid = Convert.ToInt64(Queryvalue[1]);//here TforId refer to the usersurveyidreference
                    status = _dbExtraActivity.MailOpenSite(Tforid);
                }

            }

            return status;

        }

        public bool SaveIpAddress(long userid,string ipaddress)
        {
            return _dbExtraActivity.SaveIpAddress(userid, ipaddress);
        }


    }
}
