﻿using BusinessAccessLayer.GeneralServices;
using BusinessAccessLayer.InterfaceDeclaration;
using BusinessEntites;
using CommonMethod.Defination;
using CommonMethod.Interface;
using DataBaseAccess.Defination;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace BusinessAccessLayer.ServiceDefination
{
    public class UserServicesDefination: Iuser
    {
        SqlConnection Conn;
        IDataAccessUser _DaUser;
        CommonMethod.Interface.ICommon _Comm;
        public UserServicesDefination(SqlConnection _Conn)
        {
            Conn = _Conn;
            _DaUser = new ServiceDataAccessUser(Conn);
            _Comm = new ServiceDefinationCommon(Conn);
        }
        public int editprofile(EditUserEntites _obj)
        {
            int i = _DaUser.UserEdit(_obj);
            return i;
        }

        public EditUserEntites GetUserProfile(long UserId)
        {
            try
            {
                return _DaUser.GetUserProfile(UserId);
            }
            catch(Exception ex)
            {
                return null;
            }
        }
        public IList<UserNotificationEntity> GetUserNotification(long UserId)
        {
            try
            {
                return _DaUser.GetUserNotification(UserId);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public Tuple<int, string,string, DailyFeedDetails> GetUserNotificationCount(long UserId)
        {
            DailyFeedDetails _noti = new DailyFeedDetails();
            try
            {
                return _DaUser.GetUserNotificationCount(UserId);
            }
            catch (Exception ex)
            {
                return new Tuple<int, string,string, DailyFeedDetails>(0,"","", _noti);
            }
        }

        public UserProfilePercentage GetUserProfilePercentage(long UserID)
        {
            try
            {
                return _DaUser.GetUserProfilePercentage(UserID);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public UserReferalEntites UserReferal(long UserId)
        {
            try
            {
                return _DaUser.GetReferal(UserId);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public long ConsentPrivacy(ConsentPrivacyEntity Obj)
        {
            try
            {
                return _DaUser.ConsentPrivacy(Obj);
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        /// <summary>
        /// Read Notification
        /// </summary>
        /// <param name="_obj"></param>
        /// <returns></returns>
        public int ReadNotification(long userId, int notificationId)
        {
            int i = _DaUser.ReadNotification(userId, notificationId);
            return i;
        }

        public string GetGUID(long UserId)
        {
            try
            {
                return _DaUser.GetGUID(UserId);
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        public int postSaveNotificationDevice(SaveNotification obj, long userId)
        {
            try
            {
                return _DaUser.postSaveNotification(obj, userId);
            }
            catch(Exception ex)
            {

            }
            return 0;
        }

        public int postSaveReferalCode(SaveReferalCode obj, long userId)
        {
            try
            {
                return _DaUser.postSavereferalCode(obj, userId);
            }
            catch (Exception ex)
            {

            }
            return 0;
        }

        public UserEntites GetUserDetailsByEmailAddress(string EmailAddress)
        {
            UserEntites userEntites = new UserEntites();
            try
            {
                userEntites= _DaUser.GetUserDetailsByEmailAddress(EmailAddress);
            }
            catch(Exception ex)
            {

            }
            return userEntites;
        }

        public List<notificationCategoryEntites> notificationCategory()
        {
            List<notificationCategoryEntites> notificationCategoryEntites = new List<notificationCategoryEntites>();
            DataTable dt = _DaUser.getNotificationCategroy();
            notificationCategoryEntites = ConvertDatatableToList.Convert<notificationCategoryEntites>(dt);
            return notificationCategoryEntites;
        }

        public List<SpinQuestion> getSpinQuestion()
        {
            List<SpinQuestion> spinQuestion = new List<SpinQuestion>();
            DataTable dt = _DaUser.getSpinQuestion();
            spinQuestion = ConvertDatatableToList.Convert<SpinQuestion>(dt);
            return spinQuestion;
        }

        public long saveSnipQuestion(SpinQuestionSave entity)
        {
            try
            {
                return _DaUser.SaveSpinQuestion(entity);
            }
            catch (Exception ex)
            {

            }
            return 0;
        }

        public long notificationConfiguration(NotificationConfiguration notificationConfiguration)
        {
            try
            {
                return _DaUser.notificationConfiguration(notificationConfiguration);
            }
            catch (Exception ex)
            {

            }
            return 0;
        }

        public SpinRandomQuestion getSpinRandomQuestion()
        {
            SpinRandomQuestion spinRandomQuestion = new SpinRandomQuestion();
            DataTable dt = _DaUser.getRandomSpinQuestion();
            var data = ConvertDatatableToList.Convert<SpinQuestion>(dt);
            spinRandomQuestion.Question = data[0].Question;
            spinRandomQuestion.id = data[0].Id;

            DataTable dtans = _DaUser.getRandomSpinQuestionAns(spinRandomQuestion.id);
            var ans = ConvertDatatableToList.Convert<SpinRandomQueAns>(dtans);
            spinRandomQuestion.answers = ans;
            return spinRandomQuestion;
        }

        public long spinQuestionRandomSave(SpinRandomQueAnsSave spinRandomQueAnsSave)
        {
            return _DaUser.spinQuestionRandomSave(spinRandomQueAnsSave);
        }

        public int notificationRemove(int NotificationId)
        {
            return _DaUser.notificationRemove(NotificationId);
        }

        public int notificationStar(int NotificationId)
        {
            return _DaUser.notificationStar(NotificationId);
        }

        public int notificationReport(NotificationReport notificationReport)
        {
            return _DaUser.notificationReport(notificationReport);
        }
        public List<PaymentCookiesaEntity> TangoProcess()
        {
            List<PaymentCookiesaEntity> PaymentCookiesaEntity = new List<PaymentCookiesaEntity>();
            DataTable dt = _DaUser.TangoProcess();
            PaymentCookiesaEntity = ConvertDatatableToList.Convert<PaymentCookiesaEntity>(dt);
            return PaymentCookiesaEntity;
        }

        public int paymentGetwayLogs(PaymentGatewaysLogs paymentGatewaysLogs)
        {
            return _DaUser.PaymentGatewaysLogs(paymentGatewaysLogs);
        }

        public int paymentGetwayErrorLogs(PaymentGatewaysErrorLogs paymentGatewaysLogs)
        {
            return _DaUser.PaymentGatewaysErrorLogs(paymentGatewaysLogs);
        }

    }
}
