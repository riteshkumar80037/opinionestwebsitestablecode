﻿using BusinessAccessLayer.InterfaceDeclaration;
using BusinessEntites;
using CommonMethod;
using CommonMethod.Defination;
using CommonMethod.Interface;
using DataBaseAccess.Defination;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace BusinessAccessLayer.ServiceDefination
{
    public class PasswordServicesDefination : Ipassword
    {
        SqlConnection Conn;
        IDataAccessLogin _DaLogin;
        CommonMethod.Interface.ICommon _Comm;
        public PasswordServicesDefination(SqlConnection _Conn)
        {
            Conn = _Conn;
            _DaLogin = new ServiceDataAccessLogin(Conn);
            _Comm = new ServiceDefinationCommon(Conn);
        }
        public UserEntites Recover(string Email)
        {
            UserEntites _obj = new UserEntites();
            var where = " where UserMaster.EmailAddress='" + Email.Trim() + "' and UserMaster.IsActive=1 and UserMaster.IsDelete=1 and UserMaster.IsVerify=1 and UserMaster.UserTypeId=" + Convert.ToInt64(UserType.Public);
            DataSet Ds = _DaLogin.UserPassword(Email,where);
            if (Ds.Tables.Count > 0)
            {
                _obj.UserId = Convert.ToInt32(Ds.Tables[0].Rows[0]["UserID"].ToString());
                _obj.EmailAddress = Ds.Tables[0].Rows[0]["EmailAddress"].ToString();
                _obj.FullName = Ds.Tables[0].Rows[0]["FullName"].ToString();
                _obj.UserName = Ds.Tables[0].Rows[0]["UserName"].ToString();
                _obj.Password = Ds.Tables[0].Rows[0]["Password"].ToString();
            }
            return _obj;
        }
    }
}
