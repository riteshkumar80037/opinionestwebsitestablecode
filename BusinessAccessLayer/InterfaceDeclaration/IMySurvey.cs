﻿using BusinessEntites;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessAccessLayer.InterfaceDeclaration
{
    public interface IMySurvey
    {
        MySurveyColection MySurveylist(string UserId, int pageindex, int pageSize);
        MySurveyColection BanneSurvey(string UserId);
    }
}
