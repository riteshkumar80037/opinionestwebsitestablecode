﻿using BusinessEntites;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessAccessLayer.InterfaceDeclaration
{
    public interface ISurveyAPI1
    {
        JSONResponseModel<string> SaveAPI1Surveys(long UserId, string Dbname, string Siteurl);
        List<MySurveyDataListEntites> GetAPISurveys(long UserId);
    }
}
