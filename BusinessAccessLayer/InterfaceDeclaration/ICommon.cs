﻿using BusinessEntites;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessAccessLayer.InterfaceDeclaration
{
   public interface ICommon
    {
        List<CountryEntites> CountryList();
        List<StateEntites> StateList(long CountryId);
        bool ValidUser(long userId);
        bool MobileNumberUnique(string mobileNo);

    }
}
