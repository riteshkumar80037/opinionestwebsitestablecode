﻿using BusinessEntites;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessAccessLayer.InterfaceDeclaration
{
   public interface Iuser
    {
        int editprofile(EditUserEntites _obj);
        EditUserEntites GetUserProfile(long UserId);
        Tuple<int,string,string, DailyFeedDetails> GetUserNotificationCount(long UserId);
        IList<UserNotificationEntity> GetUserNotification(long UserId);
        UserProfilePercentage GetUserProfilePercentage(long UserID);
        UserReferalEntites UserReferal(long UserId);
        
        /// <summary>
        /// Read Notification
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="notificationId"></param>
        /// <returns></returns>
        int ReadNotification(long userId, int notificationId);
        long ConsentPrivacy(ConsentPrivacyEntity Obj);
        string GetGUID(long UserId);
        int postSaveNotificationDevice(SaveNotification obj, long userId);
        int postSaveReferalCode(SaveReferalCode obj, long userId);
        UserEntites GetUserDetailsByEmailAddress(string EmailAddress);
        List<notificationCategoryEntites> notificationCategory();
        int notificationRemove(int NotificationId);
        int notificationStar(int NotificationId);
        List<SpinQuestion> getSpinQuestion();
        long saveSnipQuestion(SpinQuestionSave entity);
        long notificationConfiguration(NotificationConfiguration notificationConfiguration);
        SpinRandomQuestion getSpinRandomQuestion();
        long spinQuestionRandomSave(SpinRandomQueAnsSave spinRandomQueAnsSave);
        int notificationReport(NotificationReport notificationReport);
        List<PaymentCookiesaEntity> TangoProcess();
        int paymentGetwayLogs(PaymentGatewaysLogs paymentGatewaysLogs);
        int paymentGetwayErrorLogs(PaymentGatewaysErrorLogs paymentGatewaysLogs);



    }
}
