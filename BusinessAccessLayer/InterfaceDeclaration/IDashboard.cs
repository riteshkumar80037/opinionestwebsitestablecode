﻿using BusinessEntites;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessAccessLayer.InterfaceDeclaration
{
   public interface IDashboard
    {
        DashboardEntites GetDashboard(long UserId);
    }
}
