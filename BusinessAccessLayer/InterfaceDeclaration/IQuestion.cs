﻿using BusinessEntites;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessAccessLayer.InterfaceDeclaration
{
   public interface IQuestion
    {
        QuestionResponse OptionalQuestionGet(OptionalRequestEntites obj, long UserId,bool MandatoryType);
        long SaveAnswer(OptionalRequestEntites obj, long UserId, bool MandatoryType);
        List<ProfileOptionalQuestionResponse> ProfileOptionalQuestionGet(long UserId);
        Profile_Question_Count ProfileQuestionCount(long UserID);
        List<ProfileQuestionCategory> ProfileQuestionCategory(long UserID);
        QuestionTotal OptionalTotal(long userid);
        QuestionTotal MandatoryTotal(long userid);
    }
}
