﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessAccessLayer.InterfaceDeclaration
{
 public   interface IExtraActivity
    {
        bool MailOpen(string Value);

        bool SaveIpAddress(long userid, string ipaddress);
    }
}
