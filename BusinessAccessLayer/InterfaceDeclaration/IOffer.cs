﻿using BusinessEntites;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessAccessLayer.InterfaceDeclaration
{
    public interface IOffer
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="UserId"></param>
        /// <param name="pageindex"></param>
        /// <param name="pagesize"></param>
        /// <returns></returns>
        List<Offer> OfferList(long UserId, string offerType, int? pageindex, int pagesize);


        /// <summary>
        /// Update User Points
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        string UpdatePoints(PointsUpdateInputModel model);
        string tangoLogs(tangoLogs model);
        void DailyFeedSave(DailyFeedEntites dailyFeedEntites);

        /// <summary>
        /// Mark Offer Favourite
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        long MarkOfferFavourite(MarkOfferFavouriteInputModel model);

        /// <summary>
        /// Offer History Details
        /// </summary>
        /// <param name="UserId"></param>
        /// <param name="offerType"></param>
        /// <param name="pageindex"></param>
        /// <param name="pagesize"></param>
        /// <returns></returns>
        List<OfferHistoryDetails> OfferHistoryDetails(long UserId, string offerType, int? pageindex, int pagesize);

        /// <summary>
        /// CashVoucherDetails
        /// </summary>
        /// <param name="CountryId"></param>
        /// <returns></returns>
        List<UserRedeemRequestModel> CashVoucherDetails(long CountryId);

        /// <summary>
        /// CheckUserAccountLinked
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="paymentGatewayId"></param>
        /// <returns></returns>
        List<UserAccountLinkedStatus> CheckUserAccountLinked(long userId, long paymentGatewayId);

        /// <summary>
        /// Link User Wallet
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        long LinkUserWallet(UserLinkWalletInputModel model);

        /// <summary>
        /// Link User Wallet
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Tuple<string ,decimal > ConvertPointToCurrency(ConvertPointToCurrency model, long UserId);

        /// <summary>
        /// get paytm unique code
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        string GetUniqueNoForPaymentGateway(long PaymentGatewayId);

        /// <summary>
        /// Get Paytm User Status
        /// </summary>
        /// <param name="PhoneNo"></param>
        /// <param name="paymentgatewayId"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        PaytmApiModel GetPaytmUserStatus(string PhoneNo, long paymentgatewayId, long UserId);
        bool CheckExistVefication(long UserId,string phone);

        /// <summary>
        /// Send User Point Redeem Request
        /// </summary>
        /// <param name="PointsRedeem"></param>
        /// <param name="RequestedAmount"></param>
        /// <param name="PGId"></param>
        /// <param name="mobileNumber"></param>
        /// <param name="UserId"></param>
        /// <param name="Points"></param>
        /// <returns></returns>
        bool SendUserPointRedeemRequest(long PointsRedeem, string RequestedAmount, string PGId, string mobileNumber, long UserId, long Points);
        List<PaytmResponseMail> SendUserPointRedeemRequest(PaymentCookiesPending paymentCookiesPending, string email, string key);
        List<PaytmResponseMail> SendUserPointRedeemRequest();
        List<PaytmResponseMail> SendUserPointRedeemRequestPick();
        long PaymentCookiesSave(PaymentCookies paymentCookies);
        /// <summary>
        /// GetPointsValue
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        decimal GetPointsValue(long userId);

        /// <summary>
        /// TotalCashback
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        long TotalCashback(long userId);
        Tuple<string, string,string,List<OfferFavirout>> offerUserDetails(long useriId);
        int DeletePointTransaction(long TransId);
    }
}
