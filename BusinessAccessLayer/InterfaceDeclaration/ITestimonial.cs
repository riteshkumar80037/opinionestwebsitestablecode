﻿using BusinessEntites;
using System;
using System.Collections.Generic;
using System.Text;
namespace BusinessAccessLayer.InterfaceDeclaration
{
    public interface ITestimonial
    {
        List<TestimonialEntites> GetTestimonialList();
        int AddTestimonial(GetTestimonialEntites getTestimonial);
    }
}
