﻿using BusinessEntites;
using System;
using System.Collections.Generic;
using System.Text;
namespace BusinessAccessLayer.InterfaceDeclaration
{
   public interface ISurvey
    {
        List<MySurveyEntites> MySurveylist(long UserId);
        List<MySurveyEntites> OtherSurveylist(long UserId, int CountryId);
        FormsCount FromsCount(long UserId);
        SurveyResult ViewSurveyResult(long UserId, long Surveyid);
        int addSurvey(ForumCreate forumCreate);
        int addForumQuestion(QuestionEntites questionEntites);
    }
}
