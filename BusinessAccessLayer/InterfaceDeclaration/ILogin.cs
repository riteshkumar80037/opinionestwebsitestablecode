﻿using BusinessEntites;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessAccessLayer.InterfaceDeclaration
{
    public interface ILogin
    {
        Tuple<Int64, int, int> UserRegistation(RegistationEntites _obj,int MacValue,string SendGridKey);
        Tuple<Int64, int, int,int,string> UserMobileRegistation(RegistationEntites _obj, int MacValue, string SendGridKey);
        int UserEmailSubscrible(long UserId, bool EmailSubscrible, bool OfferEmailSubscription, bool? Cookies);
        EmailSubscriptionEntites GetUserEmailSubscrible(long UserId);
        LoginEntites Login(UseLoginEntites Obj, out bool popup);
        long LoginEmail(UseLoginEntites Obj);
        int DeleteAccount(long UserId);
        int OTPVerifiy(int Otp, string EmailId, int Minutes,string SendGridKey);
        int ResetPassword(resetpassword obj);
        int ReGenerateOtp(string EmailId,string SendGridKey,bool regenerate);

        int ForgotPassword(string EmailId, string SendGridKey);
        int SendAppLine(string sendGridKey, string email);
        bool sendEmail(string emailId, string Subject, string message, string SendGridKey);
        int mobileOtp(string mobileNo,string countryCode);
        CountryFlag countryInfo(string ip);
    }
}
