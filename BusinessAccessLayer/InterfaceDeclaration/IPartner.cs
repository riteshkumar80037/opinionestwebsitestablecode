﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessAccessLayer.InterfaceDeclaration
{
    public interface IPartner
    {
        long GetPartnerId(string PartnerRef);

        void PartnerVerification(long PartnerId, string PartnerParam);
    }
}
