﻿using BusinessEntites;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessAccessLayer.InterfaceDeclaration
{
    public interface IHome
    {
        List<BlogEntites> Bloglist(int pageindex, int pagesize);
        List<BlogEntites> BlogDetails(int blogId);
        BlogDetailsEntites BlogDetails(long Id);
        BlogDetailsEntites BlogDetails(string header);
        Tuple<List<DailyFeedModelList>,int,int> DailyFeedlist(long UserId, int pageNo,int pageSize,int? categoryId, bool? IsStart , string text);
        
        /// <summary>
        /// to get slider list on the basis of slider type
        /// </summary>
        /// <param name="_sliderType"></param>
        /// <returns></returns>
        List<Slider> SliderList(int _sliderType);

        /// <summary>
        /// To get daily feed details
        /// </summary>
        /// <param name="feedId"></param>
        /// <returns></returns>
        DailyFeedDetails DailyFeedDetails(int feedId);

        /// <summary>
        /// To get Referral details
        /// </summary>
        /// <param name="inviterId"></param>
        /// <returns></returns>
        List<ReferralDetailList> ReferralList(long UserId, int pageNo, int pageSize);

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        long HomePageTotalPoints();

        /// <summary>
        /// Contact Form Submission
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        string ContactFormSubmission(ContactUsEntities model,string clientkey);

        /// <summary>
        /// Review List
        /// </summary>
        /// <param name="pageindex"></param>
        /// <param name="pagesize"></param>
        /// <returns></returns>
        List<CustomerReview> ReviewList(int pageindex, int pagesize);
         mobileFeature mobilefeatures(string code);
    }
}
